
                              _____ _____            _____  ______  _____ 
                             / ____|  __ \     /\   |  __ \|  ____|/ ____|
                            | |  __| |__) |   /  \  | |__) | |__  | (___  
                            | | |_ |  _  /   / /\ \ |  ___/|  __|  \___ \ 
                            | |__| | | \ \  / ____ \| |    | |____ ____) |
                             \_____|_|  \_\/_/    \_\_|    |______|_____/ 
                                                           

## Description
This code allows the calculation of potential energy surfaces in arbitrary dimensions the main aim\
of the code is to allow electronic structure data to be manipulated using Gaussian processes. 
## How to compile GRAPES

* **QChem** has to compiled (hint for hylas: use **/scratch/hb407/qchem_public/svn_local/bin/qchem** as QChem path)
* **QCMagic** (**GRAPES** branch) has to be compiled locally
* If the **/scratch/** folder does not exist (for example on cerebro) the scratch variable at the top of **core.runners.main_interface.py** has to be changed 
* The code has to be compiled by running **setup.bash** located in the main directory
* A list of all the used versions for the anaconda environement can be found in the **meta** directory (your conda environment should be updated from there)
* Optionally the **.vimrc** can be copied to the home folder to have **GRAPES** specific highlighting

## How to run GRAPES

* An input file has to be written in the **InputFiles** directory. This contains all the parameters which can be parsed
* An example of H2 can be run with **tests.main_test.bash** (this should take about 1 min on 6 processors)
* In general one runs the code by calling(an example can be seen in **tests.main_test.bash**):
```
        bin/runmain.py - i input_file_name --{ExtraArguments}
        bin/runmethod_main.py -i input_file_name --{ExtraArguments}
```
* Extra arguments are added in the standard python call (as sys.argv). A complete list of available arguments can be found **core.options.option_interface.py**
* Data produced is stored in a subdirectory (intiated by **core.runners.main_interface.py** -> default: **SavedData/Label** where Label is a parsed argument) it is recommended that each system has its own directory
* Quantum chemistry programs input/output are saved in the work directory (initiated by **core.runners.main_interface.py** -> default: **/scratch/$USER/Label** where Label is a parsed argument)

## Credits
Owner: **Fabio E. A. Albertani**\
Contribuiting Authors:
 - **Kripa Panchagnula** -> `core.spectra_calculation`
 - **George Trenins** -> `core.molecular_dynamics`
