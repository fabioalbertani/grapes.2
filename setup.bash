#!/bin/bash

exelist=$(find core -executable -name "*.py*")
SAVEIFS=$IFS
IFS=$'\n'
exelist=($exelist)
IFS=$SAVEIFS

if [ -d "./bin" ]; then
    while true; do
        read -p "$(pwd)/bin/ already exists. Would you like to remove this directory before proceeding? [Y/n] - " yn
        case $yn in
            [Yy]* ) 
                echo "Removing old $(pwd)/bin/...";
                rm ./bin -r; break;;
            [Nn]* ) 
                echo "grapes cannot be set up. Please remove $(pwd)/bin/ first. Exiting...";
                exit;;
            * ) 
                echo "Please answer Y or n.";;
        esac
    done
fi

if [ ! -d "./bin" ]; then
    echo "Creating $(pwd)/bin/...";
    mkdir ./bin
fi

cd ./bin

for d in "${exelist[@]}"
do
    package=$(echo $d | grep -Po "(?<=core/)\w+(?=/)")
    module=$(echo $d | grep -Po "(?<=/)\w+(?=\.py)")
    echo "Creating executable for $package.$module..."
    if [ -z "$package" ]; then
        script=$"\
#!/usr/bin/env python2\
\n\n\
'''\n\
@module run$module\n\
'''\n\
\n\
if __name__ == '__main__':\n\
\tif __package__ is None:\n\
\t\tfrom os import sys, path\n\
\t\tsys.path.append(path.dirname(path.dirname(path.abspath(__file__))))\n\
\t\tfrom core import $module\n\
\t\t$module.main()\n\
\telse:\n\
\t\tfrom ..core import $module\n\
\t\t$module.main()"
        echo -e $script > run${module}.py
        chmod +x run${module}.py
    else
        script=$"\
#!/usr/bin/env python2\
\n\n\
'''\n\
@module run$module\n\
'''\n\
\n\
if __name__ == '__main__':\n\
\tif __package__ is None:\n\
\t\tfrom os import sys, path\n\
\t\tsys.path.append(path.dirname(path.dirname(path.abspath(__file__))))\n\
\t\tfrom core.$package import $module\n\
\t\t$module.main()\n\
\telse:\n\
\t\tfrom ..core.$package import $module\n\
\t\t$module.main()"
        echo -e $script > run${module}.py
        chmod +x run${module}.py
    fi
done

while true; do
    read -p "Would you like to set up PATH and PYTHONPATH for core? [Y/n] - " yn
    case $yn in
       [Yy]* )
            break;;
       [Nn]* )
            exit;;
       * )
            echo "Please answer Y or n.";;
    esac
done

echo "Removing any paths containing /grapes/ in PATH and PYTHONPATH..."
export PATH=$(echo ${PATH} | awk -v RS=: -v ORS=: '/core/ {next} {print}' | sed 's/:*$//')
export PYTHONPATH=$(echo ${PYTHONPATH} | awk -v RS=: -v ORS=: '/core/ {next} {print}' | sed 's/:*$//')
echo
echo "Exporting $(pwd) to PATH and and $(dirname $(pwd)) to PYTHONPATH..."
if ! grep -Fq 'export PYTHONPATH=$PYTHONPATH:'$(dirname $(pwd)) ~/.bashrc
then
    bashrccodes=$"\
if [ -d $(pwd) ]; then\n\
\texport PATH=\$PATH:$(pwd)\n\
\texport PYTHONPATH=\$PYTHONPATH:$(dirname $(pwd))\n\
\texport VMDSIMPLEGRAPHICS=1\n\
fi"
    echo "No PATH and PYTHONPATH exports for $(pwd) and $(dirname $(pwd)) found in ~/.bashrc. Writing these to ~/.bashrc..."
    echo -e $bashrccodes >> ~/.bashrc
fi
. ~/.bashrc



