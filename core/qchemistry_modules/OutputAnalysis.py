#!/bin/python
#File: OutputAnalysis.py


#Would make sense to find distance compared to ControlSurface

def FindStatesDistances(Alpha, Beta):
    from numpy import zeros

    distancesAlpha = zeros((len(Alpha), len(Alpha[0][0,:]))) 

    for point in xrange(len(Alpha)): #find distance in terms of orbitals coefficients 

            distancesAlpha[point, :] = StateDistance(Alpha[point], Alpha)
            

    if not Beta==[]: #RHF, ROHF ...
        distancesBeta = zeros((len(Beta), len(Beta[0][0,:])))

        for point in xrange(len(Beta)): 

                distancesBeta[point, :] = StateDistance(Beta[point], Beta)

        return {'Alpha': distancesAlpha, 'Beta': distancesBeta}
    else:
        return {'Alpha': distancesAlpha}




#idea: if sum (orbcoef1 - orbcoef2)^2 ~ 0 then same state

def StateDistance(Coef1, Coefficients):
    from numpy import sqrt, power, zeros

    mean_deviation = zeros(len(Coef1[0,:]))
    for compairing in xrange(len(Coefficients)):
        for MO in xrange(len(Coef1[0,:])):

            mean_deviation[MO] += sum(sqrt(power(Coef1[MO,:]-Coefficients[compairing][MO,:],2))/len(Coefficients))

    return mean_deviation





def StateSimilarity(Coeff1, Coeff2):

    if len(Coeff1)!=len(Coeff2):
        print 'Coefficients supplied are not in the same basis or are corrupted'
        return None

    else:
        from numpy import dot
        return dot(Coeff1, Coeff2)
