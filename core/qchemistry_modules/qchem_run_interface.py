#!/bin/python
#File: core.qchemistry_modules.qchem_run_interface.py

import sys, os, time
from datetime import datetime

from numpy import asarray, zeros, full, nan
import copy

from qcmagic.Utils import QCConstants
from qcmagic.QCInterface import QCInput
from qcmagic.QCInterface.QCOutput import OutputFile, QCError
from qcmagic.QCInterface.newJob import Job
from qcmagic.QCInterface.QCQueue import QCQueue

from core.grapes_utils.main_utils import HiddenPrints
from core.qchemistry_modules.run_interface import RunInterface
import core.qchemistry_modules.qchem_utils as qchem_utils




class QCRunInterface(RunInterface):

    @staticmethod
    def RegisterOptions(args):
        m = args.RegisterModule('run')

        args.Register(m, '--remote', action='store_true', default=False, 
                      help='Opposite of local calculations')
        args.Register(m, '-p', '--Processors', type=int, default=1)

        args.Register(m, '--InheritStates', action='store_false', default=True)
        args.Register(m, '--save', action='store_true', default=False)
        args.Register(m, '--scratchfiles', type=str, default=None, 
                      help='Which files are copied from previous calculations and reused')

        args.Register(m, '--mom', action='store_true', default=False, 
                      help='uses $rem mom_start 1 for provided states calculations')
        args.Register(m, '--scfError', type=float, default=8, help='DIIS Error to be coverged to')
        args.Register(m, '--scfRestart', type=float, default=7, 
                      help='Allows to rerun calculations which are close to convergence')

        args.Register(m, '--memtotal', type=int, default=3000, 
                      help='Allocated memory for QChem runs')

    @staticmethod
    def ParseOptions(args):
        pass


    def __init__(self, args, CalcDetails=None, Molecule=None, J=None):

        if args.run.scratchfiles==None:  
            if args.main.noci:
                args.queue.scratchfiles = ['53.0', '759.0', '760.0', '761.0']
            else:
                args.queue.scratchfiles = ['53.0', '751.0', '752.0', '753.0', '754.0']


        args.run.checkpoint = False

        if CalcDetails is None:
            raise RuntimeError('\tNo information on calculation provided.'\
                               'Run cannot be initiated\n')

        self.TemplateBuilder = qchem_utils.TemplateBuilder(args, CalcDetails)

        with HiddenPrints('QCMagic.log'):
            print('Files to copy: ', args.run.scratchfiles)

            if J==None:
                self.LastJ = None
                self.J = Job()
            else:
                self.LastJ = J
                self.J = J #allows to keep previous calculations

            #args.RegisterModules(QCQueue)
            self.Q = QCQueue(args.queue) 
            print('\n'+50*'*' + 'New Main class instance called:\n\n')

        super(QCRunInterface, self).__init__(args, CalcDetails, Molecule)




    def RunGeometry(self, XYZ, sample=None, restart=False, AttemptRectify=False):
        '''
        Main interface with QCMagic (specifically QCOutput, QCQueue and QCInput) to run QChem

        Args:
            XYZ:            [array] points to be run in xyz format
            scratch:        [str] scratch to be used for the run
            restart:        [bool] allows to differentiate between call from BASH and self calls
            optional_rem:   [str] rem parameters to be added to the QChem input file
            AttemptRectify: [bool] allows QCQueue to attempt to modify the rem to converge
                                   the QChem calculation
        '''

        with HiddenPrints('QCMagic.log'):

            scratch = self.TemplateBuilder.GetOptionalrem(self.args, self.LastJ, sample)
            self.TemplateBuilder.BuildTemplate(XYZ)
            I = QCInput.Input()
            I.ParseInput(self.TemplateBuilder.Input.split('\n'))
            self.J.AddInput(I)
            running=True

            if not scratch is None:
                self.args.set('scratch', scratch)
                self.J.RunNext(self.Q, InheritStates=self.Moduleargs.inheritStates, 
                                     options=self.args)
                while running:
                    print self.J.Pending, self.J.Running
                    print self.J.RunNext(self.Q, options=self.args)
                    try:
                        print(self.J[-1]['Output']+' exists')
                        running = False
                    except KeyError:
                        time.sleep(2)
                        print('checking on calc')
                        self.J.RunNext(self.Q, options=self.args)

            else:
                self.args.set('scratch', None)
                while self.J.RunNext(self.Q, options=self.args):
                    time.sleep(2)
                self.J.RunNext(self.Q, options=self.args)
#                while running:
#                    print self.J.Pending, self.J.Running
#                    print self.J.Runs
#                    print self.J[-1]
#                    print self.J.RunNext(self.Q, options=self.args)
#                    try:
#                        print(self.J[-1]['Output'])
#                        print('output exists ... starting to parse')
#                        running = False
#                    except KeyError:
#                        time.sleep(2)
#                        print('checking on calc')
#                        self.J.RunNext(self.Q, options=self.args)


            if self.J[-1]['Output'].Outputs[0].QCErr:
                self.SPError(str(datetime.now())+'\t\tQChem Calculation fail:\n')
                try:
                    O = OutputFile(self.J[-1]['Output'].Filename)
                    O.Parse()
                    self.SPError('\tError message is: '+self.J[-1]['Output'].Outputs[0].QCErr.Error+'\n\tOutput:\n')
                    self.SPError(O['Outputs'][0].__dict__)
                except:
                    pass

                if AttemptRectify:
                    import copy

                    old_scratch = self.J[-1]['Output'].Outputs[0].ScratchDirectory[:-1]
                    O = OutputFile(self.J[-1]['Output'].Filename)
                    O.Parse()
                    newI = copy.deepcopy(O.Input)
                    self.J[-1]['Output'].Outputs[0].QCErr.Rectify(newI)                
                       
                    newJ = copy.deepcopy(self.J) 
                    newJ.AddInput(newI) #Try to run rectified input Job

                    if False:
                        while newJ.RunNext(self.Q, options=self.args):
                            time.sleep(2)
                    else:
                        self.args.set('scratch', old_scratch)
                        while newJ.RunNext(self.Q, InheritStates=self.Moduleargs.inheritStates, options=self.args):
                            time.sleep(2)

                    while newJ.RunNext(self.Q, options=self.args):
                        time.sleep(2)

                    self.J[-1]['Output'] = newJ[-1]['Output']
                    return self.Get_Dictionary()

                else:
                    try:
                        return self.Get_Dictionary()
                    except:
                        return self.VoidReturn()
            else:
                return self.Get_Dictionary()





#--------------------------------------------------------------------- Format of Returned Dictionary

    def Get_Dictionary(self):
        O = OutputFile(self.J[-1]['Output'].Filename)
        O.Parse()
        print('\nQCInterface: Output:\n') 
        for k, v in O['Outputs'][0].__dict__.items():
            print('\n'+k+'\n')
            print(v)

        if int(os.getenv('verbose')):
            print('\nO instance####\n', O)
            print('\n\nJ instance####\n', self.J[-1]['Output'])

        ReturnDict = {}
        if self.details['Type']=='Energy':
            ReturnDict['Energy'], ReturnDict['Distances'] = self.ParseSCFEnergy(O)
            ReturnDict['AlphaCoef'], ReturnDict['BetaCoef'] = self.ParseOrbitalsCoefficients(O)
            if self.Globalargs.dipole:
                ReturnDict['Dipole'] = self.ParseDipole(O)

            if not self.details['correlation'] is None:
                ReturnDict['Energy'] = self.ParseCORREnergy(O)
                ReturnDict['AlphaCoef'], ReturnDict['BetaCoef'] = self.ParseOrbitalsCoefficients(O)

        elif self.details['Type']=='Hessian':
            print 'passed modes:\n', self.ParseNormalModes(O)
            ReturnDict['NormalModes'] = self.ParseNormalModes(O)
            ReturnDict['Hessian'], ReturnDict['Frequencies'] = self.ParseHessian(O)


        return ReturnDict


    def VoidReturn(self):
        ''' If nothing can be returned from self.Run an empty structure is given'''
        print('\tReturning empty dict**')

        print 'passed modes:\n', full((self.Globalargs.Dxyz,self.Globalargs.Dxyz-6), nan)
        return {'Energy': full(self.Globalargs.NStates, nan),
                'Distances': full(self.Globalargs.NStates, nan),
                'AlphaCoef': None,
                'BetaCoef': None,
                'Dipole': full((4,self.Globalargs.NStates), nan),
                'Hessian': full((self.Globalargs.Dxyz,self.Globalargs.Dxyz), nan),
                'Frequencies': full(self.Globalargs.Dxyz, nan),
                'NormalModes': full((self.Globalargs.Dxyz,self.Globalargs.Dxyz-6), nan)
               }




#---------------------------------------------------------------------------------- Compact Analysis

    def ParseSCFEnergy(self, O, distances=True, eun='har'):
        if self.Globalargs.NStates==1:
            if not O['Outputs'][0].LastInternalEnergy is None:
                E = [O['Outputs'][0].LastInternalEnergy]
            else:
                E = [float('Nan')]
            d0n = [0]
        else:
            E = []
            d0n = []
            for state in range(self.Globalargs.NStates):
                try:
                    E.append(O['Outputs'][0].CurStep.Minima[state].InternalEnergy)
                    d0n.append(O['Outputs'][0].CurStep.Minima[state].Distances[0])
                except IndexError:
                    E.append(O['Outputs'][0].InternalEnergy)
                    d0n.append(0.0)

        E = [v if (not v is None) else float('nan') for v in E]
        if distances:
            return asarray([QCConstants.ConvertE(v, eun) for v in E]), asarray(d0n)
        else:
            return asarray([QCConstants.ConvertE(v, eun) for v in E])



    def ParseCORREnergy(self, O, eun='har'):
        if self.details['correlation']=='fci':
            return self.ParseSCFEnergy(O, distances=False, eun=eun)

        else:
            if self.Globalargs.NStates==1:
                if not O['Outputs'][0].CorrelationEnergy is None:
                    E = [O['Outputs'][0].CorrelationEnergy]
                else:
                    E = [float('Nan')]
            else:
                E = []
                for state in range(self.Globalargs.NStates):
                    E.append(O['Outputs'][0].CurStep.Minima[state].CorrelationEnergy)

            return asarray([QCConstants.ConvertE(v, eun) for v in E])



    def ParseOrbitalsCoefficients(self, O):
        try:
            AlphaCoef = self.J[-1]['Output'].CurStep.FinalMinimum.Matrices['Alpha Coeff Matrix']
            if self.Globalargs.unrestricted:
                BetaCoef = self.J[-1]['Output'].CurStep.FinalMinimum.Matrices['Beta Coeff Matrix']
            else:
                BetaCoef = None
        except:
            AlphaCoef = None
            BetaCoef = None

        return AlphaCoef, BetaCoef




    def ParseDipole(self, O):
        Dipole = zeros((self.Globalargs.NStates,4))

        for state in range(self.Globalargs.NStates):
            try:
                Dipole[state,0] = float(O.CurStep.Moments['Dipole']['X'])
                Dipole[state,1] = float(O.CurStep.Moments['Dipole']['Y'])
                Dipole[state,2] = float(O.CurStep.Moments['Dipole']['Z'])
                Dipole[state,3] = float(O.CurStep.Moments['Dipole']['Total']) 

            except:
                Dipole[state,:] = [float('Nan'), float('Nan'), float('Nan'), float('Nan')]

       
        return Dipole



    def ParseHessian(self, O):
        #This is all wrong TODO TODO TODO
        Hessian = zeros((self.Globalargs.Dxyz,self.Globalargs.Dxyz))
        Freq = zeros(self.Globalargs.Dxyz) 

        try:
            for mod in range(1,self.Globalargs.Dxyz-5):
                print('\nFreq: ', O['Outputs'][0].VibAnals.__dict__[mod]['Frequency'])
                Freq[mod] = float(self.J[-1]['Output']['Outputs'][0].\
                            VibAnals.__dict__[mod]['Frequency'])
                for atoms in range(len(self.Globalargs.AtomsList)):
                    Hessian[3*atoms:3*atoms+3,mod] = [float(x) for x in self.J[-1]['Output']\
                    ['Outputs'][0].VibAnals.__dict__[mod]['Displacement'].__dict__['Atoms']\
                    [atoms].__dict__['Position'].flatten()]

            return Hessian, Freq

        except:
            Hessian[:,:] = nan
            Freq [:] = nan
            return Hessian, Freq


    def ParseNormalModes(self, O):
        Modes = zeros((self.Globalargs.Dxyz,self.Globalargs.Dxyz-6))

        try:
            for mod in range(1,self.Globalargs.Dxyz-5):
                for atoms in range(len(self.Globalargs.AtomsList)):
                    Modes[3*atoms:3*atoms+3,mod-1] = [float(x) for x in self.J[-1]['Output']\
                    ['Outputs'][0].VibAnals.__dict__[mod]['Displacement'].__dict__['Atoms']\
                    [atoms].__dict__['Position'].flatten()]

            return Modes

        except:
            Modes[:,:] = nan
            return Modes



#---------------------------------------------------------------------------------- Other Functions

    def SingleEnergy(self, XYZ, optional_rem=None):

        with HiddenPrints('QCMagic.log'):

            Template = self.TemplateM
            for i in range(len(XYZ[:,0])):
                Template += "%s\t%f\t%f\t%f\n" %(self.Globalargs.AtomsList[i],\
                                                 XYZ[i,0], XYZ[i,1], XYZ[i,2])
            Template += '$end\n' + self.TemplateP
            if optional_rem!=None:
                Template += optional_rem + '\n$end\n'
            else:
                Template += '\n$end\n'

            I = QCInput.Input()
            I.ParseInput(Template.split('\n'))
            self.J.AddInput(I)
            self.J.RunNext(self.Q, options=self.args)
            self.J.RunNext(self.Q, options=self.args)

            O = OutputFile(self.J[-1]['Output'].Filename)
            O.Parse()
            print('\nQCInterface Single Energy (Output):\n', O['Outputs'][0].__dict__)

        return O['Outputs'][0].LastInternalEnergy


