#!/usr/python
#File: core.qchemistry_modules.run_interface.py

import abc

import typing

import core.grapes_utils.main_utils as UTILS
from core.grapes_utils.progress_bar import DisplayBar
from core.data_management.data_utils import Process



class RunInterface:

    __metaclass__ = abc.ABCMeta

    def __init__(self, args, CalcDetails, Molecule, **kwargs):

        self.args = args
        self.Globalargs = args.main
        self.Moduleargs = args.run 

        self._details = CalcDetails

        self.Molecule = Molecule 

        self.SP = UTILS.SuperPrint('RunInterface.log', stdout=False)
        self.SPError = UTILS.SuperPrint('ErrorMessages.log', stdout=False)

 
    @property
    def details(self):
        return self._details

 
    @abc.abstractmethod 
    def RunGeometry(self, XYZ, **kwargs):
        return dict



    @classmethod
    def VoidReturn(self):
        '''returns an empty result dictionary when the calculation 
           failed'''
         
        return GetResultDictionary(self.Globalargs, None)



    def RunSamples(self, Q, skip=None):
        '''Takes in the samples to be run'''

        PB = DisplayBar(len(Q[:,0]))
        Data = Process(self.Globalargs)
        XYZ = self.Molecule.q2cart(Q)

        self.SP('\n\tRunning '+str(Q.shape[0])+' points\n')

        for sample in range(XYZ.shape[1]):

            if skip is None or (isinstance(skip, list) and sample not in skip):
                self.SP('\n\tRunning sample '+str(sample)+'...\n')

                D = self.RunGeometry(XYZ[:,sample,:], sample=sample)

                Data.FeedData(Q[sample,:], D)
                PB.Show(sample)

        self.SP('\nFinal Data for '+str(len(Data.DataDict['Points'][:,0]))+'points\n')
        for k, v in Data.__dict__.items():
            self.SP(str(k)+' >>> '+str(v)+'\n')

        return Data.Return()


    def PrintFinal(self, Data):
        #Probably will be a super functin with extra steps  
        import pickle

        with open(self.Globalargs.CDPath+'/SavedData/'+self.Globalargs.Label+\
                  '/Run_'+UTILS.GetDataLabel(self.details)+'.pickled', 'wb') as f:

            try:
                pickle.dump({'args': self.args, 'Job':self.J, 'CalcDetails': self.details,\
                             'Data': Data}, f, pickle.HIGHEST_PROTOCOL)

            except AttributeError:
                '''Some run interfaces do not have self.J equivalent (not interesting)'''
                pass
            
            except pickle.PicklingError:
                print('\tNot possible to pickle the run ... skipping this step\n')



    def ReadPickledRun(self):
        '''
        Import a pickled file written by self.PrintFinal
        '''
        import pickle

        args, J, CalcDetails, Data = pickle.load(self.Globalargs.CDPath+'/SavedData/'+\
                                     self.Globalargs.Label+'/Run_'+UTILS.GetDataLabel(self.details)\
                                     +'.pickled')

        return args, J, CalcDetails, Data






def GetResultDictionary(args, keys):
    '''Builds a dictionary of nan to be fed back to the 
       run interfaces'''

    Available = {'Energy': full(args.NStates, nan),
                 'Distances': full(args.NStates, nan),
                 'AlphaCoef': None,
                 'BetaCoef': None,
                 'Dipole': full((4,args.NStates), nan),
                 'Hessian': full((args.Dxyz,args.Dxyz), nan),
                 'Frequencies': full(args.Dxyz, nan),
                 'NormalModes': full((args.Dxyz,args.Dxyz-6), nan)
                }


    if keys is None:
        return Available

    for key in Available.keys():
        if key in keys:
            del Available[key]

    return Available



