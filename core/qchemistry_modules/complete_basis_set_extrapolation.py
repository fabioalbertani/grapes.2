#!/bin/python


from scipy.optimize import curve_fit

from numpy import power, sqrt, exp, diag
from numpy import float64
from numpy import arange, zeros, isnan, nan, sort



class Main:

    @classmethod
    def RegisterOptions(cls, args):
        m = args.RegisterModule('CBS')
        
        args.Register(m, '--CBSc', default=-3.0)
        args.Register(m, '--CBSMethod', default='dogbox')


    def __init__(self, args, Type, CBSErrorFillValue=1.0):
        '''
        Initiates the proper form of the CBS limit
        '''

        if args['CBS'].CBSMethod=='dogbox':
            self.CBSSolve = self.CBSSolve_curve_fit

        elif args['CBS'].CBSMethod=='Gaussian':
            self.CBSSolve = self.CBSSolve_gaussian

        self.args = args

        if Type=='cc':
            self.CBSfunction = lambda x, a, b: a - b*power(x, self.args['CBS'].CBSc)

        elif Type=='hf':
            self.CBSfunction = lambda x, a, b: a - b*(x-1)*exp(self.args['CBS'].CBSc*sqrt(x))
            pass
        
        else:
            raise Exception('CBS is not defined for this exchange/correlation method')

        self.CBSErrorFillValue = CBSErrorFillValue


        


    def GetCBS(self, Q, Energy, Type):
        '''
        Constructs and returns two dictionaries of processed data containing the CBS energy
        as well as the extrapolation error
        '''
        from ..MainUtils.ProgressBar import DisplayBar
        from ..DataManagement.Utils import Process

        NSamp = len(getattr(Energy, Type+self.args['Main'].basis_sets[0])[:,0])

        x = arange(2, len(self.args['Main'].basis_sets)+1) #Cardinal values of basis sets !!assuming starting from VDZ
        y = zeros(x.shape)
        E = zeros ((NSamp,self.args['Main'].NStates))
        dE = zeros(E.shape)

        PB = DisplayBar(NSamp*self.args['Main'].NStates)

        for state in range(self.args['Main'].NStates):
            for i in range(NSamp):
                for j in range(len(x)):
                    y[j] = getattr(Energy, Type + self.args['Main'].basis_sets[j])[i,state]

                E[i,state], dE[i,state] = self.CBSSolve(x, y)
                PB.Show(state*NSamp + i)


        for state in range(self.args['Main'].NStates):
            for i in range(len(dE[:,0])):
                if not isinstance(dE[i,state], float64): 
                    dE[i,state]=self.CBSErrorFillValue
                if dE[i,state]==float('inf'): 
                    dE[i,state]=self.CBSErrorFillValue
        
        EDict = Process(self.args['Main'])
        EDict.FeedFinalData(Q, {'Energy': E})

        dEDict = Process(self.args['Main'])
        dEDict.FeedFinalData(Q, {'Energy': dE})

        return {'CBS': EDict.Return(), 'CBSError': dEDict.Return()}

        


    def CBSSolve_curve_fit(self, x, y):
        '''
        Solves the function extrapolation for a single set of values
        '''


        if sum(isnan(y)): # or not all(y==sort(y)[::-1]):
            '''
            checks that the data is consistent
            '''
            return nan, nan

        if True: #try:
            popt, pcov = curve_fit(self.CBSfunction, x, y, p0=(y[-1], 1.0), method='dogbox')
            return popt[0], sqrt(diag(pcov))[0]

        else: #except:
            return nan, nan


    def CBSSolve_gaussian(self, x, y):
        #TODO Write this function to solve the CBS limit
        pass

