#!/bin/python
#File: AnalRunInterface.py


import sys, os
from datetime import datetime

from numpy import asarray


ErrFile = open('ErrorMessages.log', 'a')


class Harmonic:
    def __init__(self, args):
        from numpy import ones
        self.w2 = ones(args.D)

    def get(self, Q):
        return sum(0.5*self.w2*Q*Q) 




class Anharmonic:
    def __init__(self, args):
        from numpy import ones
        self.w2 = ones(args.D)
        self.wan2 = 0.2*ones(args.D)

    def get(self, Q):
        return sum(0.5*self.w2*Q*Q + self.wan2*Q*Q*Q)



class LennardJones:
    ''' parameters for O2 '''
    def __init__(self, args, coupling=False):
        from numpy import ones
        self.w2 = 2.85*ones(args.D)
        self.wan2 = 2.85*2*ones(args.D)

    def get(self, Q):
        return 0.2*sum(self.w2/(Q**12.0) - self.wan2/(Q**6.0))





class Main:

    def __init__(self, args):

        self.args = args
        self.Globalargs = args['Main']
    
        if self.Globalargs.PotentialType=='Harmonic':
            self.Potential = Harmonic(self.Globalargs)

        elif self.Globalargs.PotentialType=='Anharmonic':
            self.Potential = Anharmonic(self.Globalargs)

        elif self.Globalargs.PotentialType=='LennardJones':
            self.Potential = LennardJones(self.Globalargs)

        else:
            raise Exception('No modules available for selected potential function')


    def Run(self, Q):
        '''
        Simply return a dictonary with some values
        '''
        from numpy import full, nan

        return {
                'Energy': self.Potential.get(Q),
                'Distances': full(self.Globalargs.NStates, nan),
               }



    def VoidReturn(self):
        from numpy import full, nan

        return {'Energy': full(self.Globalargs.NStates, nan),
                'Distances': full(self.Globalargs.NStates, nan),
                'AlphaCoef': None, 
                'BetaCoef': None,
                'Dipole': full(4, nan)
               }



    def PrintFinal(self, Type, basis_set, Data):
        '''
        Placeholder (since no calculations were run no information, 
        on the run itself, is worth storing)
        '''
        pass





 


