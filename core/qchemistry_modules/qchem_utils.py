#!/bin/python
#File: QchemTemplates.py

'''
Roughly order by level of theory
'''

#----------------------------------------------------------------- Main $rem templates

templates = {
    'Hessian': '''
$rem
    jobtype freq
    vibman_print 6
    exchange {exchange}
    basis {basis_set}

    scf_convergence {scfError}
               ''',
    'Energy': '''
$rem

    jobtype sp
    exchange {exchange}
    correlation {correlation}
    unrestricted {unrestricted}

    basis {basis_set}

    sym_ignore true
    scf_convergence {scfError}
    max_scf_cycles 2000
    print_orbitals true

    mem_total {memtotal}
              ''',
    'NOCI': '''
$rem

    jobtype sp
    exchange {exchange}
    correlation {correlation}
    unrestricted {unrestricted}

    basis {basis_set}

    sym_ignore true
    scf_convergence {scfError}
    max_scf_cycles 2000
    print_orbitals true

    use_libnoci true

    mem_total {memtotal}
            ''',
#-------------------------------------------------------------------------------------------- Specific rem blocks
    'BasisRead': '''
    basis2 {basis_set_2}
    basisprojtype ovprojection
                 ''',
    'MultipleStates': '''
    scf_saveminima              {NStates}
    gen_scfman                  false
    scf_guess                   gwh
    scf_minfind_mixmethod       1
    scf_minfind_randommixing    -15708
                      ''',
    'noci_StateRead': '''
    noci_refgen {refgen}
    noci_detgen {detgen}
                      ''',
    'StateRead': '''
    scf_saveminima  {NStates}
    gen_scfman      false
    scf_readminima  {NStates}
                 ''',
    'mom': '''
    mom_start 100
           '''
            }

#TODO Remove ?
templates['noci_MultipleStates'] = templates['NOCI'] +\
        '''
    SCF_MINFIND_RANDOMMIXING    30000
    SCF_MINFIND_MIXMETHOD       1
    scf_saveminima              {NStates}
    gen_scfman                  false
        '''




class TemplateBuilder:
    '''
    Templates class for QChem input files
    '''

    def __init__(self, args, CalcDetails):
        '''
        Produce templates for QChem input files

        Args:
            args:           [Options] Options containing global arguments and module arguments
            BasisIteration: [int] specify which basis set to use
        '''

        context = {
            'Charge': args.main.Charge,
            'Spin': args.main.Spin
                 }

        template = '''
$molecule
    {Charge} {Spin}
                   '''

        self.molecule = template.format(**context)
        self.AtomsList = args.main.AtomsList

        context = {
            'exchange': CalcDetails['exchange'],
            'correlation': str(CalcDetails['correlation']),
            'unrestricted': args.main.unrestricted,
            'basis_set': CalcDetails['basis_set'],
            'scfError': args.run.scfError,
            'memtotal': args.run.memtotal,
            'NStates': args.main.NStates
                   }


        if CalcDetails['Type']=='Energy':
            if args.main.noci:
                template = templates['NOCI']

            else:
                template = templates['Energy']

            if CalcDetails['Difference'] and CalcDetails['basis_set']!=CalcDetails['Difference']['basis_set']:
                template += templates['BasisRead']
                context['basis_set_2'] = CalcDetails['Difference']['basis_set']


        elif CalcDetails['Type']=='Hessian':
            template = templates['Hessian']


        self.rem = template.format(**context)
        self.Input = ''




    def GetOptionalrem(self, args, J, sample):

        if not J is None and not args.main.scratchfeed=='none':
            if args.main.noci:
                opts = {'refgen': 1, 'detgen': 0}
                optional_rem = templates['noci_StateRead'].format(**opts)

            else:
                opts = {'NStates': args.main.NStates}
                optional_rem = templates['StateRead'].format(**opts)

            try:
                scratch = J[sample]['Output'].ScratchDirectory
            except IndexError:
                scratch = None

        elif args.main.scratchfeed=='def':

            if J is None:
                if args.main.noci:
                    opts = {'refgen': 0, 'detgen': 3, 'NStates': args.main.NStates}
                    optional_rem = templates['noci_MultipleStates'].format(**opts)

                else:
                    opts = {'NStates': args.main.NStates}
                    optional_rem = templates['MultipleStates'].format(**opts)

                scratch=None

            else:
                if args.main.noci:
                    opts = {'refgen': 1, 'detgen': 0}
                    optional_rem = templates['noci_StateRead'].format(**opts)

                else:
                    opts = {'NStates': args.main.NStates}
                    optional_rem = templates['StateRead'].format(**opts)

                try:
                    scratch = J[sample]['Output'].ScratchDirectory
                except IndexError:
                    scratch = None

        elif args.main.scratchfeed=='ordered':

            if not sample and args.main.ProvidedStates is None:
                if args.main.noci:
                    opts = {'refgen': 0, 'detgen': 3, 'minima': args.main.NStates}
                    optional_rem = templates['noci_multiple_state_search'].format(**opts)

                else:
                    opts = {'minima': args.main.NStates}
                    optional_rem = templates['multiple_state_search'].format(**opts)

                scratch=None

            else:
                if args.main.noci:
                    opts = {'refgen': 1, 'detgen': 0}
                    optional_rem = templates['noci'].format(**opts)

                else:
                    opts = {'minima': args.main.NStates}
                    optional_rem = templates['scf_read'].format(**opts)

                if not sample and not args.main.ProvidedStates is None:
                    scratch = args.main.ProvidedStates+'/'

                else:
                    scratch = J[-1]['Output'].ScratchDirectory

        else:
            if args.main.noci:
                opts = {'refgen': 0, 'detgen': 3, 'minima': args.main.NStates}
                optional_rem = templates['noci_multiple_state_search'].format(**opts)

            else:
                opts = {'minima': args.main.NStates}
                optional_rem = templates['multiple_state_search'].format(**opts)

            scratch=None

        if not hasattr(self, 'optional_rem'):
            self.optional_rem = optional_rem
        return scratch




    def BuildTemplate(self, XYZ):
        '''The optional rem fed here could be given right before the run, for
           example to rerun a given sample and is different from the one
           given by the self.GetOptionalrem function'''

        self.Input = self.molecule
        print XYZ, XYZ.shape

        for atom in range(XYZ.shape[0]):
            self.Input += self.AtomsList[atom]+'\t'+\
                          '\t'.join([str(x) for x in XYZ[atom,:]])+'\n'

        self.Input += '$end\n' + self.rem + self.optional_rem + '\n$end'


