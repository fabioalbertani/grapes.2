#!/usr/bin/python
#File: core.__init__.py

'''
@package core
Package to manipulate data and interpolate potential energy surfaces, normal modes
and other quantites using machine learning methods

author: Fabio E. A. Albertani


Structure:

runners/                -- Contains the specific ways in which a run is performed
options/                -- Routines to read options in
main_utils/             -- Printing utils

interpolation/          -- Routines to interpolate data 
data_management/        -- data storage and manipulation
plotting/               -- Routines to plot data and interpolated data
geometry/               -- Routines to manipulate geometries
qchemistry_modules/     -- Routines to interface with quantum chemistry packages 
                           such as QChem, MRCC ...

spectra_calculation/    -- Spectra calculation from interpolation modules

'''

__all__ = ['data_management', 'geometry', 'grapes_utils', 'interpolation', 'options', 
           'plotting', 'qchemistry_modules', 'spectra_calculation', 'testing']
