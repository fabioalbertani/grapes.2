# File: interp.py
"""
Created on Thu May 31 15:30:23 2018

@author: gt317

Grid forces approximated by cubic splines.

"""

from __future__ import division, print_function
from .base import PES
from .champ import ChampagnePES
from scipy import interpolate
import numpy as np

__all__ = []


class OneDimForce(PES):

    def __init__(self, knots, values, ndim=1, mode='cl'):
        """
        Initialise the force using the grid data

        Input:
            knots (1d array) ............... coordinates at which the force
                                             has been calculated in a
                                             uniformly increasing order
            values (1d array) .............. force values calculated
                                             at the knots
            ndim (int, optional) ........... not used by this class, present
                                             for consistency
            mode (str, optional) ........... data storage mode, 'cl' or 'rp'

        """

        super(OneDimForce, self).__init__(ndim, mode)
        self._force_interp = interpolate.InterpolatedUnivariateSpline(
                knots, values
                )
        # Construct the antiderivative of the force
        self._anti_force = self._force_interp.antiderivative()


    def force(self, q, f, *args, **kwargs):
        """
        Cubic spline approximation to the force at point(s) q.

        """

        f[...] = self._force_interp(q)
        return f

    def potential(self, q, *args, **kwargs):
        """
        The negative antiderivative of the cubic spline interpolant
        of the force at point(s) q.

        """

        pot = self._anti_force(q)
        pot *= -1

        return pot

class ChampagneInterpForce(ChampagnePES, OneDimForce):

    def __init__(self, knots, values, ndim=2, mode='cl'):
        """
        Initialise the force using the grid data

        Input:
            knots (1d array) ............... radial coordinates at which
                                             the radial force has been
                                             calculated in a uniformly
                                             increasing order
            values (1d array) .............. radial force values calculated
                                             at the knots
            ndim (int, optional) ........... number of dimensions (default 2)
            mode (str, optional) ........... data storage mode, 'cl' or 'rp'

        """

        super(ChampagneInterpForce, self).__init__(knots, values, ndim, mode)

    def radial_force(self, r, f, *args, **kwargs):
        """
        Cubic spline approximation to the force at the radial displacement r.

        """

        f[...] = self._force_interp(r)
        return f

    def radial_potential(self, r, *args, **kwargs):
        """
        The negative antiderivative of the cubic spline interpolant
        of the force at point(s) q.

        """

        pot = self._anti_force(r)
        pot *= -1

        return pot

class PlanePolarForce(PES):
    """
    Force expressed as a function of (r, t).

    """

    def __init__(self, rknots, tknots, fx, fy, ndim=2, mode='cl'):
        """
        Input:
            rknots (1d array) ................ radial coordinates at which
                                               the radial force has been
                                               calculated in a uniformly
                                               increasing order
            tknots (1d array) ................ angular coordinates
            fx (2d array) .................... forces on x with r varying
                                               along the first axis
                                               and t along the last
            fy (2d array) .................... forces on y
            ndim (int, optional) ............. number of dimensions (default 2)
            mode (str, optional) ............. data storage mode, 'cl' or 'rp'

        """

        super(PlanePolarForce, self).__init__(ndim, mode)
        self._fx_interp = interpolate.RectBivariateSpline(rknots, tknots, fx)
        self._fy_interp = interpolate.RectBivariateSpline(rknots, tknots, fy)

    def force(self, q, f, *args, **kwargs):
        """
        Cubic spline approximation to the force at point(s) q.

        """

        init_shape = q.shape
        if self.mode == 'cl':
            q.shape = (-1, self.ndim)
        else:
            q.shape = (-1, self.ndim, init_shape[-1])

        f.shape = q.shape
        r = np.sqrt(np.sum(q**2, axis=1))
        t = np.arctan2(q[:,1], q[:,0])
        f[:,0] = self._fx_interp(r, t, grid=False)
        f[:,1] = self._fy_interp(r, t, grid=False)

        for arr in (q, f):
            arr.shape = init_shape

        return f

    def potential(self, q, *args, **kwargs):
        # To implement, convert Cartesian forces to forces on r and theta
        # and construct the corresponding anti-derivative
        raise NotImplementedError