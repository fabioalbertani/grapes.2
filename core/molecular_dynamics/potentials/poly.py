# File: poly.py
"""
Created on Mon May 14 17:22:50 2018

@author: gt317

Polynomial potentials of the form  ax^2 + bx^3 + ... and their straightforward
generalisation to many dimensions.

"""

from __future__ import division, print_function, absolute_import
from .base import PES
from functools import wraps
import numpy as np


def validate_coeff(func):
    @wraps(func)
    def wrapper(self, coeff):
        msg = """
Custom coefficients expected to be integers, floats, or arrays thereof.
"""
        if not isinstance(coeff, (int, float)):
            try:
                arr = np.asarray(coeff)
                arr_type = arr.dtype
            except AttributeError:
                raise TypeError(msg)
            except:
                raise

            if arr_type not in [np.int64, np.float64]:
                raise TypeError(msg)
            else:
                a = arr
        else:
            a = coeff
        return func(self, a)
    return wrapper


def reshape_force_args(func):
    @wraps(func)
    def wrapper(self, q, f, *args, **kwargs):
        q_shape = q.shape
        if self.mode == 'rp':
            nbeads = q_shape[-1]
            if len(q_shape) > 2:
                # Assume the array already shaped such that
                # the penultimate axis corresponds to spatial dimensions
                restore_shape = False
            elif len(q_shape) == 2:
                restore_shape = True
                q.shape = (-1, self.ndim, nbeads)
                f.shape = q.shape
            else:
                raise IndexError(
"Expecting an array of rank >= 2 for declared 'rp' storage mode."
                )
        elif self.mode == 'cl':
            if len(q_shape) > 1:
                # Assume the array already shaped such that
                # the last axis corresponds to spatial dimensions
                restore_shape = False
            else:
                restore_shape = True
                q.shape = (-1, self.ndim)
                f.shape = q.shape
        else:
            # This is redundant with the way self.mode is set
            raise ValueError(
"Input array storage mode incorrectly initialised in force class!"
            )
        f = func(self, q, f, *args, **kwargs)
        if restore_shape:
            q.shape = q_shape
            f.shape = q_shape
        return f
    return wrapper


def reshape_pot_args(func):
    @wraps(func)
    def wrapper(self, q, *args, **kwargs):
        q_shape = q.shape
        if self.mode == 'rp':
            nbeads = q_shape[-1]
            ax = -2
            if len(q_shape) > 2:
                # Assume the array already shaped such that
                # the penultimate axis corresponds to spatial dimensions
                restore_shape = False
            elif len(q_shape) == 2:
                restore_shape = True
                q.shape = (-1, self.ndim, nbeads)
            else:
                raise IndexError(
"Expecting an array of rank >= 2 for declared 'rp' storage mode."
                )
        elif self.mode == 'cl':
            ax = -1
            if len(q_shape) > 1:
                # Assume the array already shaped such that
                # the last axis corresponds to spatial dimensions
                restore_shape = False
            else:
                restore_shape = True
                q.shape = (-1, self.ndim)
        else:
            # This is redundant with the way self.mode is set
            raise ValueError(
"Input array storage mode incorrectly initialised in force class!"
            )
        pot_arr = np.empty_like(q)
        pot_arr = func(self, q, pot_arr, *args, **kwargs)
        if restore_shape:
            q.shape = q_shape
        # Sum over the dimensions
        pot = pot_arr.sum(axis=ax)
        return pot
    return wrapper


class PolynomialPES(PES):
    """
    Polynomial potentials of the form  ax^2 + bx^3 + ... for systems of
    decoupled particles.

    """

    coupled = False


class Harmonic(PolynomialPES):
    """
    Harmonic potential a*x^2/2.

    """

    def __init__(self, a=1.0, ndim=1, mode='rp'):
        """
        Initialise the harmonic potential with the spring constant.

        Input:
            a (float or array of floats) ........ coefficient in a*x^2/2
                                                  (default 1.0)
            ndim (int) .......................... number of spatial dimensions
                                                  (default 1)
            mode (str) .......................... indicates whether the input
                                                  coordinate is in ring-polymer
                                                  ('rp') or classical ('cl')
                                                  mode.

        """

        super(Harmonic, self).__init__(ndim, mode)
        self._a2 = None
        self.a = a

    @property
    def a(self):
        return self.__a

    @a.setter
    @validate_coeff
    def a(self, a):
        self.__a = a
        self._a2 = self.a/2

    @reshape_force_args
    def force(self, q, f, *args, **kwargs):
        """
        Harmonic force f(x) = -a*x.
        Input:
            q (array of floats) ............ configuration array
            f (array of floats) ............ force array

        Ouput:
            f .............................. modified in-place
        """

        f[...] = q
        f *= -self.a

        return f

    @reshape_pot_args
    def potential(self, q, *args, **kwargs):
        """
        Harmonic potential f(x[, y, ...]) = 0.5*a * (x**2[+ y**2 + ...]).

        Input:
            q (array of floats) ........... configuration array


        Output:
            pot (array of floats) ......... array of particle potentials

        """

        pot = q.copy()
        pot **= 2
        pot *= self._a2

        return pot

class Anharmonic(PolynomialPES):
    """
    Mildly anharmonic potential a*x^2/2 + b*x^3 + c*x^4.

    """

    def __init__(self, a=1.0, b=0.1, c=0.01, ndim=1, mode='rp'):
        """
        Initialise the mildly anharmonic potential with the
        coefficients a, b, c.

        Input:
            a (float or array of floats) ........ coefficient in a*x^2/2
                                                  (default 1.0)
            b (__"__"__) ........................ coefficient of the cubic term
                                                  (default 0.1)
            c (__"__"__) ........................ coefficient of the quartic
                                                  term (default 0.01)
            ndim (int) .......................... number of spatial dimensions
                                                  (default 1)
            mode (str) .......................... indicates whether the input
                                                  coordinate is in ring-polymer
                                                  ('rp') or classical ('cl')
                                                  mode.

        """

        super(Anharmonic, self).__init__(ndim, mode)
        self._a2 = None
        self._three_b = None
        self._four_c = None
        self.a = a
        self.b = b
        self.c = c

    @property
    def a(self):
        return self.__a

    @a.setter
    @validate_coeff
    def a(self, a):
        self.__a = a
        self._a2 = a/2

    @property
    def b(self):
        return self.__b

    @b.setter
    @validate_coeff
    def b(self, b):
        self.__b = b
        self._three_b = 3*b

    @property
    def c(self):
        return self.__c

    @c.setter
    @validate_coeff
    def c(self, c):
        self.__c = c
        self._four_c = 4*c


    @reshape_force_args
    def force(self, q, f, *args, **kwargs):
        """
        Anharmonic force f(x) = -(a*x + 3b*x^2 + 4c*x^3).
        Input:
            q (array of floats) ............ configuration array
            f (array of floats) ............ force array

        Ouput:
            f .............................. modified in-place
        """

        # f = x*(-a - x*(3b + x*4c))
        f[...] = q
        f *= self._four_c
        f += self._three_b
        f *= -q
        f -= self.a
        f *= q

        return f

    @reshape_pot_args
    def potential(self, q, *args, **kwargs):
        """
        Anharmonic potential f(x[, y, ...]) =
            0.5*a * (x**2[+ y**2 + ...]) +
            b * (x**3[+ y**3 + ...]) +
            c * (x**4[+ y**4 + ...])

        Input:
            q (array of floats) ........... configuration array


        Output:
            pot (array of floats) ......... array of particle potentials

        """

        # V = x^2*(a/2 + x*(b + x*c))
        pot = q.copy()
        pot *= self.c
        pot += self.b
        pot *= q
        pot += self._a2
        pot *= q
        pot *= q

        return pot


class Quartic(PolynomialPES):
    """
    The quartic potential a*x**4/4

    """

    def __init__(self, a=1.0, ndim=1, mode='rp'):
        """
        Initialise the quartic potential with the coefficient a.

        Input:
            a (float or array of floats) ........ coefficient in a*x^4/4
                                                  (default 1.0)
            ndim (int) .......................... number of spatial dimensions
                                                  (default 1)
            mode (str) .......................... indicates whether the input
                                                  coordinate is in ring-polymer
                                                  ('rp') or classical ('cl')
                                                  mode.

        """

        super(Quartic, self).__init__(ndim, mode)
        self._a4 = None
        self.a = a

    @property
    def a(self):
        return self.__a

    @a.setter
    @validate_coeff
    def a(self, a):
        self.__a = a
        self._a4 = a/4

    @reshape_force_args
    def force(self, q, f, *args, **kwargs):
        """
        Quartic force f(x) = -a*x^3
        Input:
            q (array of floats) ............ configuration array
            f (array of floats) ............ force array

        Ouput:
            f .............................. modified in-place
        """

        # f = x*(-a - x*(3b + x*4c))
        f[...] = q
        f **= 3
        f *= -self.a

        return f

    @reshape_pot_args
    def potential(self, q, *args, **kwargs):
        """
        Quartic potential f(x[, y, ...]) =
            0.25*a * (x**4[+ y**4 + ...])

        Input:
            q (array of floats) ........... configuration array
            pot (array of floats) ......... array of particle potentials

        Output:
            pot (array of floats) ......... array of particle potentials

        """

        pot = q.copy()
        pot **= 4
        pot *= self._a4

        return pot

