# File: champ.py
"""
Created on Tue May 15 14:18:26 2018

@author: gt317

Champagne-bottle potentials i.e. potentials that are expressable as
functions of a single radial coordinate, and are typically strongly
repulsive near the origin.

"""

from __future__ import division, print_function, absolute_import
import sys
import os
root = os.path.split(os.path.split(os.path.abspath(os.curdir))[0])[0]
sys.path.insert(0,root)

from qclpysim.potentials.base import PES
import numpy as np
import warnings
from abc import abstractmethod
from functools import wraps

__all__ = ['Harmonic', 'Morse', 'QTIP4Pf', 'MorseDiabat',
           'MorseCharge', 'QTIP4PfCharge']


def validate_coeff(func):
    @wraps(func)
    def wrapper(self, coeff):
        msg = """
Custom coefficients expected to be integers or floats.
"""
        if not isinstance(coeff, (int, float)):
            raise TypeError(msg)
        return func(self, coeff)
    return wrapper


def reshape_pot_args(func):
    @wraps(func)
    def wrapper(self, q, *args, **kwargs):
        q_shape = q.shape
        if self.mode == 'rp':
            nbeads = q_shape[-1]
            if len(q_shape) > 2:
                # Assume the array already shaped such that
                # the penultimate axis corresponds to spatial dimensions
                restore_shape = False
            elif len(q_shape) == 2:
                restore_shape = True
                q.shape = (-1, self.ndim, nbeads)
            else:
                raise IndexError(
"Expecting an array of rank >= 2 for declared 'rp' storage mode."
                )
        elif self.mode == 'cl':
            if len(q_shape) > 1:
                # Assume the array already shaped such that
                # the last axis corresponds to spatial dimensions
                restore_shape = False
            else:
                restore_shape = True
                q.shape = (-1, self.ndim)
        else:
            # This is redundant with the way self.mode is set
            raise ValueError(
"Input array storage mode incorrectly initialised in force class!"
            )
        pot = func(self, q, *args, **kwargs)
        if restore_shape:
            q.shape = q_shape
        return pot
    return wrapper


def reshape_force_args(func):
    @wraps(func)
    def wrapper(self, q, f, *args, **kwargs):
        q_shape = q.shape
        if self.mode == 'rp':
            nbeads = q_shape[-1]
            if len(q_shape) > 2:
                # Assume the array already shaped such that
                # the penultimate axis corresponds to spatial dimensions
                restore_shape = False
            elif len(q_shape) == 2:
                restore_shape = True
                q.shape = (-1, self.ndim, nbeads)
                f.shape = q.shape
            else:
                raise IndexError(
"Expecting an array of rank >= 2 for declared 'rp' storage mode."
                )
        elif self.mode == 'cl':
            if len(q_shape) > 1:
                # Assume the array already shaped such that
                # the last axis corresponds to spatial dimensions
                restore_shape = False
            else:
                restore_shape = True
                q.shape = (-1, self.ndim)
                f.shape = q.shape
        else:
            # This is redundant with the way self.mode is set
            raise ValueError(
"Input array storage mode incorrectly initialised in force class!"
            )
        f = func(self, q, f, *args, **kwargs)
        if restore_shape:
            q.shape = q_shape
            f.shape = q.shape
        return f
    return wrapper


class ChampagnePES(PES):
    """
    Parent class for all PES that depend on a single radial coordinate.

    """

    coupled = False

    @property
    def ndim(self):
        return self.__ndim

    @ndim.setter
    def ndim(self, ndim):
        if not isinstance(ndim, int):
            raise TypeError(
"Number of dimensions expected to be integer."
            )
        elif ndim < 0:
            raise ValueError(
"Number of spatial dimensions cannot be negative."
            )
        elif ndim == 1:
            warnings.warn("""
Using ndim = 1 with champagne-bottle forces may produce unexpected behaviour
in force and potential functions. Consider accessing radial_force and
radial_potential instead.
"""
)
        self.__ndim = ndim

    @property
    def mode(self):
        return self.__mode

    @mode.setter
    def mode(self, mode):
        try:
            implemented = mode.lower() in ['rp', 'cl']
        except:
            raise TypeError(
"Mode specification expected to be a string."
            )

        if implemented:
            self.__mode = mode.lower()
            # Set th axis for spatial dimensions in reshaped input arrays
            if self.__mode == 'rp':
                self._axis = -2
            else:
                self._axis = -1
        else:
            raise ValueError(
"Input data storage mode '{:s}' not implemented.".format(mode.lower())
            )

    @staticmethod
    def _q2r(q, axis=-2):
        """
        Convert atomic coordinates to radii. By default the penultimate
        axis is taken to vary over spatial dimensions.

        """

        q_sq = q.copy()
        q_sq **= 2
        r = np.sum(q_sq, axis=axis)
        r **= 0.5

        return r

    @staticmethod
    def _r2q(r, fr, q, fq, axis=-2):
        """
        Convert radial forces into Cartesian.

        """

        idx = (axis)%r.ndim+1
        r_shape = list(r.shape)
        r_shape.insert(idx, 1)
        r_mould = np.reshape(r, r_shape)
        fr_mould = np.reshape(fr, r_shape)

        if (r.shape == q.shape):
            fq[...] = fr_mould
        else:
            fq[...] = q
            fq *= fr_mould
            fq /= r_mould

        return fq

    @abstractmethod
    def radial_force(self, r, f, *args, **kwargs):
        pass

    @abstractmethod
    def radial_potential(self, r, *args, **kwargs):
        pass

    @reshape_pot_args
    def potential(self, q, *args, **kwargs):
        """
        Calculates V(r) where r = sqrt(x**2 + y**2[+ z**2 + ...])

        Input:
            q (array of floats) .......... particle coordinates


        Return:
            pot (array of floats) ........ array of potential energies

        """

        r = self._q2r(q, axis=self._axis)
        pot = self.radial_potential(r, *args, **kwargs)
        return pot

    @reshape_force_args
    def force(self, q, f, *args, **kwargs):
        """
        Calculates -dV(r)/dr where r = sqrt(x**2 + y**2[+ z**2 + ...])

        Input:
            q (array of floats) .......... particle coordinates
            f (array of floats) .......... conforming array to store
                                           forces

        Return:
            f ............................ modified in-place

        """

        r = self._q2r(q, axis=self._axis)
        fr = np.empty_like(r)
        fr = self.radial_force(r, fr)
        f = self._r2q(r, fr, q, f, axis=self._axis)

        return f


class Morse(ChampagnePES):
    """
    Champagne-bottle Morse potential
        V(r) = d*(1 - exp[-alpha*(r - req)])**2

    """

    def __init__(self,
                 d=0.187482528,
                 alpha=1.1604604353,
                 req=1.832382172,
                 ndim=2, mode='rp'):

        super(Morse, self).__init__(ndim, mode)

        self.d = d
        self.alpha = alpha
        self.req = req

    @property
    def d(self):
        return self.__d

    @d.setter
    @validate_coeff
    def d(self, d):
        self.__d = d

    @property
    def alpha(self):
        return self.__alpha

    @alpha.setter
    @validate_coeff
    def alpha(self, alpha):
        self.__alpha = alpha

    @property
    def req(self):
        return self.__req

    @req.setter
    @validate_coeff
    def req(self, req):
        self.__req = req

    def radial_potential(self, r, *args, **kwargs):
        """
        Calculates V(r) = d*(1 - exp[-alpha*(r - req)])**2.

        Input:
            r (array of floats) .......... radial displacements

        Return:
            pot (array of floats) ........ array of potential energies

        """

        r_shift = r-self.req
        r_shift *= -self.alpha
        pot = -np.exp(r_shift)
        pot += 1
        pot **= 2
        pot *= self.d

        return pot

    def radial_force(self, r, f, *args, **kwargs):
        """
        Calculates minus the radial derivative of
                V(r) = d*(1 - exp[-alpha*(r - req)])**2.

        Input:
            r (array of floats) .......... radial displacements
            f (array of floats) .......... conforming array to store
                                           radial forces

        Return:
            f ............................ modified in-place

        """

        f[...] = r
        f -= self.req
        f *= -self.alpha
        exp_r = np.exp(f)
        f[...] = 1-exp_r
        f *= exp_r
        f *= -2*self.alpha*self.d

        return f

class QTIP4Pf(Morse):
    """
    The radial intramolecular part of the flexible water potential
    due to Habershon et al. DOI: 10.1063/1.3167790

    """

    def __init__(self,
                 d=0.185,
                 alpha=1.21,
                 req=1.78,
                 ndim=2, mode='rp'):

        super(QTIP4Pf, self).__init__(d, alpha, req, ndim, mode)

    def radial_potential(self, r, *args, **kwargs):
        """
        Calculates the fourth-order Taylor series expansion of
            V(r) = d*(1 - exp[-alpha*(r - req)])**2
        about equilibrium.

        Input:
            r (array of floats) .......... radial displacements

        Return:
            pot (array of floats) ........ array of potential energies

        """

        r_shift = r-self.req
        r_shift *= self.alpha
        term = r_shift.copy()
        term *= r_shift
        pot = term.copy()
        term *= r_shift
        pot -= term
        term *= r_shift
        term *= 7.0/12.0
        pot += term
        pot *= self.d

        return pot

    def radial_force(self, r, f, *args, **kwargs):
        """
        Calculates the fourth-order Taylor series expansion of the negative
        gradient of
            V(r) = d*(1 - exp[-alpha*(r - req)])**2
        about equilibrium.

        Input:
            r (array of floats) .......... radial displacements
            f (array of floats) .......... conforming array to store
                                           radial forces

        Return:
            f ............................ modified in-place

        """

        r_shift = r-self.req
        r_shift *= self.alpha
        term = r_shift.copy()
        f = 2*term
        term *= r_shift
        f -= 3*term
        term *= r_shift
        term *= 7.0/3.0
        f += term
        f*= -self.d*self.alpha

        return f

class Harmonic(ChampagnePES):
    """
    Harmonic Morse potential
        V(r) = k/2*(r - req)^2

    """

    def __init__(self,
                 k=0.49536,
                 req = 1.89,
                 ndim=2, mode='rp'):

        super(Harmonic, self).__init__(ndim, mode)

        self.k = k
        self.req = req

    @property
    def k(self):
        return self.__k

    @k.setter
    @validate_coeff
    def k(self, k):
        self.__k = k
        self._k2 = self.__k/2

    @property
    def req(self):
        return self.__req

    @req.setter
    @validate_coeff
    def req(self, req):
        self.__req = req

    def radial_potential(self, r, *args, **kwargs):
        """
        Calculates V(r) = k/2*(r - req)**2.

        Input:
            r (array of floats) .......... radial displacements

        Return:
            pot (array of floats) ........ array of potential energies

        """

        pot = r - self.req
        pot **= 2
        pot *= self._k2

        return pot

    def radial_force(self, r, f, *args, **kwargs):
        """
        Calculates minus the radial derivative of
                V(r) = k/2*(r - req)**2.

        Input:
            r (array of floats) .......... radial displacements
            f (array of floats) .......... conforming array to store
                                           radial forces

        Return:
            f ............................ modified in-place

        """

        f[...] = r
        f -= self.req
        f *= -self.k

        return f


class MorseDiabat(Morse):
    """
    The ground-state of the diabatic PES model of the hydrogen bond due to
        R.H. McKenzie, Chem. Phys. Lett. 535 (2012) 196-200

    """

    def __init__(self,
                 d=0.187482528,
                 alpha=1.1604604353,
                 req=1.832382172,
                 Delta1=None,   # 0.4*d
                 beta=None,     # alpha
                 rOO=5.5,
                 ndim=2,
                 mode='rp'):
        """
        V = (V_M(r)+V_M(r*) - SQRT[(V_M(r)-V_M(r*))/4 + Delta**2]
            V_M(r) = d*[1-exp(-alpha*(r-req)]**2
            r = SQRT[x**2 + y**2 + z**2]
            r* = SQRT[(x-rOO)**2 + y**2 + z**2]
            Delta = Delta1*exp(-beta(rOO-r1)) *
                    cos(theta)*(rOO-r*cos(theta))/r*
            r1 = 2*req+1/alpha
            cos(theta) = x/r

        Initialise with the following parameters:
        d .................... default 0.187482528
        alpha ................ default 1.1604604353
        req .................. default 1.832382172
        Delta1 ............... default 0.4*d
        alpha ................ default equal to alpha
        rOO .................. default 5.5
        ndim ................. default 2
        mode ................. default 'rp'


        """

        super(MorseDiabat, self).__init__(d, alpha, req, ndim, mode)
        # Calculate Delta0 = Delta1*exp(-beta(rOO-r1))
        if Delta1 is None:
            Delta1 = 0.4*self.d

        if beta is None:
            beta = self.alpha

        self.rOO = rOO
        self.Delta0 = Delta1*np.exp(-beta*(rOO-(2*self.req+1/self.alpha)))

    def coupling_term(self, q, r, r_star):
        """
        Return the coupling term
            Delta(q, r, r*) = Delta0*cos(theta)*(rOO-r*cos(theta))/r*
        where cos(theta) = x/r
        Spatial dimensions are assumed to vary along the second axis.

        """

        cos_theta = q[:,0].copy()
        ans = self.rOO-cos_theta
        eps = 1.0e-12
        bool_arr = r > eps
        temp = np.where(bool_arr, r, eps)
        cos_theta /= temp
        cos_theta[np.logical_not(bool_arr)] = 0.0
        ans *= cos_theta
        ans *= self.Delta0
        ans /= np.where(r_star > eps, r_star, eps)

        return ans

    def coupling_grad(self, q, f, r, r_star):
        """
        Return minus the gradient of the coupling term defined above with
        respect to the coordinate q.

        Spatial dimensions are assumed to vary along the second axis.
        """

        rOO_x = self.rOO - q[:,0:1]
        rOO_2x = rOO_x - q[:,0:1]
        r_sq = r**2

        if r_sq.ndim == 2:
            r_sq.shape = (r_sq.shape[0],1,-1)
        elif r_sq.ndim == 1:
            r_sq.shape = (r_sq.shape[0],1)
        else:
            raise NotImplementedError

        f[:,0:1] = rOO_2x
        f[:,0:1] *= r_sq - q[:,0:1]**2
        f[:,0:1] *= (self.rOO*rOO_x + r_sq)

        f[:,1:] = q[:,0:1]
        f[:,1:] *= -rOO_x
        f[:,1:] *= 2*r_sq + self.rOO*rOO_2x
        f[:,1:] *= q[:,1:]

        r_sq *= r[:,None,...]
        f /= r_sq
        eps = 1.0e-12
        f /= -np.where(r_star > eps, r_star, eps)[:,None,...]**3

        return f

    @reshape_pot_args
    def potential(self, q, *args, **kwargs):

        r = self._q2r(q, axis=self._axis)
        V_D = self.radial_potential(r, *args, **kwargs)
        q_shift = q.copy()
        q_shift[:,0] -= self.rOO
        r_star= self._q2r(q_shift, axis=self._axis)
        V_A = self.radial_potential(r_star, *args, **kwargs)
        Delta = self.coupling_term(q, r, r_star)
        Delta **= 2

        pot = V_D-V_A
        pot /= 2
        pot **= 2
        pot += Delta
        pot **= 0.5
        pot *= -1
        pot += (V_D+V_A)/2

        return pot

    @reshape_force_args
    def force(self, q, f, *args, **kwargs):

        r = self._q2r(q, axis=self._axis)
        fr = np.empty_like(r)
        fr = self.radial_force(r, fr)
        grad_V_A = np.empty_like(f)
        grad_V_A = self._r2q(r, fr, q, grad_V_A, axis=self._axis)

        q_shift = q.copy()
        q_shift[:,0] -= self.rOO
        r_star = self._q2r(q_shift, axis=self._axis)
        fr = self.radial_force(r_star, fr)
        grad_V_D = np.empty_like(f)
        grad_V_D = self._r2q(r, fr, q_shift, grad_V_D, axis=self._axis)

        grad_Delta = np.empty_like(f)
        grad_Delta = self.coupling_grad(q, grad_Delta, r, r_star)

        Delta = self.coupling_term(q, r, r_star)
        V_D = self.radial_potential(r, *args, **kwargs)
        V_A = self.radial_potential(r_star, *args, **kwargs)

        V_D -= V_A
        f[...] = V_D[:,None,...]/2
        f **= 2
        f += Delta[:,None,...]**2
        f **= -0.5

        grad_V_D -= grad_V_A     # grad[V_D-V_A]
        grad_V_A *= 2
        grad_V_A += grad_V_D     # grad[V_D+V_A]
        grad_V_D *= V_D[:,None,...]

        grad_Delta *= Delta[:,None,...]
        grad_Delta *= 2

        grad_V_D += grad_Delta
        f *= grad_V_D
        f -= grad_V_A
        f /= 2

        return f

def charge_pot(q, shift, charge, ax):

    q_shift = q.copy()
    q_shift[:,0] -= shift
    r_star= ChampagnePES._q2r(q_shift, axis=ax)
    eps = 1.0e-12
    pot = np.where(r_star>eps, r_star, eps)
    pot **= -1
    pot *= -charge**2

    return pot

def charge_force(q, shift, charge, ax):

    q_shift = q.copy()
    q_shift[:,0] -= shift
    r_star = ChampagnePES._q2r(q_shift, axis=ax)
    fr = eps = 1.0e-12
    fr = np.where(r_star>eps, r_star, eps)
    fr **= -2
    fr *= charge**2
    f = np.empty_like(q)
    f = ChampagnePES._r2q(r_star, fr, q_shift, f, axis=ax)

    return f

class MorseCharge(Morse):
    """
    A particle in a Morse potential neighbouring an oppositely-charged centre.

    """

    def __init__(self,
                 d=0.187482528,
                 alpha=1.1604604353,
                 req=1.832382172,
                 charge=1.0,
                 rOO=5.5,
                 ndim=2,
                 mode='rp'):
        """
        V = (V_M(r)-charge**2/r*
            r* = SQRT[(x-rOO)**2 + y**2 + z**2]

        Initialise with the following parameters:
        d .................... default 0.187482528
        alpha ................ default 1.1604604353
        req .................. default 1.832382172
        charge ............... default 1
        rOO .................. default 5.5
        ndim ................. default 2
        mode ................. default 'rp'


        """

        super(MorseCharge, self).__init__(d, alpha, req, ndim, mode)
        self.rOO = rOO
        self.charge = charge

    @reshape_pot_args
    def potential(self, q, *args, **kwargs):

        r = self._q2r(q, axis=self._axis)
        pot = self.radial_potential(r, *args, **kwargs)
        pot += charge_pot(q, self.rOO, self.charge, self._axis)

        return pot

    @reshape_force_args
    def force(self, q, f, *args, **kwargs):

        f = super(MorseCharge, self).force(q, f, *args, **kwargs)
        f += charge_force(q, self.rOO, self.charge, self._axis)

        return f

class QTIP4PfCharge(QTIP4Pf):
    """
    A particle in a qTIP4P/F Morse potential neighbouring an
    oppositely-charged centre.

    """

    def __init__(self,
                 d=0.185,
                 alpha=1.21,
                 req=1.78,
                 charge=1.0,
                 rOO=5.5,
                 ndim=2,
                 mode='rp'):
        """
            V = V_qTIP4P/F-charge**2/r*
            r* = SQRT[(x-rOO)**2 + y**2 + z**2]

        Initialise with the following parameters:
        d .................... default 0.185,
        alpha ................ default 1.21,
        req .................. default 1.78,
        charge ............... default 1
        rOO .................. default 5.5
        ndim ................. default 2
        mode ................. default 'rp'


        """

        super(QTIP4PfCharge, self).__init__(d, alpha, req, ndim, mode)
        self.rOO = rOO
        self.charge = charge

    @reshape_pot_args
    def potential(self, q, *args, **kwargs):

        r = self._q2r(q, axis=self._axis)
        pot = self.radial_potential(r, *args, **kwargs)
        pot += charge_pot(q, self.rOO, self.charge, self._axis)

        return pot

    @reshape_force_args
    def force(self, q, f, *args, **kwargs):

        f = super(QTIP4PfCharge, self).force(q, f, *args, **kwargs)
        f += charge_force(q, self.rOO, self.charge, self._axis)

        return f

class AnharmonicAngleMorse(Morse):
    """
    A particle in a 2D champagne-bottle Morse potential with
    an anharmonic angular dependence.

    """

    def __init__(self,
             d=0.187482528,
             alpha=1.1604604353,
             req=1.832382172,
             beta1=0.025,
             beta2=0.005,
             beta3=0.001,
             mode='rp'):

        super(AnharmonicAngleMorse, self).__init__(d, alpha, req,
                                                   2, mode)
        self.beta1 = beta1
        self.beta2 = beta2
        self.beta3 = beta3

    @staticmethod
    def _t2q(r, t, ft, q, axis=-2):
        """
        Convert 2d angular forces into Cartesian.

        """

        # fx = -y*ft/r^2
        # fy = x*ft/r**2

        f = q[:,::-1].copy()
        f[:,0] *= -ft
        f[:,1] *= ft
        f /= r[:,None,...]**2

        return f


    def angular_potential(self, t, *args, **kwargs):
        """
        Return the angular part of the potential

        """

        tpow = t**2
        pot = self.beta1*tpow

        tpow *= t
        pot += self.beta2*tpow

        tpow += t
        pot += self.beta3*tpow

        return pot

    def angular_force(self, t, f, *args, **kwargs):

        f[...] = t
        f *= -2*self.beta1

        tpow = t**2
        f += -3*self.beta2*tpow

        tpow *= t
        tpow *= -4*self.beta3
        f += tpow

        return f


    @reshape_pot_args
    def potential(self, q, *args, **kwargs):
        """
        Calculate the potential
            V(r, t) = V_M(r) + b1*t**2 + b2*t**3 + b3*t**4
        """

        pot = super(AnharmonicAngleMorse, self).potential(q, *args, **kwargs)
        t = np.arctan2(q[:,1], q[:,0])
        pot += self.angular_potential(t, *args, **kwargs)

        return pot

    @reshape_force_args
    def force(self, q, f, *args, **kwargs):
        """
        Calculate the cartesian forces derived from
            V(r, t) = V_M(r) + b1*t**2 + b2*t**3 + b3*t**4

        """

        r = self._q2r(q, axis=self._axis)
        t = np.arctan2(q[:,1], q[:,0])
        fr = np.empty_like(r)
        fr = self.radial_force(r, fr)
        f = self._r2q(r, fr, q, f, axis=self._axis)
        fr = self.angular_force(t, fr)
        f += self._t2q(r, t, fr, q, axis=self._axis)

        return f