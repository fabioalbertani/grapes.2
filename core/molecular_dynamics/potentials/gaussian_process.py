#File: core.molecular_dynamics.potentials.gaussian_process.py

from numpy import zeros_like
from copy import copy

from core.molecular_dynamics.potentials.base import PES



__all__ = ['GaussianProcessPES']


class GaussianProcessPES(PES):
    '''
    Interface between core.interpolation modules
    and core.molecular_dynamics modules
    '''

    def __init__(self, GP, ndim=1, mode='rp'):

        self.GP = GP
        self._analytical_gradient = self.GP.kernel_.analytical_gradient

        super(GaussianProcessPES, self).__init__(ndim, mode)


    @property
    def analytical_gradient(self):
        '''
        is true if the kernel associated to the GP has a defined analytical
        gradient associated to it (otherwise a standard finite difference
        method is used)
        '''
        return self._analytical_gradient



    def force(self, q, f, CalcDetails, el, dq=0.005):
        '''
        Can be implemented as both brute force approach of finite difference
        or from derivative of kernel (easy for RBF but might be a ot harder 
        for more extensive kernels)
        '''
        if self.anaytical_gradient:
            return -self.GP.PredictMeanForce(q.reshape((q.shape[0],1)),
                                             CalcDetails, el, full=True
                                             )

        else:
            q = q.reshape((q.shape[0],1))
            f = zeros_like(q)
            for d in range(q.shape[0]):
                q_calc = copy(q)
                q_calc[d,1] += dq
                f[d,1] = self.GP.PredictMean(q_calc, CalcDetails, el) 
                q_calc[d,1] += -2.0*dq
                f[d,1] += -self.GP.PredictMean(q_calc, CalcDetails, el)

            return -0.5*f/dq


    def potential(self, q, CalcDetails, el):

        return self.GP.PredictMean(q.reshape((q.shape[0],1)), 
                                   CalcDetails, el
                                  )

