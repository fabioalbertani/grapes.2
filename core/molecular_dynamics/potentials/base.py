# File: base.py
"""
Created on Mon May 14 17:57:12 2018

@author: gt317

Define an abstract class for all forces.

"""

from __future__ import division, print_function
from abc import ABCMeta, abstractmethod

__all__ = ['PES']

class PES:
    """
    An abstract class for all potential energy surfaces and associated
    forces.

    """
    __metaclass__ = ABCMeta

    @abstractmethod
    def __init__(self, ndim=1, mode='rp'):
        """
        Record how the input configuration stores its data.

        Input:
            ndim (int) .......................... number of spatial dimensions
                                                  (default 1)
            mode (str) .......................... indicates whether the input
                                                  coordinate is in ring-polymer
                                                  ('rp') or classical ('cl')
                                                  mode.
        """

        self.ndim = ndim
        self.mode = mode

    @property
    def ndim(self):
        return self.__ndim

    @ndim.setter
    def ndim(self, ndim):
        if not isinstance(ndim, int):
            raise TypeError(
"Number of dimensions expected to be integer."
            )
        elif ndim < 0:
            raise ValueError(
"Number of spatial dimensions cannot be negative."
            )
        self.__ndim = ndim

    @property
    def mode(self):
        return self.__mode

    @mode.setter
    def mode(self, mode):
        try:
            implemented = mode.lower() in ['rp', 'cl']
        except:
            raise TypeError(
"Mode specification expected to be a string."
            )

        if implemented:
            self.__mode = mode.lower()
        else:
            raise ValueError(
"Input data storage mode '{:s}' not implemented.".format(mode.lower())
            )


    @abstractmethod
    def force(self, q, f, *args, **kwargs):
        pass

    @abstractmethod
    def potential(self, q, *args, **kwargs):
        pass
