# File: r_theta_grid.py
"""
Created on Tue Jun 12 11:22:57 2018

Classes for 2D PIMD simulations on a grid in r and theta.

"""

from __future__ import division, print_function
import numpy as np
import sys
import os
root = os.path.split(os.path.split(os.path.abspath(os.curdir))[0])[0]
sys.path.insert(0,root)


class CentroidGrid(object):
    """
    Perform PIMD calculations with the centroid fixed at points
    on a grid in plane polar coordinates.

    """

    def __init__(self, rgrid, tgrid, stride, ntherm, nsample,
                 fxns, outname, fmt, delim, rp):
        """
        Initialise the simulation with the following parameters:
            rgrid (1d array) .................. grid of radial coordinates
            tgrid (1d array) .................. grid of angular coordinates
            stride (int) ...................... sampling stride ("record
                                                property X every 'stride'
                                                timesteps")
            ntherm (int) ...................... number of thermalisation
                                                steps per grid-point
            nsample (int) ..................... number of samples per
                                                grid-point
            fxns (list) ....................... a list of functions that
                                                take a ring-polymer object
                                                and return the property
                                                of interest
            outname (string) .................. name of the output file
            fmt (string) ...................... format string
            delim (string) .................... delimiter string
            rp (object) ....................... an instantiated ring-polymer
                                                class used for sampling
        """

        self.rgrid = rgrid.reshape(-1).copy()
        self.tgrid = tgrid.reshape(-1).copy()
        self._idx = 0
        self._len_y = self.tgrid.size
        self._len_grid = self._len_y*self.rgrid.size
        self.r0, self.t0 = self._read_point(self._idx)
        self.stride = stride
        self.ntherm = ntherm
        self.nsample = nsample
        self.fxns = fxns
        self.outname = outname
        self.fmt = fmt
        self.delim = delim
        self.system = rp
        self.value_list = []
        for fxn in self.fxns:
            val = fxn(self.system).reshape(-1)
            self.value_list.append(np.zeros((1, val.size)))

    def _read_point(self, idx):

        i = idx//self._len_y
        j = idx%self._len_y

        return self.rgrid[i], self.tgrid[j]

    def next_point(self, step=1):

        self._idx += step
        if self._idx < 0 or self._idx >= self._len_grid:
            ans = -1
        else:
            ans = None
            self.r0, self.t0 = self._read_point(self._idx)

            for arr in self.value_list:
                arr *= 0.0

            q0 = self.r0*np.array([np.cos(self.t0), np.sin(self.t0)])
            self.system.q.shape = (-1, 2, self.system.q.shape[-1])
            self.system.q[:,:,0] = q0
            self.system.q.shape = (-1, self.system.q.shape[-1])
            self.system.f = self.system.force(self.system.q,
                                              self.system.f)

        return ans

    def thermalise(self, ndt, **kwargs):

        pslc = kwargs.pop('pslc', [slice(None), slice(1,None)])
        self.system.evolve(ndt, pslc=pslc, **kwargs)

    def sample(self, **kwargs):

        for i in range(self.nsample):
            self.thermalise(self.stride, **kwargs)
            for arr, fxn in zip(self.value_list, self.fxns):
                arr += fxn(self.system)

        for arr in self.value_list:
            arr /= self.nsample

        return

    def export_data(self):

        with open(self.outname, 'ab') as f:
            np.savetxt(f, np.concatenate(
                [
                np.array([[self.r0, self.t0]])
                ]+self.value_list, axis=-1),
                fmt=self.fmt, delimiter=self.delim)

    def run(self, **kwargs):

        self._idx = -1
        for i in range(self._len_grid):
            self.next_point()
            self.thermalise(self.ntherm, **kwargs)
            self.sample(**kwargs)
            self.export_data()

class PolarGrid(CentroidGrid):
    """
    Perform PIMD calculations with a RATTLE-constrained RP object,
    constraining the mean radius and angle to lie on the grid

    """

    def next_point(self, step=1):

        self._idx += step
        if self._idx < 0 or self._idx >= self._len_grid:
            ans = -1
        else:
            ans = None
            self.r0, self.t0 = self._read_point(self._idx)

            for arr in self.value_list:
                arr *= 0.0

            for val in self.system.propa.const_vals:
                val[...] = np.array([self.r0, self.t0])

            self.system.q = self.system.propa.mould(self.system.q)
            self.system.propa.reset_constraints(self.system.q)
            self.system.f = self.system.force(self.system.q,
                                              self.system.f)

        return ans

    def thermalise(self, ndt, **kwargs):
        self.system.nvt_evolve(ndt, **kwargs)