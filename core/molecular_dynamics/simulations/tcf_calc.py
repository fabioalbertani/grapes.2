# File: tcf_calc.py
"""
Created on Wed Jun 13 10:59:37 2018


Run a CMD simulation on an interpolated potential to calculate
a set of time-correlation functions.

"""

from __future__ import division, print_function
import numpy as np
import sys
import os
root = os.path.split(os.path.split(os.path.abspath(os.curdir))[0])[0]
sys.path.insert(0,root)
from core.molecular_dynamics.utils.tcfs import FFTCorrelationFxn
from core.grapes_utils.main_utils import SuperPrint
from core.grapes_utils.progress_bar import DisplayBar


class ParticleSystem(object):
    """
    An ensemble of particles on a pre-calculated potential of mean field,
    with methods for calculating time-correlation functions

    """

    @staticmethod
    def RegisterOptions(args):
        m = args.RegisterModule('method')

        args.Register(m, '--rpmd', action='store_true', default=False,
                      help='allows to switch to rpmd from classical')
        args.Register(m, '--beads', default=4, type=int,
                      help='number of beads')

        args.Register(m, '--beta', default=2.0, type=float,
                      help='inverse temperature')
        args.Register(m, '--dt', default=0.2, type=float,
                      help='time step ifor propagator')
        args.Register(m, '--ntherm', default=50000, type=int,
                      help='Number of thermalisation steps')
        args.Register(m, '--nsample', default=5000, type=int, 
                      help='Number of samples for tcf calculations')
        args.Register(m, '--stride', default=2, type=int,
                      help='stride option for tcf calculations')
        args.Register(m, '--nstride', default=2500, type=int,
                      help='nstride option for tcf calculations')


    @staticmethod
    def ParseOptions(args):
        pass

    
    def __init__(self, ntherm, nsample, stride, nstride,
                 particles, tcf_fxns, static_fxns=None,
                 nrelax=None, outname='out.txt',
                 fmt='%23.16e', delim='  '):

        """
        Initialise the simulation with the following parameters:
            ntherm (int) ................... number of NVT propagation steps for
                                             initial system thermalisation
            nsample (int) .................. number of time averages contibuting
                                             to each TCF
            stride (int) ................... length of the TCF time-step in
                                             multiples of the simulation time-step
            nstride (int) .................. number of strides per time average
            particles (object) ............. an instantiated particles class
            tcf_fxns (list of tuples) ...... list of tuples of callable objects
                                             for calculating the TCFs
            static_fxns (list) ............. list of static properties to be
                                             calculated on-the-fly
                                             (default None)
            nrelax (int, optional) ......... number of equilibration steps
                                             between contributions to the TCF
                                             average; if None, the simulation
                                             is run as a continous trajectory.
            outname (str, optional) ........ name of the output file
                                             (default out.txt)
            fmt (str, optional) ............ output format string
                                             (default '%23.16e')
            delim (str, optional) .......... delimiter string
                                             (default '  ')

        """
        self.SP = SuperPrint('TCFs.log', stdout=False)
    
        self.ntherm = ntherm
        self.nsample = nsample
        self.stride = stride
        self.nstride = nstride
        self.tcf_fxns = tcf_fxns
        self.outname = outname
        self.fmt = fmt
        self.delim = delim
        self.system = particles
        if static_fxns is None:
            self._get_static = False
        else:
            self._get_static = True
            self.static_fxns = static_fxns
            static_list = []
            self._static_idx = []
            lo = 0
            for A in self.static_fxns:
                est = np.asarray(A(self.system)).reshape(-1)
                static_list += est.size*[0,]
                hi = lo+est.size
                self._static_idx.append((lo,hi))
                lo = hi
            self.static_arr = np.asarray(static_list)


        if nrelax is None:
            self.nrelax = 0
            self._shift_estimators = True
        else:
            self.nrelax = nrelax
            self._shift_estimators = False

        # Construct arrays to hold estimators:
        A_list = []
        B_list = []
        self.A_fxn_list = []
        self.B_fxn_list = []
        lo = 0
        self._idx_list = []
        for AB in tcf_fxns:
            try:
                A,B = AB
            except:
                raise ValueError("""
Expecting tuples of estimator functions.
""")
            for fxn, fxn_list, est_list in zip(AB,
                    [self.A_fxn_list, self.B_fxn_list],
                    [A_list, B_list]):
                fxn_list.append(fxn)
                est = np.asarray(fxn(self.system)).reshape(-1)
                est_list.append(np.empty((est.size,2*self.nstride-1)))
                # Make a record of estimator array chunks that
                # correspond to a given property
                if est_list is A_list:
                    hi = lo+est.size
                    self._idx_list.append((lo,hi))
                    lo = hi

        self.A_arr = np.concatenate(A_list, axis=0)
        self.B_arr = np.concatenate(B_list, axis=0)
        self.tcf_arr = np.zeros((self.A_arr.shape[0], self.nstride))

        self.fft_corr = FFTCorrelationFxn(self.tcf_arr.shape)

    def record_est(self, istride):
        """
        Record the values of the estimators for the current stride index.

        """

        if self._get_static is True:
            for i, idces in enumerate(self._static_idx):
                lo, hi = idces
                self.static_arr[lo:hi] += np.asarray(
                    self.static_fxns[i](self.system)
                    ).reshape(-1)

        for i, idces in enumerate(self._idx_list):
            lo, hi = idces
            self.A_arr[lo:hi, istride] = np.asarray(
                self.A_fxn_list[i](self.system)).reshape(-1)
            if self.B_fxn_list[i] is self.A_fxn_list[i]:
                self.B_arr[lo:hi, istride] = self.A_arr[lo:hi, istride]
            else:
                self.B_arr[lo:hi, istride] = np.asarray(
                    self.B_fxn_list[i](self.system)).reshape(-1)

    def add_time_avg(self):
        """
        Add the correlation of the values currently in the A and B estimator
        arrays to the time-correlation function.

        """

        tcf = self.fft_corr(self.A_arr, self.B_arr)
        self.tcf_arr += tcf

    def shift_est(self):
        """
        Shift the estimator values from the second to the first half of
        the estimator array.

        """

        self.A_arr[:,:self.nstride-1] = self.A_arr[:,self.nstride:]
        self.B_arr[:,:self.nstride-1] = self.B_arr[:,self.nstride:]

    def thermalise(self, ndt, **kwargs):
        """
        NVT-evolve the system for ndt time-steps. **kwargs
        optionally specify the array slices to be propagated
        and thermostatted respectively.

        """
        self.system.nvt_evolve(ndt, **kwargs)

    def run_sample(self, lb, **kwargs):

        slc = kwargs.get('pslc', slice(None))
        self.thermalise(self.nrelax, **kwargs)

        for j in range(lb, 2*self.nstride-1):
            self.system.nve_evolve(self.stride, slc, **kwargs)
            self.record_est(j)

    def run(self, **kwargs):
        """
        Run the simulation to calculate the specified TCFs.
        **kwargs optionally specify the array slices to be propagated
        and thermostatted respectively.

        """

        PB = DisplayBar(self.nsample)
        self.SP('\nRunning '+str(self.nsample)+' samples for tcfs calculations\n', stdout=True)
        self.tcf_arr *= 0
        if self._get_static is True:
            self.static_arr *0

        self.thermalise(self.ntherm, **kwargs)
        for i in range(self.nsample):
            if self._shift_estimators:
                self.shift_est()
                if i == 0:
                    lb = 0
                else:
                    lb = self.nstride-1
            else:
                lb = 0
            PB.Show(i)

            self.run_sample(lb, **kwargs)
            self.add_time_avg()

    def average_static(self):

        try:
            static_avg = self.static_arr.copy()
        except:
            print("Static averages not defined in this instance.")
            return

        if self._shift_estimators:
            norm = self.nsample*(self.nstride+1)-1
        else:
            norm = self.nsample*(2*self.nstride-1)
        static_avg /= norm

        ans = []

        for idces in self._static_idx:
            lo, hi = idces
            if hi-lo == 1:
                ans.append(static_avg[lo])
            else:
                ans.append(static_avg[lo:hi])

        return ans

    def average_tcf(self):

        tcf_avg = np.empty((len(self.A_fxn_list), self.nstride))
        for i, idces in enumerate(self._idx_list):
            lo, hi = idces
            tcf_avg[i,:] = self.tcf_arr[lo:hi,:].mean(axis=0)

        tcf_avg /= self.nsample

        return tcf_avg

    def export_tcf(self, t_arr=None, tcf_avg=None):

        if tcf_avg is None:
            tcf_avg = self.average_tcf()

        if t_arr is None:
            t_arr = np.arange(self.nstride)*self.stride*self.system.propa.dt

        np.savetxt(self.outname, np.c_[t_arr, tcf_avg.T],
                   fmt=self.fmt, delimiter=self.delim)
