# File: nmtrans.py
"""
Created on Fri May 11 12:17:55 2018

@author: gt317

Transformation routines to and from normal mode coordinates. These use the
normalisation convention from the Matsubara paper (DOI: 10.1063/1.4916311)

"""

from __future__ import division, print_function, absolute_import
import numpy as np
from core.molecular_dynamics.utils.rfft import RealFFT

    
class FFTNormalModes(RealFFT):
    """
    An FFT-based class for normal-mode transformations. 
    """
    
    def __init__(self, ndims, nbeads):
        """
        Initialise the transform with the number of beads (nbeads)
        and the number of ring-polymers times the dimensionality
        of the space (ndims).
        """
        
        super(FFTNormalModes, self).__init__((ndims, nbeads), kshape=None)
 
        _norm = [1.0,] + [j for i in range(1,nbeads//2+1) 
                          for j in [1.0, -1.0]]
        
        if nbeads%2 == 0:
            _norm.pop(-1)
            ub = -1
        else:
            ub = None
            
        self._norm = np.array(_norm)
        self._norm[1:ub] *= np.sqrt(2.0)
        self._norm /= nbeads
        
    def cart2mats(self, cart, mats=None):
        
        mats = self.rfft(cart, mats)
        mats *= self._norm
        
        return mats
        
    def mats2cart(self, mats, cart=None, overwrite_mats=False):
        
        mats /= self._norm
        cart = self.irfft(mats, cart)
        
        if not overwrite_mats:
            mats *= self._norm
            
        return cart
        
