# File: nmtrans.py
"""
Created on Thu May 24 17:21:55 2018

@author: gt317

Functions for constructing rotation matrices and converting between different
descriptions of orientation in three-dimensional space.

"""


from __future__ import division, print_function, absolute_import
from scipy import linalg
import numpy as np


def axangle2matrix(axis, angle):
    """
    Return the rotation matrix corresponding to the specified
    rotation axis and angle.
    """

    norm_ax = np.asarray(axis, dtype=float).copy()
    norm_ax /= np.sqrt(np.sum(axis**2, axis=-1))[...,None]

    theta = np.mod(angle, 2*np.pi)
    if isinstance(theta, (int, float)):
        if theta > np.pi:
            theta = 2*np.pi-theta
            norm_ax *= -1
    else:
        bool_arr = np.asarray(theta > np.pi, dtype=bool)
        theta[bool_arr] *= -1
        theta[bool_arr] += 2*np.pi
        norm_ax[bool_arr] *= -1

    cos_angle = np.cos(theta)
    sin_angle = np.sqrt(1.0-cos_angle**2)
    rot_mat = np.eye(3)*cos_angle[...,None,None]

    # Construct the cross-product matrix of the axis
    cross_mat = np.zeros(norm_ax.shape[:-1]+(3,3))
    cross_mat[...,0,1] = -norm_ax[...,2]
    cross_mat[...,1,0] = norm_ax[...,2]

    cross_mat[...,0,2] = norm_ax[...,1]
    cross_mat[...,2,0] = -norm_ax[...,1]

    cross_mat[...,1,2] = -norm_ax[...,0]
    cross_mat[...,2,1] = norm_ax[...,0]

    cross_mat *= sin_angle[...,None,None]
    rot_mat += cross_mat

    # Construct the outer product of the axis
    cross_mat[...] = norm_ax[...,None]*norm_ax[...,None,:]
    cross_mat *= 1.0 - cos_angle[...,None,None]

    rot_mat += cross_mat

    return rot_mat

def quaternion2matrix(qua):
    """
    Return the rotation matrix corresponding to the specified quaternion
    """

    norm_qua = np.asarray(qua, dtype=float).copy()
    norm_qua /= np.sqrt(np.sum(norm_qua**2, axis=-1))[...,None]
    norm_qua_squared = norm_qua**2
    metric = np.array([1, -1, -1, -1])

    rot_mat = np.eye(3)*np.sum(
            metric*norm_qua_squared, axis=-1)[...,None,None]

    # Outer product of vector quaternion components
    cross_mat = norm_qua[...,1:,None]*norm_qua[...,None,1:]
    cross_mat *= 2
    rot_mat += cross_mat

    # Cross-product matrix of vector quaternion components
    cross_mat *= 0
    cross_mat[...,0,1] = -norm_qua[...,3]
    cross_mat[...,1,0] = norm_qua[...,3]

    cross_mat[...,0,2] = norm_qua[...,2]
    cross_mat[...,2,0] = -norm_qua[...,2]

    cross_mat[...,1,2] = -norm_qua[...,1]
    cross_mat[...,2,1] = norm_qua[...,1]

    cross_mat *= norm_qua[...,0,None,None]
    cross_mat *= 2

    rot_mat += cross_mat

    return rot_mat

def quaternion2axangle(qua):
    """
    Convert a rotational quaternion to axis-angle representation.

    """

    norm_qua = np.asarray(qua, dtype=float).copy()
    norm_qua /= np.sqrt(np.sum(norm_qua**2, axis=-1))[...,None]

    angle = np.arccos(norm_qua[...,0])
    angle *= 2
    axis = norm_qua[...,1:]/np.sqrt(1.0 - norm_qua[...,0]**2)[...,None]

    return axis, angle


def eckart_rotation(config, eq, mass):
    """
    Construct the rotation matrix that transforms the input configuration
    so that it satisfies the rotational Eckart condition.

    Input:
        config (2d array) .............. cofiguration array in its CoM;
                                         atom types vary along first dimension,
                                         spatial dimensions along second.
        eq (2d array) .................. equilibrium configuration in its CoM;
                                         same storage convention as in config
        mass (1d array) ................ array of particle masses; size
                                         broadcastable to the first axis
                                         of config.

    Output:
       rot_mat (2d array) .............. the rotation matrix that achieves
                                         the appropriate alignment, so that
                                         np.dot(config, rot_mat) puts the
                                         system into the Eckart frame.

    """

    # Construct the C-matrix (see DOI: 10.1063/1.4870936)
    mass_shape = mass.shape+(eq.ndim-mass.ndim)*(1,)

    r_sum = eq.copy()
    r_sum += config
    r_sum *= np.sqrt(mass).reshape(mass_shape)

    r_diff = eq.copy()
    r_diff -= config
    r_diff *= np.sqrt(mass).reshape(mass_shape)

    outer_prod = r_diff[...,None]*r_diff[...,None,:]
    c_matrix = np.zeros((4,4))
    c_matrix[1:,1:] += outer_prod.sum(axis=tuple(range(eq.ndim-1)))
    c_matrix[0,0] = np.diagonal(outer_prod, axis1=-2, axis2=-1).sum()

    outer_prod[...] = r_sum[...,None]*r_sum[...,None,:]
    c_matrix[1:,1:] += np.eye(3,3) * \
            np.diagonal(outer_prod, axis1=-2, axis2=-1).sum()
    c_matrix[1:,1:] -= outer_prod.sum(axis=tuple(range(eq.ndim-1)))

    c_matrix[0,1:] = np.cross(r_sum, r_diff,
        axis=-1).sum(axis=tuple(range(eq.ndim-1)))
    c_matrix[1:,0] = c_matrix[...,0,1:]

    # Diagonalise to find the lowest eigenvalue-eigenvector pair
    w, qua = linalg.eigh(c_matrix, eigvals=(0,0), overwrite_a=True)

    # Generate the rotation matrix
    rot_mat = quaternion2matrix(qua.flatten())

    return rot_mat.T