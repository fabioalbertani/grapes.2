#!/usr/bin/env python3
"""
Created on Sat Jun  2 15:33:19 2018

@author: gt317

Discrete variable representation approach to solving the Schroedinger
equation in low-dimensional systems.

"""

from __future__ import division, print_function
import numpy as np
from scipy import linalg, sparse
from scipy.sparse.linalg import eigsh
from scipy.integrate import romb
from scipy.interpolate import InterpolatedUnivariateSpline as interp1d
import sys
import os
sys.path.insert(0,os.environ['QCLPYDIR'])
from core.molecular_dynamics.utils import calculus


#TODO: add calculation of "standard" TCFs

class DVR1D(object):
    """
    Solve the one-dimensional Schroedinger equation using discrete variable
    representation due to Miller et al. (10.1063/1.462100) and calculate
    the associated matrix elements.

    """


    ndim = 1

    def __init__(self, potential, mass, lb, ub, npts,
                 neig, V_c, ratio, pad=4):
        """
        Calculate the numerical solutions to the Schroedinger equation.

        Input:
            potential (object) ............... the potential energy surface
            mass (scalar) .................... mass of the particle
            lb, ub (scalar) .................. lower and upper coordinate
                                               grid bounds
            npts (int) ....................... number of points on the grid
            neig (int) ....................... maximum number of eigenvalues
                                               to be calculated
            V_c (scalar) ..................... energy cut-off used in the
                                               construction of the kinetic
                                               energy matrix.
            ratio (scalar) ................... ratio of the highest retained
                                               energy eigenvalue to V_c
            pad (int, optional) .............. by what amount to pad the
                                               spatial represenation on each
                                               side when converting to momentum
                                               space (default 4)

        Return:
            A DVR1D object with DVR1D.evals and DVR1D.evecs storing the
            energy eigenvalues and eigenvectors respectively.
            NOTE: DVR1D.evecs[n,:] corresponds to DVR1D.evals[n]
            Cubic spline interpolation is used on all of the eigenvectors
            to bulk up the number of grid points to the nearest 2**n+1

        """

        self.m = mass
        self.V_c = V_c
        self.xvec, hh = np.linspace(lb, ub, npts, retstep=True)
        self.qgrid_list = [self.xvec]
        self.potgrid = potential(self.xvec)
        HH = self.ke_matrix(npts, hh, self.m)
        # Discard points in enegetically inaccessible regions
        bool_arr = self.exceeds_cutoff(self.potgrid, V_c)
        HH[bool_arr] = 0.0
        HH += np.diag(self.potgrid)
        self.evals, self._evecs = \
            self.diagonalise(HH, neig, V_c/ratio)

        self._process_solns(lb, ub, npts, pad, self._evecs)
        self.xvec, = self.qgrid_list
        self.pxvec, = self.pgrid_list

    def _process_solns(self, lb, ub, npts, pad, init_evecs):
        """
        Normalise the output of matrix diagonalisation and construct
        wavefunctions in momentum-space representation.

        """

        ndim_ones = np.ones(self.ndim, dtype=float)
        lb_, ub_ = [ndim_ones*np.asarray(arg)
                    for arg in (lb, ub)]
        ndim_ones = np.ones(self.ndim, dtype=int)
        npts_, pad_ = [ndim_ones*np.asarray(arg, dtype=int)
                    for arg in (npts, pad)]

        # Calculate the eigenfunctions in momentum representation
        self.pevecs, self.pgrid_list  = \
            self.momentum_rep(init_evecs, lb_, ub_, npts_, pad_)
        # Normalise the wavefunctions
        self.evecs = init_evecs.astype(np.complex)
        self.evecs, self.qgrid_list = self.normalise(
                                                   self.evecs,
                                                   *self.qgrid_list
                                                   )

        self.pevecs, self.pgrid_list = self.normalise(
                                                    self.pevecs,
                                                    *self.pgrid_list
                                                    )

        self.hh_list = []
        self.npts_list = []

        for vec in self.qgrid_list:
            self.npts_list.append(len(vec))
            self.hh_list.append((vec.max()-vec.min())/(len(vec)-1))

        self.phh_list = []
        for vec in self.pgrid_list:
            self.phh_list.append((vec.max()-vec.min())/(len(vec)-1))


    @staticmethod
    def exceeds_cutoff(pot, V_c):
        """
        Determine which points on the grid exceed the cut-off energy
        V_c and return a boolean array with True entries corresponding
        to such points.

        """

        bool_arr = pot > V_c
        bool_i, bool_j = np.meshgrid(bool_arr, bool_arr)
        return np.logical_or(bool_i, bool_j)

    @staticmethod
    def normalise(psi, *grid_args):
        """
        Normalise a function psi defined on a grid. The function assumes
        that the first dimension of psi runs over the eigenvalues,
        and is therefore excluded from indexing.

        Input:
            psi (array) ................. array of wavevectors
            *grid_args .................. lists of knots along each
                                          principal coordinate direction;
                                          the integration grid is selected
                                          by using grid_args[axes]

        Return:
            arr (array) ................ normalised wavevectors
            grids (list) ............... list of expanded grid-point vectors
        """

        if psi.ndim-1 != len(grid_args):
            raise ValueError("""
The normalise method expects an array psi as input, such that the first
axis varies over the eigenvalues, and the remaining axes correspond to
the Cartesian grid vectors.
            """)

        ax_arr = np.arange(psi.ndim-1)[::-1]
        grids = list(grid_args)
        arr = psi
        hh_arr = []

        for ax in ax_arr:
            shape_arr = list(arr.shape)
            # Pad axis 'ax' to a power of two + 1
            npts = np.power(2, int(np.ceil(np.log2(shape_arr[ax+1]))))+1
            # Get the corresponding grid bounds
            grid = grids.pop(ax)
            lo = grid.min()
            hi = grid.max()
            # Create a finer coordinate grid
            fine_grid, hh = np.linspace(lo, hi, npts, retstep=True)
            if ax == -1 or ax == len(grid_args)-1:
                grids.append(fine_grid)
            else:
                grids.insert(ax, fine_grid)
            hh_arr.append(hh)

            # Create a finer grid of wavefunction vaules
            shape_arr[ax+1] = npts
            fine_arr = np.zeros(shape_arr, dtype=np.complex128)
            # Get the indices for a wavefunction array with the dimension 'ax'
            # removed
            shape_arr.pop(ax+1)
            try:
                ind = np.reshape(np.indices(shape_arr[1:]),
                                 (len(shape_arr[1:]), -1)).T
            except:
                ind = np.array([[None,]])

            # Cycle over eigenvectors:
            for i,subarr in enumerate(arr):
                # Cycle over "complementary dimensions":
                for indices in ind:
                    idx_list = indices.tolist()
                    if (subarr.ndim==1):
                        idx_list = [slice(None)]
                    elif ax == -1 or ax == len(grid_args)-1:
                        idx_list.append(slice(None))
                    else:
                        idx_list.insert(ax, slice(None))
                    re_spline = interp1d(grid, np.real(subarr[idx_list]))
                    im_spline = interp1d(grid, np.imag(subarr[idx_list]))
                    fine_arr[[i]+idx_list] += im_spline(fine_grid)
                    fine_arr[[i]+idx_list] *= 1j
                    fine_arr[[i]+idx_list] += re_spline(fine_grid)
            # Point arr to the bulked-up array
            arr = fine_arr

        # Normalise the wavefunctions
        psi_sq = np.empty(arr.shape[1:], dtype=np.complex128)
        ax_arr = ax_arr.tolist()
        for subarr in arr:
            psi_sq[...] = subarr.copy()
            psi_sq *= np.conj(psi_sq)
            N = np.real(psi_sq)
            for hh, ax in zip(hh_arr, ax_arr):
                N = romb(N, dx=hh, axis=ax)
            subarr /= np.sqrt(N)

        return arr, grids

    @staticmethod
    def ke_matrix(npts, hh, m):
        """
        Calculate the kinetic energy matrix for a coordinate grid
        with npts points and a step of hh.

        """

        TT = np.identity(npts)
        TT *= np.pi**2 / 3
        indices = np.arange(npts, dtype=float)
        idx_grid = indices[:,None] - indices[None,:]
        # Add the identity matrix to the grid to avoid division by zero
        idx_grid += np.identity(npts)
        idx_grid  = (-1)**idx_grid/idx_grid**2
        idx_grid += np.identity(npts)
        idx_grid *= 2
        TT += idx_grid
        TT /= 2 * hh**2 * m

        return TT

    @staticmethod
    def diagonalise(HH, neig, E_max):
        """
        Diagonalise the Hamiltonian matrix and return normalised
        eigenvectors and the associated eigenvalues.

        """

        evals, evecs = linalg.eigh(HH, overwrite_a=True,
                                   eigvals=(0,neig-1))
        evecs = evecs.T
        bool_arr = evals < E_max
        evals = evals[bool_arr]
        evecs = evecs[bool_arr]
        return evals, evecs

    @classmethod
    def momentum_rep(cls, evecs, lb, ub, npts, pad):
        """
        Return the momentum representation of eigenvectors "evecs" supplied
        in position representation.

        """

        pgrid_list = []
        pevecs = evecs
        ft = calculus.FFTFourierIntegral(lb, ub, npts, pad)
        for i in range(cls.ndim):
            ax = -cls.ndim+i
            slc_list = pevecs.ndim*[slice(None)]
            slc_list[ax] = slice(None,None,-1)
            pevecs = ft(pevecs, axes=ax)[slc_list]
            pgrid_list.append(-2*np.pi*ft.freqs[ax][::-1])

        return pevecs, pgrid_list

    def matrix_elements(self, fxn, mode='q', eigmax=None):
        """
        Calculate the matrix elements of an operator O = fxn(X) or fxn(P)
        in the basis of DVR eigenfunctions.

        Input:
            fxn(object) .................. a function that expresses the
                                           relevant property in terms of
                                           position.
            mode (str) ................... 'q' for position and
                                           'p' for momentum
			  eigmax (int, default all) .... index of the highest eigenstate
			                                 to be used in the calculations
        Return:
            me (array) ................... matrix elements of the operator
                                           in the DVR eigenbasis.

        """

        if mode.lower() == 'q':
            psi = self.evecs
            grid_list = self.qgrid_list
            hh_list = self.hh_list.copy()
        elif mode.lower() == 'p':
            psi = self.pevecs
            grid_list = self.pgrid_list
            hh_list = self.phh_list.copy()
        else:
            raise ValueError("""
Invalid operator mode specified for matrix element evaluation in DVR.
            """)

        ub = eigmax
        me = np.empty(2*(self.evals[:ub].size,), dtype=np.complex128)
        fgrid = fxn(*grid_list)
        for i in range(len(self.evals[:ub])):
            bra_f = fgrid.astype(np.complex128)
            bra_f *= np.conj(psi[i])
            for j in range(len(self.evals[:ub])):
                bra_f_ket = bra_f.copy()
                bra_f_ket *= psi[j]
                hh_copy = hh_list.copy()
                for ax in range(-1, -1-self.ndim, -1):
                    hh = hh_copy.pop()
                    bra_f_ket = romb(bra_f_ket, dx=hh)
                me[i,j] = bra_f_ket

        return me


    def kubo_tcf(self, A, B, betas, t_arr, modA='q', modB='q', eigmax=None):
        """
        Return the Kubo-transformed time-correlation function
            Tr[A_beta B(t)],
            A_beta = 1/beta Integrate[e^{-y H} A e^{-(beta-y)H}, {y, 0, beta}]

        Input:
            A,B (object) ................... functions to evaluate the matrix
                                             elements
            betas (array) .................. 1/kB T
            t_arr (array) .................. array of times for which to
                                             calculate the TCF
            modA, modB (str, optional) ..... whether the operator is a function
                                             of position ('q') or momentum ('p')
            eigmax (int, default ) ......... index of the highest eigenstate to
			                                   be used in a calculation

        Return:
            tcf_AB, me_A, me_B ............. matrix elements of operators A and
                                             B, and the Kubo-transformed TCF


        """

        me_A = self.matrix_elements(A, mode=modA, eigmax=eigmax)
        if B is A:
            me_B = me_A
        else:
            me_B = self.matrix_elements(B, mode=modB, eigmax=eigmax)
        tcf_AB = self.me2kubo(t_arr, betas, self.evals[:eigmax], me_A, me_B)

        return tcf_AB, me_A, me_B

    @staticmethod
    def me2kubo(t_arr, betas, evals, me_A, me_B=None):
        """
        Given the matrix elements for the relevant operators,
        calculate the Kubo-transformed TCF.

        Input:
            t_arr(array) ................ array of times for which to
                                          calculate the TCF
            betas(array)  ............... array of 1/kB T __"__"__
            evals(array) ................ array of energy eigenvalues
            me_A(array) ................. matrix elements of A
            me_B(array, optional) ....... matrix elements of B (if none,
                                          calculate the auto-correlation
                                          function of A).

        Return:
            tcf_AB(list)................. time-correlation function
        """

        beta_arr = np.asarray(betas).reshape(-1)
        tcf_AB = []
        if me_B is None:
            me_B = me_A
        for beta in beta_arr:
            e_bE = np.exp(-beta*evals)
            Z = e_bE.sum()
            bE_nm = evals[:,None] - evals[None,:]
            w_nm = bE_nm.copy()
            bE_nm *= beta
            # Construct the array [exp(beta * E_nm) - 1.0]/[beta * E_nm]
            eps = 1e-08
            bool_arr = np.abs(bE_nm) > eps
            temp = np.where(bool_arr, bE_nm, 1.0)
            arr = np.where(bool_arr,
                           (np.exp(temp)-1.0)/temp,
                           1.0+0.5*bE_nm).astype(np.complex)
            arr *= me_A
            arr *= np.conj(me_B)
            arr *= e_bE[:,None]
            arr = np.real(arr)
            tcf = np.sum(np.cos(w_nm*t_arr[:,None,None])*arr, axis=(-1,-2))
#            tcf = np.empty_like(t_arr)
#            for i,t in enumerate(t_arr):
#                tcf[i] = np.real(np.sum(arr*np.cos(w_nm*t)))
            tcf /= Z
            tcf_AB.append(tcf)

        return tcf_AB


class DVR2D(DVR1D):

    ndim = 2

    def __init__(self, potential, mass, lb, ub, npts,
                 neig, V_c, ratio, pad=4):
        """
        Calculate the numerical solutions to the Schroedinger equation.

        Input:
            potential (object) ............... the potential energy surface
            mass (scalar) .................... mass of the particle
            lb, ub (list) .................... lower and upper coordinate
                                               grid bounds
            npts (list) ...................... number of points on the grid
            neig (int) ....................... maximum number of eigenvalues
                                               to be calculated
            V_c (scalar) ..................... energy cut-off used in the
                                               construction of the kinetic
                                               energy matrix.
            ratio (scalar) ................... ratio of the highest retained
                                               energy eigenvalue to V_c
            pad (list, optional) ............. by what amount to pad the
                                               spatial represenation on each
                                               side when converting to momentum
                                               space (default 4)

        Return:
            A DVR2D object with DVR2D.evals and DVR2D.evecs storing the
            energy eigenvalues and eigenvectors respectively.
            NOTE: DVR1D.evecs[n,:] corresponds to DVR1D.evals[n]
            Cubic spline interpolation is used on all of the eigenvectors
            to bulk up the number of grid points to the nearest 2**n+1

        """

        self.m = mass
        self.V_c = V_c
        if isinstance(npts, list):
            try:
                xpts, ypts = npts
            except:
                xpts = npts[0]
                ypts = xpts
        else:
            xpts = npts
            ypts = xpts

        (self.xvec, xhh), (self.yvec, yhh) = [
             np.linspace(lb[i], ub[i], npts[i], retstep=True)
             for i in range(2)
             ]
        self.qgrid_list = [self.xvec, self.yvec]
        self.potgrid = potential(*self.qgrid_list)
        HH = self.potgrid.flatten()

        # Initialise the list of indices with non-zero entries,
        # starting with the diagonals
        HH_row = np.arange(len(HH))
        HH_col = HH_row.copy()

        # Construct the kinetic part of the Hamiltonian matrix
        for i, (pts, hh) in enumerate(zip([xpts, ypts],
                                          [xhh, yhh])):
            TT_sub = self.ke_matrix(pts, hh, self.m)
            TT, row, col = self.ke_scan(TT_sub, self.potgrid, V_c, i)
            HH = np.append(HH, TT)
            HH_row = np.append(HH_row, row)
            HH_col = np.append(HH_col, col)

        xypts = xpts*ypts
        HH = sparse.csr_matrix((HH,(HH_row,HH_col)),shape=(xypts, xypts))

        E_max = V_c/ratio
        self.evals, self._evecs = self.diagonalise(HH, neig, E_max)
        self._evecs.shape = (-1, xpts, ypts)
        self._process_solns(lb, ub, npts, pad, self._evecs)
        self.xvec, self.yvec = self.qgrid_list
        self.pxvec, self.pyvec = self.pgrid_list

    @staticmethod
    def ke_scan(TT_sub, potgrid, V_c, axis):
        """
        Given the kinetic submatrix TT_sub for coordinate `axis'
        (0 is x, 1 is y), construct the corresponding non-zero
        contribution to the total Hamiltonian
             H_{ik,jl} = (TT_sub)_{i,j} * dirac_delta{k,l}

        Input:
            TT_sub ................... a square KE submatrix
            pot_grid ................. a grid of potential energies
            V_c ...................... the cut-off energy
            axis ..................... index of the Cartesian coordinate

        Return:
            TT, row, column .......... 1d arrays with non-zero KE matrix
                                       elements and the corresponding row
                                       and column indices



        """

        slc_list = [slice(None), slice(None)]
        xpts, ypts = potgrid.shape
        # Construct the array of indices with the convention that
        # idx%ypts gives the index of the y coordinate along yvec
        # and idx//(ypts) gives the index of the x coordinate along xvec
        if axis == 0:
            ik, jl = np.mgrid[0:ypts*xpts:ypts,
                              0:ypts*xpts:ypts]
            ubound = ypts
            stride = 1
            c_ax = 1 # Complementary axis
        elif axis == 1:
            ik, jl = np.mgrid[0:ypts,
                              0:ypts]
            ubound = ypts*xpts
            stride = ypts
            c_ax = 0
        else:
            raise ValueError("Invalid choice of axis, can only be 0 or 1")

        ik, jl = [arr.flatten() for arr in (ik, jl)]
        TT_flat = TT_sub.reshape(-1)

        # Start at the lower end of the complementary axis excluding
        # parts of the kinetic energy where the potential exceeds the
        # specified cut-off
        row = np.empty(ik.size*ubound//stride, dtype=np.int)
        col = np.empty_like(row, dtype=np.int)
        TT = np.empty_like(row, dtype=np.float)
        lo = 0

        for i, offset in enumerate(np.arange(0, ubound, stride)):
            slc_list[c_ax] = i
            bool_arr = DVR2D.exceeds_cutoff(potgrid[slc_list], V_c)
            bool_arr = np.logical_not(bool_arr)
            temp = ik[bool_arr]
            hi = len(temp)
            row[lo:lo+hi] = temp
            col[lo:lo+hi] = jl[bool_arr]
            TT[lo:lo+hi] = TT_flat[bool_arr]
            lo += hi
            ik += stride
            jl += stride

        TT, row, col = [arr[:lo] for arr in (TT, row, col)]
        return TT, row, col


    @staticmethod
    def exceeds_cutoff(pot, V_c):
        """
        Determine which points on the grid exceed the cut-off energy
        V_c and return a flattened boolean array with True entries
        corresponding to such points.

        """

        return DVR1D.exceeds_cutoff(pot, V_c).flatten()

    @staticmethod
    def diagonalise(HH, neig, E_max):
        """
        Diagonalise the Hamiltonian matrix and return normalised
        eigenvectors and the associated eigenvalues.

        """

        evals, evecs = eigsh(HH, k=neig, which='SA')
        evecs = evecs.T
        bool_arr = evals < E_max
        evals = evals[bool_arr]
        evecs = evecs[bool_arr]
        return evals, evecs


if __name__ == '__main__':

    import argparse
    import pickle
    from qclpysim.utils.constants import mass, conversion
    from qclpysim.potentials import poly, champ

    parser = argparse.ArgumentParser(
        description=
        """
        Solve the Schroedinger equation in one or two dimensions
        using DVR and return the position, square position,
        velocity and square velocity time auto-correlation functions.
        All input assumed to be in atomic units except for time,
        which may be supplied in fs. In the latter case, TCFs are
        converted to units of fs, angstrom, angstrom per ps and
        derivatives thereof.
        """)


    parser.add_argument("-M", "--mass", type=str,
                        help="""
                        Particle mass or a valid keyword from the
                        mass dictionary in qclpysim.utils.constants
                        """)

    parser.add_argument("--ndims", type=int, choices=[1,2],
                        help="""
                        Number of spatial dimensions.
                        """)

    parser.add_argument("--ptype", type=str,
                        choices=['poly', 'champ'],
                        help = 'Type of the potential energy surface.')

    parser.add_argument("--pname", type=str,
                        help ="""
                        A valid class name for a potential from either
                        the poly of the cham module, as defined in
                        qclpysim.potentials
                        """)

    parser.add_argument("-V", "--cutoff", type=float,
                        help="""Cut-off potential for constructing the kinetic
                        energy matrix in the DVR basis."""
                        )

    parser.add_argument("-l", "--lim", type=float,
                        help="""
                        Upper bound for the square grid on which
                        the calculation is to be performed.
                        """)

    parser.add_argument("-n", "--npts", type=int,
                        help=""""
                        Number of points in the grid along each direction.
                        """)

    parser.add_argument("-N", "--neig", type=int,
                        help= """
                        The maximum number of solutions to be calculated
                        (may be limited by the cutoff potential supplied.
                        """)

    parser.add_argument("-t", "--tmax", type=float,
                        help="Time range of the TCFs.")

    parser.add_argument("--tpts", type=int,
                        help="Number of data points in the TCFs.")

    parser.add_argument("--fs", action="store_true",
                        help="""
                        Indicates the timeis supplied in fs.
                        """)

    parser.add_argument("-T", nargs='+', type=float,
                        help="""
                        A range of temperatures; if --fs is flagged up,
                        assumed to be in K.
                        """)


    args = parser.parse_args()
    pes_str = args.ptype+'.'+args.pname+'(ndim={:d}, mode="cl")'.format(
        args.ndims)
    pes = eval(pes_str)
    grid = args.ndims*(np.linspace(-args.lim, args.lim, args.npts),)
    try:
        m = float(args.mass)
    except:
        m = mass[args.mass]

    if args.ndims == 1:
        def dvr_pes(x):
            return pes.potential(x)
        DVR = DVR1D
    elif args.ndims == 2:
        def dvr_pes(x, y):
            xarr = np.ones_like(x[:,None]+y)*x[:,None]
            yarr = np.ones_like(xarr)*y
            coords = np.vstack((xarr.flatten(),
                                yarr.flatten())).reshape(-1, order='F')
            return pes.potential(coords).reshape(xarr.shape)
        DVR = DVR2D
    else:
        raise NotImplementedError

    psi = DVR(dvr_pes, m, args.ndims*[-args.lim], args.ndims*[args.lim],
              args.ndims*[args.npts], args.neig, args.cutoff, 3)
    neig = len(psi.evals)
    mask = '{:03d}PT_{:03d}N'.format(args.npts, neig)
    file = open(mask+'.obj', 'wb')
    pickle.dump(psi, file)
    file.close()

    t_arr = np.linspace(0, args.tmax, args.tpts)
    T_arr = np.asarray(args.T)
    if args.fs:
        betas = 1/(conversion['K2Eh']*T_arr)
        t_arr *= conversion['fs2au']
    else:
        betas = 1/T_arr

    for i, (beta, T) in enumerate(zip(betas, T_arr)):
        if args.ndims == 1:
            x_fxn = lambda x: x.copy()
            x2_fxn = lambda x: x**2

            if i == 0:
                fxn_list = 2*[x_fxn, x2_fxn]
                mode_list = 2*['q',]+2*['p']
                me_list = [psi.matrix_elements(fxn, mode=mode)
                           for fxn, mode in zip(fxn_list, mode_list)]

            (cqq, cq2q2, cvv, cv2v2) = [np.asarray(
                                        psi.me2kubo(t_arr, beta,
                                                    psi.evals, me)
                                        ).flatten() for me in
                                                    me_list]
        elif args.ndims == 2:
            x_fxn = lambda x, y: np.ones_like(x[:,None]+y)*x[:,None]
            y_fxn = lambda x, y: np.ones_like(x[:,None]+y)*y
            x2_fxn = lambda x, y: x_fxn(x, y)**2
            y2_fxn = lambda x, y: y_fxn(x, y)**2
            if i == 0:
                fxn_list = 2*[x_fxn, y_fxn, x2_fxn, y2_fxn]
                mode_list = 4*['q',]+4*['p']
                me_list = [psi.matrix_elements(fxn, mode=mode)
                           for fxn, mode in zip(fxn_list, mode_list)]

            tcf_list = [
                        np.asarray(psi.me2kubo(t_arr, beta, psi.evals, me)
                        ).flatten() for me in me_list]

            cqq, cq2q2, cvv, cv2v2 = [x+y for x,y in zip(
                                      tcf_list[0::2], tcf_list[1::2])]

        else:
            raise NotImplementedError

        cvv /= m**2
        cv2v2 /= m**4

        if args.fs:
            beta = T
            tstring = 'K'
            t, cqq, cq2q2, cvv, cv2v2 = [arr/conv for arr, conv in zip(
                [t_arr, cqq, cq2q2, cvv, cv2v2],
                [conversion['fs2au'],
                 conversion['AA2au']**2,
                 conversion['AA2au']**4,
                 conversion['AA_ps2au']**2,
                 conversion['AA_ps2au']**4])]
        else:
            tstring = 'B'
            t = t_arr

        outname = '{:03d}{:s}_'.format(int(beta), tstring)+mask+'_tcfs.dat'
        outarr = np.c_[t, cqq, cq2q2, cvv, cv2v2]
        np.savetxt(outname, outarr, fmt='%23.16e', delimiter='  ')
