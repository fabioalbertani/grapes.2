# File: tcfs.py

"""
Created on Thu May 17 14:40:25 2018

@author: gt317

Tools for calculating time-correlation functions.

"""

from __future__ import print_function, division, absolute_import
import numpy as np
from core.molecular_dynamics.utils.rfft import RealFFT


class FFTCorrelationFxn(RealFFT):
    """
    Fast calculation of correlation between two data sets using FFT.
    
    """
    
    def __init__(self, shape):
        """
        Initialise class with the shape of the correlation array.
        
        """
        
        # Store the number of data points along the last axis
        self.ndata = shape[-1]     
        
        # Pad the last axis
        new_shape = shape[:-1]+(self.ndata*3,)
        self._arr1 = np.zeros(new_shape)
        self._arr2 = np.zeros(new_shape)
        # Determine the type and shape of FT arrays later
        self._ft_arr1 = None
        self._ft_arr2 = None
        
        super(FFTCorrelationFxn, self).__init__(new_shape)
        
        
    def __call__(self, arr1, arr2):
        """
        Calculate the correlation of two array
            *\ ans[i] = SUM(arr1[j]*arr2[j+i], j=range(0,N))/N *\
            
        """
        # Pad out input with zeros
        self._arr1 *= 0
        self._arr1[...,:self.ndata] = arr1[...,:self.ndata] #up to nstride
        self._arr2 *= 0
        self._arr2[...,:2*self.ndata-1] = arr2[...,:2*self.ndata-1] #
        self._ft_arr1 = self.crfft(self._arr1, self._ft_arr1)
        self._ft_arr2 = self.crfft(self._arr2, self._ft_arr2)

        # Calculate the product of the two arrays in Fourier space
        np.conj(self._ft_arr1, self._ft_arr1)
        self._ft_arr2 *= self._ft_arr1
        
        out = self.cirfft(self._ft_arr2)
        out /= self.ndata
        
        return out[...,:self.ndata]
        
