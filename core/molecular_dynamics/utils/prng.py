# File: prng.py
"""
Created on Wed May  9 12:03:09 2018

@author: gt317

A wrapper for the NumPy random generator
"""

from __future__ import division, print_function
import numpy as np

class PRNG(object):
    
    def __init__(self, idum=None, state=None):
        """
        Initialise the random-number generator.
        
        Input:
            idum (int of 1D array) ......... seed for the random number
                                             generator (optional)
            state (tuple) .................. state of the random number
                                             generator - overrides default
                                             initialisation if present
        """
        
        self.generator = None
        self.seed = idum
        
        if state is not None:
            self.state = state
            
    @property
    def seed(self):
        return self.__seed
        
    @seed.setter
    def seed(self, idum):
        self.__seed = idum
        self.generator = np.random.RandomState(idum)

    @property
    def state(self):
        return self.generator.get_state()
        
    @state.setter
    def state(self, state):
        self.generator.set_state(state)
            
    def urand(self, **kwargs):
        """
        Call the uniform random number generator with the following
        optional arguments:
        low (float or array) ............. lower bound
        high (__"__"__) .................. upper bound
        size (int or tuple) .............. output size

        Output:
            u ............................ float or array of floats,
                                           drawn from the uniform random
                                           distribution
        """
        
        return self.generator.uniform(**kwargs)
    
    def normal(self, **kwargs):
        """
        Call the Gaussian random number generator with the following
        optional arguments:
        loc (float or array) ............. mean of the distribution
        scale (__"__"__) ................. standard deviation
        size (int or tuple) .............. output size

        Output:
            g ............................ float or array of floats,
                                           drawn from the uniform random
                                           distribution
        """
        
        return self.generator.normal(**kwargs)
        