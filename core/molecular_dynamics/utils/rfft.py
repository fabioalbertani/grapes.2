# -*- coding: utf-8 -*-
"""
Created on Thu May 17 15:00:23 2018

@author: gt317

FFT for real input data using the FFTW algorithm whenever possible.

"""

from __future__ import division, print_function, absolute_import
import numpy as np

__all__ = ['RealFFT']

try:
    import pyfftw

    class RealFFT(object):
        """
        A wrapper for a real Fourier Transforms.

        """

        def __init__(self, rshape, kshape=None):
            """
            Input:
                rshape ........................ shape of the real-space array
                kshape ........................ shape of the Fourier-space
                                                array

            """

            if kshape is None:
                kshape = rshape

            # Input to rfft
            self._rfft_arr = pyfftw.empty_aligned(rshape, dtype='float64')

            # Input to irfft
            try:
                ishape = kshape[:-1]+(kshape[-1]//2+1,)
            except TypeError:
                ishape = (kshape//2+1,)
            except:
                raise
            self._m = ishape[-1]

            try:
                self._n = rshape[-1]
            except TypeError:
                self._n = rshape
            except:
                raise

            self._irfft_arr = pyfftw.empty_aligned(ishape, dtype='complex128')

            # FFTW objects
            self._crfft = pyfftw.builders.rfft(self._rfft_arr)
            self._cirfft = pyfftw.builders.irfft(self._irfft_arr, n=self._n)

        def _rfft2real(self, out=None):
            """
            Convert the complex output of rfft to a an array of reals.
            """

            fft_real = self._irfft_arr.view(np.float64)
            if out is None:
                out = np.empty(fft_real.shape[:-1] +
                               (fft_real.shape[-1]-2+self._n%2,))

            out[...,0] = fft_real[...,0]
            if self._n%2 == 0:
                ub = -1
            else:
                ub = None
            out[...,1:] = fft_real[...,2:ub]

            return out

        def _real2irfft(self, arr):
            """
            Setup the complex input to the inverse FFT
            """

            fft_real = self._irfft_arr.view(dtype=np.float64)
            fft_real[...,0] = arr[...,0]
            fft_real[...,1] = 0.0

            if self._n%2 == 0:
                fft_real[...,-1] = 0.0
                ub = -1
            else:
                ub = None

            fft_real[...,2:ub] = arr[...,1:]


        def crfft(self, in_arr, out_arr=None):
            """
            Calculate the Fourier transform of a real real dataset with
            complex output. If the number of elements in the r-space
            is greater than in k-space, the output is truncated

            """

            self._rfft_arr[...] = in_arr
            self._irfft_arr[...] = self._crfft()[...,:self._m]
            if out_arr is None:
                out_arr = self._irfft_arr.copy()
            else:
                out_arr[...] = self._irfft_arr

            return out_arr

        def cirfft(self, in_arr, out_arr=None):
            """
            Calculate the inverse Fourier transform from real input.

            """

            self._irfft_arr[...] = in_arr
            self._rfft_arr[...] = self._cirfft()
            if out_arr is None:
                out_arr = self._rfft_arr.copy()
            else:
                out_arr[...] = self._rfft_arr

            return out_arr

        def rfft(self, in_arr, out_arr=None):
            """
            Calculate the Fourier transform of a real real dataset with
            real output.

            """

            self._rfft_arr[...] = in_arr
            self._irfft_arr[...] = self._crfft()
            out_arr = self._rfft2real(out_arr)

            return out_arr

        def irfft(self, in_arr, out_arr=None):
            """
            Calculate the inverse Fourier transform from real input.

            """

            self._real2irfft(in_arr)
            self._rfft_arr[...] = self._cirfft()
            if out_arr is None:
                out_arr = self._rfft_arr.copy()
            else:
                out_arr[...] = self._rfft_arr

            return out_arr

except ImportError:
    from scipy import fftpack
    from numpy import fft
    from functools import wraps

    def specify_out(fxn):
        @wraps(fxn)
        def wrapper(self, in_arr, out_arr=None):
            if out_arr is None:
                return fxn(self, in_arr)
            else:
                out_arr[...] = fxn(self, in_arr)
                return out_arr
        return wrapper

    class RealFFT(object):
        """
        A wrapper for a real Fourier Transforms.

        """

        def __init__(self, rshape, kshape=None):
            """
            Input:  shape (int or tuple) - shape of input arrays.

            """

            if kshape is None:
                kshape = rshape

            try:
                self._n = rshape[-1]
            except TypeError:
                self._n = rshape
            except:
                raise

            try:
                self._im = kshape[-1]//2+1
                self._m = kshape[-1]
            except TypeError:
                self._im = kshape//2+1
                self._m = kshape
            except:
                raise

        @specify_out
        def crfft(self, in_arr):
            """
            Calculate the Fourier transform of a real dataset with
            complex output, truncated if the specified number of points
            is smaller in Fourier space

            """
            return fft.rfft(in_arr)[...,:self._im]

        @specify_out
        def cirfft(self, in_arr):
            """
            Calculate the inverse Fourier transform from complex input.
            The input is padded with zeros if the number of points in
            r-space exceeds that in k-space

            """
            return fft.irfft(in_arr, n=self._n)

        @specify_out
        def rfft(self, in_arr):
            """
            Calculate the Fourier transform of a real real dataset with
            real output. Output is truncated if the number of points
            is smaller in Fourier space

            """
            return fftpack.rfft(in_arr)[...,:self._m]

        @specify_out
        def irfft(self, in_arr):
            """
            Calculate the inverse Fourier transform from real input.
            Input array is padded with zeros if the number of points
            is greater in r-space

            """
            return fftpack.irfft(in_arr, n=self._n)

except:
    raise
