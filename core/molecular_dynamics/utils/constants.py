# File: constants.py
"""
Created on Mon May 14 16:14:19 2018

@author: gt317

Flags and constants used by the qclpysim package.

"""

import math

#---------------- Physical constants in atomic units ----------------#
physical_constants = dict()


physical_constants['kB'] = 1.0
physical_constants['hbar'] = 1.0
physical_constants['me'] = 1.0
physical_constants['mp'] = 1836.15267376
# Unified atomic mass unit (Dalton)
physical_constants['Da'] = 1822.888486192
# Speed of light in vaccum
physical_constants['c'] = 1.0/137.035999139
physical_constants['c_SI'] = 299792458.0


#------------------ Atomic masses in atomic units ------------------#
mass = dict()

mass['1H'] = 1836.152697
mass['16O'] = 29148.9464
mass['H'] = 1837.36223
mass['O'] = 29165.1223
mass['mu_OH'] = 1741.05198

#---------------------- Conversion factors -------------------------#
conversion = dict()

conversion['s2au'] = 4.134137333661264e16
conversion['ps2au'] = conversion['s2au'] * 1.0e-12
conversion['fs2au'] = conversion['s2au'] * 1.0e-15
conversion['AA2au'] = 1.889726124565062
conversion['m2au'] = conversion['AA2au'] * 1.0e+10
conversion['AA_ps2au'] = (conversion['AA2au'] / conversion['ps2au'] )
conversion['AA_fs2au'] = (conversion['AA2au'] / conversion['fs2au'] )
conversion['K2Eh'] = 3.1668114e-6
conversion['kcalmol2Eh'] = 1/627.509474
conversion['kg2au'] = 1/9.10938356e-031
conversion['deg2rad'] = math.pi/180
conversion['au2wn'] = conversion['s2au']/(100*physical_constants['c_SI'])