#!/usr/bin/env python3

"""
Created on Mon Jun 11 16:26:35 2018

@author: gt317

Utilities for calculating spectra.
"""

from __future__ import division, print_function, absolute_import
import numpy as np
import sys
import os
sys.path.insert(0,os.environ['QCLPYDIR'])
from core.molecular_dynamics.utils import calculus


class TCFSpectrum(calculus.FFTFourierIntegral):
    """
    An object for calculating IR spectra from time-correlation
    functions.

    """

    def __init__(self, t_arr, pad, window='welch', **kwargs):
        """
        Initialise the converter.

        Input:
            t_arr (1d array) .............. a uniform grid of t >= 0
                                            time points
            pad (int) ..................... padding of the input array
                                            for the FT
            window (string) ............... one of 'none', 'fermi',
                                            'hann', 'welch'
            **kwargs ...................... key-word arguments for the
                                            window
                                            fermi ...... t_half, tau
                                            rest ....... t_max

        """

        lo = t_arr[0]
        hi = t_arr[-1]
        npts = t_arr.shape[-1]
        self.dt = (hi-lo)/(npts-1)
        super(TCFSpectrum, self).__init__(-hi, hi, 2*npts-1, pad)
        self.t_arr = self._symmetrise(t_arr)

        w = window.lower()
        if w == 'none':
            self.damping = np.ones_like(self.t_arr)
        elif w == 'fermi':
            temp = np.abs(self.t_arr)
            temp -= kwargs.get('t_half')
            temp /= kwargs.get('tau')
            self.damping = np.exp(np.where(temp > 100.0, 100.0, temp))
            self.damping += 1
            self.damping **= -1
        elif w == 'hann':
            t_max = kwargs.get('t_max', hi)
            temp = np.pi*self.t_arr/(2*t_max)
            self.damping = np.where(np.abs(temp) < np.pi/2,
                                    np.cos(temp)**2, 0.0)
        elif w == 'welch':
            t_max = kwargs.get('t_max', hi)
            temp = self.t_arr/t_max
            self.damping = np.where(np.abs(temp) < 1.0,
                                    1.0-temp**2, 0.0)
        else:
            raise NotImplementedError("""
Unrecognised type of window function '{:s}'.
            """.format(window))

    @staticmethod
    def _symmetrise(arr):

        npts = arr.shape[-1]
        ans = np.empty(arr.shape[:-1]+(2*npts-1,))
        ans[...,:npts-1] = arr[...,:0:-1]
        ans[...,npts-1:] = arr[:]

        return ans

    def __call__(self, tcfs, weight=False, freq_conv=1.0):
        """
        Return the power spectrum of the TCF.

        Input:
            tcfs (array) .................. an array with one or more TCFs,
                                            with time running over the final
                                            axis
            weight (bool) ................. if True, the result is multiplied
                                            by the square of the frequency
                                            (default False)
            freq_conv (float) ............. conversion factor for the
                                            frequencies (default 1.0)

        Return:
            freqs ......................... array of frequencies
            I_arr ......................... absorption intensities
        """

        sym_tcfs = self._symmetrise(tcfs)
        sym_tcfs *= self.damping
        ans = np.real(
                super(TCFSpectrum, self).__call__(sym_tcfs)
                )

        freqs = self.freqs[0].copy()
        bool_arr = freqs > -1e-12
        ans = ans[...,bool_arr]
        freqs = freqs[bool_arr]
        freqs *= freq_conv
        if weight:
            ans *= freqs**2

        return freqs, ans


if __name__ == '__main__':

    import argparse
    from qclpysim.utils.constants import physical_constants

    # Create an instance of argument parser:
    parser = argparse.ArgumentParser(
            description="""
            Return the  IR spectrum calculated  from the data  supplied. The
            frequency array,  Fourier transform  of the data  and absorbance
            array are all returned in a  file with the suffix '_IR' appended
            to the originale file name, before the extension.
            """)

    # Add the positional arguments (list of data files):
    parser.add_argument('datafiles', nargs='+', type=str)

    # Add the optional arguments:
    parser.add_argument(
                    "-p", "--padding", type=int,
                    default=4,
                    help='Ratio, by which to oversample the data (default 4).')

    parser.add_argument("-d", "--damping", choices=['none', 'fermi',
                                                    'hann', 'welch'],
                        default='welch',
                        help="""
                        Indicate how to damp the tail of the TCF data.
                        Default is 'welch'.
                        """)

    parser.add_argument("--t_max", type=float, default=None, help="""
                        Time at which the damping envelope goes strictly
                        to zero; only relevant for 'hann' and 'welch'
                        """)

    parser.add_argument("--t_half", type=float, default=None,
                        help="""
                        The point in time at which the TCF is reduced to
                        half its value by 'fermi' damping.
                        """)

    parser.add_argument("--tau",  type=float, default=None,
                        help="""
                        The timescale of damping decay in a 'fermi'
                        damping envelope.
                        """)

    parser.add_argument("-c", "--col", type=int, default=1,
                        help="""
                        Which column of the input text file contains
                        the relevant time-correlation function
                        (default 1; column 0 always reserved for time).
                        """)

    parser.add_argument("-v", "--cvv", action='store_true',
                        default=False, help="""
                        The TCF supplied is a time-derivative
                        auto-correlation function, and therefore
                        the Fourier transform should not be weighted
                        by the frequency to get the spectrum.
                        """)

    parser.add_argument("--conv", type=float,
                        default=1e13/physical_constants['c_SI'],
                        help="""
                        Conversion factor for the frequencies to get
                        from t0^(-1) to the desired unit
                        (default converts fs^(-1) to wavenumbers).
                        """)

    args = parser.parse_args()

    for data in args.datafiles:
        root, extension = os.path.splitext(data)

        if extension == '.npz':
            with np.load(data) as f:
                t_arr = f['t_arr']
                if args.cvv:
                    cxx = f['cvv']
                else:
                    cxx = f['cqq']
        else:
            t_arr, cxx = np.loadtxt(data, usecols=(0, args.col), unpack=True)

        kwargs = dict()
        if args.t_max is not None:
            kwargs['t_max'] = args.t_max
        if args.t_half is not None:
            kwargs['t_half'] = args.t_half
        if args.tau is not None:
            kwargs['tau'] = args.tau
        ftspec = TCFSpectrum(t_arr, args.padding, args.damping, **kwargs)
        freqs, I_abs = ftspec(cxx, freq_conv=args.conv)
        if not args.cvv:
            I_abs *= freqs**2
        outname = root+''.join(('_IR', '.dat'))
        np.savetxt(outname, np.c_[freqs, I_abs],
                   fmt='%23.16e', delimiter='  ')
