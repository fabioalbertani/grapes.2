# File: curvilinear.py
"""
Created on Tue May 29 16:18:11 2018

@author: gt317

Constraints in terms of curvilinear bead coordinates.
"""


from __future__ import division, print_function, absolute_import
from functools import wraps
import sys
import os
import math
root = os.path.split(os.path.split(os.path.abspath(os.curdir))[0])[0]
sys.path.insert(0,root)

from qclpysim.constraints import base
from qclpysim.utils import rotations
import numpy as np
from scipy import sparse


__all__ = ['eckart_triatomic', 'radial', 'polar']


def radial_fxn(qarr, idx, ndim, **kwargs):
    """
    Return the value of the radial constraint function defined as the
    mean radial displacement of the beads; idx is ignored and is present
    from consistency

    Input:
        qarr ........................ configuration array.
        idx ......................... index of the constraint
        ndim ........................ number of spatial dimensions
        **kwargs .................... nmtrans - if present, input is assumed
                                      to be in Matsubara coordinates

    Output:
        sigma ....................... value of the constraint (scalar)
        grad_sigma .................. gradient of constraint, same shape
                                      as qarr, converted into Matsubara mode
                                      coordinates if such was the input

    """

    if 'nmtrans' in kwargs:
        nmtrans = kwargs['nmtrans']
        qcart = nmtrans.mats2cart(qarr)
    else:
        nmtrans = None
        qcart = qarr

    old_shape, qcart.shape = qcart.shape, (ndim, -1)
    grad = np.zeros_like(qcart)

    r = np.sqrt(np.sum(qcart**2, axis=0))
    sigma = r.mean()
    grad = qcart/r
    grad.shape = old_shape
    qcart.shape = old_shape

    if nmtrans is not None:
        grad, qcart = qcart, grad
        grad = nmtrans.cart2mats(qcart, grad)

    qarr.shape, grad.shape = old_shape, old_shape

    return sigma, grad

def radial_setter(qarr, const_vals, ndim, **kwargs):
    """
    Set the system to the specified mean radial displacement.

    """

    if 'nmtrans' in kwargs:
        nmtrans = kwargs['nmtrans']
        qcart = nmtrans.mats2cart(qarr)
    else:
        nmtrans = None
        qcart = qarr

    old_shape, qcart.shape = qcart.shape, (ndim, -1)
    r_vec = np.sqrt(np.sum(qcart**2, axis=0))
    r = r_vec.mean()
    scale = const_vals[0]/r
    qcart[...] *= scale

    if nmtrans is not None:
        qcart.shape = old_shape
        qarr = nmtrans.cart2mats(qcart, qarr)

    return qarr

def radial_getter(qarr, ndim, **kwargs):
    """
    Get the mean radial displacement of the input system.

    """

    if 'nmtrans' in kwargs:
        nmtrans = kwargs['nmtrans']
        qcart = nmtrans.mats2cart(qarr)
    else:
        nmtrans = None
        qcart = qarr

    qcart.shape = (ndim, -1)
    const_vals = np.empty(1)
    r_vec = np.sqrt(np.sum(qcart**2, axis=0))
    const_vals[0] = r_vec.mean()

    return const_vals, (ndim,), kwargs

radial = base.HolonomicConstraints(radial_fxn, radial_setter, radial_getter)


def polar_fxn(qarr, idx, ndim, **kwargs):
    """
    Return the value of the polar constraint function defined as:

            idx                   constraint
        ==============================================
             0                    mean radial displacement from the origin
             1 ... (ndim-1)       angular coordinate(s) of the centroid

    Input:
        qarr ........................ configuration array.
        idx ......................... index of the constraint
        ndim ........................ number of spatial dimensions
                                      (accepts 2 or 3)
        **kwargs .................... nmtrans - if present, input is assumed
                                      to be in Matsubara coordinates

    Output:
        sigma ....................... value of the constraint (scalar)
        grad_sigma .................. gradient of constraint, same shape
                                      as qarr, converted into Matsubara mode
                                      coordinates if such was the input

    """

    if ndim not in [2, 3]:
        raise NotImplementedError(
        """
        Radial constraint only defined for plane-polar or spherical polar
        coordinates.
        """
        )

    if idx == 0:
        sigma, grad = radial_fxn(qarr, idx, ndim, **kwargs)

    elif idx == 1:
        old_shape, qarr.shape = qarr.shape, (ndim, -1)

        if 'nmtrans' in kwargs:
            sigma = np.arctan2(qarr[1,0], qarr[0,0])
            r_sq = np.sum(qarr[:2,0]**2) # x^2 + y^2 for centroid
            grad = np.zeros_like(qarr)
            grad[0,0] = -qarr[1,0]/r_sq
            grad[1,0] = qarr[0,0]/r_sq
        else:
            xy0 = qarr[:2,:].mean(axis=-1)
            sigma = np.arctan2(*xy0[::-1])
            r_sq = np.sum(xy0**2)
            grad = np.zeros_like(qarr)
            grad[0,:] = -xy0[1]/r_sq
            grad[1,:] = xy0[0]/r_sq

        qarr.shape, grad.shape = old_shape, old_shape

    elif idx == 2 and ndim == 3:

        old_shape, qarr.shape = qarr.shape, (ndim, -1)

        if 'nmtrans' in kwargs:
            r = np.sqrt(np.sum(qarr[:,0]**2))
            cos_theta = qarr[2,0]/r
            sigma = np.arccos(cos_theta)
            grad = np.zeros_like(qarr)
            # Get grad[cos_theta]
            grad[:,0] = -qarr[:,0]/r
            grad[:,0] *= -grad[2,0]
            grad[2,0] += 1.0
            grad[:,0] /= r
            # Convert to grad[theta]
            grad[:,0] /= -np.sqrt(1.0-cos_theta**2)
        else:
            q0 = qarr.mean(axis=-1)
            r = np.sqrt(np.sum(q0**2))
            cos_theta = q0[2]/r
            sigma = np.arccos(cos_theta)
            temp = -q0/r
            temp *= -temp[2]
            temp[2] += 1.0
            temp /= r
            temp /= -np.sqrt(1.0-cos_theta**2)
            grad = np.empty_like(qarr)
            grad[...] = temp[:,None]

        qarr.shape, grad.shape = old_shape, old_shape

    return sigma, grad

def polar_setter(qarr, const_vals, ndim, **kwargs):
    """
    Set the system to the specified mean radial displacement and
    centroid orientation.

    """

    qarr[...] = radial_setter(qarr, const_vals, ndim, **kwargs)
    qcart = qarr.reshape((ndim, -1))

    if 'nmtrans' in kwargs:
        q0 = qcart[:,0].copy()
    else:
        q0 = qcart.mean(axis=-1)

    r0 = np.sqrt(np.sum(q0**2))
    q0 /= r0

    if ndim == 2:
        phi = const_vals[1]
        phi -= math.atan2(q0[1], q0[0])
        rot_mat = np.array([[math.cos(phi), -math.sin(phi)],
                            [math.sin(phi), math.cos(phi)]])
    elif ndim == 3:
        q0_new = np.array([
                          math.cos(const_vals[1])*math.sin(const_vals[2]),
                          math.sin(const_vals[1])*math.sin(const_vals[2]),
                          math.cos(const_vals[2])
                          ])
        cos_theta = np.dot(q0, q0_new)

        if np.abs(cos_theta-1.0) < 1.0e-8:
            return qarr

        axis = np.cross(q0, q0_new)/np.sqrt(1.0-cos_theta**2)
        angle = np.arccos(cos_theta)
        rot_mat = rotations.axangle2matrix(axis, angle)

    else:
        raise NotImplementedError(
        """
        Radial constraint only defined for plane-polar or spherical polar
        coordinates.
        """
        )

    qcart[...] = np.dot(rot_mat, qcart)

    return qarr

def polar_getter(qarr, ndim, **kwargs):
    """
    Get the mean radial displacement and centroid orientation of
    the input system.

    """

    const_vals = np.empty(ndim)
    const_vals[0] = radial_getter(qarr, ndim, **kwargs)[0][0]

    qcart = qarr.reshape((ndim, -1))
    if 'nmtrans' in kwargs:
        q0 = qcart[:,0]
    else:
        q0 = qcart.mean(axis=-1)

    const_vals[1] = math.atan2(q0[1], q0[0])
    if ndim == 3:
        r0 = np.sqrt(np.sum(q0**2))
        const_vals[2] = math.acos(q0[2]/r0)

    return const_vals, (ndim,), kwargs

polar = base.HolonomicConstraints(polar_fxn, polar_setter, polar_getter)


#-------------------------------------------------------------------------#
#                 TRIATOMIC-SPECIFIC CONSTRAINTS                          #
#-------------------------------------------------------------------------#

def triatomic_internal_coords(x1, x2, x3):
    """
    Given the coordinates of atoms (1-3) in a triatomic, return the
    internal coordiantes.

    Input:
        x1, x2, x3 (array) ............ coordinates of the atoms or sets of
                                        atom replicas. In the latter case the
                                        first axis is taken to run over
                                        Cartesian coordinates.
    Output:
        q12, q32 ...................... displacement vectors from the central
                                        atom ("2")
        r12, r32 ...................... their respective lengths
        cos_theta ..................... cosine of the 1--2--3 bond angle

    """

    q12 = x1.copy()
    q12 -= x2
    r12 = np.sqrt(np.sum(q12**2, axis=0))

    q32 = x3.copy()
    q32 -= x2
    r32 = np.sqrt(np.sum(q32**2, axis=0))

    cos_theta = np.sum(q12*q32, axis=0)
    cos_theta /= r12
    cos_theta /= r32

    return q12, q32, r12, r32, cos_theta

def check_triatomic_dims(fxn):
    """
    Test that the first argument supplied to fxn is a one- or two-dimensional
    NumPy array with the first dimension of size nine (triatomic).

    """
    @wraps(fxn)
    def wrapper(qarr, *args, **kwargs):

        try:
            rank = qarr.ndim
        except AttributeError:
            raise AttributeError(
'Constraint function expects NumPy array as input.\n'
                     )
        except Exception as e:
            print("""
Constraint function experienced an error when processing
the constraint array: """+ str(e))
            raise
        if rank > 2:
            raise ValueError(
'Constraint function expects a one- ord two-dimensional array.\n'
            )
        elif qarr.shape[0] != 9:
            raise ValueError("""
Constraint function expects the first index of the input array to
range over the Cartesian coordinates of a triatomic.
            """)

        return fxn(qarr, *args, **kwargs)
    return wrapper

@check_triatomic_dims
def eckart_triatomic_fxn(qarr, idx, mass, eq, meq, **kwargs):
    """
    Return the value of the Exkart triatomic constraint defined as follows:

             idx               constraint
        =========================================
              0                mean value of R_12
              1                mean value of R_23
              2                mean value of theta
              3-5              Eckart translational conditions
              6-8              Eckart rotational conditions

    Input:
        qarr ........................ configuration array.
        idx ......................... index of the constraint
        mass (array) ................ shape = (3,) contains the atomic masses
        eq (array) .................. shape = (3,3) equilibrium atomic
                                      configuration (atoms along first axis,
                                      spatial dimensions along second)
        meq (array) ................. mass-weighted equilibrium configuration
        **kwargs .................... nmtrans - if present, input is assumed
                                      to be in Matsubara coordinates and is
                                      first transformed to Cartesians

    Output:
        sigma ....................... value of the constraint (scalar)
        grad_sigma .................. gradient of constraint, same shape
                                      as qarr, converted into Matsubara mode
                                      coordinates if such was the input

    """

    if 'nmtrans' in kwargs:
        nmtrans = kwargs['nmtrans']
        qcart = nmtrans.mats2cart(qarr)
    else:
        nmtrans = None
        qcart = qarr

    old_shape = qcart.shape
    qcart.shape = (3,3,-1)
    grad = np.zeros_like(qcart)

    if idx in [0, 1]:
        i = idx
        j = i+1
        qij = qcart[i].copy()
        qij -= qcart[j]
        rij = np.sqrt(np.sum(qij**2, axis=0))
        sigma = rij.mean()
        grad[i] = qij/rij
        grad[j] -= grad[i]

    if idx == 2:
        (q12, q32, r12, r32, cos_theta) = triatomic_internal_coords(*qcart)
        sigma = np.arccos(cos_theta).mean()

        # Normalise the displacement vecotrs
        q12 /= r12
        q32 /= r32

        # Calculate grad[cos(theta)]
        grad[0] = -q12
        grad[0] *= cos_theta
        grad[0] += q32
        grad[0] /= r12

        grad[2] = -q32
        grad[2] *= cos_theta
        grad[2] += q12
        grad[2] /= r32

        grad[1] -= grad[0]
        grad[1] -= grad[2]

        # Convert to grad[theta]
        grad *= -1/np.sqrt(1.0-cos_theta**2)

    if idx in [3, 4, 5]:
        i = idx-3
        sigma = np.sum(mass*np.mean(qcart[:,i,:], axis=-1))
        grad[:,i] = mass[:,None]

    if idx in [6, 7, 8]:
        if idx == 6:
            i, j = (1, 2)
        elif idx == 7:
            i, j = (2, 0)
        else:
            i, j = (0, 1)

        # m * eq_j * (q_i - eq_i):
        delta_i = qcart[:,i,:].copy()
        delta_i -= eq[:,i,None]
        delta_i *= meq[:,j,None]

        # m * eq_i * (q_j - eq_j):
        delta_j = qcart[:,j].copy()
        delta_j -= eq[:,j,None]
        delta_j *= meq[:,i,None]

        sigma = np.sum((delta_j - delta_i).mean(axis=-1))
        grad[:,j,:] = meq[:,i,None]
        grad[:,i,:] = -meq[:,j,None]

    qcart.shape = old_shape
    grad.shape = old_shape

    if nmtrans is not None:
        # Transform to normal modes.
        grad, qcart = (qcart, grad)
        grad = nmtrans.cart2mats(qcart, grad)

    return sigma, grad


@check_triatomic_dims
def eckart_triatomic_setter(qarr, const_vals, mass, eq, meq, **kwargs):
    """
    Set the system to a configuration with the speicified (mean) radii
    and angles, and re-orient it so that the Eckart conditions are
    satisfied

    Input:
        qarr ........................ configuration array.
        const_vals .................. ignored; values calculating from
                                      the array of equilibrium configs
        other arguments as in eckart_triatomic fxn.

    Output:
        qarr ........................ modified configuration array

    """


    if 'nmtrans' in kwargs:
        nmtrans = kwargs['nmtrans']
        qcart = nmtrans.mats2cart(qarr)
    else:
        nmtrans = None
        qcart = qarr

    old_shape = qcart.shape
    qcart.shape = (3,3,-1)

    # Calculate the internal coordinates
    (q12, q32, r12, r32, cos_theta) = triatomic_internal_coords(*qcart)
    q12 = q12.T
    q32 = q32.T
    theta = np.arccos(cos_theta)

    (q12_eq, q32_eq,
     r12_eq, r32_eq, theta_eq) = triatomic_internal_coords(*eq)
    theta_eq = np.arccos(theta_eq)

    # Calculate the normals to the triatomic planes
    normals = np.cross(q12, q32, axis=-1)
    normals /= r12[:,None]
    normals /= r32[:,None]
    normals /= np.sqrt(1.0 - cos_theta**2)[:,None]

    # Uniformly scale the radii so that the means agree
    r_init = np.mean(r12)
    scale = r12_eq/r_init
    q12 *= scale

    r_init = np.mean(r32)
    scale = r32_eq/r_init
    q32 *= scale

    # Uniformly rotate the q32 vectors to the specified mean angle
    theta_init = np.mean(theta)
    del_theta = theta_eq - theta_init
    rot_mat = rotations.axangle2matrix(normals,
                                       np.ones(len(normals))*del_theta)
    q32 = np.sum(rot_mat*q32[:,None,:], axis=-1)

    # Calculate equilibrium CoM
    mtot = mass.sum()
    com_eq = meq.sum(axis=0)/mtot
    # Eq in CoM frame
    shifted_eq = eq - com_eq
    # Distorted configuration in CoM frame
    mu1 = mass[0]/mtot
    mu2 = mass[1]/mtot
    mu3 = 1.0 - mu1 - mu2

    trans_mat = sparse.diags((3*[-mu1],
                              3*[-mu1]+3*[mu1+mu2],
                              3*[mu2+mu3]+3*[-mu3]+3*[1.0],
                              3*[-mu3]+3*[1.0],
                              3*[1.0]), offsets=(-6,-3,0,3,6))

    trans_mat = trans_mat.toarray()
    qcart[0] = q12.T
    qcart[1] = q32.T
    qcart[2] = 0.0
    qcart.shape = (9,-1)
    qcart[...] = np.dot(trans_mat, qcart)

    # Generate the rotation matrix that enforces rotational Eckart constraints
    qcart.shape = (3,3,-1)
    q0 = qcart.mean(axis=-1)
    rot_mat = rotations.eckart_rotation(q0, shifted_eq, mass)
    qcart[...] = np.sum(rot_mat[...,None]*qcart[:,None,...], axis=-2)
    # Shift to CoM
    qcart += com_eq[:,None]
    qcart.shape = old_shape

    # Convert back to normal modes if required
    if nmtrans is not None:
        qarr = nmtrans.cart2mats(qcart, qarr)

    return qarr

@check_triatomic_dims
def eckart_triatomic_getter(qarr, mass, **kwargs):
    """
    Calculate the mean bond-lengths, angle and position of the CoM
    for a triatomic ring-polymer.

    Input:
        qarr .................... configuration array
        mass .................... mass array of length (3,)
        **kwargs ................ nmtrans - if present, input is assumed
                                  to be in Matsubara coordinates and is
                                  first transformed to Cartesians

    Output:
        const_vals .............. values of the constraint function
        (mass, eq, meq) ......... list of additional positional
                                  arguments required by eckart_fxn
                                  and eckart_setter
        kwargs .................. list of keyword arguments

    """


    if 'nmtrans' in kwargs:
        nmtrans = kwargs['nmtrans']
        qcart = nmtrans.mats2cart(qarr)
    else:
        nmtrans = None
        qcart = qarr

    qcart.shape = (3,3,-1)
    (q12, q32, r12, r32, cos_theta) = triatomic_internal_coords(*qcart)
    const_vals = np.empty(9)
    const_vals[0] = r12.mean()
    const_vals[1] = r32.mean()
    const_vals[2] = np.arccos(cos_theta).mean()
    eq = qcart.mean(axis=-1)
    meq = eq*mass[:,None]
    const_vals[3:6] = meq.sum(axis=0)
    const_vals[6:] = 0.0

    return const_vals, (mass, eq, meq), kwargs


eckart_triatomic = base.HolonomicConstraints(
        eckart_triatomic_fxn,
        eckart_triatomic_setter,
        eckart_triatomic_getter)