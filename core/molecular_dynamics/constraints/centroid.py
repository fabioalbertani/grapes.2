# File: centroid.py
"""
Created on Tue May 29 16:17:07 2018

@author: gt317

Constraints expressed in terms of the ring-polymer centroid.
"""


from __future__ import division, print_function, absolute_import
from functools import wraps
import sys
import os
root = os.path.split(os.path.split(os.path.abspath(os.curdir))[0])[0]
sys.path.insert(0,root)

from qclpysim.constraints import base
import numpy as np

__all__ = ['fix_all']


def check_qdims(fxn):
    """
    Test that the first argument supplied to fxn is a two-dimensional
    NumPy array.

    """
    @wraps(fxn)
    def wrapper(qarr, *args, **kwargs):

        try:
            ndim, nb = qarr.shape
        except AttributeError:
            raise AttributeError(
'Constraint function expects NumPy array as input.\n'
                     )
        except ValueError:
            raise ValueError(
'Constraint function expects a two-dimensional configuration array.\n'
                     )
        except Exception as e:
            print("""
Constraint function experienced an error when processing
the constraint array: """+ str(e))
            raise

        return fxn(qarr, *args, **kwargs)
    return wrapper


@check_qdims
def centroid_fxn(qarr, idx, *args, **kwargs):
    """
    Return the centroid coordinate for column `idx' of the configuration
    array qarr, and the constraint gradient for the same component.

    Input:
        qarr ........................ configuration array.
        idx ......................... index of the centroid in the input
                                      column.
        *args ....................... for general compatibility with
                                      HolonomicConstraints
        **kwargs .................... specifies whether the the input
                                      configuration is in normal mode
                                      coordinates or not.

    Output:
        sigma ....................... value of the constraint (scalar)
        grad_sigma .................. gradient of constraint, same shape
                                      as qarr.

    """

    grad = np.zeros_like(qarr)
    nm = kwargs.get('nm', True)
    if nm:
        sigma = qarr[idx, 0]
        grad[idx, 0] = 1.0
    else:
        sigma = np.mean(qarr[idx,:])
        grad[idx, :] += 1/len(qarr[idx,:])

    return sigma, grad


@check_qdims
def centroid_setter(qarr, const_vals, *args, **kwargs):
    """
    Set the system to a configuration with constraints specified
    by const_vals.

    Input:
        qarr ........................ configuration array.
        const_vals .................. array of centroid positions
        *args ....................... for general compatibility with
                                      HolonomicConstraints
        **kwargs .................... specifies whether the the input
                                      configuration is in normal mode
                                      coordinates or not.

    Output:
        qarr ........................ updated configuration array

    """

    nm = kwargs.get('nm', True)
    if nm:
        qarr[:,0] = const_vals
    else:
        q0_init = qarr.mean(axis=-1)
        # Calculate the difference between those and the declared values
        q0_init -= const_vals
        qarr -= q0_init[:, None]

    return qarr


@check_qdims
def centroid_getter(qarr, *args, **kwargs):
    """
    Calculate the centroid positions given the input configuration.

    Input:
        qarr .................... configuration array
        *args ................... empty tuple for compatibility with
                                  general HolonomicConstraints
        **kwargs ................ specify if working in NM coordinate system
                                  (default nm=True).

    Output:
        const_vals .............. values of the constraint function
        args .................... updated list of additional positional
                                  arguments (empty)
        kwargs .................. updated list of keyword arguments
                                  (unchanged)

    """

    nm = kwargs.get('nm', True)
    if nm:
        const_vals = qarr[:,0]
    else:
        const_vals = qarr.mean(axis=-1)

    return const_vals, (), kwargs.copy()


fix_all = base.HolonomicConstraints(
        centroid_fxn, centroid_setter, centroid_getter
        )
