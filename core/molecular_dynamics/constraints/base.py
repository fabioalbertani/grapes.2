# File: constraints.py
"""
Created on Mon Apr 30 12:46:59 2018

@author: gt317

Constraint class used by the RATTLE algorithm with some default initialisation
options.
"""


from __future__ import division, print_function, absolute_import
from functools import wraps
import sys
import os
root = os.path.split(os.path.split(os.path.abspath(os.curdir))[0])[0]
sys.path.insert(0,root)


__all__ = ['HolonomicConstraints']


def check_idx(fxn):
    """
    Test that the requested constraint index does not exceed the maximum
    allowed value.

    """
    @wraps(fxn)
    def wrapper(self, qarr, idx, *args, **kwargs):

        if self.n is None:
            pass
        elif idx > self.n:
            raise ValueError("""
Error: requested constraint index exceeded maximum permitted.
""")

        return fxn(self, qarr, idx, *args, **kwargs)
    return wrapper

class HolonomicConstraints(object):
    """
    A set of holonomic constraints applied to a molecule or group of molecules
    that contains the methods for calculating constraint functions,
    their gradients, as well as a "setter" and a "getter" to
    update a molecular configuration given the current state of the set
    of constraints or vice versa.
    """

    def __init__(self, fxn, setter, getter, n=None):
        """
        Initialise the class with the following list of functions:
            const ...................... function that returns the value of
                                         the specified constraint and
                                         its gradient.
            setter ..................... method to update a molecular
                                         configuration given the current
                                         constraint values
            getter ..................... method to update the current
                                         constraint values given a
                                         molecular configuration.
            n (default None) ........... number of different constraints; if
                                         None, the number is determined by
                                         the input.
        """

        # Initialise the functions:
        self.n = n
        self.fxn = fxn
        self.setter = setter
        self.getter = getter

    @property
    def n(self):
        return self.__n

    @n.setter
    def n(self, n):
        # Assert that n is a positive integer or None
        msg = """
Error: expecting positive integer or None to specify the number of constraints.
"""
        if n is None:
            pass
        elif isinstance(n, int):
            if (n < 1):
                raise ValueError(msg)
        else:
            raise TypeError(msg)

        self.__n = n

    @check_idx
    def __call__(self, qarr, idx, const_val, *args, **kwargs):
        """
        Return the difference between the constraint function and
        the supplied constraint value.

        Input:
            qarr ................. particle configuration
            idx .................. index of the constraint function
            const_val ........... target constraint value
            args, kwargs ......... further parameters require by the
                                   constraint function.

        Output:
            sigma ................ difference between the constraint function
                                   and its target value
            grad_sigma ........... gradient of the constraint function

        """

        sigma, grad_sigma = self.fxn(qarr, idx, *args, **kwargs)
        sigma -= const_val

        return sigma, grad_sigma

    def set_constraint(self, qarr, const_vals, *args, **kwargs):
        """
        Update the molecular configuration qarr given the current
        constraint values.

        Input:
            qarr ................ molecular configuration
            const_vals .......... target values of the constraint function
            args, kwargs ........ further arguments required by the setter.

        Output:
            qarr ................ modified to fit the declared constraints.

        """

        qarr = self.setter(qarr, const_vals, *args, **kwargs)
        return qarr

    def get_constraint(self, qarr, *args, **kwargs):
        """
        Given a molecular configuration, return new values of the constraint
        function and updated values of additional arguments

        Input:
            qarr ................... molecular configuration
            args, kwargs ........... further arguments for the constraint
                                     function.

        Output:
            const_vals ............. new values of the constraint function
            args ................... an updated sequence of arguments required
                                     by the constraint function
            kwargs ................. an updated sequence of keyword arguments
                                     requred by the constraint function.

        """

        const_vals, args, kwargs = self.getter(qarr, *args, **kwargs)
        return const_vals, args, kwargs


