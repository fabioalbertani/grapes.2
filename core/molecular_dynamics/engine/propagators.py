# File: propagators.py

"""
Created on Thu Apr 26 11:59:22 2018

@author: gt317

Utilities for integrating equations of motion of atoms and ring-polymer
objects.
"""

from __future__ import division, print_function, absolute_import
import sys
import numpy as np
from functools import wraps

__all__ = ['VelocityVerlet', 'Rattle', 'RPFreeModes']


# Module-specific errors
class ShakeError(Exception):
    pass

class RattleError(Exception):
    pass


# Decorators to check input dimensions
def check_gps(fxn):
    """
    Test that the number of atom groups matches the number of constraints.

    """
    @wraps(fxn)
    def wrapper(self, *args, **kwargs):

        if len(self.atom_groups) != len(self.constraints):
            raise ValueError("""
Error: the number of atom groups does not match the number of contraint groups.
""")

        return fxn(self, *args, **kwargs)
    return wrapper

def check_dims(fxn):
    """
    Test that the number of atoms matches the sum over atom groups.

    """
    @wraps(fxn)
    def wrapper(self, q, *args, **kwargs):

        if len(q) != self.atom_groups[-1].stop:
            raise ValueError("""
Error: the first dimension of the input array does not match the declared
number of atoms.
""")

        return fxn(self, q, *args, **kwargs)
    return wrapper

#--------------- PROPAGATOR CLASSES ------------------#

class VelocityVerlet(object):
    """
    Velocity Verlet propagator.
    """

    def __init__(self, dt):
        """
        Initialise the propagator with the timestep dt.

        """

        self.dt = dt

    @property
    def dt(self):
        return self.__dt


    @dt.setter
    def dt(self, dt):
        # Assert that dt is scalar:
        if not isinstance(dt, float):
            raise TypeError(
                "Error: expecting integration time step dt to be a float."
                )

        self.__dt = dt

    def propa_0_to_dt2(self, p, q, f, m, slices=slice(None)):
        """
        First half of the velocity Verlet algorithm.

        Input:
            p ................ momenta at t=0
            q ................ positions at t=0
            f ................ forces at t=0
            m ................ particle masses
            slices ........... which slices of the arrays to propagate
                               (everything propagated by default)

        Output:
            p, q are modified in place to yield p(dt/2) and q(dt).

        """
        p_slc, q_slc, f_slc, m_slc = [arr[slices] for arr in [p, q, f, m]]

        p_slc[...] += f_slc*self.dt/2
        q_slc[...] += p_slc*self.dt/m_slc

        return p, q

    def propa_p_for_dt2(self, p, f, slices=slice(None)):
        """
        Second half of the velocity Verlet.

        Input:
            p .............. momenta at t=dt/2
            f .............. forces at t=dt
            slices ......... which slices of the arrays to propagate
                             (everything propagated by default)

        Output:
            p is modified in place to return p(dt).
        """

        p[slices] += f[slices]*self.dt/2

        return p

    def step(self, p, q, f, m, force, *args, **kwargs):
        """
        Perform a single propagation step.

        Input:
            p ................. particle momenta
            q ................. partcile positions
            f ................. particle forces
            m ................. masses
            force ............. force function
            *args, **kwargs ... arguments supplied to the force
                                function.
            kwargs['slices'] .. if present, specifies the array
                                slice to be propagated

        Output:
            p, q, f are updated in place.
        """

        slices = kwargs.get('slices', slice(None))

        p, q = self.propa_0_to_dt2(p, q, f, m, slices=slices)
        f = force(q, f, *args, **kwargs)
        p = self.propa_p_for_dt2(p, f, slices=slices)

        return p, q, f


class Rattle(VelocityVerlet):
    """
    RATTLE propagator for motion subject to holonomic constraints.

    """

    def __init__(self, dt, atom_groups, constraints, maxcycle, toll, tolm):
        """
        Initialise the propagator with the following arguments:
        dt (positive float) ............. integration timestep
        atom_groups (list) .............. the sizes of groups of
                                          atoms subject to a collective
                                          constraint (assumed to be
                                          consecutive)
        constrainsts (list) ............. list of objects of type
                                          HolonomicConstraints or None
        maxcycle (int) .................. maximum number of passes over the
                                          shake/rattle cycles to reach
                                          convergence
        toll (float) .................... convergence parameter for the
                                          'lambda' Lagrange multipliers
        toll (float) .................... convergence parameter for the
                                          'mu' Lagrange multipliers
        """

        super(Rattle, self).__init__(dt)
        self.atom_groups = atom_groups
        self.constraints  = constraints
        self.maxcycle = maxcycle
        self.toll = toll
        self.tolm = tolm

        # Create uninitialised RATTLE variables
        self.sigma = None
        self.grad_sigma = None
        self.const_vals = None
        self.const_args = None
        self.const_kwargs = None
        self.lambda_ = None
        self.mu = None

    @property
    def atom_groups(self):
        return self.__atom_groups

    @atom_groups.setter
    def atom_groups(self, atom_groups):
        try:
            natoms = np.array(atom_groups, dtype=int)
        except ValueError:
            raise ValueError(
"Error: expecting a list or array of integers for atom groups"
            )
        # Flatten the array
        natoms.shape = (-1,)

        # Calculate the cumulative sum of the number of atoms
        natoms = np.cumsum(natoms)

        # Generate a list of slices
        self.__atom_groups = []
        start = 0
        for atom in natoms:
            self.__atom_groups.append(slice(start, atom))
            start = self.__atom_groups[-1].stop

    @property
    def constraints(self):
        return self.__constraints

    @constraints.setter
    def constraints(self, constraints):
        try:
            self.__constraints = np.array(constraints)
        except:
            sys.stderr.write("""
Unknown error initialising the constraints in RATTLE.
""")
            raise
        # Flatten the array
        self.__constraints.shape = (-1,)

    @check_gps
    @check_dims
    def reset_constraints(self, q, arg_list=None, kwarg_list=None):
        """
        Reset RATTLE variables

        Input:
            q ............................. configuration array required
                                            by the "getter" functions
            args_list,
            kwarg_list .................... list of additional positional and
                                            keyword arguments for each atom
                                            group required by the getter
        """

        # Reset internal arguments
        self.const_vals = []
        self.lambda_ = []
        self.mu = []
        self.sigma = []
        self.grad_sigma = []

        if arg_list is None:
            if self.const_kwargs is None:
                arg_list = len(self.atom_groups)*[(),]
            else:
                arg_list = self.const_args.copy()

        if kwarg_list is None:
            if self.const_kwargs is None:
                kwarg_list = len(self.atom_groups)*[{},]
            else:
                kwarg_list = self.const_kwargs.copy()

        self.const_args = []
        self.const_kwargs = []

        # Cycle over the groups of atoms:
        for i, (slc, const) in enumerate(zip(self.atom_groups,
                                             self.constraints)):
            const_vals, args, kwargs = const.get_constraint(
                            q[slc], *arg_list[i], **kwarg_list[i])
            self.const_vals.append(const_vals)
            self.const_args.append(args)
            self.const_kwargs.append(kwargs)

            # Evaluate the constraint functions and their gradients
            sigma_list = []
            grad_list = []
            for j in range(len(const_vals)):
                sigma, grad = const(q[slc], j, const_vals[j],
                                    *args, **kwargs)
                sigma_list.append(sigma)
                grad_list.append(grad)

            self.sigma.append(np.stack(sigma_list))
            self.grad_sigma.append(np.stack(grad_list))

            lambda_ = np.zeros_like(const_vals)
            # Reshape lambda to broadcast with const_grad
            lambda_.shape = (len(lambda_),) + (grad_list[0].ndim)*(1,)
            self.lambda_.append(lambda_)
            self.mu.append(np.zeros_like(lambda_))


    @check_gps
    @check_dims
    def mould(self, q):
        """
        Modify the input configuration to satisfy the constraints.

        Input:
            q ...................... configuration array

        """

        for slc, const, val, args, kwargs in zip(
                self.atom_groups, self.constraints,
                self.const_vals, self.const_args, self.const_kwargs):
            q[slc] = const.set_constraint(q[slc], val, *args, **kwargs)

        return q

    def _combine_slices(self, arr, slices):
        """
        Given an input coordinates array and a list of slicing
        specifications, generate an array of slicing specs appropriate
        to each group of atoms.

        Input:
            arr .................. configuration array
            slices ............... an array of slicing specs applicable
                                   to all of arr

        Output:
            config_slices ........ a list of slices for the entire
                                   configuration array
            group_slices ......... a list of slices for the groups of
                                   atoms

        """

        # Convert slices to list
        try:
            slices = slices.tolist()
        except AttributeError:
            pass
        except:
            raise

        if isinstance(slices, slice):
            slices = [slices]

        # Try reading the slicing steps for the first dimension
        try:
            start, stop, step = slices[0].indices(len(arr))
        except:
            raise ValueError("""
Error: fancy indexing cannot be used with RATTLE.
""")
        if step < 0:
            raise ValueError("""
Error: slicing in RATTLE requires a positive step.
""")

        new_slices = slices.copy()
        config_slices = []
        group_slices = []

        # Cycle over the groups of atoms
        for slc in self.atom_groups:

            # For atom groups outside the range append None
            if slc.stop <= start or slc.start >= stop:
                config_slices.append(None)
                group_slices.append(None)
                continue

            start_in_slice = slc.start <= start
            stop_in_slice = slc.stop >= stop

            if start_in_slice:
                new_start = start
            else:
                new_start = (start - slc.start)%step + slc.start

            if stop_in_slice:
                new_stop = stop
            else:
                new_stop = slc.stop

            # Modify the first slice spec
            new_slices[0] = slice(new_start, new_stop, step)
            config_slices.append(new_slices.copy())
            new_slices[0] = slice(new_start-slc.start,
                                  new_stop-slc.start,
                                  step)
            group_slices.append(new_slices.copy())

        return config_slices, group_slices

    @staticmethod
    def _calc_delta_lambda(m, sigma, grad, new_grad):
        """
        Calculate a the linear correction term to the undetermined
        Lagrange multiplier for the position.

        Input:
            m ....................... particle masses
            sigma ................... deviation from the constraint value
            grad .................... initial gradient of the constraint
                                      function
            new_grad ................ current estimate of the gradient at
                                      a later time t+dt

        Output:
            delta_ll ................ correction to the Lagrange multiplier

        """

        denom_arr = grad.copy()
        denom_arr *= new_grad
        denom_arr /= m
        denom = np.sum(denom_arr)
        delta_ll = -sigma/denom
        return delta_ll

    @staticmethod
    def _calc_delta_mu(p, m, grad, fact):
        """
        Calculate a the linear correction term to the undetermined
        Lagrange multiplier for the velocity.

        Input:
            p ....................... particle momenta
            m ....................... particle masses
            grad .................... gradient of the constraint function
            fact .................... -1/SUM[grad**2/m]

        Output:
            delta_mu ................ correction to the Lagrange multiplier

        """

        num_arr = grad.copy()
        num_arr *= p
        num_arr /= m
        delta_mu = np.sum(num_arr)
        delta_mu *= fact
        return delta_mu

    @check_gps
    @check_dims
    def shake(self, p, q, f, m, *args, **kwargs):
        """
        Propagate positions for a full timestep and momenta
        for half a timestep under the constraints.

        NOTE: momenta are assumed to have already been updated
        with contributions from the external force

        Input:
            p ................ momenta
            q ................ positions
            f ................ forces
            m ................ masses

        Keyword arguments:
            slices ........... a slice object or list of slice
                               objects specifying which atoms to
                               propagate (all atoms propagated
                               by default)
            toll ............. convergence parameter for the lambda
                               Lagrange undetermined multipliers
                               (default 1.0e-4)
            maxcycle ......... maximum number of cycles over the
                               constraints to converge the multipliers
                               (default 100)

        """

        slices = kwargs.get('slices', slice(None))
        config_slices, group_slices = self._combine_slices(q, slices)

        tol = self.toll
        if not isinstance(tol, float):
            raise TypeError("""
Lagrange multiplier tolerance parameter expected to be of type float.
""")
        tol = abs(tol)  # Ensure tol > 0

        maxcycle = self.maxcycle
        if not isinstance(maxcycle, int):
            raise TypeError("""
Maximum number of cycles over constraints expected to be integer
""")
        maxcycle = max(abs(maxcycle), 1) # maxcycle > 0

        for k, (gp, slc, gp_slc, fxn, vals,
                fxn_args, fxn_kwargs) in enumerate(zip(
                        self.atom_groups, config_slices, group_slices,
                        self.constraints, self.const_vals,
                        self.const_args, self.const_kwargs)):

            if slc is None:
                # Atom group not propagated
                continue

            # Get views of the atom groups subject to the current
            # set of constraints
            p_gp, q_gp, f_gp, m_gp = [arr[gp] for arr in (p, q, f, m)]

            # Get views of the coordinates to be propagated
            # within that group
            p_slc, q_slc, f_slc, m_slc = [arr[slc] for arr in (p, q, f, m)]

            # Get views of the relevant constraint arrays for the atom group
            sigma = self.sigma[k]
            grad_gp = self.grad_sigma[k]
            grad_slc = self.grad_sigma[k][[slice(None)]+gp_slc]
            lambda_ = self.lambda_[k]

            if fxn is None:
                # Propagate without constraints
                q_slc[...] += p_slc*self.dt/m_slc
            else:
                # Use an initial guess for the Lagrange multipliers
                # to update the momenta and positions
                p_slc[...] += np.sum(lambda_ * grad_slc/self.dt,
                                     axis=0)
                q_slc[...] += p_slc*self.dt/m_slc

                new_grads = grad_gp.copy()
                delta_p = p_slc.copy()

                # Loop over all constraints until converged
                for i in range(maxcycle):
                    all_converged = True
                    for j, val in enumerate(vals):
                        # Calculate the new value of sigma and grad
                        sigma[j], new_grads[j,...] = fxn(q_gp, j, val,
                                                         *fxn_args,
                                                         **fxn_kwargs)
                        # Hence calculate delta_lambda and assess convergence
                        delta_ll = self._calc_delta_lambda(m_gp, sigma[j],
                                                           grad_gp[j,...],
                                                           new_grads[j,...])
                        if not (abs(delta_ll) < tol and
                                all_converged is True):
                            all_converged = False

                        # Update momenta and positions accordingly
                        lambda_[j] += delta_ll

                        delta_p[...] = delta_ll*grad_slc[j,...]/self.dt
                        p_slc[...] += delta_p

                        delta_p /= m_slc
                        delta_p *= self.dt
                        q_slc[...] += delta_p

                    if all_converged:
                        grad_gp[...] = new_grads.copy()
                        break

                if not all_converged:
                    msg = "Convergence failed for group {:d} after {:d} cycles"
                    raise ShakeError(msg.format(k, maxcycle))

        return p, q


    @check_gps
    @check_dims
    def rattle(self, p, m, *args, **kwargs):
        """
        Propagate momenta for half a timestep under the constraints,
        such that the latter are tangent to the constraint hypersurface
        by the end of the propagation

        NOTE: momenta are assumed to have already been updated
        with contributions from the external force

        Input:
            p ................ momenta
            m ................ masses

        Keyword arguments:
            slices ........... a slice object or list of slice
                               objects specifying which atoms to
                               propagate (all atoms propagated
                               by default)
            tolm ............. convergence parameter for the mu
                               Lagrange undetermined multipliers
                               (default 1.0e-4)
            maxcycle ......... maximum number of cycles over the
                               constraints to converge the multipliers
                               (default 100)

        """

        slices = kwargs.get('slices', slice(None))
        config_slices, group_slices = self._combine_slices(p, slices)

        tol = self.tolm
        if not isinstance(tol, float):
            raise TypeError("""
Lagrange multiplier tolerance parameter expected to be of type float.
""")
        tol = abs(tol)  # Ensure tol > 0

        maxcycle = self.maxcycle
        if not isinstance(maxcycle, int):
            raise TypeError("""
Maximum number of cycles over constraints expected to be integer
""")
        for k, (gp, fxn, slc, gp_slc) in enumerate(zip(
                        self.atom_groups, self.constraints,
                        config_slices, group_slices)):

            if slc is None:
                # Atom group not propagated
                continue

            # Get views of the atom groups subject to the current
            # set of constraints
            p_gp, m_gp = [arr[gp] for arr in (p, m)]

            # Get views of the coordinates to be propagated
            # within that group
            p_slc, m_slc = [arr[slc] for arr in (p, m)]

            grad_gp = self.grad_sigma[k]
            grad_slc = self.grad_sigma[k][[slice(None)]+gp_slc]
            mu = self.mu[k]

            if fxn is not None:
                # Use an initial guess for the Lagrange multipliers
                # to update the momenta
                p_slc[...] += np.sum(mu*grad_slc, axis=0)
                # Store the multiplicative factor that gets reused when
                # calculating delta_mu
                fact_arr = grad_gp.copy()
                fact_arr **= 2
                fact_arr /= m_gp
                fact_arr = -np.sum(fact_arr,
                                   axis=tuple(range(fact_arr.ndim))[1:])
                fact_arr **= -1

                # Cycle over constraints until convergence
                for i in range(maxcycle):
                    all_converged = True
                    for j,fact  in enumerate(fact_arr):
                        # Calculate a correction to mu
                        delta_mu = self._calc_delta_mu(p_slc, m_slc,
                                                       grad_slc[j], fact)
                        # Assess convergence
                        if not (abs(delta_mu) < tol and
                                all_converged is True):
                            all_converged = False

                        # Update momenta and positions accordingly
                        mu[j] += delta_mu

                        # Update the momenta accordingly
                        p_slc[...] += delta_mu*grad_slc[j]

                    if all_converged:
                        break

                if not all_converged:
                    msg = "Convergence failed for group {:d} after {:d} cycles"
                    raise RattleError(msg.format(k, maxcycle))

        return p

    @check_gps
    @check_dims
    def step(self, p, q, f, m, force, *args, **kwargs):
        """
        Perform a single constrained propagation step.

        Input:
            p ..................... particle momenta
            q ..................... partcile positions
            f ..................... particle forces
            m ..................... masses
            force ................. force function
            *args, **kwargs ....... arguments supplied to the force
                                    function.
            kwargs['slices'] ...... if present, specifies the array
                                    slice to be propagated

        Output:
            p, q, f are updated in place.
        """

        slc = kwargs.get('slices', slice(None))

        p[slc] = self.propa_p_for_dt2(p[slc], f[slc])
        p, q = self.shake(p, q, f, m, force, *args, **kwargs)

        f = force(q, f, *args, **kwargs)

        p[slc] = self.propa_p_for_dt2(p[slc], f[slc])
        p = self.rattle(p, m, *args, **kwargs)

        return p, q, f

class RPFreeModes(VelocityVerlet):
    """
    REference System Propagator algorithm (RESPA) for ring-polymers
    based on the free-polymer Liouvillian
    DOI: 10.1063/1.3489925

    """

    def __init__(self, dt, freqs, shape=None):
        """
        Initialise the ring-polymer propagator, based on the
        free-polymer Liouvillian

        Input:
            dt(float) ...................... integration time-step
            freqs(float or ndarray) ........ ring-polymer normal-mode
                                             frequencies
            shape(int or list optional) .... shape of the input (if
                                             unspecified, read from freqs)
        """

        super(RPFreeModes, self).__init__(dt)

        if shape is None and isinstance(freqs, (int, float)):
            self.scalar = True
            self.freqs = freqs
            self.coeffs = np.ones(4)
        else:
            self.scalar = False
            self.freqs = np.ones(shape)*np.asarray(freqs, dtype=float)
            self.coeffs = np.ones((4,)+self.freqs.shape)

        self.coeffs *= self.freqs*self.dt
        self.coeffs[0] = np.cos(self.coeffs[0])
        self.coeffs[3] = self.coeffs[0].copy()

        self.coeffs[1] = np.sin(self.coeffs[1])
        self.coeffs[2] = self.coeffs[1].copy()
        self.coeffs[1] *= -self.freqs

        eps = 1.0e-8
        temp = np.where(np.abs(self.freqs) > eps, self.freqs, eps)
        self.coeffs[2] = np.where(np.abs(self.freqs) > eps,
                                  self.coeffs[2]/temp,
                                  self.dt*(1.0 - (self.freqs*self.dt)**2/6))

    @staticmethod
    def matsubara_freqs(beta, nmodes):
        """
        Calculate the Matsubara frequencies 0, -/+(2*pi/beta), ...
        for the given number of modes.

        """

        freqs = [0]+[i for j in range(1,nmodes//2+1) for i in [-j, j]]
        if nmodes%2 == 0:
            freqs.pop(-2)

        freqs = np.asarray(freqs, dtype=float)
        freqs *= 2*np.pi/beta

        return freqs

    @staticmethod
    def RPMD_freqs(beta, nbeads, nmodes=None):
        """
        Calculate the RPMD normal mode frequencies for the given
        number of beads and (optionally) number of modes. If the
        latter is not specified, nmodes = nbeads

        """

        if nmodes is None:
            nmodes_ = nbeads
        else:
            nmodes_ = nmodes

        freqs = RPFreeModes.matsubara_freqs(beta, nmodes_)
        two_ww_n = 2*nbeads/beta
        freqs /= two_ww_n
        freqs[...] = np.sin(freqs)
        freqs *= two_ww_n

        return freqs

    @classmethod
    def RPMD_propa(cls, dt, beta, nbeads, nmodes=None, shape=None):
        """
        Returns an instance of an RPMD propagator.
        Input
        dt (float) ............... integration timestep
        beta (float) ............. 1 / kB T in a.u.
        nbeads (int) ............. number of RP beads
        nmodes (int) ............. number of modes (optional,
                                   default = nbeads)
        shape (int or tuple) ..... shape of the input (default None)

        """

        return cls(dt, cls.RPMD_freqs(beta, nbeads, nmodes), shape)

    @property
    def nmodes(self):
        return self.__nmodes


    @nmodes.setter
    def nmodes(self, nmodes):
        # Assert that dt is scalar:
        if not isinstance(nmodes, int):
            raise TypeError(
                "Error: expecting integration number of beads to be integer."
                )

        if (nmodes < 1):
            raise ValueError(
                "Error: expecting number of modes to be positive."
            )

        self.__nmodes = nmodes

    def RESPA_propa(self, p, q, m, slices=slice(None)):
        """
        Propagate the phase-space coordinates according
        to the free RP equations of motion for a single
        simulation timestep.

        Input:
            p ................ momenta in normal mode coordinates
            q ................ positions in normal mode coordinates
            m ................ the associated masses
            slices ........... parts of the arrays to be propagated
        """

        # Get the views of the slices to be propagated
        p_slc, q_slc, m_slc = [arr[slices] for arr in [p, q, m]]

        p_init = p_slc.copy()
        p_slc[...] *= self.coeffs[[0]+slices]
        p_slc[...] += self.coeffs[[1]+slices]*q_slc*m_slc

        q_slc[...] *= self.coeffs[[3]+slices]
        q_slc[...] += self.coeffs[[2]+slices]*p_init/m_slc

        return p, q

    def step(self, p, q, f, m, force, *args, **kwargs):
        """
        Propagate the ring-polymer for a single intergraion step
        under the influence of the external forces

        Input:
            p (ndarray) ....................... normal mode momenta
            q (ndarray) ....................... normal mode coordinates
            f (ndarray) ....................... normal mode forces at t=0
            m (ndarray) ....................... normal mode masses
            force (object) .................... a callable function that
                                                returns normal mode forces
            *args, **kwargs ................... arguments passed on to force
            slices ............................ which slices to propagate
                                                (everything by default)

        Output:
            p, q, f are updated in place.

        """

        slices = kwargs.get('slices', slice(None))
        try:
            slices = slices.tolist()
        except AttributeError:
            pass
        except:
            raise

        if isinstance(slices, slice):
            slices = [slices]
        else:
            slices = list(slices)

        p = self.propa_p_for_dt2(p, f, slices=slices)
        p, q = self.RESPA_propa(p, q, m, slices)
        f = force(q, f, *args, **kwargs)
        p = self.propa_p_for_dt2(p, f, slices=slices)

        return p, q, f