# File: ring_polymers.py

"""
Created on Wed May 16 14:57:14 2018

@author: gt317

Defines high-level classes for ring-polymer propagation.

"""

from __future__ import division, print_function, absolute_import
import sys
import os
root = os.path.split(os.path.split(os.path.abspath(os.curdir))[0])[0]
sys.path.insert(0,root)
from core.molecular_dynamics.engine.particles import Particles
from core.molecular_dynamics.engine.propagators import RPFreeModes, Rattle
from core.molecular_dynamics.engine.thermostats import Langevin
from core.molecular_dynamics.utils.nmtrans import FFTNormalModes
import numpy as np
from functools import wraps

__all__ = ['RingPolymers','ThermRingPolymers','ConstrainedPolymers']


def check_qdims(fxn):
    @wraps(fxn)
    def wrapper(cls, qcart, *args, **kwargs):
        if qcart.ndim == 1:
            # Assume centroid configuration
            q_in = qcart.reshape((-1,1))
        elif qcart.ndim != 2:
            raise ValueError(
"""
Error: factory method only accepts one- or two-dimensional configuration
arrays as input.
"""
            )
        else:
            q_in = qcart
        return fxn(cls, q_in, *args, **kwargs)
    return wrapper

def check_mdims(fxn):
    @wraps(fxn)
    def wrapper(cls, qcart, m, *args, **kwargs):
        if isinstance(m, (float, int)):
            m0 = float(m)
        elif m.ndim == 1:
            m0 = m.reshape((-1, 1))
        else:
            raise ValueError("""
Error: expecting scalar or 1D input for masses.
"""
            )
        return fxn(cls, qcart, m0, *args, **kwargs)
    return wrapper


class RingPolymers(Particles):
    """
    An ensemble of ring polymers, complete with a potential energy
    surface, a propagator, and a thermostat.

    """

    def __init__(self, q, m, beta, nbeads, nmodes,
                 pes, propa, thermostat, nmtrans,
                 freqs=None, p=None, f=None):

        """
        System set-up

        Input:
            q .............................. normal mode configuration
            m .............................. normal mode masses
            beta ........................... temperature 1/kB*T
            nbeads (int) ................... number of RP beads
            nmodes (int) ................... number of RP modes
            pes ............................ potential energy surface
                                             (has to have `force' and
                                              `potential' attributes)
            propa .......................... propagator class
            thermostat ..................... a thermostat object with
                                             `sample' and `kick_0/dt'
                                             attributes
            nmtrans ........................ normal mode transformation
                                             required for calling force
                                             and potential energy functions
            freqs (optional) ............... normal mode frequencies;
                                             if absent, will be extracted
                                             from the propagator
            p (optional) ................... initial normal mode momenta
                                             (by default drawn from the
                                              Boltzmann distribution)
            f (optional) ................... initial normal mode forces
                                             (by default calculated from
                                              pes.force)
        """

        self.nbeads = nbeads
        self.nmodes = nmodes
        self.qcart = np.empty(q.shape[:-1]+(self.nbeads,))
        self.fcart = np.empty_like(self.qcart)
        self.nmtrans = nmtrans

        if self.nbeads < self.nmodes:
            raise ValueError(
"The number of beads cannot be smaller than the number of normal modes."
            )

        if freqs is None:
            self.freqs = np.ones(q.shape[-1])*propa.freqs
        else:
            self.freqs = np.ones(q.shape[-1])*freqs

        super(RingPolymers, self).__init__(q, m, beta, pes, propa,
                                           thermostat, p, f)


    @classmethod
    @check_qdims
    @check_mdims
    def RPMD(cls, qcart, m, dt, beta, nbeads, pes, ThermCls=None,
             *args, **kwargs):
        """
        Construct a system of RPMD ring-polymers with a RESPA based on
        free ring polymers.

        Input:
            qcart (array) ..................... array of Cartesian ring-polymer
                                                coordinate. If the array is
                                                1D, collapsed geometries are
                                                returned.
            m (float or 1d array) ............. array of particle masses
            dt (float) ........................ integration time-step
            beta (float) ...................... 1 / kB T
            nbeads (int) ...................... number of beads
            pes ............................... potential energy surface
            ThermCls .......................... a thermostat class (currently
                                                only set up for Langevin)
            *args ............................. prng object and state
            **kwargs .......................... Thermostat.from_freqs
                                                keyword arguments

        Returns:
            an instance of ring_polymers initialised as a standard
            RPMD system

        """

        therm_kwargs = kwargs.copy()
        propa = RPFreeModes.RPMD_propa(dt, beta, nbeads, shape=qcart.shape)
        freqs = therm_kwargs.pop('freqs', propa.freqs)
        therm_kwargs['freqs'] = freqs
        nmtrans = FFTNormalModes(qcart.shape[0], nbeads)
        nmodes = nbeads

        if ThermCls is None:
            thermostat = Langevin.from_freqs(dt, pes.ndim, *args,
                                             **therm_kwargs)
        else:
            thermostat = ThermCls.from_freqs(dt, pes.ndim, *args,
                                             **therm_kwargs)

        q = nmtrans.cart2mats(np.ones((qcart.shape[0], nbeads))*qcart)
        pes.mode = 'rp'

        return cls(q, m, beta, nbeads, nmodes, pes, propa,
                   thermostat, nmtrans)

    def potential(self, q, *args, **kwargs):
        """
        Calculate the potential energy specified in the PES.

        """

        self.qcart = self.nmtrans.mats2cart(q, self.qcart)
        return self.pes.potential(self.qcart, *args, **kwargs)

    def force(self, q, f, *args, **kwargs):
        """
        Calculate the normal mode forces.

        """

        self.qcart = self.nmtrans.mats2cart(q, self.qcart)
        self.fcart = self.pes.force(self.qcart, self.fcart, *args, **kwargs)
        return self.nmtrans.cart2mats(self.fcart, f)

    def PE_spring(self):
        """
        Return the harmonic spring Matsubara potential energy

        """

        ans = self.q.copy()
        ans *= self.freqs
        ans **= 2
        ans *= self.m
        ans /= 2

        if not self.coupled:
            ans.shape = (self.q.shape[0]//self.pes.ndim,
                         self.pes.ndim, -1)

        return ans.sum(axis=(-2,-1))

    def KE(self):
        """
        Return the Matsubara kinetic energy of the system.

        """
        ans = self.p.copy()
        ans **= 2
        ans /= self.m
        ans /= 2

        if not self.coupled:
            ans.shape = (self.p.shape[0]//self.pes.ndim,
                         self.pes.ndim, -1)

        return ans.sum(axis=(-2,-1))

    def PE(self):
        """
        Return the Matsubara potential energy of the system.

        """

        ans = self.potential(self.q)
        ans /= self.nbeads

        return ans.sum(axis=-1)

    def energy(self, springs=True):
        """
        Return the total energy of the system. `slc' specifies
        the normal modes whose contributions are considered.
        If springs is True (default), contributions from harmonic
        springs are included

        """
        ans = self.KE()
        ans += self.PE()
        if springs:
            ans += self.PE_spring()

        return ans

    def Mats_phase(self, nmodes):
        """
        Return the Matsubara phase SUM(ww_{n}*P_{n} Q_{n}) where SUM
        runs from n = 0 to nmodes-1

        """

        if nmodes == 1:
            ans = np.array([1.0])
        else:
            freq_slc, p_slc, q_slc = [arr[...,1:nmodes] for arr in (self.freqs,
                                                                    self.p,
                                                                    self.q)]
            ans = freq_slc[...,::2]*p_slc[...,::2]*q_slc[...,1::2]
            ans += freq_slc[...,1::2]*p_slc[...,1::2]*q_slc[...,::2]

            if not self.coupled:
                ans.shape = (self.q.shape[0]//self.pes.ndim,
                             self.pes.ndim, -1)

            ans = np.sum(ans, axis=(-1,-2))
        return ans

    def virial(self, slc=slice(None)):
        """
        Return the virial estimator for kinetic energy. `slc' specifies
        the normal modes whose contributions are considered.

        """

        ans = self.q[...,slc].copy()
        ans *= self.f[...,slc].copy()
        ans /= -2

        if not self.coupled:
            ans.shape = (self.q.shape[0]//self.pes.ndim,
                         self.pes.ndim, -1)

        return ans.sum(axis=-2)


class ThermRingPolymers(RingPolymers):
    """
    An ensemble of ring polymers, complete with a potential energy
    surface, a propagator, and a thermostat, for which some of the
    normal modes are thermostatted throughout the simulation
    (this includes TRPMD and PA-CMD)

    """

    def __init__(self, q, m, beta, nbeads, nmodes,
                 pes, propa, thermostat, nmtrans,
                 freqs=None, p=None, f=None,
                 ncentral=1, gamma=1):

        """
        System set-up

        Input:
            q .............................. normal mode configuration
            m .............................. normal mode masses
            beta ........................... temperature 1/kB*T
            nbeads (int) ................... number of RP beads
            nmodes (int) ................... number of RP modes
            pes ............................ potential energy surface
                                             (has to have `force' and
                                              `potential' attributes)
            propa .......................... propagator class
            thermostat ..................... a thermostat object with
                                             `sample' and `kick_0/dt'
                                             attributes
            nmtrans ........................ normal mode transformation
                                             required for calling force
                                             and potential energy functions
            freqs (optional) ............... normal mode frequencies;
                                             if absent, will be extracted
                                             from the propagator
            p (optional) ................... initial normal mode momenta
                                             (by default drawn from the
                                              Boltzmann distribution)
            f (optional) ................... initial normal mode forces
                                             (by default calculated from
                                              pes.force)
            ncentral (int) ................. # modes excluded from
                                             thermostatting in "NVE"
                                             propagation (default 1)
            gamma (int) .................... number of "small" propagation
                                             steps dt/gamma step.

        """


        super(ThermRingPolymers, self).__init__(
                q, m, beta, nbeads, nmodes,
                pes, propa, thermostat, nmtrans,
                freqs, p, f)

        self.ncentral = ncentral
        self.gamma = gamma

    @classmethod
    @check_qdims
    @check_mdims
    def TRPMD(cls, qcart, m, dt, beta, nbeads, pes, ThermCls=None,
              *args, **kwargs):
        """
        Construct a TRPMD system with a Langevin thermostat.

        Input:
            qcart (array) ..................... array of Cartesian ring-polymer
                                                coordinate. If the array is
                                                1D, collapsed geometries are
                                                returned.
            m (float or 1d array) ............. array of particle masses
            dt (float) ........................ integration time-step
            beta (float) ...................... 1 / kB T
            nbeads (int) ...................... number of beads
            pes ............................... potential energy surface
            ThermCls .......................... a thermostat class (currently
                                                only set up for Langevin)
            *args ............................. prng object and state
            **kwargs .......................... Thermostat.from_freqs
                                                keyword arguments
                                                (default lambda_ ovewritten
                                                to 0.5)

        Returns:
            an instance of ring_polymers initialised as a standard
            TRPMD system

        """

        therm_kwargs = kwargs.copy()
        therm_kwargs['lambda_'] = therm_kwargs.pop('lambda_', 0.5)

        system = cls.RPMD(qcart, m, dt, beta, nbeads, pes, ThermCls,
                          *args, **therm_kwargs)

        system.gamma = 1
        system.ncentral = 1

        return system

    @classmethod
    @check_qdims
    @check_mdims
    def PACMD(cls, qcart, m, dt, beta, nbeads, gamma, pes, ThermCls=None,
              *args, **kwargs):
        """
        Construct a partially-adiabatic centroid molecular dynamics system.

        Input:
            qcart (array) ..................... array of Cartesian ring-polymer
                                                coordinate. If the array is
                                                1D, collapsed geometries are
                                                returned.
            m (float or 1d array) ............. array of particle masses
            dt (float) ........................ (large) integration time-step;
                                                the actual time-step used is
                                                dt/gamma
            beta (float) ...................... 1 / kB T
            nbeads (int) ...................... number of beads
            gamma (int) ....................... adiabatic separation
            pes ............................... potential energy surface
            ThermCls .......................... a thermostat class
            *args ............................. prng object and state
            **kwargs .......................... Thermostat.from_freqs arguments
                                                (default lambda_ ovewritten
                                                to 0.01)

        Returns:
            an instance of ring_polymers initialised as a partially-adiabatic
            CMD system

        """

        # Set up the normal mode transformation
        nmtrans = FFTNormalModes(qcart.shape[0], nbeads)
        q = nmtrans.cart2mats(qcart)
        nmodes = nbeads

        # Scale the normal mode masses
        freqs = RPFreeModes.RPMD_freqs(beta, nbeads)
        Omega = gamma*nbeads/beta # adiabatic frequency
        m_scaled = np.ones_like(q)*m
        m_scaled[...,1:] *= (freqs[...,1:]/Omega)**2
        freqs[...,1:] = Omega

        small_dt = dt/gamma
        propa = RPFreeModes(small_dt, freqs, shape=q.shape)
        therm_kwargs = kwargs.copy()
        therm_kwargs['freqs'] = therm_kwargs.pop('freqs', propa.freqs)
        therm_kwargs['lambda_'] = therm_kwargs.pop('lambda_', 0.01)

        if ThermCls is None:
            thermostat = Langevin.from_freqs(small_dt, pes.ndim,
                                             *args, **therm_kwargs)
        else:
            thermostat = ThermCls.from_freqs(small_dt, pes.ndim,
                                             *args, **therm_kwargs)

        pes.mode='rp'
        system = cls(q, m_scaled, beta, nbeads, nmodes, pes, propa,
                     thermostat, nmtrans)

        system.gamma = gamma
        system.ncentral = 1

        return system

    @classmethod
    @check_qdims
    @check_mdims
    def MFMats(cls, qcart, m, dt, beta, nbeads, nmodes,
               gamma, pes, ThermCls=None, *args, **kwargs):
        """
        Construct a partially-adiabatic mean-field Matsubara dynamics system

        Input:
            qcart (array) ..................... array of Cartesian ring-polymer
                                                coordinate. If the array is
                                                1D, collapsed geometries are
                                                returned.
            m (float or 1d array) ............. array of particle masses
            dt (float) ........................ (large) integration time-step;
                                                the actual time-step used is
                                                dt/gamma
            beta (float) ...................... 1 / kB T
            nbeads (int) ...................... number of beads
            nmodes (int) ...................... number of non-mean-
                                                fielded modes
            gamma (int) ....................... adiabatic separation
            pes ............................... potential energy surface
            ThermCls .......................... a thermostat class
            *args ............................. prng object and state
            **kwargs .......................... Thermostat.from_freqs keyword
                                                arguments

        Returns:
            an instance of ring_polymers initialised as a partially-adiabatic
            MF Matsubara system

        """

        # Set up the normal mode transformation
        nmtrans = FFTNormalModes(qcart.shape[0], nbeads)
        q = nmtrans.cart2mats(qcart)

        # Scale the normal mode masses
        freqs = RPFreeModes.RPMD_freqs(beta, nbeads)
        Omega = gamma*nbeads/beta # adiabatic frequency
        m_scaled = np.ones_like(q)*m
        m_scaled[...,nmodes:] *= (freqs[...,nmodes:]/Omega)**2
        freqs[...,nmodes:] = Omega

        propa_freqs = freqs.copy()
        propa_freqs[...,:nmodes] *= 0
        small_dt = dt/gamma
        propa = RPFreeModes(small_dt, propa_freqs, shape=qcart.shape)

        therm_kwargs = kwargs.copy()
        therm_kwargs['freqs'] = therm_kwargs.pop('freqs', propa.freqs)

        if ThermCls is None:
            thermostat = Langevin.from_freqs(small_dt, pes.ndim,
                                             *args, **therm_kwargs)
        else:
            thermostat = ThermCls.from_freqs(small_dt, pes.ndim,
                                             *args, **therm_kwargs)

        pes.mode='rp'
        system = cls(q, m_scaled, beta, nbeads, nmodes, pes, propa,
                     thermostat, nmtrans)

        system.gamma = gamma
        system.ncentral = 1

        return system


    def nve_evolve(self, ndt=1, slc=slice(None), *args, **kwargs):
        """
        Propagate the system in the absence of thermalisation of
        "central" modes (centroid in TRPMD and CMD, low-lying
        Matrsubara modes in MF-Matsubara)


        Input:
            ndt ....................... number of propagation time-steps
                                        (default 1)
            slc (slice) ............... specifies the particles to be
                                        propagated
        """
        if self.nbeads == 1:
            super(ThermRingPolymers, self).nve_evolve(
                ndt=self.gamma*ndt,
                slc=[slc,slice(None)],
                *args, **kwargs)
        else:
            super(ThermRingPolymers, self).nvt_evolve(
                    ndt=self.gamma*ndt,
                    pslc=[slc,slice(None)],
                    tslc=[slc,slice(self.ncentral,None)],
                    *args, **kwargs)

    def nvt_evolve(self, ndt=1, slc=slice(None), *args, **kwargs):
        """
        Thermalise all of the normal modes.

        Input:
            ndt ....................... number of propagation time-steps
                                        (default 1)
            slc (slice) ............... specifies the particles to be
                                        propagated
        """

        super(ThermRingPolymers, self).nvt_evolve(
                 ndt=self.gamma*ndt,
                 pslc=[slc,slice(None)],
                 tslc=[slc,slice(None)],
                 *args, **kwargs)

    def evolve(self, ndt=1, pslc=slice(None), tslc=slice(None),
               *args, **kwargs):
        """
        A general evolution with explicitly specified propagation
        and thermostatting slices.

        Input:
            ndt ....................... number of propagation time-steps
                                        (default 1)
            pslc (slice) .............. specifies the particles to be
                                        propagated
            tslc (slice) .............. specifies the momenta to be
                                        thermostatted

        """

        super(ThermRingPolymers, self).nvt_evolve(
                 ndt=self.gamma*ndt,
                 pslc=pslc, tslc=tslc,
                 *args, **kwargs)


class ConstrainedPolymers(RingPolymers):
    """
    An ensemble of ring-polymers subject to holonomic constraints.
    Propagaion is done in normal mode coordinates with RATTLE,
    which is also used to project the thermostatted momenta
    onto the constraint tangent space.

    """

    @classmethod
    @check_qdims
    @check_mdims
    def PIMD(cls, qcart, m, dt, beta, nbeads,
             pes, atom_groups, constraints,
             maxcycle=100, toll=1e-04, tolm=1e-04,
             arg_list=None, kwarg_list=None,
             gamma=1, ThermCls=None, *args, **kwargs):

        """
        Construct a system of contrained ring-polymers with a RATTLE
        propagator.

        Input:
            qcart (array) ..................... array of Cartesian ring-polymer
                                                coordinate. If the array is
                                                1D, collapsed geometries are
                                                returned.
            m (float or 1d array) ............. array of particle masses
            dt (float) ........................ integration time-step
            beta (float) ...................... 1 / kB T
            nbeads (int) ...................... number of beads
            pes ............................... potential energy surface
            atom_groups (list) ................ sizes of groups of
                                                atomic coordinates subject
                                                to a collective constraint
                                                (assumed to be stored
                                                consecutively)
            constrainsts (list) ............... list of objects of type
                                                HolonomicConstraint or None
            maxcycle (int) .................... maximum number of passes over
                                                shake/rattle cycles to reach
                                                convergence
            toll (float) ...................... convergence parameter for the
                                                'lambda' Lagrange multipliers
            toll (float) ...................... convergence parameter for the
                                                'mu' Lagrange multipliers
            arg_list .......................... a list of additional arguments
                                                required by the constraint fxn
                                                for each atom group
                                                (default None)
            kwarg_list ........................ a list of additional keyword
                                                arguments required by the
                                                constraints
                                                (default None)
            gamma (int) ....................... the frequencies of non-centroid
                                                modes are scaled to
                                                gamma*nbeads / (beta*hbar)
                                                and masses adjusted accordingly
            ThermCls .......................... a thermostat class
                                                (default Langevin)
            *args ............................. prng object and state
            **kwargs .......................... thermostat keyword arguments

        """


        nmtrans = FFTNormalModes(qcart.shape[0], nbeads)
        nmodes = nbeads
        q = nmtrans.cart2mats(np.ones((qcart.shape[0], nbeads))*qcart)

        if kwarg_list is None:
            kwarg_list_copy = len(atom_groups)*[dict()]
        elif len(kwarg_list) != len(atom_groups):
            raise ValueError("""
The list of keyword argument dictionaries does not match up
with the list of groups of atoms.
            """)
        else:
            kwarg_list_copy = kwarg_list.copy()

        if arg_list is None:
            arg_list_copy = len(atom_groups)*[tuple()]
        elif len(arg_list) != len(atom_groups):
            raise ValueError("""
The list of positional argument dictionaries does not match up
with the list of groups of atoms.
            """)
        else:
            arg_list_copy = arg_list

        for i, n in enumerate(atom_groups):
            kwarg_list_copy[i]['nmtrans'] = FFTNormalModes(n, nbeads)

        propa = Rattle(dt, atom_groups, constraints, maxcycle, toll, tolm)
        propa.reset_constraints(q, arg_list_copy, kwarg_list_copy)

        therm_kwargs = kwargs.copy()
        freqs = RPFreeModes.RPMD_freqs(beta, nbeads, nmodes)
        Omega = gamma*nbeads/beta
        m_scaled = np.ones_like(q)*m
        m_scaled[...,1:] *= (freqs[...,1:]/Omega)**2
        freqs[...,1:] = Omega
        therm_kwargs['freqs'] = therm_kwargs.pop('freqs', freqs)

        if ThermCls is None:
            thermostat = Langevin.from_freqs(dt, pes.ndim, *args,
                                             **therm_kwargs)
        else:
            thermostat = ThermCls.from_freqs(dt, pes.ndim, *args,
                                             **therm_kwargs)
        pes.mode = 'rp'
        return cls(q, m_scaled, beta, nbeads, nmodes, pes, propa,
                   thermostat, nmtrans, freqs=freqs)

    @classmethod
    @check_qdims
    @check_mdims
    def RPMD(cls, qcart, m, dt, beta, nbeads,
             pes, atom_groups, constraints,
             maxcycle=100, toll=1e-04, tolm=1e-04,
             arg_list=None, kwarg_list=None,
             ThermCls=None, *args, **kwargs):

        """
        Construct a system of contrained ring-polymers with a RATTLE
        propagator.

        Input:
            qcart (array) ..................... array of Cartesian ring-polymer
                                                coordinate. If the array is
                                                1D, collapsed geometries are
                                                returned.
            m (float or 1d array) ............. array of particle masses
            dt (float) ........................ integration time-step
            beta (float) ...................... 1 / kB T
            nbeads (int) ...................... number of beads
            pes ............................... potential energy surface
            atom_groups (list) ................ sizes of groups of
                                                atomic coordinates subject
                                                to a collective constraint
                                                (assumed to be stored
                                                consecutively)
            constrainsts (list) ............... list of objects of type
                                                HolonomicConstraint or None
            maxcycle (int) .................... maximum number of passes over
                                                shake/rattle cycles to reach
                                                convergence
            toll (float) ...................... convergence parameter for the
                                                'lambda' Lagrange multipliers
            toll (float) ...................... convergence parameter for the
                                                'mu' Lagrange multipliers
            arg_list .......................... a list of additional arguments
                                                required by the constraint fxn
                                                for each atom group
                                                (default None)
            kwarg_list ........................ a list of additional keyword
                                                arguments required by the
                                                constraints
                                                (default None)
            ThermCls .......................... a thermostat class
                                                (default Langevin)
            *args ............................. prng object and state
            **kwargs .......................... thermostat keyword arguments

        """


        nmtrans = FFTNormalModes(qcart.shape[0], nbeads)
        nmodes = nbeads
        q = nmtrans.cart2mats(np.ones((qcart.shape[0], nbeads))*qcart)

        if kwarg_list is None:
            kwarg_list_copy = len(atom_groups)*[dict()]
        elif len(kwarg_list) != len(atom_groups):
            raise ValueError("""
The list of keyword argument dictionaries does not match up
with the list of groups of atoms.
            """)
        else:
            kwarg_list_copy = kwarg_list.copy()

        if arg_list is None:
            arg_list_copy = len(atom_groups)*[tuple()]
        elif len(arg_list) != len(atom_groups):
            raise ValueError("""
The list of positional argument dictionaries does not match up
with the list of groups of atoms.
            """)
        else:
            arg_list_copy = arg_list

        for i, n in enumerate(atom_groups):
            kwarg_list_copy[i]['nmtrans'] = FFTNormalModes(n, nbeads)

        propa = Rattle(dt, atom_groups, constraints, maxcycle, toll, tolm)
        propa.reset_constraints(q, arg_list_copy, kwarg_list_copy)

        therm_kwargs = kwargs.copy()
        freqs = RPFreeModes.RPMD_freqs(beta, nbeads, nmodes)
        therm_kwargs['freqs'] = therm_kwargs.pop('freqs', freqs)

        if ThermCls is None:
            thermostat = Langevin.from_freqs(dt, pes.ndim, *args,
                                             **therm_kwargs)
        else:
            thermostat = ThermCls.from_freqs(dt, pes.ndim, *args,
                                             **therm_kwargs)
        pes.mode = 'rp'
        return cls(q, np.ones_like(q)*m, beta, nbeads, nmodes, pes, propa,
                   thermostat, nmtrans, freqs=freqs)


    def psample(self):
        """
        Draw momenta from the Boltzmann distribution and project
        them onto the tangent subspace of the constraint surface.

        """

        super(ConstrainedPolymers, self).psample()
        return self.propa.rattle(self.p, self.m)

    def force(self, q, f, *args, **kwargs):
        """
        Calculate the force on the normal modes with spring contributions
        included.

        """

        f = super(ConstrainedPolymers, self).force(q, f, *args, **kwargs)
        spring_force = q.copy()
        spring_force *= -1
        spring_force *= self.m
        spring_force *= self.freqs**2
        f += spring_force
        return f

    def nvt_evolve(self, ndt=1, pslc=slice(None), tslc=slice(None),
                   *args, **kwargs):
        """
        Propagate the system subject to thermostatting and a set of
        constraints.

        """

        if tslc == slice(None):
            tslc = pslc

        for i in range(ndt):
            self.p = self.thermostat.kick_0(self.p, self.sdev, slices=tslc)
            self.p = self.propa.rattle(self.p, self.m, slices=tslc)

            self.p, self.q, self.f = self.propa.step(self.p, self.q,
                                                     self.f, self.m,
                                                     self.force,
                                                     slices=pslc,
                                                     *args, **kwargs)

            self.p = self.thermostat.kick_dt(self.p, self.sdev, slices=tslc)
            self.p = self.propa.rattle(self.p, self.m, slices=tslc)

