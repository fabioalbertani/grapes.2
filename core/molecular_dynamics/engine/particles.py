# File: particles.py

"""
Created on Wed May 16 14:57:14 2018

@author: gt317

Defines high-level classes for classical particle propagation.

"""

from __future__ import division, print_function, absolute_import
import sys
import os
root = os.path.split(os.path.split(os.path.abspath(os.curdir))[0])[0]
sys.path.insert(0,root)
from core.molecular_dynamics.engine.thermostats import Langevin
from core.molecular_dynamics.engine.propagators import VelocityVerlet, Rattle
import warnings
import numpy as np

__all__ = ['Particles',]


class Particles(object):
    """
    An ensemble of particles, complete with a potential energy
    surface, a propagator, and a thermostat.

    """

    def __init__(self, q, m, beta, pes, propa, thermostat,
                 p=None, f=None):

        """
        System set-up

        Input:
            q .............................. particle configuration
            m .............................. particle masses
            beta ........................... temperature 1/kB*T
            pes ............................ potential energy surface
                                             (has to have `force' and
                                              `potential' attributes)
            propa .......................... propagator class
            thermostat ..................... a thermostat object with
                                             `sample' and `kick_0/dt'
                                             attributes
            p (optional) ................... initial particle momenta
                                             (by default drawn from the
                                              Boltzmann distribution)
            f (optional) ................... initial particle forces
                                             (by default calculated from
                                              pes.force)
        """

        try:
            self.q = np.asarray(q, dtype=np.float64)
        except ValueError:
            raise TypeError(
"Configuration expected to be an array of floats!\n"
            )
        except:
            raise

        try:
            self.m = np.ones_like(self.q)*np.asarray(m, dtype=np.float64)
        except ValueError:
            raise TypeError("""
Masses expected to be a float or an array of floats broadcastable
to the shape of the configuration array!
""")

        if np.any(self.m < 1.0e-16):
            warnings.warn("""
One or more of the specified masses are close to or below zero.
""")

        if isinstance(beta, (int,float)):
            self.beta = float(beta)
        else:
            raise TypeError(
"Temperature expected to be a float or integer."
            )

        if self.beta < 1.0e-16:
            warnings.warn(
"The specified temperature beta = 1/kb*T is close to or below zero.\n"
            )

        # Boltzmann standard deviations for thermalisation
        self.sdev = np.sqrt(self.m/self.beta)
        self.pes = pes

        # Determine whether the PES describes a coupled system. If the
        # property is left unspecified, play safe and assume coupled.
        try:
            is_bool = isinstance(self.pes.coupled, bool)
        except:
            self.coupled = True
        else:
            if is_bool:
                self.coupled = self.pes.coupled
            else:
                self.coupled = True

        self.thermostat = thermostat
        self.propa = propa

        self.p = np.zeros_like(self.q)
        if p is None:
            self.psample()
        else:
            self.p[...] = p

        self.f = np.zeros_like(self.q)
        #self.f = self.force(self.q, self.f)

    @classmethod
    def default(cls, qcart, m, dt, beta, pes,
                ThermCls=None, *args, **kwargs):
        """
        Construct a system of particles propagated with plain velocity Verlet.

        Input:
            qcart (array) ..................... array of coordinates.
            m (float or 1d array) ............. array of particle masses
            dt (float) ........................ integration time-step
            beta (float) ...................... 1 / kB T
            pes ............................... potential energy surface
            ThermCls .......................... a thermostat class
                                                (default Langevin)
            *args ............................. prng object and state
            **kwargs .......................... thermostat keyword arguments

        Returns:
            an instance of Particles instantiated with a velocity Verlet
            propagator and the specified thermostat

        """

        propa = VelocityVerlet(dt)

        if ThermCls is None:
            thermostat = Langevin(dt, pes.ndim, *args, **kwargs)
        else:
            thermostat = ThermCls(dt, pes.ndim, *args, **kwargs)

        pes.mode = 'cl'

        return cls(qcart, m, beta, pes, propa, thermostat)

    def potential(self, q, *args, **kwargs):
        """
        Calculate the potential energy specified in the PES.

        """

        return self.pes.potential(self.q, *args, **kwargs)

    def force(self, q, f, *args, **kwargs):
        """
        Calculate the normal mode forces.

        """

        return self.pes.force(self.q, self.f, *args, **kwargs)

    def psample(self):
        """
        Resample the energy of all the particles

        """
        self.p = self.thermostat.sample(self.p, self.sdev)

    def nve_evolve(self, ndt=1, slc=slice(None), *args, **kwargs):

        """
        Propagate the system in the absence of thermalisation.

        Input:
            ndt ....................... number of propagation time-steps
                                        (default 1)
            slc (slice or
                 array of slices,
                 optional) ............ specifies the array slice to be
                                        propagated
        """

        for i in range(ndt):
            self.p, self.q, self.f = self.propa.step(self.p, self.q,
                                                     self.f, self.m,
                                                     self.force,
                                                     slices=slc,
                                                     *args, **kwargs)

    def nvt_evolve(self, ndt=1, pslc=slice(None), tslc=slice(None),
                   *args, **kwargs):
        """
        Thermalise the system.

        Input:
            ndt ....................... number of propagation time-steps
                                        (default 1)
            pslc (slice or
                 array of slices,
                 optional) ............ specifies the array slice to be
                                        propagated
            tslc (__"__"__) ........... specifies the array slice to be
                                        thermostatted
        """

        for i in range(ndt):
            self.p = self.thermostat.kick_0(self.p, self.sdev, slices=tslc)
            self.p, self.q, self.f = self.propa.step(self.p, self.q,
                                                     self.f, self.m,
                                                     self.force,
                                                     slices=pslc,
                                                     *args, **kwargs)
            self.p = self.thermostat.kick_dt(self.p, self.sdev, slices=tslc)

    def KE(self):
        """
        Return the kinetic energy of the system. If the PES describes
        a set of non-interacting particles, the output is an array of indi-
        vidual particle/molecule kinetic energies.

        """
        ans = self.p.copy()
        ans **= 2
        ans /= self.m
        ans /= 2

        if not self.coupled:
            ans.shape = (-1, self.pes.ndim)

        return ans.sum(axis=-1)

    def PE(self):
        """
        Return the potential energy of the system. This is a wrapper for
        the particles.pes.potential function.

        """
        return self.potential(self.q)

    def energy(self):
        """
        Return the total energy of the system.

        """
        ans = self.KE()
        ans += self.PE()

        return ans

class ConstrainedParticles(Particles):
    """
    An ensemble of particles, onto which are imposed holonomic constraints,
    complete with a potential energy surface, a propagator and a thermostat.

    """

    @classmethod
    def default(cls, qcart, m, dt, beta, pes, atom_groups, constraints,
                maxcycle=100, toll=1e-04, tolm=1e-04,
                arg_list=None, kwarg_list=None,
                ThermCls=None, *args, **kwargs):
        """
        Construct a system of particles propagated with constraints
        under RATTLE.

        Input:
            qcart (array) ..................... array of coordinates.
            m (float or 1d array) ............. array of particle masses
            dt (float) ........................ integration time-step
            beta (float) ...................... 1 / kB T
            pes ............................... potential energy surface
            atom_groups (list) ................ sizes of groups of
                                                atomic coordinates subject
                                                to a collective constraint
                                                (assumed to be stored
                                                consecutively)
            constrainsts (list) ............... list of objects of type
                                                HolonomicConstraint or None
            maxcycle (int) .................... maximum number of passes over
                                                shake/rattle cycles to reach
                                                convergence
            toll (float) ...................... convergence parameter for the
                                                'lambda' Lagrange multipliers
            toll (float) ...................... convergence parameter for the
                                                'mu' Lagrange multipliers
            arg_list .......................... a list of additional arguments
                                                required by the constraint fxn
                                                for each atom group
                                                (default None)
            kwarg_list ........................ a list of additional keyword
                                                arguments required by the
                                                constraints
                                                (default None)
            ThermCls .......................... a thermostat class
                                                (default Langevin)
            *args ............................. prng object and state
            **kwargs .......................... thermostat keyword arguments

        Returns:
            an instance of Particles instantiated with a RATTLE propagator
            and the specified thermostat

        """

        propa = Rattle(dt, atom_groups, constraints, maxcycle, toll, tolm)
        propa.reset_constraints(qcart, arg_list, kwarg_list)

        if ThermCls is None:
            thermostat = Langevin(dt, pes.ndim, *args, **kwargs)
        else:
            thermostat = ThermCls(dt, pes.ndim, *args, **kwargs)

        pes.mode = 'cl'
        return cls(qcart, m, beta, pes, propa, thermostat)

    def psample(self):
        """
        Draw momenta from the Boltzmann distribution and project
        them onto the tangent subspace of the constraint surface.

        """

        super(ConstrainedParticles, self).psample()
        return self.propa.rattle(self.p, self.m)

    def nvt_evolve(self, ndt=1, pslc=slice(None), tslc=slice(None),
                   *args, **kwargs):
        """
        Propagate the system subject to thermostatting and a set of
        constraints.

        """

        if tslc == slice(None):
            tslc = pslc

        for i in range(ndt):
            self.p = self.thermostat.kick_0(self.p, self.sdev, slices=tslc)
            self.p = self.propa.rattle(self.p, self.m, slices=tslc)

            self.p, self.q, self.f = self.propa.step(self.p, self.q,
                                                     self.f, self.m,
                                                     self.force,
                                                     slices=pslc,
                                                     *args, **kwargs)

            self.p = self.thermostat.kick_dt(self.p, self.sdev, slices=tslc)
            self.p = self.propa.rattle(self.p, self.m, slices=tslc)
