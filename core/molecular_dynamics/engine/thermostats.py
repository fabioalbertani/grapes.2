# File: thermostats.py

"""
Created on Wed May  9 11:57:32 2018

@author: gt317

Thermostats for sampling the canonical ensemble in path-integral
and classical simulations
"""

from __future__ import division, print_function, absolute_import
from abc import ABCMeta, abstractmethod
import numpy as np
from functools import wraps
import sys
import os
root = os.path.dirname(os.path.dirname(__file__))
sys.path.append(str(root))
from ..utils.prng import PRNG
import warnings


__all__ = ['Langevin', 'LangevinG', 'FFL']


# Decorator to process slice specifications
def get_slices(fxn):
    @wraps(fxn)
    def wrapper(self, arr1, arr2, slices=slice(None)):
        try:
            slices = slices.tolist()
        except AttributeError:
            pass
        except:
            raise

        if isinstance(slices, slice):
            slices = [slices]
        else:
            slices = list(slices)

        return fxn(self, arr1, arr2, slices)
    return wrapper


class Thermostat:
    """
    A base class for all thermostats that defines a standard set of
    attributes.

    """
    __metaclass__ = ABCMeta

    def __init__(self, twostep):

        if isinstance(twostep, bool):
            self.twostep = twostep
        else:
            raise TypeError("""
Expecting a Boolean for variable `twostep'.
""")


    @abstractmethod
    def sample(self):
        pass

    @abstractmethod
    def kick_dt(self):
        pass

    def kick_0(self, p, *args, **kwargs):
        if self.twostep:
            return self.kick_dt(p, *args, **kwargs)
        else:
            return p


class Langevin(Thermostat):
    """
    A white-noise Langevin equation thermostat for classical and path-integral
    simulations.

    """

    def __init__(self, dt, ndim, prng=None, state=None, **kwargs):
        """
        Initialise the (local) Langevin thermostat

        Input:
            dt (float) ................. integration time-step
            ndim ....................... number of spatial dimensions
            idum (int or array) ........ seed for the pseudo-random number
                                         generator
            prng (object) .............. a pseudo-random number generator
                                         (overrides default usage of
                                         np.random.RandomState)
            state (tuple, optional) .... initial state of the PRNG (overrides
                                         the seed)
        Keyword arguments
            =================================================
            idum (int or array)          - seed for the PRNG, default 1
            friction (float ot 1D array) - default 0.1/dt
            twostep (bool)               - split the thermostat Liovillian
                                           into one or two factors per
                                           propagation step (default False)

        Return
            An instance of the local Langevin thermostat.

        """
        super(Langevin, self).__init__(kwargs.get('twostep', False))

        self.dt = dt
        self.friction = np.asarray(kwargs.get('friction', 0.1/dt),
                                   dtype=float)
        self.ndim = ndim
        self.c1 = np.exp(-self.dt*np.abs(self.friction)/2)
        if not self.twostep:
            self.c1 **= 2
        self.c2 = np.sqrt(1.0 - self.c1**2)

        if prng is not None:
            self.prng = prng
        else:
            self.prng = PRNG(kwargs.get('idum', 1), state)

    @property
    def dt(self):
        return self.__dt

    @dt.setter
    def dt(self, dt):
        if not isinstance(dt, float):
            raise TypeError(
                "Error: thermostat integration step expected to be a float."
                )
        elif dt < 0:
            raise ValueError(
                "Error: thermostat integration step expected to be positive."
                )

        self.__dt = dt


    @property
    def ndim(self):
        return self.__ndim

    @ndim.setter
    def ndim(self, ndim):

        if not isinstance(ndim, int):
            raise TypeError(
"Error: thermostat expects an integer number of dimensions."
            )
        elif ndim < 1:
            raise ValueError(
"Error: thermostat expects a positive number of dimensions."
            )

        self.__ndim = ndim

    @classmethod
    def from_freqs(cls, dt, ndim, prng=None, state=None, **kwargs):
        """
        Return a propagator class with the friction coefficients calculated
        from the frequency array.

        Input:
            dt (float) ................. integration time-step
            ndim ....................... number of spatial dimensions

            state (tuple, optional) .... initial state of the PRNG (overrides
                                         the seed)
            prng (object) .............. a pseudo-random number generator
                                         (overrides default usage of
                                         np.random.RandomState)

        Keyword arguments:
            ========================================================
            freqs (float or array) ..... normal mode frequencies; if supplied
                                         as an array, this is expected to be
                                         broadcastable to the shape of the
                                         input to Langevin.sample and
                                         Lagevin.kick (default 0)
            tau0 (float or array) ...... thermostat time-constant for
                                         zero-frequency modes. If supplied as
                                         an array, expected to be of the same
                                         shape as freqs[freqs == 0]
                                         (default 10*dt)
            idum (int or array) ........ seed for the pseudo-random number
                                         generator (default 1)
            lambda_ (float, optional) .. multiplicative factor to convert
                                         lambda_*(2*freqs) to frictions
                                         (default 1)
            twostep (bool) ............. indicate whether the associated
                                         Liouvillian is split into two
                                         or one factors per integration step
                                         (default False)
        """

        eps = 1.0e-8
        init_kwargs = kwargs.copy()
        lambda_ = init_kwargs.pop('lambda_', 1)
        if not isinstance(lambda_, (int, float)):
            raise TypeError(
"Langevin thermostats expects a scalar for the value of lambda_"
            )
        elif lambda_ < 0:
            raise ValueError(
"Langevin thermostat expects a positive scalar for lambda_"
            )

        tau0 = init_kwargs.pop('tau0', 10*dt)
        if 'freqs' not in kwargs:
            friction = 1/tau0
        else:
            friction = 2*lambda_*np.abs(init_kwargs.pop('freqs'))
            friction[np.abs(friction) < eps] = 1/tau0
        init_kwargs['friction'] = friction

        return cls(dt, ndim, prng, state, **init_kwargs)

    @get_slices
    def sample(self, p, sdev, slices=slice(None)):
        """
        Re-sample the momenta

        Input:
            p ................ array of momenta (modified in-place)
            sdev ............. standard deviations sqrt(m/beta)
            slices ........... slices of p to be modified

        Output:
            p ................ a view of the resampled momentum array

        """

        p[slices] = self.prng.normal(loc=0.0, scale=sdev[slices])
        return p

    @get_slices
    def kick_dt(self, p, sdev, slices=slice(None)):
        """
        Scale and add a random "kick" to the momenta as part
        of the Verlet-like propagation scheme.

        Input:
            p ................ array of momenta (modified in-place)
            sdev ............. standard deviations sqrt(m/beta)
            slices ........... slices of p to be modified

        Output:
            p ................ a view of the updated momentum array

        """

        xi = (self.c1*p)[slices]
        scale = (self.c2*sdev)[slices]
        xi += self.prng.normal(loc=0.0, scale=scale)
        p[slices] = xi

        return p


class LangevinG(Langevin):
    """
    A implementation of Langevin, where a subset of coordinates are
    propagated under "global" thermostatting. If the input array is
    one-dimensional, it is interpreted as a configuration of a classical
    system and entirely subject to a global Langevin. Otherwise, global
    thermostatting is applied to input_arr[..., 0] in standard PILE-G
    fashion.

    """

    def __init__(self, dt, ndim, prng=None, state=None, **kwargs):
        """
        Initialise the global Langevin thermostat

        Input:
            dt (float) ................. integration time-step
            ndim ....................... number of spatial dimensions
            idum (int or array) ........ seed for the pseudo-random number
                                         generator
            prng (object) .............. a pseudo-random number generator
                                         (overrides default usage of
                                         np.random.RandomState)
            state (tuple, optional) .... initial state of the PRNG (overrides
                                         the seed)
        Keyword arguments
            =================================================
            idum (int or array)          - seed for the PRNG, default 1
            friction (float ot 1D array) - default 0.1/dt
            twostep (bool)               - split the thermostat Liovillian
                                           into one or two factors per
                                           propagation step (default False)

        NOTE: the global friction coefficient c is calculated using
        the first element of the flattened friction array

        """

        super(LangevinG, self).__init__(
                dt, ndim, prng=None, state=None, **kwargs
                )

        self.c01 = self.c1.ravel()[0]
        self.c01 **= 2
        self.c02 = 1.0-self.c01
        self.c03 = self.c01 * self.c02

    @get_slices
    def kick_dt(self, p, sdev, slices=slice(None)):
        """
        Scale and add a randomly "kick" the momenta as part
        of the Verlet-like propagation scheme.

        Input:
            p ................ array of momenta (modified in-place)
            sdev ............. standard deviations sqrt(m/beta)
            slices .............. slices of p to be modified

        Output:
            p ................ a view of the updated momentum array

        """

        ndim = p.ndim
        if ndim == 1:
            # Propagate entire array with global Langevin
            p = self._scale(p, sdev, slices)

        elif ndim > 1:
            # PILE-G
            nslc = len(slices)
            # Generate a new slice that explicitly gives the indices
            # of the modes to be propagated
            if nslc == ndim:
                start, stop, step = slices[-1].indices(p.shape[-1])
                if start == 0:
                    kick_p0 = True
                    # Omit the centroid from new slice (start at step)
                    new_slc = [slices[:-1],slice(step, stop, step)]
                else:
                    new_slc = slices
                    kick_p0 = False
            else:
                new_slc = slices+(ndim-nslc-1)*[slice(None)]+[slice(1, None)]
                kick_p0 = True

            p = super(LangevinG, self).kick_dt(p, sdev, new_slc)
            if kick_p0:
                # Propagate centroids according to global Langevin
                p0 = p[...,0]
                p0[...] = self._scale(p0, sdev[...,0], new_slc[:-1])

        else:
            raise ValueError(
"Error: LangevinG expects a 1- or higher-dimensional input array."
            )

        return p

    def _scale(self, p, sdev, slices=slice(None)):
        """
        Scale the input array of momenta as prescribed by global Langevin

        Input:
            p (array) ......................... momenta array
            sdev .............................. standard deviations
                                                sqrt(m/beta)
            slices ............................ the slice of p to be modified

        Return:
            p is modified in-place

        """

        p_slc, sdev_slc = [arr[slices] for arr in (p, sdev)]

        two_beta_KE = p_slc.copy()
        two_beta_KE /= sdev_slc
        two_beta_KE **= 2
        two_beta_KE = np.sum(two_beta_KE)

        xi = self.prng.normal(size=p_slc.shape)
        xi_sq = np.sum(xi**2)

        alpha_sq = self.c01 + self.c02*xi_sq/two_beta_KE + \
                2*xi[0]*np.sqrt(self.c03/two_beta_KE)

        alpha = np.sqrt(alpha_sq)
        sign = xi[0] + np.sqrt(two_beta_KE*self.c01/self.c02)
        if sign < 0:
            alpha *= -1

        p_slc[...] *= alpha

        return p

class FFL(Langevin):
    """
    The velocity-rescaling fast-forward Langevin (FFL) from
               arXiv:1804.08913v1

    """

    @get_slices
    def kick_dt(self, p, sdev, slices=slice(None)):
        """
        Scale the input array of momenta as prescribed by fast-forward Langevin

        Input:
            p (array) ......................... momenta array
            sdev .............................. standard deviations
                                                sqrt(m/beta)
            slices ............................ the slice of p to be modified

        Return:
            p is modified in-place

        """

        xi = (self.c1*p)[slices]
        scale = (self.c2*sdev)[slices]
        xi += self.prng.normal(loc=0.0, scale=scale)

        xi_s = xi.shape
        if p.ndim == 1:
            # Assume classical configuration
            xi_snew = (-1, self.ndim)
            scale_shape = xi_snew[:-1] + (1,)
        else:
            # PI configuration, last dimension is beads
            xi_snew = (-1, self.ndim, xi_s[-1])
            scale_shape = xi_snew[:-2] + (1, xi_s[-1])

        xi **= 2
        xi.shape = xi_snew
        scale = np.sqrt(np.sum(xi, axis=1))

        # Avoid division by zero
        eps = 1.0e-08
        p_squared = p[slices].copy()
        p_squared **= 2

        p_squared = np.where(p_squared < eps, eps, p_squared)
        scale /= np.sqrt(np.sum(p_squared.reshape(xi_snew), axis=1))

        scale.shape = scale_shape
        p[slices] = np.reshape(p[slices].reshape(xi_snew)*scale, xi_s)

        return p
