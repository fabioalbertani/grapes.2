#!/bin/python
#File: core.grapes_utils.module_selection.py


def SelectRunInterface(args, CalcDetails, Molecule=None, J=None):
    '''
    Allows to select the run interface needed
    '''
    if CalcDetails['interface']=='QChem':
        from core.qchemistry_modules.qchem_run_interface import QCRunInterface
        args.RegisterModules(QCRunInterface)
        args.Parse('run')
        QCRunInterface.ParseOptions(args)
        return QCRunInterface(args, CalcDetails, Molecule, J)

    elif CalcDetails['interface']=='Analytical':
        from core.qchemistry_modules.analytical_run_interface import AnalyticalRunInterface
        return AnalyticalRunInterface(args, CalcDetails)



def SelectDataType(args):
    '''
    Allows to select the correct data type and initiate it

    Args:
        args:   [GlobalOption] GlobalOptions object
    Returns:
        Data:   [Structure] Data structure type
    '''
    if args.main.InterpolationMethod=="GaussianProcesses":
        if args.main.MeshType=='Delaunay':
            from core.data_management.delaunay_structure import DelaunayStructure
            Data = DelaunayStructure(args)
        elif args.main.MeshType=='Points':
            from core.data_management.points_structure import PointsStructure
            Data = PointsStructure(args)

    elif args.main.InterpolationMethod=="CubicSpline":
        from core.data_management.points_structure import PointsStructure
        Data = PointsStructure(args)

    elif args.main.InterpolationMethod=="Barycentric":
        if args.D==1:
            raise Exception('Delaunay 1D is not defined (Cubic spline is more appropriate)\n')
        else:
            from core.data_management.delaunay_structure import DelaunayStructure
            Data = DelaunayStructure(args)

    else:
        raise Exception('The interpolation selected is not available')

    return Data


def SelectSamplingType(args):
    '''
    Allows to select the correct sample type and initiate it

    Args:
        args:   [GlobalOption] GlobalOptions object
    Returns:
        Sample:   [ModuleObject] sampling module
    '''
    from core.geometry import sampling

    if args.main.SamplingMethod=='Random':
        Sample = sampling.Random

    elif args.main.SamplingMethod=='FramedGrid':
        Sample = sampling.FramedGrid

    elif args.main.SamplingMethod=='FramedRandom':
        Sample = sampling.FramedRandom

    elif args.main.SamplingMethod=='FramedGaussian':
        Sample = sampling.FramedGaussian

    else:
        raise Exception('The interpolation selected is not available')

    return Sample



def SelectSamplingMinType(args, Data):
    '''
    Allows to select the correct minimalistic sampling module and initiate it

    Args:
        args:   [GlobalOption] GlobalOptions object
        Data:   [Structure] Data structure type

    Returns:
        Sample:   [ModuleObject] sampling module
    '''
    from core.geometry.minimalistic_sampling import MinimalisticSampling

    return MinimalisticSampling(args, Data)



def SelectInterpolationType(args):

        if args.main.InterpolationMethod=='Barycentric':
            from core.interpolation.barycentric import Barycentric
            args.RegisterModules(Barycentric)
            args.Parse('interpolation')
            Barycentric.ParseOptions(args)
            return Barycentric(args)

        elif args.main.InterpolationMethod=='GaussianProcesses':
            from core.interpolation.gaussian_processes_interface\
                 import GaussianProcessesInterface
            args.RegisterModules(GaussianProcessesInterface)
            args.Parse('interpolation')
            GaussianProcessesInterface.ParseOptions(args)
            return GaussianProcessesInterface(args)

        else:
            raise Exception('Interpolation method not implemented in PointClass')




def SelectTestType(args, MOLECULE):
    '''
    Allows to select the correct test types and initiate them

    Args:
        args:   [GlobalOption] GlobalOptions object
    Returns:
        Test:   [ModuleObjec] test module
    '''
    Tests = []

    for test in args.main.TestingMethods:
        if test=='PointsVariance':
            from core.testing.point_metric import PointMetric
            args.RegisterModules(PointMetric)
            args.Parse('testing')
            PointMetric.ParseOptions(args)
            Tests += [PointMetric(args, method='Variance')]

        elif test=='PointsError':
            from core.testing.point_metric import PointMetric
            args.RegisterModules(PointMetric)
            args.Parse('testing')
            PointMetric.ParseOptions(args)
            Tests += [PointMetric(args, method='Error')]

        elif test=='DissociationEnergy':
            from core.testing.dissociation_energy import DissociationEnergy
            args.RegisterModules(DissociationEnergy)
            args.Parse('testing')
            DissociationEnergy.ParseOptions(args)
            Tests += [DissociationEnergy(args)]

        elif test=='ExtremaRefining':
            from core.testing.extrema_refining import ExtremaRefining
            args.RegisterModules(ExtremaRefining)
            args.Parse('testing')
            ExtremaRefining.ParseOptions(args)
            Tests += [ExtremaRefining(args)]

        elif test=='RMSD':
            from core.testing.rmse import RMSE
            args.RegisterModules(RMSE)
            args.Parse('testing')
            RMSD.ParseOptions(args)
            Tests += [RMSD(args, MOLECULE)]

        else:
            raise Exception('The selected test type is not available ('+\
                            test+')\n')

    return Tests



def SelectPlotTypes(args):
    '''
    Allows to select the correct modules to plot data and initiate them

    Args:
        args:   [GlobalOption] GlobalOptions object

    Returns:
        Plot:       [PlotObject] Plot any sort of surfaces, contours, samples from core. GPs ...
    '''
    from core.plotting.plot_interface import PlotInterface
    args.RegisterModules(PlotInterface)
    args.Parse('plotting')
    PlotInterface.ParseOptions(args)
    return PlotInterface(args)




def SelectValidationType(args):
    '''
    Allows to select the validation type needed
    '''
    Validators = []
    
    for validate in args.main.ValidationMethods:
        if validate=='TestSet':
            from core.validation.test_set import TestSet
            args.RegisterModules(TestSet)
            args.Parse('validation')
            TestSet.ParseOptions(args)
            Validators += [TestSet(args)]
 

        else:
            raise Exception('The selected validation type is not available ('+\
                            validate+')\n')

    return Validators



def SelectSpectraType(args, MethodDetails):

    if MethodDetails['system'] == 'diatomic': #####
        from core.spectra_calculation.diatomic_main import DiatomicMain

        args.RegisterModules(DiatomicMain)
        args.Parse('spectra')
        DiatomicMain.ParseOptions(args)

        return DiatomicMain(args)



