#!/bin/python
#File: core.grapes_utils.progress_bar.py

#Displays progress bar for a calculation

from sys import stdout

class DisplayBar:
    '''
    Displays a progress bar that fits the width of the terminal window
    '''

    def __init__(self, Total, ColsShift=50, ProgressSymbol=u"\u2588", extrastr=' '):
        from os import popen

        self.ProgressSymbol = '|' #ProgressSymbol
        self.ProgressType = extrastr

        self.Total = float(Total)

        rows, cols = popen('stty size', 'r').read().split()

        self.TerminalCols = int(cols) - ColsShift
        self.TerminalCols=max(20,self.TerminalCols)


    def Show(self, n):

        progress = n/self.Total


        if n!=(self.Total-1):

            ProgressDisplay = self.TerminalCols*progress
            
            try:
                print('\t'+self.ProgressSymbol*int(ProgressDisplay)+" %.2f %% %s" %(100.0*progress, self.ProgressType))
                stdout.write("\033[F")
            except:
                print('\t'+"%.2f %% %s" %(100.0*progress, self.ProgressType))
                stdout.write("\033[F")
                pass

        else:

            ProgressDisplay = self.TerminalCols
            try: 
                print('\t'+ self.ProgressSymbol*int(ProgressDisplay)+ '100.00 %\n')
            except:
                pass        


