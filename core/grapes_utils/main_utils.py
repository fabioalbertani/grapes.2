#!/bin/python
#File: core.grapes_utils.main_utils.py

import sys, os
from getpass import getuser
from subprocess import call
from datetime import datetime


def MoveToWorkDirectory(subfolder=None, partial=True):
    '''
    Creates and moves to a working directory in the scratch space. If no subfolder is provided
    calculations are run into /$SCRATCH/$USER    
    '''
    scratch = '/scratch/'
    if subfolder:
        try:
            call("mkdir -p "+scratch+getuser()+'/'+subfolder, shell=True)        
        except OSError:
            raise Exception('\tscratch has no writing permissions (or does not exist) ...'\
                            '\ttry changing /scratch* to /sharedscratch*\n')

        os.chdir(scratch+getuser()+'/'+subfolder)
        print('\n\tMoving to working directory: '+str(os.getcwd()))
        if partial:
            return scratch+getuser()
        else:
            return scratch+getuser()+'/'+subfolder

    else:
        os.chidr(scratch+getuser())
        print('\n\tMoving to working directory: '+str(os.getcwd()))
        return scratch+getuser()


def MoveFromWorkDirectory(path):
    os.chdir(path)



def GetInputFileName(CDPath, args):
    '''
    Checks that the input file provided is acceptable and if not 
    asks the user to provide the path to the file
    '''
    if len(args)>1:
        if '=' in args[1]:
            InputFileName = CDPath+'/InputFiles/'+args[1].split('=')[1]+'.input'
            if not os.path.isfile(InputFileName): 
                print('\Provided input file: '+InputFileName)
                raise RuntimeError('The input file specfied was not found: '+InputFileName)

        elif args[1]=='-i' or args[1]=='--InputFile':
            InputFileName = CDPath+'/InputFiles/'+args[2]+'.input'
            if not os.path.isfile(InputFileName):
                print('\Provided input file: '+InputFileName)
                raise RuntimeError('The input file specfied was not found: '+InputFileName)
        else:
            InputFileName = CDPath+'/InputFiles/'+args[1]+'.input'
            decision = raw_input('Did you want to use '+InputFileName+' as an input file? '\
                                 ' (Yes/No)\n')
            if decision=='No':
                InputFileName=None
                print('Default values will be used')

    else:
        #Checks if a simple filename i.e. CxHyOz.input exists 
        System = raw_input('No input files provided: What is the system?\n')
        InputFileName = CDPath+'/InputFiles/'+System+'.input'
        if os.path.isfile(InputFileName):
            decision = raw_input('Input file found: '+InputFileName+' Would you like to use it? '\
                                 '(Yes/No)\n')
            if decision=='No':
                InputFileName=None
                print('Default values will be used')
        else:
            InputFileName=None
            print('Default values will be used')

    return InputFileName



def MoveDataFiles(WDPath, CDPath, label):
    '''
    Move the data files produced in the working directory to the standard 
    '''
    print('\tMoving files from '+WDPath+'/'+label+' ...\n')
    NULL = open(os.devnull, 'w')
    call('mv '+WDPath+'/'+label+'/*.log '+CDPath+'/SavedData/'+label+'/.',
         shell=True, stderr=NULL)
    call('mv '+WDPath+'/'+label+'/*.pdf '+CDPath+'/SavedData/'+label+'/.',
         shell=True, stderr=NULL)
    call('mv '+WDPath+'/'+label+'/*.xyz '+CDPath+'/SavedData/'+label+'/.',
         shell=True, stderr=NULL)
    call('mv '+CDPath+'/summary '+CDPath+'/SavedData/'+label+'/.',
         shell=True, stderr=NULL)

    for datfile in [os.path.join(path, file) for (path, dirs, files) in\
            os.walk(WDPath+'/GRAPESData/SavedData/'+label) for file in files]:
        if os.path.getsize(datfile)<1073741824: #1GB
            call('cp '+datfile+' '+CDPath+'/SavedData/'+label+'/.',
                 shell=True, stderr=NULL)



class SuperPrint:
    '''
    Allows to print all the statment in a much more compact form
    both the standard print (which can be removed) and the given
    output file write will be performed
    '''
    def __init__(self, filename='summary', stdout=True, twofile=False):
        self.file = open(filename, 'a')
        if twofile:
            self.file2 = open('summary', 'a')
        self.stdout = stdout

    def __call__(self, obj, stdout=False):
        '''
        stdout allows to overwrite the self.stdout statment
        '''
        if self.stdout or stdout:
            print(obj)
        self.file.write('\n@'+str(datetime.now())+'\n')
        self.file.write(obj)
        if hasattr(self, 'file2'):
            self.file2.write('\n@'+str(datetime.now())+'\n')
            self.file2.write(obj)



class HiddenPrints:
    '''
    Redirects most of the stdout to allow a cleaner output (for example
    when running QChem)
    '''
    def __init__(self, filename):
        self.filename = filename

    def __enter__(self):
        self._original_stdout = sys.stdout
        sys.stdout = open(self.filename, 'a')

    def __exit__(self, exc_type, exc_val, exc_tb):
        sys.stdout.close()
        sys.stdout = self._original_stdout





def GetTypeDetails(Type):
    '''Expected format: hf@6-31g or cc:ccsdt@cc-pVDZ etc...
       For non QChem calculations: GRAPES:minSampling'''

    if Type is None:
        '''When the difference type is not required this string will be passed'''
        return False

    else:
        spl_main = Type.split('--')
        Details = {
            'interface': spl_main[0].split(':')[0],
            'Type': spl_main[0].split(':')[1],
            'label': Type
                  }


        if Details['interface']=='Analytical':
            Details['potential'] = spl_main[1]
            return Details

        else:
            spl = spl_main[1].split('@')

            if not ':' in spl[0]:
                Details['exchange'] = spl[0]
                Details['correlation'] = None
            else:
                Details['exchange'] = spl[0].split(':')[0]
                Details['correlation'] = spl[0].split(':')[1]
            Details['basis_set'] = spl[1]

            return Details



def CompareDetails(Details, Details2):
    '''Checks if two calculation details dictionary have the
       same exchange, correlation and basis set'''
    while True:
        for x in ['exchange', 'correlation', 'basis_set']:
            if Details[x]!=Details2[x]:
                return False

        return True

def MethodCalculationDetails(Details, Types):
    '''
    Allows to load the calculation details of the corresponding method details
    to handle properly the calcualtions needed in method runs
    '''
    for CalcDetails in [GetTypeDetails(x) for x in Types]:
        if CompareDetails(Details, CalcDetails):
            return CalcDetails


def GetLabel(Details):
    '''label for printing'''
    label = Details['exchange']
    if not Details['correlation'] is None:
        label+=' with correlation '+Details['correlation']
    return label+' @ '+Details['basis_set']


def GetDataLabel(Details):
    '''label for dat files'''
    label = Details['exchange']
    if not Details['correlation'] is None: 
        label+=':'+Details['correlation']
    return label+'@'+Details['basis_set']






class SequenceOrganiser:
    '''
    This class allows to run calculations in a specific order

    i.e. cc should always be run after hf because it is a higher level of theory and so on
         it would not be useful to have the difference betwen hf and cc but it is between cc and hf

    nb: dipole calculations are embedded in hf,dft ... and minSampling too
        for thisreason they do not appear here (they are run differently)
    '''

    def __init__(self, args):
        '''
        This will initiate all the types as booleans to run or skip them
        types which are not included will stop the program immediately to save run time
        '''

        self.args = args

        self.ComputeType = [] #{'dft': False, 'hf': False, 'cc': False, 'hessian': False}
        self.ProduceType = []
        #self.Embedded = {'minSampling': False}

        for CalcType in args['Main'].CalcTypes if isinstance(args['Main'].CalcTypes,list)\
                                               else [args['Main'].CalcTypes]:
            CalcDetails = GetTypeDetails(CalcType)

            if CalcDetails['interface']=='QChem':
                CalcDetails['Difference'] = GetTypeDetails(
                                            args['Main'].CalcTypeDifference[CalcType])
                self.ComputeType.append(CalcDetails)

            elif CalcDetails['interface']=='GRAPES':
                self.ProduceType.append(CalcDetails)

        self.ReadOldPoints = args['Main'].ReadOldPoints
        self.UseOldPoints = args['Main'].UseOldPoints
        self.UseOldImprovement = False
        self.ReadFile = args['Main'].ReadFile


    def CheckMethod(self, CalcDetails):
        '''
        Defines where points are read from based on the list of types/basis sets requested
        '''

        if self.args['Main'].ReadData[CalcDetails['label']] and\
           not self.args['Main'].ImproveReadData:
            self.ImproveMesh = False

        elif CalcDetails['Difference']:

            self.UseOldPoints = True
            self.UseOldImprovement = True


        else:
            self.ImproveMesh = True


    def Skip(self):
        self.ImproveMesh = False

