module random

implicit none

integer, save:: seed=1
integer, parameter :: dp = selected_real_kind(15,307)
private:: seed, dp


contains

function gasdev() result(num)
real(dp):: num

num=gasdev_in(seed)

end function gasdev




real(8) function gasdev_in(idum)
integer :: idum

integer :: iset
real(8) :: fac, gset, rsq, v1, v2
save iset, gset
data iset/0/
integer :: init=0

if (init == 0) then
    init = 1
    idum = -abs(idum)
end if

if (idum.lt.0) iset = 0

if (iset.eq.0) then
1   v1 = 2.*ran2(idum)-1
    v2 = 2.*ran2(idum)-1
    rsq = v1**2+v2**2
    if(rsq.ge.1..or.rsq.eq.0.) goto 1
    fac = sqrt(-2.*log(rsq)/rsq)
    gset = v1*fac
    gasdev_in = v2*fac
    iset = 1
else
    gasdev_in = gset !second G variable created
    iset = 0
end if

return

end function gasdev_in

real(8) function ran2(idum)
integer :: idum

integer :: im1, im2, imm1, ia1, ia2, iq1, iq2, ir1, ir2, ntab, ndiv
real(8) :: am, eps, rnmx
parameter (im1=2147483563, im2=2147483399, am=1./im1, imm1=im1-1, ia1=40014, &
ia2=40692, iq1=53668, iq2=52774, ir1=12211, ir2=3791, ntab=32, &
ndiv=1+imm1/ntab, eps=1.20e-7, rnmx=1.-eps)
integer :: idum2, j, k, iv(ntab), iy
save iv, iy, idum2
data idum2 /123456789/, iv /ntab*0/, iy /0/
integer :: init=0

if (init == 0) then
    init = 1
    idum = -abs(idum)
end if

if (idum.le.0) then
    idum=max(-idum,1)
    idum2=idum
    do j = ntab+8, 1, -1
        k = idum/iq1
        idum = ia1*(idum-k*iq1)-k*ir1
        if (idum.lt.0) idum=idum+im1
        if (j.le.ntab) iv(j)=idum
    end do
    iy = iv(1)
end if

!idum changes between calls (but is saved) must not change

k = idum/iq1
idum = ia1*(idum-k*iq1)-k*ir1
if (idum.lt.0) idum=idum+im1
k = idum2/iq2
idum2=ia2*(idum2-k*iq2)-k*ir2
if (idum2.lt.0) idum2=idum2+im2
j = 1+iy/ndiv
iy = iv(j)-idum2
iv(j) = idum
if(iy.lt.1) iy = iy+imm1
ran2 = min(am*iy, rnmx) !ensures value is in interval
return

end function ran2

end module random

