#!/usr/bin/python
#File: core.auxiliary.__init__.py

'''
Auxiliary functions for GRAPES main program functions
'''

__all__ = ['woodbury']
