#!/bin/python
#File: core.auxiliary.woodbury.py

from copy import copy
from functools import reduce
from numpy import eye, zeros, delete, matmul
from numpy.linalg import inv


def get_sub_training_set(X, y, index=None):
    '''
    Updates the training set by removing the indexed datum 
    or, if not provided, the first one
    '''
    _X = copy(X)
    _y = copy(y)

    if not index is None:
        _X[[0,index],:] = _X[[index,0],:]
        _y[[0,index],:] = _y[[index,0],:]

    return _X[1:,:], _y[1:,:]



def get_subinverse_matrix(A, Ai=None, index=None):
    '''
    Given matrix A and A(-1), this function changes the matrices to 
    B = A(index <-> 0) and B(-1) = A(-1)(index <-> 0)

    and then returns the sub-inverse matrix if the 0th element was 
    removed using the Shermann-Morisson formula (special case of the 
    woodbury formula)

    if no inverse matrix is provided it will be calculated on the fly
    if no index is provided the index will be assumed to be 0 and no
    rearragement of the matrix A will be done
    '''
    B = copy(A)
    if Ai is None:
        if not index is None:
            B[[0,index],:] = B[[index,0],:]
            B[:,[0,index]] = B[:,[index,0]]
            return B, inv(B[1:,1:])

    else:
        Bi = copy(Ai)

    if not index is None:
        B[[0,index],:] = B[[index,0],:]
        B[:,[0,index]] = B[:,[index,0]]
        Bi[[0,index],:] = Bi[[index,0],:]
        Bi[:,[0,index]] = Bi[:,[index,0]]

    U, V = get_UV(B)
    I2 = eye(2) - reduce(matmul, [V, Bi, U])
    A_Woodbury = Bi + reduce(matmul, [Bi, U, inv(I2), V, Bi])

    B = B[1:,1:]
    Bi = A_Woodbury[1:,1:]
    return B, Bi
 
def get_UV(A):
    '''
    returns U,V matrices such that:

              | k   0 |
    A - UV =  |       | 
              | 0   K-|

    the factor k is only required to be non-zero for
    the Shermann-Morisson formula to be applicable
    '''
    
    V = zeros((2, len(A[:,0])))
    V[1,0] = 1.0
    V[0,1:] = A[0,1:]

    U = zeros((len(A[:,0]),2))
    U[0,0] = 1.0
    U[1:,1] = A[1:,0]

    return U, V 



def get_superinverse_matrix(A, k, s):
    '''
    Given matrix A( an inverse covariance matrix), this function calculates the new inverse 
    covariance matrix as if a single data point was added (i.e. the inverse of the covariance 
    matrix as to be calculated again using the woodbury identity

    the covariance vector (k) as well as the variance (s) at the point have to be 
    provided too
    '''
    if A.shape[0] == A.shape[1]:
        shp = tuple([n+1 for n in list(A.shape)]) 
        n = A.shape[0]
    else:
        raise RuntimeError('Woodbury identity cannot be performed on rectangular'\
                           'matrix `core.auxiliary.woodbury')

    B = zeros(shp)
    B[:n,:n] = A + reduce(matmul, [A, k, k.T, A])/s
    B[0:n,n] = -matmul(A, k).flatten()/s
    B[n,0:n] = -matmul(k.T, A).flatten()/s
    B[n,n] = 1.0/s

    return B
