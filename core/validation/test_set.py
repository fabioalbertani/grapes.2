#!/usr/bin/python
#File: core.validation.test_set.py

import core.grapes_utils.module_selection as MODULE
import core.grapes_utils.main_utils as UTILS

from core.testing.rmse import RMSE
from core.plotting.mpl_producer import MPLProducer


class TestSet:
    '''
    Main plotting interface allows to register various options which are specific
    to plotting and also selects, according to the data provided, the best way to plot 
    the data
    '''
 
    @staticmethod
    def RegisterOptions(args):
        m = args.RegisterModule('validation')
   
        args.Register(m, '--ValidationTypes', default=None)
        args.Register(m, '--ValidationElements', default=None, 
            help='Defines which elements are used for the validation test')

        args.Register(m, '--ValidatePure', default=False, action='store_true',
            help='Validates the GPs as differences rather than as the total value')

    @staticmethod
    def ParseOptions(options):

        options.validation.set('ValidationTypes', [s for s in\
                            options.validation('ValidationTypes').split()])
        
        if options.validation('ValidationElements') is not None: 
            options.validation.set('ValidationElements', 
                options.validation('ValidationElements').split())

    

    def __init__(self, args):

        self.args = args
        self.Moduleargs = args.validation

        self.SP = UTILS.SuperPrint()



    def __call__(self, INTERPOLATION, MOLECULE): 
        '''
        Calls for validation tests
        '''
    
        VALIDATION = RMSE(self.args, MOLECULE, validation=True)
        PLOT = MPLProducer(self.args, None)

        for ValidationType in self.Moduleargs.ValidationTypes if isinstance(
            self.Moduleargs.ValidationTypes, list) else [self.Moduleargs.ValidationTypes]:

            ValidationDetails = UTILS.GetTypeDetails(ValidationType)

            Template = '''
            Validation for '''+UTILS.GetLabel(ValidationDetails)+'''

                Elements:   ''' + ' '.join(self.Moduleargs.ValidationElements)

            self.SP(Template)


            for el in self.Moduleargs.ValidationElements if isinstance(\
                      self.Moduleargs.ValidationElements,list) else\
                      [self.Moduleargs.ValidationElements]:
    
    
                ValidationData = VALIDATION.Test(
                    INTERPOLATION, ValidationDetails, 
                    el, self.Moduleargs.ValidatePure, 
                    fromfile=True
                                                ) 


                PLOT.PlotValidation(
                    ValidationDetails, 
                    el, self.Moduleargs.ValidatePure,
                    ValidationData
                                   )


