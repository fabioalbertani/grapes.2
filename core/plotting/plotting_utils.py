#!/bin/python
#File: core.plotting.plotting_utils.py

import numpy as np

import matplotlib.pyplot as MPL
from mpl_toolkits.mplot3d import axes3d, Axes3D
from matplotlib import cm, ticker

import core.grapes_utils.main_utils as UTILS

import abc

class UtilsMPL:
    '''
    Defines classes to set and save matplotlib figures for different purposes
    through the `core.plotting.plot_interface` module 
    '''
    __metaclass__ = abc.ABCMeta

    def __init__(self):
        '''
        Defines a few colours to keep consistency throughout the plots
        '''

        self.sizeScatter = 1
        self.colourScatter = 'b'
        self.cmap = cm.get_cmap('viridis')

        MPL.rc('text', usetex=True)
        MPL.rc('font', family='serif')

        self.SP = UTILS.SuperPrint()


    def new_canvas(self, logscale=True, coordinates=True, labels=None, Volume=False, D=2):
        '''
        Creates a new instance of a figure
        '''

        if D==2:
            if Volume:
                self.figure = MPL.figure()
                self.axes = MPL.axes(projection='3d') #Axes3D(self.figure)
            else:
                self.figure, self.axes = MPL.subplots()

            if logscale:
                self.axes.set_yscale('log')
                self.log = True

            if coordinates:
                self.axes.set_xlabel(r'$q_1\ [\mathrm{\AA}]$', fontsize=22)
                self.axes.set_ylabel(r'$q_2\ [\mathrm{\AA}]$', fontsize=22)

            else:
                if labels is None:
                    self.axes.set_xlabel('x', fontsize=22)
                    self.axes.set_ylabel('y', fontsize=22)
                else:
                    self.axes.set_xlabel(labels[0], fontsize=15)
                    self.axes.set_ylabel(labels[1], fontsize=15)
                    if len(labels)>2: self.axes.set_zlabel(labels[2], fontsize=15)


            self.axes.tick_params(axis='both', labelsize=18)
            self.figure.subplots_adjust(bottom=0.22, left=0.2)#, right=0.2)

        elif D==1:        

            self.figure, self.axes = MPL.subplots()
            if labels is None:
                self.axes.set_xlabel(r'$q_1\ [\mathrm{\AA}]$', fontsize=22)
            else:
                self.axes.set_xlabel(labels[0], fontsize=22)
                self.axes.set_ylabel(labels[1], fontsize=22)

            self.axes.tick_params(axis='both', labelsize=18)
            self.figure.subplots_adjust(bottom=0.2, left=0.14)



    def produce_canvas(self, options, format='pdf'):
        '''
        Produces plot after the relevant objects to be plotted have been passed to 
        the self.axes/self.figure instances
        '''
    
        if not options['title'] is None:
            self.axes.set_title(options['title'])

        if format=='pdf': options['filename'] += '.pdf'

        try:
            if not options['filename'] is None:
                self.figure.savefig(options['filename'], format=format)
            else:
                self.figure.savefig('tempplot.pdf', format=format)

        except:
            print('\nThe figure could not be saved\n')
            MPL.show()
        




class UtilsVMD:
    '''
    Defines classes to set and save VMD files for different purposes
    through the `core.plotting.plot_interface`
    '''
    __metaclass__ = abc.ABCMeta

    def __init__(self, AtomsList, sampling_plot):
        
        self.AtomsList = AtomsList
        self.sampling_plot = sampling_plot 

        self.SP = UTILS.SuperPrint()


    def new_canvas(self, filename='none', format='xyz', labels=None):
        '''Initialise files to write xyz/tcl as well as MPL objects to plot some guidelines'''

        #------------------VMD
        self.f = open(filename+'.'+format, 'w')
        self.scriptf = open(filename+'.tcl', 'w')
        #self.scriptf.write(VMDTemplates['background'])

        #------------------MPL
        if not self.sampling_plot:
            self.figure, self.axes = MPL.subplots()
            self.axes.set_xlabel(r'$q_{\mathrm{NME}}$', fontsize=22)
            self.axes.set_ylabel(r'$h_{ij}$', fontsize=22)
            self.axes.tick_params(axis='both', labelsize=18)
            self.figure.subplots_adjust(bottom=0.2, left=0.14)


            self.figure3d = MPL.figure()
            self.axes3d = MPL.axes(projection='3d')
            if not labels is None:
                self.axes3d.set_xlabel(labels[0], fontsize=22)
                self.axes3d.set_ylabel(labels[1], fontsize=22)
                self.axes3d.set_zlabel(labels[2], fontsize=22)



    def produce_canvas(self, options=None, format='gif'):
        '''
        Produces molecular movies
        '''
        #------------------MPL
        if not self.sampling_plot:
            leg = self.axes.legend(loc='upper left')
            self.figure.savefig(self.f.name[:-4]+'.pdf', format='pdf')        

            self.axes3d.view_init(30,120)
            self.figure3d.savefig(self.f.name[:-4]+'_arrows.pdf', format='pdf')
     

        #------------------VMD
        self.f.close()
        self.scriptf.close()
        #call('vmd -dispdev text '+self.f.name+' < '+self.scriptf.name+' [&> VMDlog &]')





def MeshGrid(args, shift):
    '''
    Creates a 1/2D meshgrid to produce line or surface plots
    '''
    if len(args.CoordinatesPlot)==1: #1D plot
        R = {
        'Q0': np.linspace(args.GridLimitsPlot[2*args.CoordinatesPlot[0]]+shift,
                          args.GridLimitsPlot[2*args.CoordinatesPlot[0]+1], args.NSamplingPlot)
            }
        return R


    elif len(args.CoordinatesPlot)==2: #2D plot
        R = {
        'Q0': np.linspace(args.GridLimitsPlot[2*args.CoordinatesPlot[0]]+shift,
                          args.GridLimitsPlot[2*args.CoordinatesPlot[0]+1], args.NSamplingPlot),
        'Q1': np.linspace(args.GridLimitsPlot[2*args.CoordinatesPlot[1]]+shift,
                          args.GridLimitsPlot[2*args.CoordinatesPlot[1]+1], args.NSamplingPlot)
            }

        R['X'], R['Y'] = np.meshgrid(R['Q0'], R['Q1'])
        return R
