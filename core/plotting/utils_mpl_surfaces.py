#!/bin/python
#File: core.plotting.utils_mpl_surfaces.py

import numpy as np

import matplotlib.pyplot as MPL
from mpl_toolkits.mplot3d import axes3d, Axes3D
from matplotlib import cm, ticker

import core.grapes_utils.main_utils as UTILS
import core.plotting.plotting_utils as utils

class MPLSurfaces(utils.UtilsMPL):

    def __init__(self):

        self.opacitySurface = 0.9
        self.colourEnergyLevels = ['purple', 'blue', 'green', 'yellow', 'orange', 'red',
                                   'black', 'pink', 'red', 'blue']

        super(MPLSurfaces, self).__init__()



    def Scatter1D(self, Data):
        '''
        Scatter plots for 1/2/3-D inputs
        '''

        self.axes.scatter(Data['X'], Data['Z'],
                          marker='o', s=self.sizeScatter, c=self.colourScatter,
                          edgecolors='k', linewidth='1'
                         )


    def Scatter2D(self, Data):

        self.axes.set_xlim(min(Data['X']), max(Data['X']))
        self.axes.set_ylim(min(Data['Y']), max(Data['Y']))

        self.axes.scatter(Data['X'], Data['Y'],
                          marker='o', s=self.sizeScatter, c=self.colourScatter,
                          edgecolors='k', linewidth='1'
                         )

    def Scatter3D(self, Data):
        '''
        Creates a 2D Histogram which gives a rough idea of what the surface looks like
        '''
        from numpy import histogram2d

        self.axes.set_xlim(min(Data['X']), max(Data['X']))
        self.axes.set_ylim(min(Data['Y']), max(Data['Y']))

        self.axes.scatter(Data['X'], Data['Y'],
                          marker='o', s=self.sizeScatter, c=Data['Z'], cmap=self.CSContour,
                          edgecolors='k', linewidth='1'
                         )




    def Line(self, Data, new_line=False):

        if 'Zerr' in Data.keys():
            self.axes.fill_between(
                Data['X'], Data['Z']+Data['Zerr'], Data['Z']-Data['Zerr'],
                facecolor = 'grey', alpha=0.5
                                  )

        self.axes.set_ylim(Data['Zmin'], Data['Zmax'])

        self.axes.plot(
            Data['X'], Data['Z'], 
            color='purple' if new_line else 'red', 
            linewidth=1
                      )


    def SimpleLine(self, Data):

        self.axes.set_xlim(min(Data['X']), max(Data['X']))
        self.axes.plot(Data['X'], Data['Z'], 'r-', linewidth=0.1
                      )



    def SimpleSurface(self, Data):

        self.axes.plot_surface(Data['X'], Data['Y'], Data['Z'],
                               rstride=1, cstride=1,
                               cmap=self.cmap, edgecolor='none'
                              )

        self.axes.grid(False)



    def Contour2D(self, Data, levels=20, log=False, logticks=False, shift=0.01):
        '''
        Contour plot
        '''
        if log:
            Data['Z'] = abs(Data['Z'])
            Data['Zmax'] = 1.0
            Data['Zmin'] = 1e-8

            locator = ticker.LogLocator()
            levelsP = np.logspace(Data['Zmin'], Data['Zmax'], levels)

            CP = self.axes.contourf(Data['X'], Data['Y'], Data['Z'], levels=levelsP,
                                locator = locator,
                                cmap=self.cmap,
                                vmin=Data['Zmin'], vmax=Data['Zmax']
                               )

            cbar = MPL.colorbar(CP, ticks=['-8', '-6', '-4', '-2', '0'])
            cbar.ax.set_yticklabels([r'$10^{-8}$', '-6', '-4', '-2', '0'])
            cbar.ax.tick_params(labelsize=15)
            cbar.ax.set_ylabel(Data['cbartitle'], fontsize=20)                        


        else:
            locator = ticker.MaxNLocator()

            if not 'Zmin' in Data.keys():
                Data['Zmin'] = min([min(Data['Z'][:,i]) for i in range(Data['Z'].shape[0])])
                Data['Zmax'] = max([max(Data['Z'][:,i]) for i in range(Data['Z'].shape[0])])
            levelsP = np.linspace(Data['Zmin'], Data['Zmax'], levels)

            CP = self.axes.contourf(Data['X'], Data['Y'], Data['Z'], levels=levelsP,
                                    locator = locator,
                                    cmap=self.cmap, extend='both',
                                    vmin=Data['Zmin'], vmax=Data['Zmax']
                                   )

            cbar = MPL.colorbar(CP, ticks=np.linspace(Data['Zmin'], Data['Zmax'],5))
            cbar.ax.tick_params(labelsize=15)
            if logticks:
                cbar.ax.set_yticklabels(
                    [ r"$10^{%i}$" %int(i) for i in np.linspace(Data['Zmin'], Data['Zmax'],5)]
                                       )
            else:
                cbar.ax.set_yticklabels(
                    [ "%.2f" %i for i in np.linspace(Data['Zmin'], Data['Zmax'],5)]
                                       )
            cbar.ax.set_ylabel(Data['cbartitle'], fontsize=20)


        #self.axes.set_xlim(Data['Xmin'], Data['Xmax'])
        #self.axes.set_ylim(Data['Ymin'], Data['Ymax'])




    def SpectralLines(self, Data):

        return None


    def EnergyLevels(self, Data, Keys):

        MPL.yticks(Keys.values(), Keys.keys())
        self.figure.subplots_adjust(bottom=0.2, left=0.3)

        #self.axes.set_xlim(0.0,1.0)#Data['Xmin'], Data['Xmax'])
        self.axes.set_ylim(0, len(Keys.keys())+1)

        for name,i in Keys.items():

            self.axes.errorbar(
                Data['X'][int(i-1)], np.ones(len(Data['X'][int(i-1)]))*i,
                yerr=np.ones(len(Data['X'][int(i-1)]))*0.25,
                ecolor=self.colourEnergyLevels[int(i-1)], elinewidth=1, fmt='none'
                              )





    def Sampling(self, Data, Keys):

        MPL.yticks(Keys.values(), Keys.keys())
        self.figure.subplots_adjust(bottom=0.2, left=0.3)

        self.axes.set_xlim(0.0, 1.0)
        self.axes.set_ylim(0, len(Keys.keys())+1)

        for name,i in Keys.items():

            self.axes.errorbar(
                Data['X'][int(i-1)], np.ones(len(Data['X'][int(i-1)]))*i, 
                yerr=np.ones(len(Data['X'][int(i-1)]))*0.4,
                ecolor=self.colourEnergyLevels[int(i-1)], linewidth=1,
                fmt='none'
                              )



