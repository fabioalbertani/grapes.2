#!/usr/bin/python
#File: core.plotting.mpl_producer.py

from numpy import zeros, ones, c_, max, min, percentile

import core.grapes_utils.main_utils as UTILS
from core.grapes_utils.progress_bar import DisplayBar

from core.data_management.data_utils import UnpackInternal
from core.plotting import plotting_utils
from core.plotting.utils_mpl_surfaces import MPLSurfaces
from core.plotting.utils_mpl_statistics import MPLStatistics


class MPLProducer:

    def __init__(self, args, INTERPOLATION):
        
        self.Globalargs = args.main
        self.Moduleargs = args.plotting

        self.sigmaFit = args.interpolation.sigmaFit
        self.lambdaFit = args.interpolation.lambdaFit

        self.SP = UTILS.SuperPrint('Plotting.log')

        self.Moduleargs.set('Colours', ['rgb(255, 0, 0)', 'rgb(0, 0, 255)'])

        if not INTERPOLATION is None:
            self.ValuePlotDetails = {
                    'Energy':  INTERPOLATION.PredictMean,
#                    'Gradient': INTERPOLATION.PredictGradient,
                    'Error': INTERPOLATION.PredictVariance,
#                    'GradientError': INTERPOLATION.PredictGradientCnnfidence,
                    'Samples': INTERPOLATION.DrawSamples
                             }

        self.MPLSurfaces = MPLSurfaces()
        self.MPLStatistics = MPLStatistics()


    def surface_grid(self, PlotDetails, el, limits_shift, Value='Energy', meshgrid=True, 
                     giveMinima=False, basis_set=None, samples=None):
        '''
        Calculates the mean and the variance for a grid (for plotting purposes)
        
        meshgrid: plotly contours build their own meshgrid equivalent, this options simply returns 
        whatever grid is used for plotting (Nsamp x Nsamp for True or Nsamp X Dim for False)
        '''
        Coordinates = plotting_utils.MeshGrid(self.Moduleargs, limits_shift)
        if Value=='Samples':
            V = zeros((self.Moduleargs.NSamplingPlot, self.Moduleargs.NSamplingPlot, samples))
        else:
            V = zeros((self.Moduleargs.NSamplingPlot, self.Moduleargs.NSamplingPlot))

        PB = DisplayBar(self.Moduleargs.NSamplingPlot)
        print('\n\t-- Predicting '+Value+' on a '+str(self.Moduleargs.NSamplingPlot)+'X'+\
              str(self.Moduleargs.NSamplingPlot)+' points grid\n')


        for i in range(self.Moduleargs.NSamplingPlot):
            q1=True
            j = 0
            for d in range(self.Globalargs.D):
                if d in self.Moduleargs.CoordinatesPlot:
                    if q1:
                        if d==0:
                            Q = Coordinates['Q0'][i]*ones(self.Moduleargs.NSamplingPlot)
                        else:
                            Q = c_[Q,Coordinates['Q0'][i]*ones(self.Moduleargs.NSamplingPlot)]
                        q1=False
                    else:
                        Q = c_[Q,Coordinates['Q1']]
                else:
                    if d==0:
                        Q = self.Moduleargs.CoordinatesFrozen[j]*ones(self.Moduleargs.NSamplingPlot)
                    else:
                        Q = c_[Q, self.Moduleargs.CoordinatesFrozen[j]*\
                                ones(self.Moduleargs.NSamplingPlot)]
                    j += 1

            if Value=='Samples':
                V[i,:,:] = self.ValuePlotDetails[Value](Q, PlotDetails, el, 
                           samples=samples).reshape((self.Moduleargs.NSamplingPlot, samples))
            else:
                V[i,:] = self.ValuePlotDetails[Value](Q, PlotDetails, el).flatten()

            if giveMinima and i==len(self.Moduleargs.NSamplingPlot)-1: 
                self.PrintApproximateMinima(V)
            PB.Show(i) 

        if meshgrid:
            return Coordinates['X'], Coordinates['Y'], V
        else:
            return Coordinates['Q0'], Coordinates['Q1'], V




    def PrintApproximateMinima(self, E):
        #TODO Put in its own module ? and have some better plot for it? 
        '''
        Gives the plotted point with the lowest energy (i.e. roughly the minima)
        '''
        minl = []
        for i in range(len(E[:,0])):
            minl.append(min(E[i,:]))

        print '\n\tGrid minimum is: ', min(minl)


    

    def PointEnergies(self, PlotDetails, el):
        '''
        Projects points from (q1,q2,q3...E) to (qplot1, qplot2, EGP)
        '''
        points = self.Data.GetPoints()
        V = zeros((points.shape[0],1)) 

        Q = zeros((1,self.Globalargs.D))

        for i in range(len(points[:,0])):
            q1=True
            j = 0
            for d in range(self.Globalargs.D):
                if d in self.Moduleargs.CoordinatesPlot:
                    if q1:
                        Q[0,self.Moduleargs.CoordinatesPlot[0]] =\
                            points[i,self.Moduleargs.CoordinatesPlot[0]]
                        q1=False
                    else:
                        Q[0,self.Moduleargs.CoordinatesPlot[1]] =\
                            points[i,self.Moduleargs.CoordinatesPlot[1]]
                else:
                    Q[0,d] = self.Moduleargs.CoordinatesFrozen[j]
                    j += 1

            E[i,0] = self.ValuePlotDetails['Energy'](Q, PlotDetails, el)

        return E




    #                       FUNCTIONS CALLED FROM MAIN

    def PlotContours(self, PlotDetails, el, limits_shift=0.0, setTitle=False, scatter=False):
        '''
        Plots the surface as a contour plot (options available in UtilsMPL)
        '''
        Data = {}

        Data['X'], Data['Y'], Data['Z'] = self.surface_grid(PlotDetails, el, limits_shift, 
                                                            meshgrid=True)

        self.MPLSurfaces.new_canvas(logscale=False)

        Data['Xmin'] = self.Moduleargs.GridLimitsPlot[0] + limits_shift
        Data['Xmax'] = self.Moduleargs.GridLimitsPlot[1] - limits_shift
        Data['Ymin'] = self.Moduleargs.GridLimitsPlot[2] + limits_shift
        Data['Ymax'] = self.Moduleargs.GridLimitsPlot[3] - limits_shift

        Data['Zmin'] = self.Moduleargs.PotentialcutPlot[0] 
        Data['Zmax'] = self.Moduleargs.PotentialcutPlot[1] 
        Data['cbartitle'] = r'$\mathrm{E}\ [\mathrm{Ha}]$'

        self.MPLSurfaces.Contour2D(Data)

        if scatter:
            DataUnpacked = UnpackInternal(self.Data.GetPoints(),
                getattr(self.Data.Energy, PlotDetails['label']))
            Data['X'] = DataUnpacked["points%i" %int(el)][:,self.Moduleargs.CoordinatesPlot[0]]
            Data['Y'] = DataUnpacked["points%i" %int(el)][:,self.Moduleargs.CoordinatesPlot[1]]

            self.MPLSurfaces.Scatter2D(Data)


        self.MPLSurfaces.produce_canvas({
            'filename': 'Contours_'+UTILS.GetDataLabel(PlotDetails)+'_el:'+el,
            'title': None
                                        })


    def PlotSurface(self, PlotDetails, el, limits_shift=0.0, setTitle=False):
        '''
        Plots Surface
        '''
        Data = {}

        Data['X'], Data['Y'], Data['Z'] = self.surface_grid(PlotDetails, el, limits_shift, 
                                                            meshgrid=True)

        PE = self.PointEnergies(PlotDetails, el)
        DataUnpacked = UnpackInternal(self.Data.GetPoints(), PE)

        self.MPLSurfaces.new_canvas(logscale=False, Volume=True)

        if int(el)==0:
            Data['Zmin'] = min(Data['Z'])
            Data['Zmax'] = percentile(Data['Z'],98)

        self.MPLSurfaces.SimpleSurface(Data)

        if setTitle: Data['title'] = "Energy Landscape for %s" %self.Moduleargs.BasisPlot
        Data['Xmin'] = self.Moduleargs.GridLimitsPlot[0] + limits_shift
        Data['Xmax'] = self.Moduleargs.GridLimitsPlot[1] - limits_shift
        Data['Ymin'] = self.Moduleargs.GridLimitsPlot[2] + limits_shift
        Data['Ymax'] = self.Moduleargs.GridLimitsPlot[3] - limits_shift
        Data['Zmax'] = self.Moduleargs.PotentialcutPlot


        self.MPLSurfaces.produce_canvas({
        'filename': 'Surface_'+UTILS.GetDataLabel(PlotDetails)+'_el:'+el,
        'title': None
                                        })





    def PlotError(self, PlotDetails, el, limits_shift=0.0, setTitle=False, scatter=False):
        '''
        Plots the 95% confidence interval as a contour plot (0 is the predicted mean and z isi
        the 2|sigma| interval)
        '''
        from numpy import log
        Data = {}       
 
        Data['X'], Data['Y'], Data['Z'] = self.surface_grid(PlotDetails, el, limits_shift, 
                                                            Value='Error', meshgrid=True)

        DataUnpacked = UnpackInternal(self.Data.GetPoints(),
                                      getattr(self.Data.Energy, PlotDetails['label']))

        self.MPLSurfaces.new_canvas(logscale=False)

        Data['Xmin'] = self.Moduleargs.GridLimitsPlot[0] + limits_shift
        Data['Xmax'] = self.Moduleargs.GridLimitsPlot[1] - limits_shift
        Data['Ymin'] = self.Moduleargs.GridLimitsPlot[2] + limits_shift
        Data['Ymax'] = self.Moduleargs.GridLimitsPlot[3] - limits_shift

        Data['Zmin'] = -8 #1e-8
        Data['Zmax'] =  0  #np.max(Err[state])
        Data['cbartitle'] = r'$\mathrm{log}(\Delta_{\mathrm{E}})\ [\mathrm{Ha}]$'

        self.MPLSurfaces.Contour2D(Data, log=False, logticks=True)

        if scatter:
            Data['X'] = DataUnpacked["points%i" %int(el)][:,self.Moduleargs.CoordinatesPlot[0]]
            Data['Y'] = DataUnpacked["points%i" %int(el)][:,self.Moduleargs.CoordinatesPlot[1]]
        
            self.MPLSurfaces.Scatter2D(Data)
        

        self.MPLSurfaces.produce_canvas({
        'filename': 'Confidence_'+UTILS.GetDataLabel(PlotDetails)+'_el:'+el,
        'title': None
                                        })


    #TODO completely deprecated
    def PlotCBSError(self, PlotDetails, el='0', limits_shift=0.0, setTitle=False, scatter=False):
        '''
        Plots the CBS error as a contour plot 
        nb: the value type is energy since the CBS error is stored as a basis set
        '''
        from numpy import log
        Data = {}

        Data['X'], Data['Y'], Data['Z'] = self.surface_grid(PlotDetails, limits_shift, meshgrid=True, basis_set='CBS_error')

        DataUnpacked = UnpackInternal(self.Data.GetPoints(),
                                      getattr(self.Data.Energy, PlotDetails['label']))

        self.MPLSurfaces.new_canvas(logscale=False)

        Data['Xmin'] = self.Moduleargs.GridLimitsPlot[0] + limits_shift
        Data['Xmax'] = self.Moduleargs.GridLimitsPlot[1] - limits_shift
        Data['Ymin'] = self.Moduleargs.GridLimitsPlot[2] + limits_shift
        Data['Ymax'] = self.Moduleargs.GridLimitsPlot[3] - limits_shift

        Data['Z'] = 2.0*log(abs(Data['Z'][int(el)])) #confidence interval

        Data['Zmin'] = -8 #1e-8
        Data['Zmax'] =  0  #np.max(Err[state])
        Data['cbartitle'] = r'$\mathrm{log}(\Delta_{\mathrm{E}})\ [\mathrm{Ha}]$'

        self.MPLSurfaces.Contour2D(Data, log=False, logticks=True)

        if scatter:
            Data['X'] = DataUnpacked["points%i" %int(el)][:,self.Moduleargs.CoordinatesPlot[0]]
            Data['Y'] = DataUnpacked["points%i" %int(el)][:,self.Moduleargs.CoordinatesPlot[1]]

            self.MPLSurfaces.Scatter2D(Data)


        self.MPLSurfaces.produce_canvas({
        'filename': 'CBSError_'+UTILS.GetDataLabel(PlotDetails)+'_el:'+el,
        'title': None
                                        })





    def PlotGradient(self, PlotDetails, el, limits_shift=0.0, setTitle=False, scatter=False, D=0):
        '''
        Plots the surface as a contour plot (options available in UtilsMPL)
        '''
        Data = {}

        Data['X'], Data['Y'], Data['Z'] = self.surface_grid(PlotDetails, el, limits_shift, 
                                                            Value='Gradient', meshgrid=True)

        self.MPLSurfaces.new_canvas(logscale=False)

        DataUnpacked = UnpackInternal(self.Data.GetPoints(),
                                      getattr(self.Data.Energy, PlotDetails['label']))


        Data['Xmin'] = self.Moduleargs.GridLimitsPlot[0] + limits_shift
        Data['Xmax'] = self.Moduleargs.GridLimitsPlot[1] - limits_shift
        Data['Ymin'] = self.Moduleargs.GridLimitsPlot[2] + limits_shift
        Data['Ymax'] = self.Moduleargs.GridLimitsPlot[3] - limits_shift

        Data['Z'] = Data['Z'][D]
        Data['Zmin'] = self.Moduleargs.PotentialcutPlot[0] #np.min(E[state])-0.1
        Data['Zmax'] = self.Moduleargs.PotentialcutPlot[1] #np.percentile(E[state],80)
        Data['cbartitle'] = r"$\mathrm{G}_%i\ [\mathrm{Ha}/\mathrm{\AA}]$" %D

        self.MPLSurfaces.Contour2D(Data)

        if scatter:
            Data['X'] = DataUnpacked["points%i" %int(el)][:,self.Moduleargs.CoordinatesPlot[0]]
            Data['Y'] = DataUnpacked["points%i" %int(el)][:,self.Moduleargs.CoordinatesPlot[1]]

            self.MPLSurfaces.Scatter2D(Data)


        self.MPLSurfaces.produce_canvas({
            'filename': 'Samples_'+UTILS.GetDataLabel(PlotDetails)+'_el:'+el,
            'title': None
                                        })





    def PlotSamples(self, PlotDetails, el, limits_shift=0.0, setTitle=False, samples=1):
        '''
        Draws Samples from the GP and plots them (gives some insights on the learning process)
        '''
        Data = {}

        Data['X'], Data['Y'], Data['Z'] = self.surface_grid(PlotDetails, el, limits_shift, 
                                             Value='Samples', meshgrid=True, samples=samples)

        self.MPLSurfaces.new_canvas(logscale=False, Volume=True)
        
        for sample in range(samples):
            Data['Z'] = Data['Z'][:,:,sample]

            self.MPLSurfaces.SimpleSurface(Data)


        if setTitle: Data['title'] = "GP Samples" 
        Data['Xmin'] = self.Moduleargs.GridLimitsPlot[0] + limits_shift
        Data['Xmax'] = self.Moduleargs.GridLimitsPlot[1] - limits_shift
        Data['Ymin'] = self.Moduleargs.GridLimitsPlot[2] + limits_shift
        Data['Ymax'] = self.Moduleargs.GridLimitsPlot[3] - limits_shift
        Data['Zmax'] = self.Moduleargs.PotentialcutPlot


        self.MPLSurfaces.produce_canvas({
        'filename': 'Samples_'+UTILS.GetDataLabel(PlotDetails)+'_el:'+el,
        'title': None
                                        })




    def PlotValidation(self, PlotDetails, el, pure, ValidationData):

        label = {'Energy': r'$f_{\mathrm{GP}}$' if pure else r'$E$',
                 'anal': r'$v$',
                 'Hessian': r'$h$'}

        self.MPLStatistics.new_canvas(
            labels=[label[PlotDetails['Type']]+r'$_{\mathrm{'+str(el)+'}}$',
            r'$\Delta$'+label[PlotDetails['Type']]+r'$_{\mathrm{'+str(el)+'}}$'],
            D=1
                                     )

        ValidationData['Zmin'] = self.Moduleargs.PotentialcutPlot[0]
        ValidationData['Zmax'] = self.Moduleargs.PotentialcutPlot[1]

        self.MPLStatistics.RMSE(ValidationData)

        self.MPLStatistics.produce_canvas({
        'filename': 'RMSE_'+UTILS.GetDataLabel(PlotDetails)+'_el:'+el,
        'title': None
                                          })



    def PlotTrainingSet(self, DataGP):

        self.MPLStatistics.new_canvas()

        self.MPLStatistics.Distribution(DataGP)

        self.MPLStatistics.produce_canvas({
        'filename': 'TrainingSet_analysis' ,
        'title': None
                                          })
        
