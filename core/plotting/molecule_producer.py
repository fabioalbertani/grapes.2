#!/bin/python
#File: core.plotting.molecule_producer.py

from numpy import linspace, ones, zeros, zeros_like, meshgrid, c_, insert

import core.grapes_utils.main_utils as UTILS
from core.data_management.data_utils import Imap

from core.plotting import plotting_utils
from core.data_management import data_utils
from core.plotting.utils_vmd_samples import VMDSamples
from core.plotting.utils_vmd_modes import VMDModes
from core.grapes_utils.progress_bar import DisplayBar 


class MOLProducer:

    def __init__(self, args, Data, Molecule):

        self.Globalargs = args.main
        self.Moduleargs = args.plotting
        self.Data = Data
        self.Molecule = Molecule

        self.VMDSamples = VMDSamples(self.Globalargs.AtomsList)
        self.VMDModes = VMDModes(self.Globalargs.AtomsList)


    def new_canvas(self, filename):
        pass


    def PlotSampling(self, PlotDetails, el=None, XYZ=None):
        '''
        Produces a xyz VMD-readable file to see the sampling in action
        '''
        Data = {}
       
        #Data['NSamples'] = self.Data.CountSamples(PlotDetails, el)       
        if XYZ is None:
            Data['XYZ'] = self.Molecule.q2cart(self.Data.GetPoints(), fix=True) 
        else:
            Data['XYZ'] = XYZ
    
        self.VMDSamples.new_canvas(
            filename='VMDSampling_'+UTILS.GetDataLabel(PlotDetails)
                                  )
        self.VMDSamples.PlotSamples(Data)
        self.VMDSamples.produce_canvas()


    def PlotCoordinate(self, PlotDetails, qPlot=0): #TODO make more than 1D
        '''
        Produces a movie along one coordinate
        '''
        from numpy import sort

        print('\n\tProducing VMD Molecular movie files for '+UTILS.GetLabel(PlotDetails)+' ...\n')

        N = (PlotDetails, '0')
        XYZ = self.Molecule.q2cart(sort(self.Data.GetPoints(), axis=0))
        for i, q in enumerate(['X','Y','Z']):
            Data[q] = XYZ[:,:,i].astype(float)


        self.VMDSamples.new_canvas(
            filename='Coordinate'+str(qPlot)+'_'+UTILS.GetDataLabel(PlotDetails)
                                  )
        self.VMDSamples.PlotSamples(Data, progressive=True)
        self.VMDSamples.produce_canvas()




    def mode_grid(self, PlotDetails, extent, N):
        QExtent = linspace(-extent, extent, N)
        Coordinates = plotting_utils.MeshGrid(self.Moduleargs, 0.0)
        XYZ = zeros((self.Moduleargs.NSamplingPlot,self.Globalargs.Dxyz))
        V = zeros((self.Moduleargs.NSamplingPlot, 2*N, self.Globalargs.Dxyz, 
                   self.Globalargs.Dxyz-6, self.Globalargs.NStates))
        MTot = zeros((self.Moduleargs.NSamplingPlot, self.Globalargs.Dxyz,
                   self.Globalargs.Dxyz-6, self.Globalargs.NStates))
        MErrTot = zeros_like(MTot)

        j=0
        for d in range(self.Globalargs.D):
            if d in self.Moduleargs.CoordinatesPlot:
                if d==0:
                    Q = Coordinates['Q0']
                else:
                    Q = c_[Q,Coordinates['Q0']]
            else:
                if d==0:
                    Q = self.Moduleargs.CoordinatesFrozen[j]*ones(self.Moduleargs.NSamplingPlot)
                else:
                    Q = c_[Q, self.Moduleargs.CoordinatesFrozen[j]*\
                           ones(self.Moduleargs.NSamplingPlot)]
                j += 1

        for d in range(self.Moduleargs.NSamplingPlot):
            XYZ[d,:] = self.Molecule.q2cart(Q[d,:].reshape((1,self.Globalargs.D))).flatten()


        for el in data_utils.UnpackIndices(
            tuple([1, 3*len(self.Globalargs.AtomsList), self.Globalargs.D, 1])):

            Mo = self.Data.PredictMean(Q, PlotDetails, el)
            MoErr = self.Data.PredictVariance(Q, PlotDetails, el)

            idx = data_utils.Imap(el)
            MTot[idx] = Mo.reshape((self.Moduleargs.NSamplingPlot,1,1,1))
            MErrTot[idx] = MoErr.reshape((self.Moduleargs.NSamplingPlot,1,1,1))

            for j in range(2*N):
                indices = [slice(None),slice(j,j+1)] + idx[1:]
                if j<N:
                    V[indices] = QExtent[j]*\
                        Mo.reshape((self.Moduleargs.NSamplingPlot,1,1,1,1))
                else:
                    V[indices] = QExtent[2*N-j-1]*\
                        Mo.reshape((1,1,self.Moduleargs.NSamplingPlot,1,1,1,1))

        for j in range(2*N):
            for mode in range(self.Globalargs.Dxyz-6):
                for state in range(self.Globalargs.NStates):
                    V[:,j,:,mode,state] += XYZ  

        f=open('XYZ.dat','w')
        for i in range(self.Moduleargs.NSamplingPlot):
            f.write('3\nH3\n')
            for at in range(len(self.Globalargs.AtomsList)):
                f.write(self.Globalargs.AtomsList[at]+'\t'+'\t'.join([str(x)\
                        for x in XYZ[i,3*at:3*at+3]])+'\n')
        f.close()

        return V, MTot, MErrTot, Coordinates['Q0'], self.Molecule.q2cart(Q, fix=True)
 




    def PlotModes(self, PlotDetails, extent=0.05, N=20):
        '''
        Produces the data in a file used to plot a single mode at a fixed geometry
        The file is provided to allow for both normal mode display and general coordinates 
        to use this function
        '''
        Data = {}

        XYZ, M, MErr, Q, XYZ_coo = self.mode_grid(PlotDetails, extent, N)

        for mode in range(self.Globalargs.Dxyz-6):

            indices = 3*[slice(None)] + [slice(mode,mode+1), slice(None)]

            Data = {
            'X': Q,
            'XYZ': XYZ_coo,
            'Z': XYZ[indices].reshape((self.Moduleargs.NSamplingPlot, 2*N, self.Globalargs.Dxyz, 
                              self.Globalargs.NStates)),
            'ZMode': M,
            'ZErr': MErr,
            'label': str(mode),
                   }

            self.VMDModes.new_canvas(
                filename='Mode'+str(mode)+'_'+UTILS.GetDataLabel(PlotDetails), 
                labels=[r'$r_1$',r'$r_2$',r'$\theta$']
                                    )
            self.VMDModes.PlotMode(Data)
            self.VMDModes.produce_canvas()



