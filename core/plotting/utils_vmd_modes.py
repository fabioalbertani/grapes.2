#!/bin/python
#File: core.plotting.utils_vmd_modes.py

from subprocess import call

from numpy import array, meshgrid, linspace, ravel, zeros, isnan, mean

import matplotlib.pyplot as MPL
from mpl_toolkits.mplot3d import axes3d, Axes3D
from matplotlib import cm, ticker

import core.plotting.plotting_utils as utils

class VMDModes(utils.UtilsVMD):

    def __init__(self, AtomsList):

        self.AtomsMarkers = {
            'H': ('w', 1.0),
            'C': ('k', 4.0),
            'O': ('r', 6.0),
            'N': ('b', 5.0)
                            }

        super(VMDModes, self).__init__(AtomsList, False) 


    def PlotMode(self, Data):
        '''
        Main interface to call all the different parts of the plot
        '''

        self.SP('\n\tProducing 2D-MPL plots of normal mode components:\n')
        self.PlotMode_Components(Data)
        self.SP('\n\tProducing 3D-MPL plots of normal mode:\n')
        self.PlotMode_Arrows(Data)
        self.SP('\n\tProducing VMD movie of normal mode:\n')
        self.PlotMode_Movie(Data)






    def PlotMode_Movie(self, Data):
        '''
        Writes a VMD-readable file that produces a movie of the mode along a coordinate change
        '''
        for sample in range(Data['Z'].shape[0]):
            for modesample in range(Data['Z'].shape[1]):
                if modesample or sample: self.f.write('\n')
                self.f.write(str(len(self.AtomsList))+'\n'+''.join(self.AtomsList))
                for atom in range(len(self.AtomsList)):
                    self.f.write('\n\t'+self.AtomsList[atom]+'\t'+'\t'.join([str(x)\
                                 for x in Data['Z'][sample,modesample,3*atom:3*atom+3,0]]))



    def PlotMode_Components(self, Data):
        '''
        2D plot of the components of the normal modes
        '''

        for ax in range(Data['Z'].shape[2]):
            self.axes.plot(Data['X'].flatten(), Data['ZMode'][:,ax,0].flatten(),
                           linewidth=1.0, label='q'+Data['label']+str(ax)
                          )
            if 'ZErr' in Data.keys():
                self.axes.fill_between(Data['X'].flatten(), Data['ZMode'][:,ax,0].flatten()+\
                          Data['ZErr'][:,ax,0].flatten(), Data['ZMode'][:,ax,0].flatten()-\
                          Data['ZErr'][:,ax,0].flatten(),
                                       alpha=0.25
                                      )



    def PlotMode_Arrows(self, Data):
        '''
        Plotting the normal modes as arrows starting from the nuclei

        x and z are inverted to allow a simpler handling of the orientation of the plot    
        '''
        c = MPL.cm.viridis(range(Data['XYZ'].shape[1]))

        #plotting the geometries with black dots
        for sample in range(Data['XYZ'].shape[1]):

            self.PlotGeometry(Data['XYZ'][:,sample,:], sample)

            for at in range(len(self.AtomsList)):

                self.axes3d.quiver(
                    Data['XYZ'][at,sample,2]+sample*0.1, Data['XYZ'][at,sample,1], 
                    Data['XYZ'][at,sample,0], Data['ZMode'][sample,3*at+2,0]+sample*0.1, 
                    Data['ZMode'][sample,3*at+1,0], Data['ZMode'][sample,3*at,0],
                    cmap='viridis', color=c[sample],
                    normalize=True, length=1.3, alpha=0.5
                                  )


        #plotting a "sheet" with colours representing the GP variance
        X, Y = meshgrid(linspace(self.axes3d.get_xlim()[0], self.axes3d.get_xlim()[1], 
               Data['XYZ'].shape[1]),linspace(self.axes3d.get_ylim()[0],self.axes3d.get_ylim()[1]))
        Z = zeros((X.shape[0],X.shape[1])) + self.axes3d.get_zlim()[0]
        C = zeros(Data['XYZ'].shape[1]*50)
        for i in range(Data['XYZ'].shape[1]): C[50*i:50*i+50] = mean(Data['ZErr'][i,:,0])
        C = MPL.cm.viridis(array([x if not isnan(x) else 1.0 for x in C]))

        #self.axes3d.plot_surface(X, Y, Z, facecolors=C,
        #                         linewidth=0
        #                        )



        self.axes3d.w_xaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
        self.axes3d.w_yaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
        self.axes3d.w_zaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
        self.axes3d.grid(False)
        #self.axes3d.w_xaxis.line.set_color((1.0, 1.0, 1.0, 0.0))
        #self.axes3d.w_yaxis.line.set_color((1.0, 1.0, 1.0, 0.0))
        #self.axes3d.w_zaxis.line.set_color((1.0, 1.0, 1.0, 0.0))
        #self.axes3d.set_xticks([]) 
        #self.axes3d.set_yticks([]) 
        #self.axes3d.set_zticks([])



    def PlotGeometry(self, XYZ, shift_index, shift=0.1):

        for ati, atom in enumerate(self.AtomsList):
            self.axes3d.scatter(
                XYZ[ati,2]+shift_index*shift, XYZ[ati,1], XYZ[ati,0],
                marker='o', s=self.AtomsMarkers[atom][1], c=self.AtomsMarkers[atom][0], 
                linewidth='1'
                               )


