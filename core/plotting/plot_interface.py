#!/usr/bin/python
#File: core.plotting.plot_interface.py

import core.grapes_utils.module_selection as MODULE
import core.grapes_utils.main_utils as UTILS

#Producers are interfaces to specific plotting programs such as VMD, matplotlib or plotly
from core.plotting.molecule_producer import MOLProducer

from core.plotting.mpl_producer_1d import MPLProducer1D
from core.plotting.mpl_producer import MPLProducer

from core.plotting.lml_producer import LMLProducer


class PlotInterface:
    '''
    Main plotting interface allows to register various options which are specific
    to plotting and also selects, according to the data provided, the best way to plot 
    the data
    '''
 
    @staticmethod
    def RegisterOptions(args):
        m = args.RegisterModule('plotting')
   
        args.Register(m, '--plotly', action='store_true', default=False, 
            help='allows to use plotly instead of matplotlib (best for on-the-fly data'\
                 'visualisation and data sharing via html links but poor for text-embedded'\
                 'plot production)')
 
        args.Register(m, '--PlotTypes', default=None)
        args.Register(m, '--PlotElements', default=None, 
            help='Defines which states are plotted')

        args.Register(m, '--PlotTypeMultiple', type=str, default=None,
                      help='Allows multiple plots')
        args.Register(m, '--PlottingMesh', action='store_true', default=False)

        args.Register(m, '--PlotConfidence', action='store_true', default=False, 
            help='If True the GP 95% confindence interval will be plotted')    
        args.Register(m, '--PlotSurface', action='store_true', default=False,
            help='If True the surface will be plotted')
        args.Register(m, '--PlotForce', action='store_true', default=False,
            help='If True the gradient will be plotted')
        args.Register(m, '--PlotSamples', action='store_true', default=False,
            help='If True samples will be plotted')
        args.Register(m, '--GradientDirection', default=0, type=int,
            help='which gradient is plotted for PlotForce')
    
   
        args.Register(m, '--PlotLML', action='store_true', default=False,
            help='If True the lml space will be plotted')
        args.Register(m, '--PlotSampling', action='store_true', default=False,
            help='If True a vmd file to plot the sampling will be created')
 
        args.Register(m, '--CoordinatesPlot', type=str, default='0')
        args.Register(m, '--CoordinatesFrozen', type=str, default=None)
    
        args.Register(m, '--NSamplingPlot', type=int, default=250,
            help='Number of points used to sample the interpolation function'\
                 '(i.e. resolution of the surface)')
        args.Register(m, '--GridLimitsPlot', type=str, default=[], nargs='+',
            help='Sets limits for the grid of the plot along {qi qj ...qD}')
    
        args.Register(m, '--PotentialcutPlot', type=str, default=[], nargs='+',
            help='Defines max(V) for plotting, the minimum is extracted from'\
            ' the resulting surface')
    

    @staticmethod
    def ParseOptions(options):

        options.plotting.set('Label', options.main('Label'))

        options.plotting.set('PotentialcutPlot', [float(s) for s\
                                in options.plotting.PotentialcutPlot[0].split()])

        options.plotting.set('PlotTypes', [s for s in\
                            options.plotting('PlotTypes').split()])
   

        if not options.plotting('PlotTypeMultiple') is None:
            '''parses the PlotTypeMultiple provided'''
            options.plotting.set('PlotTypeMultiple',\
                [s for s in options.plotting('PlotTypeMultiple').split()])
            ops = options.plotting('PlotTypeMultiple')
            options.plotting.set('PlotTypeMultiple', {})
            for i,s in enumerate(ops):
                options.plotting('PlotTypeMultiple')[options.plotting('PlotTypes')[i]] =\
                    s if s!='none' else None
        
        else:
            '''fills dictionary with "none" since it was not provided'''
            options.plotting.set('PlotTypeMultiple', {})
            for i,s in enumerate(options.plotting('PlotTypes')):
                options.plotting('PlotTypeMultiple')[s] = None 

        if options.plotting('PlotElements') is not None: 
            options.plotting.set('PlotElements', options.plotting('PlotElements').split())

        options.plotting.set('CoordinatesPlot', [int(s) for s\
                            in options.plotting('CoordinatesPlot').split()])

        if options.plotting('CoordinatesFrozen') is None:
            options.plotting.set('CoordinatesFrozen', options.main('GridLimits')[0::2])
            options.plotting.set('CoordinatesFrozen', [x for i,x in\
                    enumerate(options.plotting('CoordinatesFrozen')) if i\
                    not in options.plotting('CoordinatesPlot')])
        else:
            options.plotting.set('CoordinatesFrozen', [float(s) for s\
                    in options.plotting('CoordinatesFrozen').split()])


        if options.plotting('GridLimitsPlot'):
            for ci, li in enumerate(options.plotting('CoordinatesPlot')):
                options.plotting('GridLimitsPlot')[2*ci] = options.plotting('GridLimitsPlot')[2*li]
                options.plotting('GridLimitsPlot')[2*ci+1] =\
                                    options.plotting('GridLimitsPlot')[2*li+1]
        else:
            options.plotting.set('GridLimitsPlot', options.main('GridLimits'))


    

    def __init__(self, args):

        self.args = args
        self.SP = UTILS.SuperPrint()


    def __call__(self, INTERPOLATION, MOLECULE): 
        '''
        Main call to the class which allow to produce all the possible

        the core.options.options "args" will select which plots to create
        '''
    
        Template = ''' 
            Plotting
    
                Energy Range:               ''' + ' '.join([str(x) for x in\
                                                      self.args.plotting.PotentialcutPlot])
        Template += '''
    
                Plotting along:             ''' + ' '.join([str(x) for x in\
                                                      self.args.plotting.CoordinatesPlot])
        Template += '''
                Frozen Coordinates:         ''' + ' '.join([str(x) for x in\
                                                      self.args.plotting.CoordinatesFrozen])\
                                                + '\n'


        for PlotType in self.args.plotting.PlotTypes if isinstance(
            self.args.plotting.PlotTypes, list) else [self.args.plotting.PlotTypes]:

            PlotDetails = UTILS.GetTypeDetails(PlotType)
            PlotDetails['Multiple'] = UTILS.GetTypeDetails(
                          self.args.plotting.PlotTypeMultiple[PlotType])

            if PlotDetails['Type']=='Hessian':
                self.ModesPlot(INTERPOLATION, MOLECULE, PlotDetails, Template)

            else:
                for el in self.args.plotting('PlotElements') if isinstance(\
                      self.args.plotting('PlotElements'),list) else\
                      [self.args.plotting('PlotElements')]:

                    Template2 = Template + '\n\t\t' + UTILS.GetLabel(PlotDetails) + '\n\n\t\t'
                    Template2 += el + ' with ' + str(INTERPOLATION.number_of_samples(PlotDetails,
                                 el)) + ' points\n' 
                    self.SP(Template2)
        
                    if self.args.main('D')==1:
                        MPL = MPLProducer1D(self.args, INTERPOLATION) 
            
                    else:
                        MPL = MPLProducer(self.args, INTERPOLATION)
        
                    self.SP("\tPlotting Contours:\n")
           
                    MPL.PlotContours(PlotDetails, el)
                    if self.args.plotting('PlotConfidence'): MPL.PlotError(PlotDetails, el)
            
                    if PlotDetails['basis_set']=='CBS':
                        self.SP("\tPlotting CBS Error:\n")
                        Plot.PlotCBSError(PlotDetails, el)
        
                    if self.args.plotting('PlotSurface'):
                        self.SP("\tPlotting Surfaces:\n")
                        MPL.PlotSurface(PlotDetails, el)
            
                    if self.args.plotting('PlotForce'):
                        self.SP("\tPlotting Gradients:\n")
                        MPL.PlotGradient(PlotDetails, el, D=self.args.plotting('GradientDirection'))
            
                    if self.args.plotting('PlotSamples'): #GP SAMPLES
                        self.SP("\tPlotting Samples:\n")
                        MPL.PlotSamples(PlotDetails,el)
             


    def ModesPlot(self, INTERPOLATION, MOLECULE, PlotDetails, Template):

        MOL = MOLProducer(self.args, INTERPOLATION, MOLECULE)

        Template += '\n\t\t' + UTILS.GetLabel(PlotDetails) + '\n\n\t\t' +\
                    'Producing VMD files for all modes\n'
        self.SP(Template)

        MOL.PlotModes(PlotDetails)



    def LMLPlot(self, INTERPOLATION):
        '''Direct call to the lml plot functions'''

        Template = '''
            Plotting LML

                Dimensionality:             ''' + str(self.args.main.D) + '\n'


        for PlotType in self.args.plotting('PlotTypes') if isinstance(self.args.plotting('\
                        PlotTypes'), list) else [self.args.plotting('PlotTypes')]:

            PlotDetails = UTILS.GetTypeDetails(PlotType)

            for el in self.args.plotting('PlotElements') if isinstance(\
                      self.args.plotting('PlotElements'),list) else\
                      [self.args.plotting('PlotElements')]:

                Template2 = Template + '\n\t\t' + UTILS.GetLabel(PlotDetails) + '\n\n\t\t'
                Template2 += el + ' with ' + str(INTERPOLATION.number_of_samples(PlotDetails,el))\
                             +' points\n'
                self.SP(Template2)
           
                key = INTERPOLATION.KeyOrganiser.GetSingleKey(PlotDetails, el) 
                LML.Load(INTERPOLATION.InterpolationKeys[key])
                
                LML.new_canvas()
                LML.produce_canvas(PlotDetails, el)
         
                


    def ValidationPlot(self, PlotDetails, el, pure, Data):
        '''Direct call to the sampling plots'''

        Template = '''
            Plotting Validation
                   ''' + '\n'

        self.SP(Template)
    
        MPL = MPLProducer(self.args, INTERPOLATION)
        MPL.PlotValidation(PlotDetails, el, pure, Data)



    def SamplingPlot(self, INTERPOLATION, MOLECULE):
        '''Direct call to the sampling plots'''

        Template = '''
            Plotting Sampling

                Dimensionality:               ''' + str(self.args.main.Dxyz) + '\n'


        for PlotType in self.args.plotting('PlotTypes') if isinstance(self.args.plotting('\
                        PlotTypes'), list) else [self.args.plotting('PlotTypes')]:

            PlotDetails = UTILS.GetTypeDetails(PlotType)

            for el in self.args.plotting('PlotElements') if isinstance(self.args.plotting('\
                      PlotElements'),list) else [self.args.plotting('PlotElements')]:         

                Template2 = Template + '\n\t\t' + UTILS.GetLabel(PlotDetails) + '\n\n\t\t'
                Template2 += el + ' with ' + str(INTERPOLATION.number_of_samples(PlotDetails,el))\
                             +' points\n'
                self.SP(Template2)

                #SHOULD ONLY TAKES THE SAMPLES FROM THE GP (AVOIDING THE NAN SAMPLES)
                MOL = MOLProducer(self.args, None, None, sampling_plot=True)
                key = INTERPOLATION.KeyOrganiser.GetSingleKey(PlotDetails, el)
                XYZ = MOLECULE.q2cart(INTERPOLATION.InterpolationKeys[key].X_train_)
                MOL.PlotSampling(PlotDetails, XYZ=XYZ)
 


