#!/bin/python
#File: core.plotting.utils_vmd_samples.py

from subprocess import call

from numpy import array, meshgrid, linspace, ravel, zeros, isnan, mean

import matplotlib.pyplot as MPL
from mpl_toolkits.mplot3d import axes3d, Axes3D
from matplotlib import cm, ticker

import core.plotting.plotting_utils as utils

class VMDSamples(utils.UtilsVMD):

    def __init__(self, AtomsList):

        super(VMDSamples, self).__init__(AtomsList, True) 



    def PlotSamples(self, Data, progressive=False):

        self.f.write(str(int(Data['XYZ'].shape[0]*Data['XYZ'].shape[1]))+'\nSamples\n')

        for sample in range(Data['XYZ'].shape[1]):
            for atom in range(len(self.AtomsList)):
                self.f.write('\t'+self.AtomsList[atom]+'\t'+'\t'.join([str(x)\
                             for x in Data['XYZ'][atom,sample,:]])+'\n')


