#!/usr/bin/python
#File: core.plotting.mpl_producer_1d.py

import numpy as np

import core.grapes_utils.main_utils as UTILS
from core.grapes_utils.progress_bar import DisplayBar

from core.data_management.data_utils import UnpackBasic
from core.plotting import plotting_utils
from core.plotting.utils_mpl_surfaces import MPLSurfaces 
from core.plotting.utils_mpl_statistics import MPLStatistics

class MPLProducer1D:

    def __init__(self, args, INTERPOLATION):

        self.Globalargs = args.main
        self.Moduleargs = args.plotting

        self.sigmaFit = args.interpolation.sigmaFit
        self.lambdaFit = args.interpolation.lambdaFit

        self.Moduleargs.set('Colours', ['rgb(255, 0, 0)', 'rgb(0, 0, 255)'])

        self.ValueType = {
                'Energy':  INTERPOLATION.PredictMean,
                'Error': INTERPOLATION.PredictVariance,
                'Samples': INTERPOLATION.DrawSamples
                         }

        self.MPLSurfaces = MPLSurfaces()



    def surface_grid(self, PlotDetails, el, limits_shift, Value='Energy',
                     basis_set=None, giveMinima=False, samples=None):
        '''
        Calculates the mean and the variance for a grid (for plotting purposes)

        meshgrid: plotly contours build their own meshgrid equivalent, this options simply returns
        whatever grid is used for plotting (Nsamp x Nsamp for True or Nsamp X Dim for False)
        '''
        from numpy import zeros, linspace, ones, c_

        Coordinates = plotting_utils.MeshGrid(self.Moduleargs, limits_shift)

        V = []
        print('\n\t-- Predicting '+Value+' on a '+str(self.Moduleargs.NSamplingPlot)+\
              ' points grid\n')

        if Value=='Samples':
            V.append(zeros((self.Moduleargs.NSamplingPlot,samples)))
        else:
            V.append(zeros(self.Moduleargs.NSamplingPlot))

        Q = Coordinates['Q0'].reshape((self.Moduleargs.NSamplingPlot,1))

        if Value=='Samples':
            V = self.ValueType[Value](Q, PlotDetails, el, samples=samples)
        elif basis_set is None:
            V = self.ValueType[Value](Q, PlotDetails, el)
        else:
            V = self.ValueType[Value](Q, PlotDetails, el)

        if giveMinima and i==len(self.Moduleargs.NSamplingPlot)-1:
            self.PrintApproximateMinima(V)

        return Coordinates['Q0'], V



    def PrintApproximateMinima(self, E):
        '''
        Gives the plotted point with the lowest energy (i.e. roughly the minima)
        '''

        minl = []
        for i in range(len(E[:,0])):
            minl.append(min(E[i,:]))

        print('\n\tGrid minimum is: ', min(minl))



    def PointEnergies(self, Type):
        '''
        Projects points from (q1,q2,q3...E) to (qplot1, qplot2, EGP)
        '''

        if self.Globalargs.MeshType=='Points':
            points = getattr(self.Data, 'Points')

        elif self.Globalargs.MeshType=='Delaunay':
            points = getattr(self.Data, 'Delaunay').points

        E = np.zeros((len(points[:,0]),self.Globalargs.NStates))
        Q = np.zeros((1,self.Globalargs.D))

        for el in range(self.Globalargs.NStates):
            for i in range(len(points[:,0])):
                q1=True
                j = 0
                for d in range(self.Globalargs.D):
                    if d in self.Moduleargs.CoordinatesPlot:
                        if q1:
                            Q[0,self.Moduleargs.CoordinatesPlot[0]] =\
                                points[i,self.Moduleargs.CoordinatesPlot[0]]
                            q1 = False
                        else:
                            Q[0,self.Moduleargs.CoordinatesPlot[1]] =\
                                points[i,self.Moduleargs.CoordinatesPlot[1]]
                    else:
                        Q[0,d] = self.Moduleargs.CoordinatesFrozen[j]
                        j += 1
                Ed = self.ValueType['Energy'](Type, self.Moduleargs.BasisPlot, Q)
                E[i,:] = Ed[:,el]

        return E


    def PlotSurface(self, PlotDetails, el):
        '''
        Mirrors the PlotInterface module
        '''
        pass


    def PlotError(self, limits_shift=0.0, setTitle=False):
        '''
        Mirrors the PlotInterface module
        '''
        pass



    def PlotContours(self, PlotDetails, el, limits_shift=0.0, setTitle=False, 
                     scaleError=1, add_plot=False):
        '''
        Plots the surface as a contour plot (options available in UtilsMPL)
        '''
        Data = {}

        Data['X'], Data['Z'] = self.surface_grid(PlotDetails, el, limits_shift)
        Data['X'], Data['Zerr'] = self.surface_grid(PlotDetails, el, limits_shift, Value='Error')

        if not add_plot:
            self.MPLSurfaces.new_canvas(
                logscale=False, D=1, labels=[r'$q_1\ [\mathrm{\AA}]$',r"$\mathrm{E}/%i\times"\
                " \Delta \mathrm{E} [Har]$" %int(scaleError)]
                                  )

        Data['Xmin'] = self.Moduleargs.GridLimitsPlot[0] + limits_shift
        Data['Xmax'] = self.Moduleargs.GridLimitsPlot[1] - limits_shift

        Data['Z'] = Data['Z'].flatten()
        Data['Zerr'] = scaleError*Data['Zerr'].flatten()
        Data['Zmin'] = self.Moduleargs.PotentialcutPlot[0]
        Data['Zmax'] = self.Moduleargs.PotentialcutPlot[1]
        Data['cbartitle'] = r'$\mathrm{E}\ [\mathrm{Ha}]$'


        if not add_plot and PlotDetails['Multiple']:
            self.MPLSurfaces.Line(Data, new_line = True)
            self.PlotContours(PlotDetails['Multiple'], el, limits_shift, setTitle, scaleError, 
                              add_plot=True)
        else:
            self.MPLSurfaces.Line(Data, new_line = False)

        if not add_plot:
            self.MPLSurfaces.produce_canvas({
    'filename': 'Contours_'+UTILS.GetDataLabel(PlotDetails)+'_el:'+el, 
    'title': None
                            })





    def PlotGradient(self, PlotDetails, el, limits_shift=0.0, setTitle=False, scatter=False, D=0):
        '''
        Plots the surface as a contour plot (options available in UtilsMPL)
        '''
        Data = {}

        Data['X'], Data['Z'] = self.surface_grid(PlotDetails, el, limits_shift, Value='Gradient')

        self.MPLSurfaces.new_canvas(logscale=False, D=1, 
                               labels=[r'$q_1\ [\mathrm{\AA}]$',r'$\mathrm{G}'])


        Data['Xmin'] = self.Moduleargs.GridLimitsPlot[0] + limits_shift
        Data['Xmax'] = self.Moduleargs.GridLimitsPlot[1] - limits_shift


        Data['Z'] = Data['Z'].flatten()
        Data['Zmin'] = self.Moduleargs.PotentialcutPlot[0]
        Data['Zmax'] = self.Moduleargs.PotentialcutPlot[1]
        Data['cbartitle'] = r"$\mathrm{G}_%i\ [\mathrm{Ha}/\mathrm{\AA}]$" %D

        self.MPLSurfaces.SimpleLine(Data)


        self.MPLSurfaces.produce_canvas({
            'filename': 'GradientContours_'+UTILS.GetDataLabel(PlotDetails)+'el:'+el,
            'title': None
                        })





    def PlotSamples(self, el, limits_shift=0.0, setTitle=False, samples=10):
        '''
        Draws Samples from the GP and plots them (gives some insights on the learning process)
        '''
        Data = {}

        for Type in self.Moduleargs.TypePlot:
            Data['X'], Data['Ztot'] = self.surface_grid(Type, limits_shift, Value='Samples', samples=samples)
            Data['X'], Data['Z'] = self.surface_grid(Type, limits_shift, Value='Energy')
            Data['X'], Data['Zerr'] = self.surface_grid(Type, limits_shift, Value='Error')

            DataUnpacked = UnpackBasic(self.Data, Type, self.Moduleargs.BasisPlot)

            self.MPLSurfaces.new_canvas(logscale=False, D=1, labels=[r'$q_1\ [\mathrm{\AA}]$',r"$\mathrm{E}[Har]$"])
            Data['Xmin'] = self.Moduleargs.GridLimitsPlot[0] + limits_shift
            Data['Xmax'] = self.Moduleargs.GridLimitsPlot[1] - limits_shift

            Data['Z'] = Data['Z'][int(el)]

            Data['Xp'] = DataUnpacked["Points%i"%int(el)]
            Data['Zp'] = DataUnpacked["Data%i"%int(el)]

            Data['Zerr'] = Data['Zerr'][int(el)]
            Data['Zmin'] = self.Moduleargs.PotentialcutPlot[0] #np.min(E[state])-0.1 #TODO make shift more automatic
            Data['Zmax'] = self.Moduleargs.PotentialcutPlot[1] #np.percentile(E[state],80)
            Data['cbartitle'] = r'$\mathrm{E}\ [\mathrm{Ha}]$'

            self.MPLSurfaces.Line(Data)


            for sample in range(samples):
                Data['Z'] = Data['Ztot'][int(el)][:,sample]

                self.MPLSurfaces.SimpleLine(Data)


            if setTitle: Data['title'] = "GP Samples"
            Data['Zmax'] = self.Moduleargs.PotentialcutPlot


            self.MPLSurfaces.produce_canvas({
            'filename': 'Samples_'+UTILS.GetDataLabel(PlotDetails)+'el:'+el,
            'title': None
                            })
