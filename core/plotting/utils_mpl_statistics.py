#!/bin/python
#File: core.plotting.utils_mpl_statistics.py

import numpy as np
import pandas as pd

import matplotlib.pyplot as MPL
from mpl_toolkits.mplot3d import axes3d, Axes3D
from matplotlib import cm, ticker

import core.grapes_utils.main_utils as UTILS
import core.plotting.plotting_utils as utils


class MPLStatistics(utils.UtilsMPL):

    def __init__(self):

        self.n_bins = 100
        super(MPLStatistics, self).__init__()



    def DistributionTrend(self, Data, trendline=True):

        distribution = pd.DataFrame(Data['Z'])

        if trendline:
            distribution.plot.kde(
                ax=self.axes,
                style = {'color': 'b', 'size': 1}
                                 )
        distribution.plot.hist(
            density=True, ax=self.axes, bins=self.n_bins
                              )



    def RMSEHistogram(self, Data, CE=0.05): 
        
        _range = tuple([Data['PredictionError'].min()-CE,Data['PredictionError'].max()+CE])

        distribution = pd.DataFrame(Data['Z'])

        distribution.plot.kde(
            ax=self.axes,
            style = {'color': 'b', 'size': 1}
                             )
        distribution.plot.hist(
            density=True, ax=self.axes, bins=self.n_bins
                              )


    def RMSE(self, Data, CE=0.01, log_variance=True):

        colours = Data['StandardDev'].flatten() 
        if log_variance:
            with np.errstate(divide='ignore'): 
                colours = np.log(colours)
            colours[np.isneginf(colours)] = -15.0 #random variance to show on the plot

        SCAT = self.axes.scatter(
            Data['Predicted'].flatten(), Data['PredictionError'].flatten(),
            marker='o', s=2*self.sizeScatter, c=colours
                                    )

        self.axes.set_xlim(
            Data['Predicted'].min()-CE, Data['Predicted'].max()+CE)
                          

        #Plot the mean of the distribution as well as a gray zone for the chemical accuracy 
        X = np.linspace(self.axes.get_xlim()[0], self.axes.get_xlim()[1],2)
        self.axes.plot(
            X, [np.mean(Data['PredictionError'].flatten()), 
                np.mean(Data['PredictionError'].flatten())],
            'k--', linewidth=1
                      )
        self.axes.fill_between(
            X, np.zeros_like(X) + 0.0016, np.zeros_like(X) - 0.0016,
            facecolor='grey', alpha=0.5
                              )


        cbar = MPL.colorbar(SCAT)
        cbar.ax.tick_params(labelsize=15)
        cbar.ax.set_ylabel(r'$\sigma_{\mathrm{GP}}$', fontsize=20)                        
