#!/bin/python
#File: core.plotting.lml_producer.py

import numpy as np
from copy import copy

import matplotlib.pyplot as MPL
from mpl_toolkits.mplot3d import axes3d, Axes3D
from matplotlib import cm, ticker

import core.interpolation.gaussian_processes_utils as utils
from core.interpolation.hyperparameters_utils import getHP, setHP
from core.grapes_utils.progress_bar import DisplayBar

class LMLProducer:

    def __init__(self):
        self.CSSurface = cm.get_cmap('viridis')
        
        MPL.rc('text', usetex=True)
        MPL.rc('font', family='serif')


    def Load(self, GP):
        self.GP = GP

        if hasattr(getHP(GP, 'length_scale'), '__len__'):
            self.D = len(getHP(GP, 'length_scale'))
        else:
            self.D = 1 



    def new_canvas(self, linesearch=False):
        '''
        Creates a new instance of a figure
        '''

        if self.D==1:
            self.figure, self.axes = MPL.subplots()
    
        else:
            if self.D<5:
                self.subD = 2
            elif self.D<10:
                self.subD = 3
            elif self.D<17:
                self.subD = 4

            self.figure, self.axes = MPL.subplots(self.subD,self.subD)
            self.figure.subplots_adjust(bottom=0.2, left=0.14)#, right=0.2)

    
        if linesearch:
            self.ShowLineSearch()

        else:
            self.MakeContours()



    def Plot(self, Type, basis_set, el, format='pdf'):
        '''
        Produces plot after the relevant objects to be plotted have been passed to 
        the self.axes/self.figure instances
        '''

        try:
            self.figure.savefig('LogMarginalLikelihood_'+Type+'_'+basis_set+'_'+str(el)+'.pdf', 
                                format=format)

        except TypeError:
            print('\nThe figure could not be saved\n')
            MPL.show()
        

    def PlotIndex(self,n):
        return n%self.subD, n//self.subD


    def ShowLineSearch(self):
        #TODO
        pass


    def MakeContours(self, NS=50, levels=20, log=False, logticks=False):

        PB = DisplayBar(self.D*NS*NS)
        print('\t-- Calculating log-marginal-likelihood values on a '+str(NS)+'X'+str(NS)+' grid\n')

        for hpi in range(self.D):

            self.axes[self.PlotIndex(hpi)].set_xlabel(r'$\lambda \ [\mathrm{\AA}]$', fontsize=22)
            self.axes[self.PlotIndex(hpi)].set_ylabel(r'$\sigma '+str(hpi)+r'\ [\mathrm{\AA}]$', 
                                                      fontsize=22)

            theta = copy(self.GP.kernel_.theta)
            Data = {'X': np.logspace(1e-2,1e1,NS),
                    'Y':np.logspace(1e-2,1e1,NS),
                    'Z': np.zeros((NS,NS))
                   }

            for i in range(NS):
                for j in range(NS):
                    theta[0] = Data['X'][i]
                    theta[hpi+1] = Data['Y'][j]
                    Data['Z'][i,j] = self.GP.log_marginal_likelihood(theta)
                    
                    PB.Show(hpi*NS*NS+i*NS+j)


            locator = ticker.LogLocator()
            levelsP = np.logspace(1e-2, 1e1, levels)
 
            CP = self.axes[self.PlotIndex(hpi)].contourf(
                          Data['X'], Data['Y'], Data['Z'], levels=levelsP,
                          locator = locator,
                          cmap=self.CSContour, 
                          vmin=1e-2, vmax=1e1
                          )

            cbar = MPL.colorbar(CP, ticks=['-2', '-1', '-0', '1'])
            cbar.ax.set_yticklabels([r'$10^{-2}$', '-1', '0', '1'])
            cbar.ax.tick_params(labelsize=15)
            cbar.ax.set_ylabel(Data['cbartitle'], fontsize=20)



