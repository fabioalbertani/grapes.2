#!/bin/python
#File: core.testing.rmse.py

from numpy import mean, sqrt

from core.geometry.sampling import FramedGrid

from core.data_management.data_utils import ReadInputCoordinates
import core.grapes_utils.main_utils as UTILS

from core.testing.testing_utils import TestInterface


class RMSE(TestInterface):
    '''
    This class will estimate the mean square error of the surface

    Useful for analytical examples where the on-the-fly evaluation is cheap (meant for 
    testing convergence of covariance functions wrt number of points)
    '''

    @classmethod
    def RegisterOptions(cls, args):
        '''stores the options relevant to this testing set'''
        m = args.RegisterModule('testing')

        args.Register(m, '--test_tolerance', type=float, default=1e-7)
        args.Register(m, '--NSamples_test', type=int, default=100)


    @staticmethod
    def ParseOptions(options):
        return options


    def __init__(self, args, MOLECULE, validation=False):

        self.MOLECULE = MOLECULE
        self.validation = validation

        super(RMSE, self).__init__(args)


    def Test(self, INTERPOLATION, CalcDetails, el, pure, RunOrganiser=None, Q=None, 
             filename=None, fromfile=False):

        Type = 'Energy' if CalcDetails is None else CalcDetails['Type']    

        if not filename is None:
            Data = ReadInputCoordinates(self.Globalargs, self.MOLECULE, CalcDetails, 
                                        filename=filename)

        elif fromfile:
            filename = self.Globalargs.CDPath+'/SavedData/'+self.Globalargs.Label+\
                       '/TestMeshInformation_'+UTILS.GetDataLabel(CalcDetails)+'.qdat'
            Data = ReadInputCoordinates(self.Globalargs, self.MOLECULE, CalcDetails, 
                                        filename=filename)

            Data['XYZ'] = self.MOLECULE.q2cart(Data['Points'], fix=True)

            if pure:
                RMSE_Data = self.Predict(INTERPOLATION, Data['Points'], CalcDetails, 
                            el, pure, Data['INT-'+Type])
            else:
                RMSE_Data = self.Predict(INTERPOLATION, Data['Points'], CalcDetails, 
                            el, pure, Data[Type])

            self.Print(CalcDetails, Data['Points'], RMSE_Data)
            return RMSE_Data

        else:
            if Q is None:
                Q = FramedGrid(self.Globalargs, N=self.Moduleargs.NSamples_test)

                Data = RunOrganiser.RunSamples(Q)
                Data['XYZ'] = self.MOLECULE.q2cart(Q, fix=True)

                if pure:
                    RMSE_Data = self.Predict(INTERPOLATION, Data['Points'], CalcDetails, 
                                el, pure, Data['INT-'+Type])
                
                else:
                    RMSD_Data = self.Predict(INTERPOLATION, Data['Points'], CalcDetails, 
                                el, pure, Data[Type])

                self.Print(CalcDetails, Data['Points'], RMSD_Data)

            else:
                Data = RunOrganiser.RunSamples(Q)
                Data['XYZ'] = self.MOLECULE.q2cart(Q, fix=True)

                if pure:
                    RMSE_Data = self.Predict(INTERPOLATION, Data['Points'], CalcDetails, 
                                el, pure, Data['INT-'+Type])        

                else:
                    RMSD_Data = self.Predict(INTERPOLATION, Data['Points'], CalcDetails, 
                                el, pure, Data[Type])

                self.Print(CalcDetails, Data['Points'], RMSD_Data)
    
            return RMSD_Data




    def Predict(self, INTERPOLATION, Q, CalcDetails, el, pure, E, normalise=False):

        _PE = INTERPOLATION.PredictMean(Q, CalcDetails, el, pure)
        _PV = INTERPOLATION.PredictVariance(Q, CalcDetails, el, pure)

        v_RMSD = sum(sqrt((E.flatten()-_PE.flatten())**2))/E.shape[0]
        if normalise:
            v_RMSD -= mean(E) - mean(_PE)


        return {
            'Predicted': _PE,
            'PredictionError': _PE - E,
            'PredictionVariance': _PV,
            'StandardDev': sqrt(_PV),
            'RMSD': v_RMSD,
            'Success': mean(v_RMSD) < self.Moduleargs.test_tolerance
               }



    def Print(self, CalcDetails, Q, Data):

        if self.validation: 
            f=open(self.Globalargs.CDPath+'/SavedData/'+self.Globalargs.Label+\
                   '/RMSDTestSet_'+UTILS.GetDataLabel(CalcDetails)+'.qdat', 'w')
            f.write('--Nodes Information--')

            template='\ni\t\t'
            for i in range(self.Globalargs.D):
                template+='q'+str(i)+'\t\t\t\t'

            f.write(template+'E\t\t\t\tD_E\t\t\t\tV_E\n')

            self.SP('\n\tProducing dat file for '+str(Q.shape[0])+' points\n')
            
            for n in range(Q.shape[0]):
                f.write('\n'+str(n)+'\t\t')

                for d in range(self.Globalargs.D):
                    f.write("%f\t\t" %Q[n,d])

                f.write("%f\t\t%f\t\t%f" %(Data['Predicted'][n], 
                                           Data['PredictionError'][n],
                                           Data['PredictionVariance'][n]))
            
            
            f.close()


        template = """
        Size of test set:               %i
        Root square mean error:         %f
        Mean prediction variance:       %f


        Prediction points below tolerance:      %f %%
        Overall success:                        %s

                   """ %(Q.shape[0], Data['RMSD'], 
                         mean(Data['PredictionVariance']),
                         len([x for x in Data['PredictionError'] if\
                         (abs(x)<self.Moduleargs.test_tolerance)])/len(Data['PredictionError']),
                         str(Data['Success']))
        self.SP(template)



