#!/bin/python
#File: core.testing.point_metric.py

from numpy import append, delete, zeros
import core.testing.testing_utils as utils


class PointMetric(utils.TestInterface):

    @staticmethod
    def RegisterOptions(args):
        '''stores the options relevant to this testing set'''
        m = args.RegisterModule('testing')

        args.Register(m, '--test_tolerance', type=float, default=1e-7)
        args.Register(m, '--ElementTest', default=None, type=str)


    @staticmethod
    def ParseOptions(options):
        pass

    def __init__(self, args, method='Variance'):
        '''
        method allows to select which type of metric is used to test the barycenters

        if a sampling method is selected the barycenters become irrelevant (but the 
        process is exactly the same and so this module can be used)
        '''
        if method=='Variance':
            '''
            the convergence for the GP confidence interval should be tighter
            '''
            self.method = method
            
        elif method=='Error':
            self.method = method

        else:
            raise Exception('The barycenter test method is not implemented')

        super(PointMetric, self).__init__(args)



    def Test(self, INTERPOLATION, CalcDetails, el, RunOrganiser=None, Q=None, noMesh=False, notrim=False):

        if self.method=='Variance':
            fTest = INTERPOLATION.PredictVariance(Q, CalcDetails, el)
            idx = []

            for i in range(len(fTest)):
                if fTest[i] < self.Moduleargs.test_tolerance:
                    idx += [i]

            self.NP = tuple([len(idx), Q.shape[0]])

        elif self.method=='Error':
            fTest = INTERPOLATION.PredictMean(Q, CalcDetails, el)
            fTrue = RunOrganiser.RunSamples(Q)
            idx = []

            for i in range(len(fTest)):
                try:
                    if abs(fTest[i]-fTrue['Energy'][i]) < self.Moduleargs.test_tolerance: 
                        idx += [i]
                except ValueError:
                    #TODO TODO Make the element be associated to the corresponding element 
                    for j in range(Nstates):
                        if abs(fTest[i]-fTrue['Energy'][i,j]) < self.Moduleargs.test_tolerance:
                            idx += [i] 

            self.NP = tuple([len(idx), Q.shape[0]])

        #These are the indices of points that should be kept
        idxneg = delete(range(0, len(Q[:,0])), idx)

        if noMesh:
            if self.method=='Variance':
                fTrue = RunOrganiser.RunSamples(Q, skip=idx)
            elif self.method=='Error':
                fTrue = Utils.SelectPoints(fTrue, idxneg) 


            if len(idx)==len(Q[:,0]):
                return {'Success': True, 'Data': fTrue}
            else:
                return {'Success': False, 'Data': fTrue} 


        else:
            self.SP('\n\t-- Adding '+str(min(len(idxneg),self.NMax))+' of '+\
                          str(Q.shape[0])+' points to the mesh\n')

            if len(Q[:,0]) - len(idx) > self.NMax:
                from numpy import argsort

                Q = Q[argsort(fTest, axis=0).flatten(),:]
                #idxneg of argsort is taken and then idxneg is reorder to reflect changes in Q
                idxneg = [i for i in idxneg[argsort(delete(fTest, idx), axis=0).flatten()] 
                          if i<self.NMax]
                Q = Q[:self.NMax,:].reshape((self.NMax,self.Globalargs.D))


            if Q is None:
                if not len(Q[idxneg,:]):
                    return {'Success': True, 'Data': None}

                else:
                    if self.method=='Variance':
                        fTrue = RunOrganiser.RunSamples(Q[idxneg,:])
                    elif self.method=='Error':
                        fTrue = Utils.SelectPoints(fTrue, idxneg)
                    return {'Success': False, 'Data': fTrue}

            else:
                if self.method=='Variance': 
                    fTrue = RunOrganiser.RunSamples(Q, skip=idx)
                elif self.method=='Error':
                    fTrue = Utils.SelectPoints(fTrue, idxneg)
                return {'Success': False, 'Data': fTrue}

            

    def PrintTestResult(self):
        print """
        Accurate new samples:           %i of %i (%.2f %%)
              """ %(self.NP[0], self.NP[1], self.NP[0]/self.NP[1])





