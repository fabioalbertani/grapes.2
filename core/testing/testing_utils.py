#!/bin/python
#File: testing_utils.py

import abc

from numpy import delete

import core.grapes_utils.main_utils as UTILS

class TestInterface:
    '''
    Testing classes 
    '''
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def RegisterOptions(cls, args):
        '''stores the options relevant to this testing set'''


    @abc.abstractmethod
    def ParseOptions(options):
        pass

    def __init__(self, args):
        '''
        Stores the arguments needed for testing 

        NMax allows to keep the number of testing point to
        remain below the initial sampling to avoid runs of
        big bulks of data at once (it is preferable to run
        multiple small batches than a large one containing
        many points that will be eventually discarded)
        '''
        self.Globalargs = args.main
        self.Moduleargs = args.testing
        self.SP = UTILS.SuperPrint('Testing.log', twofile=True)

        self.NMax = max(5**self.Globalargs.D+1,self.Globalargs.NSampling)


    @abc.abstractmethod
    def Test(self, INTERPOLATION, CalcDetails, el, *args):
        return {'Success': None,
                'Data': None}

    def PrintTestResult(self):
        '''Printing is specific to the type of test being performed'''
        pass




def DelaunayBarycenters(QSimplices, AccurateSimplices=None):
    from numpy import mean, zeros

    NSimplices = len(QSimplices[:,0,0])
    D = len(QSimplices[0,0,:])   

    def Barycenter(Q):
        return mean(Q, axis=0) 

    if AccurateSimplices is None:
        NAccurate = 0
    else:
        NAccurate = len([s for s in AccurateSimplices if s])


    B = zeros((NSimplices-NAccurate, D))


    simplexcount = 0

    for simplex in range(NSimplices):

        if AccurateSimplices is None or not AccurateSimplices[simplex]:
            B[simplexcount, :] = Barycenter(QSimplices[simplex,:,:])          
            simplexcount += 1


    return B.reshape((NSimplices-NAccurate,D))

            


def GeneralBarycenters(QSimplices, AccurateSimplices=None):
    from numpy import mean, zeros

    NSimplices = len(QSimplices[:,0])-1
    if AccurateSimplices is None:
        NAccurate = 0
    else:
        NAccurate = len(AccurateSimplices)

    D = len(QSimplices[0,:])

    def Barycenter(Q):
        return mean(Q, axis=0)

    B = zeros((NSimplices, D))

    simplexcount = 0

    for simplex in range(NSimplices):

        if AccurateSimplices is None or not AccurateSimplices[simplex]:
            B[simplexcount, :] = Barycenter(QSimplices[simplex:simplex+2,:])
            simplexcount += 1


    return B.reshape((NSimplices-NAccurate,D))




def SelectPoints(Data, idx):
    '''Removes the data from the relevant indices in a data dictionary'''

    for key in Data.keys():

        if Data[key] is None:
            pass

        elif isinstance(Data[key], list):
            Data[key] = [Data[key][i] for i in range(len(Data[key])) if not i in idx]

        else:
            Data[key] = delete(Data[key], idx, axis=0)

    return Data








