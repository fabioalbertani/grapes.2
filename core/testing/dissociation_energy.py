#!/bin/python


from numpy import asarray
from numpy.random import shuffle


#TODO WILL MAKE MORE SENSE WHEN USED WITH GP TRAINED ON HESSIAN MATRICES 


import sys, os
sys.path.append(os.environ['MODULEPATH'])

import Testing.Utils as Utils




class DissociationEnergy(Utils.TestInterface):

    @staticmethod
    def RegisterOptions(args):
        '''stores the options relevant to this testing set'''
        m = args.RegisterModule('testing')
    
        args.Register(m, '--DissociationEnergy', type=float, default=0.166107, 
                      help='H2 dissociation energy is given as default')
        args.Register(m, '--WhichStretch', type=int, default=0, 
                      help='Defines which motion is being "dissociated" i.e. q(WhichStretch)')
        args.Register(m, '--test_tolerance', type=float, default=1e-5)


    @staticmethod
    def ParseOptions(options):
        return options


    #ISSUE: The input is not compatible with other test modules and dissociation energy is not given as an input
    def __init__(self, args=None):

        self.Globalargs = args['Main']
        self.Moduleargs = args['Testing']

        self.progression = []



    def PotentialEnergy(self, q, state=0): #TODO decide how to test statea

        Q = self.Globalargs.GridLimits[1::2]
        Q[self.Moduleargs.WhichStretch] = q

        InterpolatedData = self.Data.Interpolate(self.Type, self.basis_set, asarray(Q).reshape((1,self.Globalargs.D)))

        return InterpolatedData.energy[0]




    def Test(self, RunOrganiser, Data, Type, basis_set, NStates, Q=None):

        self.Data = Data
        self.basis_set = basis_set
        self.Type = Type

        from scipy import optimize
        Result = optimize.minimize(self.PotentialEnergy, 1.0, jac=False, method='L-BFGS-B', tol=1e-10)

        Edis = -(self.PotentialEnergy(Result.x) - self.PotentialEnergy(self.Globalargs.GridLimits[self.Moduleargs.WhichStretch+1]))

        if self.progression==[]:
            self.progression.append(Edis)
            return {'NewPoints': True, 'points': self.Run(RunOrganiser, Data, Q)}

        else:
            self.progression.append(Edis)
            if abs(Edis - self.progression[-2]) < self.Moduleargs.Testtol:
                return {'NewPoints': False, 'points': None}

            else:
                return {'NewPoints': True, 'points': self.Run(RunOrganiser, Data, Q)}



    def Run(self, RunOrganiser, Data, Q, RandomSampling=False, NMax=200):

        if Q is None:
            if RandomSampling:
                from ..Geometry.Sampling import RandomSampling
                QAdd = RandomSampling(self.args)
                if len(QAdd[:,0])>NMax:
                    shuffle(QAdd)
                    QAdd = QAdd[:NMax,:]
            else:
                from ..Geometry import Utils

                QAdd = Utils.DelaunayBarycenters(getattr(Data, 'Delaunay').points[getattr(Data, 'Delaunay').simplices], AccurateSimplices=Data.MeshDetails[basis_set+'AccurateSimplices'])

                if len(QAdd[:,0])>NMax:
                    shuffle(QAdd)
                    QAdd = QAdd[:NMax,:]
        else:
            QAdd = Q

        return RunOrganiser.RunPoints(QAdd)




    def PlotProgression(self):

        try:
            import matplotlib.pyplot as plt
            from numpy import ones
            
            plt.plot(range(1,len(self.progression)+1), self.progression)
            plt.plot(range(1,len(self.progression)+1), self.Moduleargs.DissociationEnergy*ones(len(self.progression)))
            plt.show()


        except ImportError:

            print('matplolib module not available...\nDumping data in DissociationEnergy.log\n')
            f = open('DissociationEnergy.log', 'w')
            for i in range(len(self.progression)):
                f.write("%i\t%f\t%f\n" %(i, self.Moduleargs.DissociationEnergy, self.progression[i]))
            f.close()
            return




