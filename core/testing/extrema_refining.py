#!/bin/python


#Point error:
#   - The accuracy of the mesh is calculated by taking the error of the interpolation at new points 
#   - The calculated values which are out of the error tolerance are used to improve the mesh
#   - The simplices which give an accurate enough interpolation are not tested after that point

import sys



class Main:

    def __init__(self, args, tol=1e-3):
        from ..Geometry.ExtremaLocator import Main

        self.D = args['Main'].D
        self.MeshType = args['Main'].MeshType
        self.GridLimits = args['Main'].GridLimits
        self.tol = tol
        
        self.ExtremaLocator = Main(args['Main'])



    def Test(self, RunOrganiser, Data, Type, basis_set, Nstates, Q=None, notrim=False, NMax=200):
        from numpy import zeros

        Extrema = self.ExtremaLocator.Locate(Data, Type, basis_set)

        #Removes extrema found outside the considered limits

        idx = []
        for i in xrange(len(Extrema['Minima'])):
            for d in xrange(self.D):
                
                if Extrema['Minima'][i][d]<self.GridLimits[2*d] or Extrema['Minima'][i][d]>self.GridLimits[2*d+1]:
                    idx.append(i)
        
        Extrema['Minima'] = [l for i,l in enumerate(Extrema['Minima']) if i not in idx]

        idx = []
        for i in xrange(len(Extrema['Maxima'])):
            for d in xrange(self.D):

                if Extrema['Maxima'][i][d]<self.GridLimits[2*d] or Extrema['Maxima'][i][d]>self.GridLimits[2*d+1]:
                    idx.append(i)

        Extrema['Maxima'] = [l for i,l in enumerate(Extrema['Maxima']) if i not in idx]


        print("\n\tFound %i minima and %i maxima" %(len(Extrema['Minima']),len(Extrema['Maxima'])))

        if len(Extrema['Minima'])+len(Extrema['Maxima'])!=0:
            print('\n\n\t-- Adding them to the current mesh\n')


        QTest = zeros((len(Extrema['Minima'])+len(Extrema['Maxima']),self.D))

        for i in xrange(len(Extrema['Minima'])+len(Extrema['Maxima'])):

            if i < len(Extrema['Minima']):
                QTest[i,:] = Extrema['Minima'][i] 
            else:
                QTest[i,:] = Extrema['Maxima'][i-len(Extrema['Minima'])]


        if len(QTest)!=0:
            Calculated = RunOrganiser.RunPoints(QTest)

            return {'Success': False, 'Data': Calculated}
        
        else:
            return {'Success': True, 'Data': None}











