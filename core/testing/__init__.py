#!/usr/bin/python
#File: core.testing.__init__.py

'''
Modules to improve the accuracy of the training set.
possible dependencies on core.interpolation.interpolation_interface.py
'''

__all__ = ['dissociation_energy', 'extrema_refining', 'rmse', 'point_metric',
           'testing_utils']
