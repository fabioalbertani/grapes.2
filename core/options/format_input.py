#!/bin/python
#File: format_input.py


import string

class PartialFormatter(string.Formatter):
    def __init__(self, missing=' ', bad_fmt='!!'):
        self.missing = missing
        self.bad_fmt = bad_fmt

    def get_field(self, field_name, args, kwargs):
        try:
            val=super(PartialFormatter, self).get_field(field_name, args, kwargs)
        except (KeyError, AttributeError):
            val=None,field_name 
        return val 

    def format_field(self, value, spec):
        if value==None: 
            return self.missing
        try:
            return super(PartialFormatter, self).format_field(value, spec)
        except ValueError:
            if self.bad_fmt is not None: return self.bad_fmt   
            else: raise




def ReadParameters(InputFileName):
    '''
    This functions reads in all parameters and stores them in two dictionaries:

    One contains all the names to produce a template in MainInterface.py while the other has
    the "values" containing the dashes to pass them as arguments to Main.py
    '''

    import re

    ReadParameters = {'InputFileName': '--InputFileName='+str(InputFileName)}

    if InputFileName==None:
        #Most arguments will be given default values
        ReadParameters['InputFile']='Not provided'
        print('\n\tInput File not provided\n')
        return None

    else:
        InputFile = open(InputFileName, 'r')

        for line in InputFile:
            if line.strip():
                line = line.rstrip('\n')
                information = line.split()

                if information[0]=='geometry':
                    InputFile.close()
                    return ReadParameters

                elif information[0][0]!='#':
               
                    Parameter = information[0]
                    
                    if len(information)==1:
                        ReadParameters[information[0]] = '-' + information[0] if\
                            len(information[0])==1 else '--' + information[0]
                        
                    elif len(information)==2:
                        ReadParameters[information[0]] =\
                            '-' + information[0] + '=' + information[1]\
                            if len(information[0])==1 else\
                            '--' + information[0] + '=' + information[1]

                    else:
                        ReadParameters[information[0]] =\
                            '-' + information[0] + '=' + ' '.join(information[1:])\
                             if len(information[0])==1 else\
                            '--' + information[0] + '=' + ' '.join(information[1:])


        InputFile.close()
        return ReadParameters
