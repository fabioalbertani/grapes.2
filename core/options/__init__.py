#!/usr/bin/python
#File: core.options__init__.py

'''
Modules to organise the main arguments of the GRAPES program, interface directly with
core.runners.main/plot_interface.py
'''

__all__ = ['format_input', 'options_utils', 'options_interface']
