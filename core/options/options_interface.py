#!/bin/python
#File: core.options.options_interface.py

import sys, os, re
from core.options.options_utils import OptionsStructure

GlobalOptions = OptionsStructure()

def ParseOptions(options, argsin=None):
    '''
    Function to import all relevant module which have a RegisterOptions @classmethod
    and format the parsed arguments with the associated @staticmethods

    Args:
        options:    [GlobalOptions] GlobalOption object
        argsin:     [list] sys.argv ouptut
    '''
    from numpy import array
    from qcmagic.QCInterface.QCQueue import QCQueue

    options.RegisterModules(
        RegisterOptions,
        QCQueue
                           )

    options.Parse('main')
    options._Parse('queue') #queue is parsed with optparse and not argparse


    options.main.set('InputFileName', 'InputFiles/' +\
        options.main.InputFileName + '.input')

    if not options.main('CalcTypes')==[]:
        options.main.set('CalcTypes', [s for s in options.main('CalcTypes').split()])
    if not options.main('MethodTypes')==[]:
        options.main.set('MethodTypes', [s for s in options.main('MethodTypes').split()])

    if not options.main('CalcTypeDifference') is None:
        '''parses the CalcTypeDifference provided'''
        options.main.set('CalcTypeDifference',\
            [s for s in options.main('CalcTypeDifference').split()])
        ops = options.main('CalcTypeDifference')
        options.main.set('CalcTypeDifference', {})
        for i,s in enumerate(ops):
            options.main('CalcTypeDifference')[options.main('CalcTypes')[i]] =\
                s if s!='none' else None
        options.queue.save = True
    
    else:
        '''fills dictionary with "none" since it was not provided'''
        options.main.set('CalcTypeDifference', {})
        for i,s in enumerate(options.main('CalcTypes')):
            options.main('CalcTypeDifference')[s] = None


    options.main.set('AtomsList', [s for s in options.main('AtomsList').split()])
    options.main.set('Dxyz', 3*len(options.main('AtomsList')))

    if options.main('ProvidedStates')!=None:
        if not os.path.isfile(options.main('ProvidedStates')):
            print('\n\tOutput file containing states not found ... running without given states')
            options.main.set('ProvidedStates', None)
        else:
            for line in open(options.main('ProvidedStates'), 'r'):
                if re.search('Scratch', line):
                    options.main.set('ProvidedStates', line.split()[4][:-1]) #removes a dash
                    print('\n\tInitial guess provided as: ', options.main('ProvidedStates'))
                    break
    
    if options.main('correlation')=='ccsdt': 
        options.main.set('correlation', 'ccsd(t)')


    if options.main('ReadData') is None:
        options.main.set('ReadData', {})

        for CalcDetails in options.main('CalcTypes'):
            options.main('ReadData')[CalcDetails] = False

    else:
        options.main.set('ReadData', [s for s in options.main('ReadData').split()])
        Dict = {}
        i=0

        while i<len(options.main('ReadData')):
            Dict[str(options.main('ReadData')[i])] = bool(int(options.main('ReadData')[i+1]))
            i += 2

        options.main.set(('ReadData'), Dict)
 
    if not options.main('TestingMethods')==[]:
        options.main.set('TestingMethods', [s for s in options.main('TestingMethods').split()])

    if not options.main('ValidationMethods')==[]:
        options.main.set('ValidationMethods', [s for s in options.main('ValidationMethods').split()])

    if not options.main('GridLimits')==[]:
        options.main.set('GridLimits', [float(s) for s in options.main('GridLimits').split()])
    else:
        Limits = []
        for d in range(options.main('D')):
            Limits.append(0.1)
            Limits.append(5.0)
        options.main.set('GridLimits', Limits)







def RegisterOptions(args):
    m = args.RegisterModule('main')
    
    args.Register(m, '-i', '--InputFileName', type=str, default=None)
    args.Register(m, '--Label', type=str, default=None,
                  help='Label is used in many parts of the code to assign specific directories'\
                       ' and names to produced data')
    args.Register(m, '--verbose', type=int, default=0)
    args.Register(m, '--CDPath', type=str, default=None, help='Current directory (where data will'\
                                                              ' eventually be saved)')
    args.Register(m, '-p', '--Processors', type=int, default=1)

    args.Register(m, '--remote', action='store_true', default=False)

    args.Register(m, '--RunningMode', type=str, default='Generational', help='Defines how points'\
                                                        ' are passed to the building modules')

    args.Register(m, '--AtomsList', type=str, default=None, #HERE
                  help='Elements of each atom in the system (used to generate inputs)')
    args.Register(m, '-D', type=int, default=0,
                  help='Refers to the dimensionality of the problem NOT the dimensionality of the'\
                       ' system: it should however be consistent with CoordinatesTransform')
    

    args.Register(m, '--Charge', type=int, default=0)
    args.Register(m, '--Spin', type=int, default=1)
    args.Register(m, '--NStates', type=int, default=1)

    args.Register(m, '--CalcTypes', default=[], help='list of calculations (or results) treated')
    args.Register(m, '--CalcTypeDifference', type=str, default=None,
                  help='fits the calculation type with the type given')
    args.Register(m, '--MethodTypes', default=[], help='list of methods treated')
    args.Register(m, '--CalcTypeFindMin', type=str, default=None, 
                  help='Defines which Type the minimalistic sampling will be found in') 
    args.Register(m, '--correlationUseJ', action='store_true', default=False)

    args.Register(m, '--dipole', action='store_true', default=False)
    args.Register(m, '--unrestricted', action='store_true', default=False)

    args.Register(m, '--noci', action='store_true', default=False, help='Enables noci calculations')

    args.Register(m, '--ProvidedStates', type=str, default=None, 
                  help='Provides the path to a previous calculation (which should be stored'\
                       'in InputFiles/foo.out)')
    args.Register(m, '--scratchfeed', type=str, default='def', 
                  help='none: never uses previous calculations, ordered: uses previous'\
                  'calculations within a single calculation batch, def: uses previous calculation'\
                  'batches (ovo projections and such)')


    args.Register(m, '--ReadData', default=None, 
                  help='List of basis sets and calculation types for which the data is to be'\
                       'read from a .dat file')
    args.Register(m, '--ReadOldPoints', default=False, action='store_true')
    args.Register(m, '--ImproveReadData', default=False, action='store_true')

    args.Register(m, '--CoordinatesFormat', type=str, default='Q', help='Defines if the data is'\
                     'written/read in XYZ or Q format')
    args.Register(m, '--CoordinatesTransform', type=str, default=None,
                  help='Name of .pyf module used to go from {qi qj ... qD} to {x y z}')
    args.Register(m, '--UseSymmetry', action='store_true', default=False)
    args.Register(m, '--ReactionCoordinates', action='store_true', default=False, help='If set to'\
                     'True the coordinates are set as RC instead of the specified one in'\
                     'CoordinatesTransform')
    args.Register(m, '--NormalModes', action='store_true', default=False, 
                  help='Calculates nm from the given geometry and uses them as q-coordinates')
    
    args.Register(m, '--MeshType', type=str, default='Delaunay',
                  help='Defines the class used to process points: Not all classes will be'\
                  'compatible with test modules (i.e. PointClass and BarycentersError do'\
                  'not work together)')

    args.Register(m, '--Potentialcut', type=float, default=1e5,
                  help='Removes all points which energy is calculated outside of the'\
                  'interval [-V_cut,V_cut]')
    args.Register(m, '--GridLimits', type=str, default=[], #nargs='+',
                  help='Sets limits for the grid along {qi qj ...qD}')
    args.Register(m, '--MaximumMeshGenerations', type=int, default=0,
                  help='After N mesh refinments the mesh is not tested anymore and taken as'\
                  'accurate enough')


    args.Register(m, '-G', '--NumericalGradient', action='store_true', default=False,
                  help='Activates the numerical calculation of gradients. If it not activated'\
                  'gradients are NOT calculated')
    args.Register(m, '-H', '--NumericalHessian', action='store_true', default=False,
                  help='Activates the numerical calculation of hessian. If it not activated'\
                  'hessians are NOT calculated')


    args.Register(m, '--SamplingMethod', type=str, default='FramedRandom',
                  help='Defines how the initial sampling is performed')
    args.Register(m, '--NSampling', type=int, default=50,
                  help='Number of points used for the initial sampling')

    args.Register(m, '--InterpolationMethod', type=str, default='GaussianProcesses',
                  help='Interpolation method to be used: GaussianProcesses, CubicSpline'\
                  'or Barycentric')

    args.Register(m, '--ValidationMethods', type=str, default=[],
                  help='Defines which method is used to valdiate the final PES')
    args.Register(m, '--TestingMethods', type=str, default=[],
                  help='Defines which method is used to self improve the sampling')


