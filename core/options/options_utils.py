#!/bin/python
#File: core.options.options_utils.py

import argparse
import optparse
from qcmagic.Utils.QCSupport import ContainerNone
from core.options.format_input import PartialFormatter, ReadParameters


class OptionCall:
    '''
    Class to simplify option calling
    '''
    def __init__(self):
        pass

    def __call__(self, key):
        '''
        Returns the option for this specific option module
        '''
        return getattr(self, key) if hasattr(self, key) else None

    def set(self, key, value):
        setattr(self, key, value)




class OptionsStructure:

    def __init__(self):

        self.modules = {}
        self.OptionNames = {}
        self.optParser = optparse.OptionParser(
            conflict_handler='resolve', 
            option_class=OptionExtended
                                              )
        self.argParser = argparse.ArgumentParser(
            conflict_handler='resolve',
            prefix_chars='-'
                                             )
    

    def set(self, key, value):
        ''' 
        OptionCall-like setter to allow some arguments outside of 
        modules
        '''
        setattr(self, key, value)

    def __call__(self, key):
        '''
        Returns the option for this specific option module
        '''
        return getattr(self, key) if hasattr(self, key) else None


    def GetInput(self, input_filename, argsin): 

        self.parameters = ReadParameters(input_filename)
        if len(argsin)>1:
            self.argsin = self.parameters.values()
            self.inputargsin = [s.split('=')[0] for s in self.argsin]
            for extra_args in argsin:
                if extra_args.split('=')[0] in self.inputargsin:
                    #replace the argument from the input file to the command line one
                    self.argsin[self.inputargsin.index(extra_args.split('=')[0])] =\
                        extra_args 
                else:
                    self.argsin += [extra_args] 
        else:
            raise RuntimeError('Not sufficient input (missing at least input file)\n')


    def RegisterModules(self, *mods):

        for m in mods:
            if type(m)==type(lambda x:x):
                m(self)   
            else:
                m.RegisterOptions(self)



    def RegisterModule(self, modulename=''):
        '''
        Called by a function/classmethod to register a group of arguments
        '''
        if modulename in self.modules.keys():
            #module already exists
            return modulename
        if modulename:
            if modulename in ['queue']:
                g=self.optParser.add_option_group(modulename)
                self.modules[modulename]={'group':g}
                setattr(self, modulename, ContainerNone())
            else:
                group = self.argParser.add_argument_group(modulename)
                self.modules[modulename]={
                    'group': group,
                    'options': [],
                    'defaults': {}
                                         }
                setattr(self, modulename, OptionCall())

        return modulename


    def Register(self, mod, *misc, **opts):
        '''
        Registering a single argument in a specificed registered group
        if the group is not registered the argument will be saved as a 
        general argument
        '''
        if mod in self.modules.keys():
            group = self.modules[mod]['group']
        else:
            #no group assigned -> general arguments
            group = self.parser

        new_argument = group.add_argument(*misc, **opts)
        self.modules[mod]['options'].append(new_argument.dest)
        self.modules[mod]['defaults'][new_argument.dest] = new_argument.default


    def Parse(self, *modules):
        import operator
        options, args = self.argParser.parse_known_args(self.argsin)

        for modname, mod in self.modules.items():
            if modname in modules:
                for option in mod['options']:
                    if not hasattr(getattr(self, modname), option):
                        #does not overwrite arguments
                        if option in vars(options):
                            getattr(self, modname).set(option, vars(options)[option])
                        else:
                            getattr(self, modname).set(option, mod['arguments']['defaults'][option])



    def _Parse(self, *modules):
        import operator
        for mod in modules:
            options = [opt._long_opts[0] for opt in self.modules[mod]['group'].option_list
                       if len(opt._long_opts)>0]
            options += [opt._short_opts[0] for opt in self.modules[mod]['group'].option_list
                        if len(opt._short_opts)>0]
            argsin = [s for s in self.argsin[1:] if s.split('=')[0] in options]
            options, args = self.optParser.parse_args(argsin)

            for n,v in self.OptionNames.items():
                value=None
                #print('\nnew##: \n', v['Modules'], sorted(v['Modules']))
                for l in sorted(v['Modules'], key=operator.itemgetter(0)):
                    m=l[0]
                    o=l[1]
                    if m:
                        if o.dest in vars(options):
                            getattr(self, m)[n]=vars(options)[o.dest]
                        elif value!=None:
                            getattr(self, m)[n]=value
                    else:
                        if o.dest in vars(options):
                            self[n]=vars(options)[o.dest]
                            value=vars(options)[o.dest]



    def _Register(self, mod, *misc, **opts):

        p=self.optParser

        if mod in self.modules:
            g=self.modules[mod]['group']
        else:
            g=p
        o=g.add_option(*misc, **opts)
        n=o.dest
        g.remove_option((o._long_opts+o._short_opts)[0])

        if mod:
            o.dest = mod + '_' + n
            o._long_opts.append('--' + mod + '-' + n)
        g.add_option(o)

        if n in self.OptionNames:
            for l in self.OptionNames[n]['Modules']:
                if l[0]=='':
                    l[1]._long_opts += l[2]
                    l[1]._short_opts += l[3]
                    self.Parser.add_option(l[1])

                if mod=='':
                    if l[0]=='':
                        l[1].help += 'Set by global options ' + ','.join(o._short_opts+o._long_opts)+'.'
                        self.modules[l[0]]['Group'].remove_option((l[1]._long_opts+l[1]._short_opts)[0])
                        self.modules[l[0]]['Group'].add_option(l[1])
                else:
                    if l[0]=='':
                        l[1].help += 'Set by global options ' + ','.join(o._short_opts+o._long_opts)+'.'
            self.OptionNames[n]['Modules'].append([mod,o,list(o._long_opts),list(o._short_opts)])
        else:
            self.OptionNames[n]={'Modules':[[mod,o,list(o._long_opts),list(o._short_opts)]]}

        g.remove_option((o._long_opts+o._short_opts)[0])
        g.add_option(o)


    def PrintHelp(self, UsageMsg=''):
        self.Parser.set_usage(UsageMsg)
        print(self.Parser.print_help())


class OptionExtended (optparse.Option):
    import copy

    ACTIONS = optparse.Option.ACTIONS + ("extend",)
    STORE_ACTIONS = optparse.Option.STORE_ACTIONS + ("extend",)
    TYPED_ACTIONS = optparse.Option.TYPED_ACTIONS + ("extend",)
    ALWAYS_TYPED_ACTIONS = optparse.Option.ALWAYS_TYPED_ACTIONS + ("extend",)
    TYPES = optparse.Option.TYPES+("intlist",)
    TYPES = TYPES+("floatlist",)
    TYPES = TYPES+("strlist",)
    TYPE_CHECKER=copy.copy(optparse.Option.TYPE_CHECKER)
    # _builtin_cvt = copy.copy(optparse._builtin_cvt)

    def check_blank(self,opt,value):
        return value
    TYPE_CHECKER["intlist"]=check_blank
    TYPE_CHECKER["floatlist"]=check_blank
    TYPE_CHECKER["strlist"]=check_blank
    TYPE_CHECKER["string"]=check_blank

    def take_action(self, action, dest, opt, value, values, parser):
        if action == "extend":
            ot=self.type
            if self.type=="intlist":
                tc=self.TYPE_CHECKER["int"]
                self.type="int"
            elif self.type=="floatlist":
                tc=self.TYPE_CHECKER["float"]
                self.type="float"
            elif self.type=="strlist":
                tc=self.TYPE_CHECKER["string"]
                self.type="string"
            else:
                tc=self.TYPE_CHECKER[self.type]
            lvalue = value.split(",")
            for i,l in enumerate(lvalue):
                lvalue[i]=tc(self,opt,l)
            values.ensure_value(dest, []).extend(lvalue)
            self.type=ot
        elif action=='append' and 'list' in self.type:
            ot=self.type
            if self.type=="intlist":
                tc=self.TYPE_CHECKER["int"]
                self.type="int"
            elif self.type=="floatlist":
                tc=self.TYPE_CHECKER["float"]
                self.type="float"
            elif self.type=="strlist":
                tc=self.TYPE_CHECKER["string"]
                self.type="string"
            else:
                tc=self.TYPE_CHECKER[self.type]
            lvalue = value.split(",")
            for i,l in enumerate(lvalue):
                lvalue[i]=tc(self,opt,l)
            values.ensure_value(dest, []).append(lvalue)
            self.type=ot
        else:
            optparse.Option.take_action(self, action, dest, opt, value, values, parser)

