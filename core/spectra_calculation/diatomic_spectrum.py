#!/bin/python
# File: core.spectra_calculation.diatomic_spectrum.py

from matplotlib import pyplot
from numpy import asarray
from sympy import exp, symbols, lambdify
from sympy.integrals.quadrature import gauss_hermite


class DiatomicSpectrum:
    """Class to calculate intensities and transition energies of P, R branches.

    Attributes:
        max vib level: int (user defined)
        max rot level: int (user defined)
        eigenfunctions: list (calculated from Hvibdiatomic)
        energies: list (calculated from Hvibdiatomic)
        rotational constants: list (calculated from PTdiatomic)
        second order constants: list (calculated from PTdiatomic)
        temperature: int (user defined)
        transitions: list (user defined)

    Methods to run:
        plot spectrum

    """

    def __init__(self, efuncs, energies, rotational_constants, second_order_constants,
                 max_vib_level, max_rot_level, temperature, transitions, molecule):
        # TODO change to tuples

        self.efuncs = efuncs
        self.energies = energies
        self.rotational_constants = rotational_constants
        self.second_order_constants = second_order_constants
        self.max_vib_level = max_vib_level
        self.max_rot_level = max_rot_level
        self.temperature = temperature
        self.transitions = transitions
        self.points, self.weights = gauss_hermite(2 * self.max_vib_level, 10)
        self.points = asarray(self.points, dtype=float)
        self.molecule = molecule

    def transition_dipole(self, w, v):
        """
        Calculates |<w|q|v>|^2 which is the unnormalised probability of observing a transition between states w,v
        :param w: initial state quantum number
        :param v: final state quantum number
        :return: float
        """
        # TODO value of operator will ideally come from DMS
        q = symbols('q')
        bra = self.efuncs[w]
        ket = self.efuncs[v]
        wavefunction = bra * q * ket / exp(-q ** 2)
        wavefunction = lambdify(q, wavefunction, "numpy")
        wavefunction = list(wavefunction(self.points))
        terms = [wavefunction[i] * self.weights[i] for i in range(len(self.weights))]
        integral = (sum(terms)) ** 2
        return integral

    def population(self, v, J):
        """
        Calculates population of state (v,J) assuming a Boltzmann distribution
        :param v: vibrational state quantum number
        :param J: rotational state quantum number
        :return: float
        """
        energy = self.energies[v] + self.rotational_constants[v] * J * (J + 1) + self.second_order_constants[v] * (
                J * (J + 1)) ** 2
        kb = 3.16685 * 10 ** -6
        return (2 * J + 1) * exp(-energy / (kb * self.temperature))

    def intensity(self, w, v):
        """
        Calculates intensities=transition_dipole*population for w->v transitions
        :param w: initial vibrational state
        :param v: final vibrational state
        :return: [[P],[R]] intensities
        """
        P_branch = [self.transition_dipole(w, v) * self.population(w, J) for J in
                    range(1, self.max_rot_level + 1)]
        R_branch = [self.transition_dipole(w, v) * self.population(w, J) for J in range(self.max_rot_level)]
        return [P_branch, R_branch]

    def energy_differences(self, w, v):
        """
        Calculates energy differences for IR observable transitions
        :param w: initial vibrational state
        :param v: final vibrational state
        :return: [[P],[R]]
        """
        wavenumber = 219475
        E_vw = (self.energies[v] - self.energies[w]) * wavenumber
        B_w = self.rotational_constants[w] * wavenumber
        B_v = self.rotational_constants[v] * wavenumber
        D_w = self.second_order_constants[w] * wavenumber
        D_v = self.second_order_constants[v] * w
        P_branch = [E_vw + J * (B_v * (J - 1) - B_w * (J + 1)) + J ** 2 * (D_v * (J - 1) ** 2 - D_w * (J + 1) ** 2) for
                    J in range(1, self.max_rot_level + 1)]
        R_branch = [E_vw + (J + 1) * (B_v * (J + 2) - B_w * J) + (J + 1) ** 2 * (D_v * (J + 2) ** 2 - D_w * J ** 2) for
                    J in range(self.max_rot_level)]
        return [P_branch, R_branch]

    def plot_spectrum(self):
        """
        Method to run whole class
        :return: IR spectrum as a stem plot for all the specified transitions
        """
        colours = ['b-', 'g-', 'r-', 'c-', 'm-', 'y-', 'k-']
        fig, ax = pyplot.subplots()
        for i in range(len(self.transitions)):
            energies = self.energy_differences(*self.transitions[i])
            energies = [y for x in energies for y in x]
            intensities = self.intensity(*self.transitions[i])
            intensities = [y for x in intensities for y in x]
            ax.stem(energies, intensities, markerfmt=',', label=str(self.transitions[i]),
                    linefmt=colours[i % 7])
        ax.set(xlabel='Wavenumber/$cm^{-1}$', ylabel='Intensity/arb units')
        ax.legend()
        # fig.savefig("%s_%s_spectrum_T=%i" %(self.molecule, self.transitions, self.temperature))
        pyplot.show()


class DiatomicSpectrum2:
    """
    Class to calculate intensities and transition energies of P, R branches.

    Attributes:
        GP_args: scale_factor, reduced_mass, equilbrium_bond_length, potential
        user_args: max_vib_level, max_rot_level, temperature, transitions, bond
        h_vib_args: E, psi
        PT_args: B, D

    Methods to run:
        plot spectrum
    """

    def __init__(self, user_args, h_vib_args, PT_args):
        self.user_args = user_args
        self.h_vib_args = h_vib_args
        self.PT_args = PT_args

        self.points, self.weights = gauss_hermite(2 * self.user_args.max_vib_level, 10)
        self.points = asarray(self.points, dtype=float)

    def transition_dipole(self, w, v):
        """
        Calculates |<w|q|v>|^2 which is the unnormalised probability of observing a transition between states w,v
        :param w: initial state quantum number
        :param v: final state quantum number
        :return: float
        """
        # TODO value of operator will ideally come from DMS
        q = symbols('q')
        bra = self.h_vib_args.psi[w]
        ket = self.h_vib_args.psi[v]
        wavefunction = bra * q * ket / exp(-q ** 2)
        wavefunction = lambdify(q, wavefunction, "numpy")
        wavefunction = list(wavefunction(self.points))
        terms = [wavefunction[i] * self.weights[i] for i in range(len(self.weights))]
        integral = (sum(terms)) ** 2
        return integral

    def population(self, v, J):
        """
        Calculates population of state (v,J) assuming a Boltzmann distribution
        :param v: vibrational state quantum number
        :param J: rotational state quantum number
        :return: float
        """
        energy = self.h_vib_args.E[v] + self.PT_args.B[v] * J * (J + 1) + self.PT_args.D[v] * (
                J * (J + 1)) ** 2
        kb = 3.16685 * 10 ** -6
        return (2 * J + 1) * exp(-energy / (kb * self.user_args.temperature))

    def intensity(self, w, v):
        """
        Calculates intensities=transition_dipole*population for w->v transitions
        :param w: initial vibrational state
        :param v: final vibrational state
        :return: [[P],[R]] intensities
        """
        P_branch = [self.transition_dipole(w, v) * self.population(w, J) for J in
                    range(1, self.user_args.max_rot_level + 1)]
        R_branch = [self.transition_dipole(w, v) * self.population(w, J) for J in range(self.user_args.max_rot_level)]
        return [P_branch, R_branch]

    def energy_differences(self, w, v):
        """
        Calculates energy differences for IR observable transitions
        :param w: initial vibrational state
        :param v: final vibrational state
        :return: [[P],[R]]
        """
        wavenumber = 219475
        E_vw = (self.h_vib_args.E[v] - self.h_vib_args.E[w]) * wavenumber
        B_w = self.PT_args.B[w] * wavenumber
        B_v = self.PT_args.B[v] * wavenumber
        D_w = self.PT_args.D[w] * wavenumber
        D_v = self.PT_args.D[v] * w
        P_branch = [E_vw + J * (B_v * (J - 1) - B_w * (J + 1)) + J ** 2 * (D_v * (J - 1) ** 2 - D_w * (J + 1) ** 2) for
                    J in range(1, self.user_args.max_rot_level + 1)]
        R_branch = [E_vw + (J + 1) * (B_v * (J + 2) - B_w * J) + (J + 1) ** 2 * (D_v * (J + 2) ** 2 - D_w * J ** 2) for
                    J in range(self.user_args.max_rot_level)]
        return [P_branch, R_branch]

    def plot_spectrum(self):
        """
        Method to run whole class
        :return: IR spectrum as a stem plot for all the specified transitions
        """
        colours = ['b-', 'g-', 'r-', 'c-', 'm-', 'y-', 'k-']
        fig, ax = pyplot.subplots()
        for i in range(len(self.user_args.transitions)):
            energies = self.energy_differences(*self.user_args.transitions[i])
            energies = [y for x in energies for y in x]
            intensities = self.intensity(*self.user_args.transitions[i])
            intensities = [y for x in intensities for y in x]
            ax.stem(energies, intensities, markerfmt=',', label=str(self.user_args.transitions[i]),
                    linefmt=colours[i % 7])
        ax.set(xlabel='Wavenumber/$cm^{-1}$', ylabel='Intensity/arb units')
        ax.legend()
        fig.savefig(
            "%s_%s_spectrum_T=%i" % (self.user_args.bond, self.user_args.transitions, self.user_args.temperature))
        pyplot.show()
