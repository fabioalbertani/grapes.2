from sympy import exp, sqrt, oo, simplify, symbols, N
from sympy.integrals import integrate
from sympy.integrals.quadrature import gauss_hermite
from sympy.core.function import diff
from sympy.polys.orthopolys import hermite_poly
from numpy import zeros, dot, exp as Exp, genfromtxt, c_, asarray, asscalar, linspace, newaxis
from scipy import linalg as LA, optimize
import time
from matplotlib import pyplot
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, WhiteKernel
start = time.time()

outfile = open("vibdiatomicpert.dat", "w")



# reads in data from file, returns a dictionary {"Energy":[],"R":[]}
# TODO not specify the exact file name eg basis set and calculation type

def get_data(Filename):
	try:

		with open(Filename, "r") as f:
		    trim = 0
		    for line in f:
		        if line.strip() == "--Nodes Information--":
		            trim_stop = trim + 1
		            break
		        else:
		            trim += 1

		DataRead = genfromtxt(Filename, skip_header=trim_stop, unpack=True, names=True)
		DataDict = {}

		# print DataRead.dtype.names
		for cols in DataRead.dtype.names:
		    if cols[0] == 'E':
		        if 'Energy' in DataDict.keys():
		            DataDict['Energy'] = c_[DataDict['Energy'], DataRead[cols]]
		        else:
		            DataDict['Energy'] = DataRead[cols]

		        # BashFile.write('Read in energy\n')
		        # for i in xrange(len(DataRead[cols])):
		        # BashFile.write('\t>' + str(DataRead[cols][i]) + '\n')

		    if cols[0:3] == 'dms':
		        if 'Dipole' in DataDict.keys():
		            DataDict['Dipole'] = c_[DataDict['Dipole'], DataRead[cols]]
		        else:
		            DataDict['Dipole'] = DataRead[cols]

		    # TODO add if col is d0i, gi, hij ...

		    if cols[0] == 'q':
		        if 'R' in DataDict.keys():
		            DataDict['R'] = c_[DataDict['R'], DataRead[cols]]
		        else:
		            DataDict['R'] = DataRead[cols]

		        # BashFile.write('Read in coordinate\n')
		        # for i in xrange(len(DataRead[cols])):
		        # BashFile.write('\t>' + str(DataRead[cols][i]) + '\n')

		    if cols == 'pgen':
		        GEN = DataRead[cols]
		        generations = []
		        for i in xrange(int(GEN[-1])):
		            generations.append([j for j, x in enumerate(GEN) if x == i + 1])


	except IOError:
		raise IOError('No files to read from')

	#if CoordinatesOnly:
        #	return Q
  	# else:
  	# Data = Process(self.Globalargs, Potentialcut = self.Globalargs.Potentialcut)
    	# Data.FeedFinalData(Q, DataDict, generations=generations)
    	# return Data.Return()
    	return DataDict
data=get_data("MeshInformation_cc_BS-aug-cc-pVDZ.dat")



#args = {"sigmaFit": 2.0, "lambdaFit": 0.0016, "nuFit": 2.5, "CovarianceFunction": "Matern", "NoiseFunction": "White",
#        "Bagging": False, "TypeDifference": {"hf": "none"}, "BasisSetDifference": {"6-31g": "none"}, "D": 1,
#        "NStates": 1}
#GP = GPM(args, _directcall=True)
## TODO tidy function inputs
#GP.ReadGPParameters(Data=data, Type="hf", BasisSet="6-31g", NStates=1, _direct=True,
#                    filenames=["SavedData/COKripa/GPInformation_hf_BS-6-31g_s0.dat"])
#
#
#def energy(q):
#    q = q.reshape((1, 1))
#    return GP.Interpolate(q, 'hf', '6-31g'), GP.GradientInterpolate(q, 'hf', '6-31g')
#
#
##def Hessian(q, dq=0.000001):
##    return (GP.GradientInterpolate(q + dq, 'hf', '6-31g') - GP.GradientInterpolate(q - dq, 'hf', '6-31g')) / (2 * dq)
#
#
#def second_derivative(q,dq=0.01):
#    return (GP.Interpolate(q+dq,'hf','6-31g')-2*GP.Interpolate(q,'hf','6-31g')+GP.Interpolate(q-dq,'hf','6-31g'))/(dq**2)
#
#
#resultmin = optimize.minimize(energy, [1.0], jac=True, tol=1e-4, options={"maxiter": 5}, method='CG')
#print resultmin.message
#





X = data["R"].reshape(300,1)
y = data["Energy"].reshape(300,1)
#y=asarray([[i+113.0613123]for i in y]).reshape(300,1)

# First run
#plt.figure()
kernel =  RBF(length_scale=100.0, length_scale_bounds=(1e-2, 1e3)) \
    + WhiteKernel(noise_level=1, noise_level_bounds=(1e-10, 1e-4))
gp = GaussianProcessRegressor(kernel=kernel,
                              alpha=0.0,n_restarts_optimizer=20,random_state=0).fit(X, y)



#rmin = asscalar(getattr(resultmin, 'x'))  # in angstrom
#print rmin
rmin=1.14
#Emin = getattr(resultmin, 'fun')
#Emin= GP.Interpolate(asarray(rmin).reshape(1,1),'hf','6-31g')
Emin= -113.0613123
Emin=-113.0566703227452
Emin=-113.0567968063994
#kmin = second_derivative(asarray(rmin).reshape(1, 1)) * ((0.529177) ** 2)
#print 'kmin', kmin



# data to plot hf
#datafile = open("data.out", 'w')
#for i in range(len(data['points0'])):
#    datafile.write(str(asscalar((data['points0'][i]) / 0.529177)) + ' ' + str((float(data['Energy0'][i]))) + '\n')
#
#datafile.close()
#
## data to plot derivative of hf
#inputs = linspace(0, 5, num=500).reshape(500, 1)
#grad = (GP.GradientInterpolate(inputs, 'hf', '6-31g'))
#gradfile = open("gradient.out", "w")
#for i in xrange(500):
#    gradfile.write(str(inputs[i, 0]) + ' ' + str(grad[i, 0]) + '\n')
#gradfile.close()
#print
#'closed'
#
# dictionary of atomic masses
atom_mass = {"C": 12.000, "O": 16.000}


def reduced_mass(atom1, atom2):
    m1 = atom_mass[atom1]
    
    m2 = atom_mass[atom2]
    
    mu = float((m1 * m2) / (m1 + m2))
    return mu


# k,mu,w_e,s,r_e as global variables in atomic units
# TODO get k,r_e,w_e from GP
k = 1.1917
w_e = 9.764087 * 10 ** -3
mu = reduced_mass("C", "O") * 1.82289 * 10 ** 3


alpha = sqrt(k * mu)

s = 1 / sqrt(alpha)
bohr = 0.529177
r_e = 2.13161  # in bohr
# max possible is (De/w)-0.5 which is 41 for CO. 30x30 v>16 eigenvalues are poor
max_vib_level = 20
max_rot_level = 40
wavenumber = 219475
k_b = 0.6950356
T = 2000
points, weights = gauss_hermite(2 * max_vib_level, 9)
#wmin=(kmin/mu)**0.5

# R in angstrom, q is in scaled bohr
R = asarray([(q * s * bohr + rmin) for q in points])#.reshape((2 * max_vib_level, 1))

#R1=asarray(linspace(0,3,num=500)).reshape(500,1)
#Test=GP.Interpolate(R1,'hf','6-31g')
##for i,b in zip(R1,Test):
#   #print i[0],' ' , b[0]
#testfile=open("testhf.out","w")  
#for i in xrange(500):    
#    testfile.write(str(R1[i,0])+' '+ str(Test[i,0]) + '\n') 
#testfile.close()
#print 'hf interpolated'
#
# TODO reset min of GP to zero
#Potential = GP.Interpolate(R, 'hf', '6-31g')
#Scaled_Potential = Potential   - Emin
#rmax=asarray(6).reshape(1,1)
#De=GP.Interpolate(rmax,'hf','6-31g')-Emin

Potential, y_std = gp.predict(R[:, newaxis], return_std=True,return_cov=False)
pot=[y for x in Potential for y in x]

Scaled_Potential=Potential-Emin

'''Scaled_Potential=[[0.263908],[0.1860938],[0.12710951],[0.08052812],[0.05271221],
[0.03378198],[0.02252012],[0.00510837],[0.005256021],[0.00536332],[0.00265547],[0.00723358],
[0.01313383],[0.02015243],[0.02894813],[0.03898752],[0.0524413],[0.065466],[0.08546267],[0.09868319]]'''



# TODO remove to tests once Interpolate works
# a = sqrt(k/2*De)
def morse(De, a):
    x, q = symbols('x q')
    MO = De * ((1 - exp(-a * x)) ** 2)
    MO1 = MO.subs(x, q * s)
    return MO1


V = morse(0.4123, 1.2168)
potential = []
for i in range(2 * max_vib_level):
    q = symbols('q')
    v = V.subs(q, points[i])
    potential.append(v)

#q1=[((i*1.88973-r_e)/s) for i in R1]
#pot=[V.subs(q,q1[i]) for i in range(len(q1))]
#pot=[i+Emin for i in pot]
#testfile=open("morse.out","w")
#for i in xrange(500):
#    testfile.write(str(R1[i,0])+' ' +str(pot[i]) +'\n')
#testfile.close()




# returns normalised HO wavefunctions
def HO_wavefunction(n):
    x, q = symbols("x q")
    a = hermite_poly(n, q)
    c = exp(-(x / s) ** 2 / 2)
    d = c.subs(x, q * s)
    e = a * d
    f = integrate(e * e, (q, -oo, oo))
    g = e / sqrt(f)
    return g


# set of HO wavefunctions
HO_basis = []
for i in range(max_vib_level):
    HO_basis.append(HO_wavefunction(i))
outfile.write("HO basis:")
outfile.write(str(HO_basis)+'\n')


# Hamiltonian with Morse potential
# TODO remove to tests once Interpolate works
def HamiltonianM(n):
    a = HO_wavefunction(n)
    x, q = symbols('x q')
    f = diff(a, q, 2)
    KE = -f * w_e / 2
    PE = V * a
    return KE + PE


# set of |H0|n>
# TODO remove to tests once Interpolate works
Hamiltonian_basis = []
for i in range(max_vib_level):
    Hamiltonian_basis.append(HamiltonianM(i))

# calculates matrix elements of H0
# analytic morse
# TODO rewrite matelem to put Morse sampling later, like PES
def matrix_element_analytic(m, n):
    a = HO_basis[m]
    b = Hamiltonian_basis[n]
    q = symbols('q')
    d = exp(-q ** 2)
    e = a * b / d
    integral = 0
    for i in range(2 * max_vib_level):
        f = e.subs(q, points[i])
        g = weights[i]
        integral += f * g
    return integral


def matrix_element_morse(m, n):
    a = HO_basis[m]
    b = HO_basis[n]
    q = symbols('q')
    c = exp(-q ** 2)
    KE = a * diff(b, q, 2) * w_e / (-2)
    KE = KE / c
    PE = a * b
    PE = PE / c
    integral = 0
    for i in range(2 * max_vib_level):
        d = KE.subs(q, points[i])
        e = PE.subs(q, points[i])
        f = potential[i]
        g = weights[i]
        h = d * g + e * f * g
        integral += h
    return integral


# PES
def matrix_element_PES(m, n):
    a = HO_basis[m]
    b = HO_basis[n]
    q = symbols('q')
    KE = a * diff(b, q, 2) * w_e / (-2)
    PE = a * b
    c = exp(-q ** 2)
    KE = KE / c
    PE = PE / c
    integral = 0
    for i in range(2 * max_vib_level):
        d = KE.subs(q, points[i])
        e = PE.subs(q, points[i])

        f = Scaled_Potential[i][0]
        g = weights[i]
        h = d * g + e * f * g
        integral += h
    return integral


# generates nxn H0 matrix analytic Morse
# TODO change to matrix_element once Interpolate works
H_analytic = zeros((max_vib_level, max_vib_level))
for i in range(max_vib_level):
    for j in range(i + 1):
        H_analytic[i][j] = matrix_element_analytic(i, j)
        H_analytic[j][i] = H_analytic[i][j]

H_morse = zeros((max_vib_level, max_vib_level))
for i in range(max_vib_level):
    for j in range(i + 1):
        H_morse[i][j] = matrix_element_morse(i, j)
        H_morse[j][i] = H_morse[i][j]



# TODO write sampling Morse

# PES
H_PES = zeros((max_vib_level, max_vib_level))
for i in range(max_vib_level):
    for j in range(i + 1):
        H_PES[i][j] = matrix_element_PES(i, j)
        H_PES[j][i] = H_PES[i][j]
#outfile.write("H0_PES:",H_PES)

# generates nxn overlap matrix
S = zeros((max_vib_level, max_vib_level))
for i in range(max_vib_level):
    for j in range(i + 1):
        q = symbols('q')
        a = HO_basis[i]
        b = HO_basis[j]
        c = exp(-q ** 2)
        d = a * b / c
        integral = 0

        for k in range(2 * max_vib_level):
            f = d.subs(q, points[k])
            g = weights[k]

            integral += f * g
        S[i][j] = integral
        S[j][i] = S[i][j]

# solves generalised eigenvalue problem Hc=ESc

# analytic Morse
evals_analytic, evecs_analytic = LA.eigh(H_analytic, S)

# TODO have sampling Morse
evals_morse, evecs_morse = LA.eigh(H_morse, S)
morse=[i*wavenumber for i in evals_morse]
outfile.write("Morse energies:")
outfile.write(str(morse)+'\n')
# PES
evals_PES, evecs_PES = LA.eigh(H_PES, S)
pes=[i*wavenumber for i in evals_PES]
outfile.write("PES energies:")
outfile.write(str(pes)+'\n')


# set of wavefunctions that diagonalise H0
# TODO update which evecs are used to check spectrum
MO_basis = []
for v in range(max_vib_level):
    a = evecs_PES[:, v]
    b = HO_basis
    c = dot(a, b)
    MO_basis.append(c)
outfile.write("H0_PES eigenfunctions:")
outfile.write(str(MO_basis)+'\n')
# calculate <1/r^2> by expanding it as a taylor series (arbitrary termination), this is a LC of
# all CDCs, Bv needs to terminate after 3 terms
# equivalent to first order energy/(J(J+1))
def rotational_constant(v):
    q = symbols('q')
    a = MO_basis[v]
    b = 0
    for i in range(10):
        t = (((-q * s / r_e) ** i) * (i + 1))
        b += t
    c = a * b * a
    d = exp(-q ** 2)
    e = c / d

    integral = 0
    for i in range(2 * max_vib_level):
        f = e.subs(q, points[i])
        g = weights[i]

        integral += f * g
    B_v = N(integral / (2 * mu * r_e ** 2))
    return B_v
# calculates one term |<w|H(1)|v>|**2/(E_w-E_v)
def second_order_energy_term(w,v):
    q=symbols('q')
    a=MO_basis[w]
    b=MO_basis[v]
    B=0
    for i in range(10):
	t=(((-q*s/r_e)**i)*(i+1))
        B +=t
    c=a*B*b
    d=exp(-q**2)
    e=c/d
    integral=0
    for i in range(2*max_vib_level):
	f=e.subs(q,points[i])
	g=weights[i]
	integral+=f*g
    integral/=(2*mu*r_e**2)
    integral**=2
    integral/=(evals_PES[w]-evals_PES[v])
    return integral




# calculates sum, when adding to energy need to put [J(J+1)]**2
def second_order_energy(v):
    term=0
    for i in range(max_vib_level):
	if i==v:
	    term+=0
	else:
	    term-=second_order_energy_term(i,v)
    return term

# list of rotational constants for each vibrational level (only first half are good)
Rotational_Constants = []
for i in range(max_vib_level):
    a = rotational_constant(i)
    Rotational_Constants.append(a)
rotconsts=[i*wavenumber for i in Rotational_Constants]
second_order_constants=[]
for i in range(max_vib_level):
    a=second_order_energy(i)
    second_order_constants.append(a)

secondorderconst=[N(i*wavenumber) for i in second_order_constants]
outfile.write("Rotational Constants:")
outfile.write(str(rotconsts)+'\n')
outfile.write("second order corrections:")
outfile.write(str(secondorderconst)+'\n')
# list of energies, elements are lists of energies associated with a vibrational state
# E=[[v=0],[v=1]....,[v=max_vib_level-1]]
Energies = []
for i in range(max_vib_level):
    a = []
    for J in range(max_rot_level):
        b = evals_PES[i] + Rotational_Constants[i] * J * (J + 1)+second_order_constants[i]*((J*(J+1))**2)
        c = b * wavenumber
        a.append(c)
    Energies.append(a)

outfile.write("Energies:")
outfile.write(str(Energies)+'\n')
# only considering fundamental transition (selection rules are v''=0 and v'=1, |J''-J'|=1)
# order [[[R0],[P0]],[[R1],[P1]]....,[[Ri-1],Pi-1]]
# TODO add more transitions 0-1,1-2,2-3,3-4,4-5
Transitions = []
for i in range(5):
    v=[]
    P_Branch = []
    R_Branch = []
    try:
        for j in range(max_rot_level):
            a = Energies[i + 1][j + 1] - Energies[i][j]
            R_Branch.append(a)
    except:
        pass
    v.append(R_Branch)
    for j in range(1, max_rot_level):
        b = Energies[i + 1][j - 1] - Energies[i][j]
        P_Branch.append(b)
    v.append(P_Branch)
    Transitions.append(v)

outfile.write("Transitions:")
outfile.write(str(Transitions)+'\n')

# calculates <v''|q|v'> - the transition dipole moment
def transition_dipole(v, w):
    q = symbols('q')
    a = MO_basis[v]
    b = MO_basis[w]
    c = a * q * b
    e = exp(-q ** 2)
    f = c / e
    integral = 0

    for i in range(2 * max_vib_level):
        g = f.subs(q, points[i])
        h = weights[i]

        integral += g * h
    ans = N(integral)
    return ans


# dictionary, but might not be necessary?
transition_dipole_moments = {}

# this gives the intensities for v'' to v' transition not including Boltzmann or rotational levels
# TODO overimpose v'' Boltzmann factor and TDS
for i in range(max_vib_level):
    for j in range(i):
        key = (j, i)
        if key not in transition_dipole_moments:
            transition_dipole_moments[key] = (transition_dipole(j, i) ** 2)

outfile.write("transition dipole:"+str(transition_dipole_moments)+'\n')


# assumes intensity proportional to (2J+1)exp(-BJ(J+1_/kT)
# inputs are initial vibrational state, initial rotational state
def population(i, J):
    rot_const = rotational_constant(i) * wavenumber
    vib_level=pes[i]
    degeneracy = 2 * J + 1
    energy = rot_const * J * (J + 1)+ vib_level
    boltzmann = exp(-energy / (k_b * T))
    ans = degeneracy * boltzmann
    return ans


# list in order [[[R0],[P0]],[[R1],[P1]],.....,[[Ri-1],[Pi-1]]]
# TODO add loop including w, for which transitions to include 0-1, 1-2, 2-3, 3-4, 4-5
intensities = []
for i in range(5):
    w=[]
    for branch in range(2):
        if branch == 0:
            R_Branch = []
            for J in range(max_rot_level - 1):
                a = population(i, J)
                R_Branch.append(a*transition_dipole_moments[(i,i+1)])
            w.append(R_Branch)
        if branch == 1:
            P_Branch = []
            for J in range(1, max_rot_level):
                a = population(i, J)
                P_Branch.append(a*transition_dipole_moments[(i,i+1)])
            w.append(P_Branch)
    intensities.append(w)

# [[(x,y)v=0],[(x,y)v=1],...,[(x,y)v=k-1]]
spectrum = []
for k in range(5):
    plotpoints=[]
    for i in range(len(Transitions[0])):
        for j in range(len(Transitions[0][i])):
            x = Transitions[k][i][j]
            y = intensities[k][i][j]
            coords = (x, y)
            plotpoints.append(coords)
    spectrum.append(plotpoints)

print len(spectrum)
print len(spectrum[0])


# [[transition],[intensity]] not split by v''
linelist=[[i[j] for k in spectrum for i in k]for j in[0,1]]
print len(linelist[0]), len(linelist[1])
#for i in range(len(linelist[1])):
#    if linelist[1][i]<0.0000001:
#	print i
    
#pyplot.stem([spectrum[0][i][0] for i in range(len(spectrum[0]))], [spectrum[0][i][1] for i in range(len(spectrum[0]))],
#            markerfmt=",")

fig, ax=pyplot.subplots()
ax.stem(linelist[0][0:78],linelist[1][0:78],markerfmt=",",label="v=0 to 1",linefmt='b-' )
ax.stem(linelist[0][78:156],linelist[1][78:156],markerfmt=",", label='v=1 to 2', linefmt='g-')
ax.stem(linelist[0][156:234],linelist[1][156:234],markerfmt=",",label='v=2 to 3', linefmt='r-')
ax.set(xlabel='Wavenumber/$cm^{-1}$', ylabel='Intensity/arb units', title='CO IR Spectrum T=%i'%T)
ax.legend()
fig.savefig('PESSpectrumPert%i.png'%T)
pyplot.show()

#pyplot.stem(linelist[0],linelist[1],markerfmt=",")
#pyplot.title('CO IR Spectrum T=%i'%T)
#pyplot.xlabel('Wavenumber/$cm^{-1}$')
#pyplot.ylabel('Intensity/arb units')
#pyplot.savefig('PESSpectrumPert%i.png'%T)



pyplot.show()

end = time.time()
print (end - start)

outfile.close()
