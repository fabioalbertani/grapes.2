#!/bin/python
# File: core.spectra_calculation.diatomic_main.py

from diatomic_h_vib import HVibDiatomic2 as HVib
from diatomic_perturbation_theory import DiatomicPerturbationTheory2 as PT
from diatomic_spectrum import DiatomicSpectrum2 as Spectrum

mol = "AB"

vib, rot, t, s, m, r = 1, 1, 1, 1, 1, 1
p, trans = [1], [(0, 1)]


class RunDiatomic(Run):
    """
    Class to run the full method on a diatomic
    Attributes (all user defined):
        GP_args: scale_factor, reduced_mass, equilbrium_bond_length, potential
        user_args: max_vib_level, max_rot_level, temperature, transitions, bond
        h_vib_args: E, psi
        PT_args: B, D
    """

    @staticmethod
    def RegisterOptions(args):
        m = args.RegisterModule('spectra')
        args.Register(m, '--max_vib_level', type=int, default=20, help='Vibrational basis set size')
        args.Register(m, '--max_rot_level', type=int, default=25,
                      help='Rotational basis set size, gives number of lines in P and R branches per transition')
        args.Register(m, '--temperature', type=int, default=298,
                      help='Temperature in Kelvin to give Boltzmann populations')
        args.Register(m, '--transitions', type=list, default=[(0, 1)],
                      help='List of tuples (v -> w) containing vibrational transitions for spectrum to be plotted')
        args.Register(m, '--bond', type=str, default="AB", help='Molecule name')

    def __init__(self, args, GP_args):
        self.GP_args = GP_args
        super(RunDiatomic, self).__init__(args, GP_args)

    def run_diatomic(self):
        # TODO interface to get GP args
        vibrational = HVib(GP_args=self.GP_args, user_args=self.Moduleargs)
        vibrational.run_hvib_diatomic()
        rotational = PT(GP_args=self.GP_args, user_args=self.Moduleargs, h_vib_args=vibrational)
        rotational.run_diatomic_perturbation_theory()
        molecule3 = Spectrum(user_args=self.Moduleargs, h_vib_args=vibrational, PT_args=rotational)
        molecule3.plot_spectrum()
