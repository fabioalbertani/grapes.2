#!/bin/python
# File: core.spectra_calculation.hamiltonian.py

import abc
import os

from numpy import zeros, dot, savez_compressed, load
from scipy import linalg as LA
from sympy import simplify


class Hamiltonian:
    __metaclass__ = abc.ABCMeta

    def __init__(self, max_quantum_numbers, potential):
        # TODO can just have def __init__(self), remove max_quantum_numbers, and have self.potential=None
        self.max_quantum_numbers = max_quantum_numbers
        self.potential = potential

        self.points, self.weights = None, None
        self.basis = None
        self.matrices_filename = None

    @abc.abstractmethod
    def kinetic_energy(self, n):
        """
        Calculates |T|n>
        :param n: int for 1D or tuple for nD
        :return: list of values at each point
        """
        pass

    @abc.abstractmethod
    def wavefunction(self, n):
        """
        Caclulates |n>
        :param n: int for 1D or tuple for nD
        :return: list of values at each point
        """
        pass

    def apply_hamiltonian(self, n):
        """
        Calculates |H|n> where H=T+V
        :param n: int for 1D or tuple for nD
        :return: list of values at each point
        """
        T = self.kinetic_energy(n)
        V = self.wavefunction(n)
        hamiltonian_applied = [T[i] + V[i] * self.potential[i] for i in range(len(self.potential))]
        return hamiltonian_applied

    def matrix_element(self, m, n):
        """
        Calculates <m|O|n> for some operator O - usually O=1 or H
        :param m: list of bra values
        :param n: list of ket values -> operator already applied
        :return: value of integral as a float
        """
        terms = [m[i] * n[i] * self.weights[i] for i in range(len(self.potential))]
        integral = sum(terms)
        return integral

    def generate_matrices(self):
        """
        Generates hamiltonian and overlap matrices to use for eigenvalue problem -> usually S=I, and saves them
        :return: H, S matrices
        """
        H = zeros((len(self.basis), len(self.basis)))
        S = zeros((len(self.basis), len(self.basis)))
        for i in range(len(self.basis)):
            for j in range(i + 1):
                H[i][j] = self.matrix_element(self.wavefunction(i), self.apply_hamiltonian(j))
                S[i][j] = self.matrix_element(self.wavefunction(i), self.wavefunction(j))
                H[j][i] = H[i][j]
                S[j][i] = S[i][j]
        saving = savez_compressed(self.matrices_filename, H, S)
        return H, S

    def diagonalise_matrices(self, H, S):
        """
        Solves generalised eigenvalue problem Hc=ESc
        :param H: hamiltonian matrix
        :param S: overlap matrix
        :return: eigenvalues E, eigenvectors c, eigenfunctions
        """
        evals, evecs = LA.eigh(H, S)
        hamiltonian_basis = []
        for v in range(len(self.basis)):
            evec = evecs[:, v]
            wavefunction = dot(evec, self.basis)
            hamiltonian_basis.append(simplify(wavefunction))
        return evals, evecs, hamiltonian_basis

    def load_matrices(self):
        """
        Opens hamiltonian, overlap matrices if files already created, else makes them
        :return: H, S matrices
        """
        if os.path.isfile(self.matrices_filename):
            opening = load(self.matrices_filename)
            H, S = opening['arr_0'], opening['arr_1']
            opening.close()
        else:
            H, S = self.generate_matrices()
        return H, S
