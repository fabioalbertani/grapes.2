#!/bin/python
# File: core.spectra_calculation.spectra_options.py

import abc


class SpectraOptions:
    __metaclass__ = abc.ABCMeta

    @staticmethod
    def RegisterOptions(args):
        m = args.RegisterModule('spectra')
        args.Register(m, '--max_vib_level', type=int, default=20, help='Vibrational basis set size')
        args.Register(m, '--max_rot_level', type=int, default=25,
                      help='Rotational basis set size, gives number of lines in P and R branches per transition')
        args.Register(m, '--temperature', type=int, default=298,
                      help='Temperature in Kelvin to give Boltzmann populations')
        args.Register(m, '--transitions', type=list, default=[(0, 1)],
                      help='List of tuples (v -> w) containing vibrational transitions for spectrum to be plotted')

    def __init__(self, args):
        self.Moduleargs = args.spectra
