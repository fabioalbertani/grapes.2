#!/bin/python
# File: core.spectra_calculation.triatomic_h_vib.py
import os

from numpy import zeros, asarray, savez_compressed, load
from scipy import linalg as LA
from sympy import exp, sqrt, symbols, cos, sin, lambdify
from sympy.core.function import diff
from sympy.integrals.quadrature import gauss_hermite, gauss_legendre

from hamiltonian import Hamiltonian


class HVibTriatomic(Hamiltonian):
    """
    Class for calculating vibrational energies and wavefunctions of a triatomic in a 3 dimensional basis
    of bond stretch AB, bond stretch BC and bend.
    Attributes:
        mass B: float (defined by molecule)
        potential: list (from GP)
        AB/BC: tuple (bond length, scaling, reduced mass, 1D eigenfunctions) first 3 are floats from GP, efuncs calculated
        ABC: tuple (equilibrium angle, 1D eigenfunctions) angle is a float from GP, efuncs calculated
        max quantum numbers: tuple (stretch AB, stretch BC, bend ABC) user defined ints
        molecule: string



    Methods to run:
        run_hvib_triatomic

    """

    def __init__(self, max_quantum_numbers, potential, AB, BC, ABC, mass_B, molecule):
        super().__init__(max_quantum_numbers, potential)
        self.r_AB_eq, self.scale_AB, self.reduced_mass_AB, self.wavefuncs_AB = AB
        self.r_BC_eq, self.scale_BC, self.reduced_mass_BC, self.wavefuncs_BC = BC
        self.theta_eq, self.wavefuncs_bend = ABC
        self.max_stretch_level_AB, self.max_stretch_level_BC, self.max_bend_level = self.max_quantum_numbers
        self.mass_B = mass_B
        self.molecule = molecule

        self.points, self.weights = self.generate_sampling_points_weights()
        self.basis_keys = [(i, j, k) for i in range(self.max_stretch_level_AB) for j in range(self.max_stretch_level_BC)
                           for k in range(self.max_bend_level)]

        # TODO check if self.basis is needed, otherwise only labels are
        self.basis = [self.full_wavefunction(i) for i in self.basis_keys]
        self.matrices_filename = self.molecule + "_Hvib_matrices_(%i,%i,%i).npz" % (
            self.max_stretch_level_AB, self.max_stretch_level_BC, self.max_bend_level)
        self.values_filename = self.molecule + "_hamiltonian_applied&wavefunction_values_(%i,%i,%i).npz" % (
            self.max_stretch_level_AB, self.max_stretch_level_BC, self.max_bend_level)

    def full_wavefunction(self, basis_keys):
        """ Calculates the wavefunction as a product of 3 one-dimensional eigenfunctions"""
        p, q = symbols('p q')
        i, j, k = basis_keys
        ab = self.wavefuncs_AB[i]
        ab = ab.subs(q, p)
        bc = self.wavefuncs_BC[j]
        abc = self.wavefuncs_bend[k]
        return ab * bc * abc

    def generate_sampling_points_weights(self):
        """Calculates the weights and sampling points for triple integral quadrature"""
        points_AB, weights_AB = gauss_hermite(2 * self.max_stretch_level_AB, 9)
        points_AB = asarray(points_AB, dtype=float)
        points_BC, weights_BC = gauss_hermite(2 * self.max_stretch_level_BC, 9)
        points_BC = asarray(points_BC, dtype=float)
        cos_points, weights_ABC = gauss_legendre(2 * self.max_bend_level, 9)
        cos_points = asarray(cos_points, dtype=float)
        sin_points = asarray([sqrt(1 - i ** 2) for i in cos_points], dtype=float)
        full_points = [(points_AB[i], points_BC[j], cos_points[k], sin_points[k]) for i in
                       range(len(points_AB)) for j in range(len(points_BC)) for k in
                       range(len(cos_points))]
        full_weights = [weights_AB[i] * weights_BC[j] * weights_ABC[k] for i in
                        range(len(points_AB)) for j in range(len(points_BC)) for k in
                        range(len(cos_points))]
        return full_points, full_weights

    def kinetic_energy(self, n):
        """
        Calculates |T|n> as defined by Handy-Sutcliffe as a lambda function of a tuple (p,q,cos(theta),sin(theta))
        """
        ab, bc, abc = n
        p, q, x, y = symbols('p q x y')
        theta = symbols('theta')
        psi_ab = self.wavefuncs_AB[ab]
        psi_ab = psi_ab.subs(q, p)
        psi_bc = self.wavefuncs_BC[bc]
        psi_abc = self.wavefuncs_bend[abc]
        r_ab = p * self.scale_AB + self.r_AB_eq
        r_bc = q * self.scale_BC + self.r_BC_eq
        first_derivative_ab = diff(psi_ab, p, 1)
        first_derivative_bc = diff(psi_bc, q, 1)
        first_derivative_abc = diff(psi_abc, theta, 1)
        second_derivative_ab = diff(psi_ab, p, 2)
        second_derivative_bc = diff(psi_bc, q, 2)
        second_derivative_abc = diff(psi_abc, theta, 2)
        psi_ab = psi_ab.subs(exp, 1)
        psi_bc = psi_bc.subs(exp, 1)
        psi_abc = psi_abc.subs(cos(theta), x)
        first_derivative_ab = first_derivative_ab.subs(exp, 1)
        first_derivative_bc = first_derivative_bc.subs(exp, 1)
        first_derivative_abc = first_derivative_abc.subs([(cos(theta), x), (sin(theta), y)])
        second_derivative_ab = second_derivative_ab.subs(exp, 1)
        second_derivative_bc = second_derivative_bc.subs(exp, 1)
        second_derivative_abc = second_derivative_abc.subs([(cos(theta), x), (sin(theta), y)])
        ke_r_ab_2 = second_derivative_ab / (-2 * self.reduced_mass_AB * self.scale_AB ** 2) * psi_bc * psi_abc
        ke_r_bc_2 = second_derivative_bc / (-2 * self.reduced_mass_BC * self.scale_BC ** 2) * psi_ab * psi_abc
        ke_bend_2 = second_derivative_abc * psi_ab * psi_bc * (
                1 / (self.reduced_mass_AB * r_ab ** 2) + 1 / (self.reduced_mass_BC * r_bc ** 2) - 2 * x) / (
                            self.mass_B * r_ab * r_bc)
        ke_bend_ab = (first_derivative_ab / self.scale_AB) * first_derivative_abc * y * psi_bc / (r_ab * self.mass_B)
        ke_bend_bc = (first_derivative_bc / self.scale_BC) * first_derivative_abc * y * psi_ab / (r_bc * self.mass_B)
        ke_r_ab_r_bc = (first_derivative_ab / self.scale_AB) * (
                first_derivative_bc / self.scale_BC) * psi_abc * x / self.mass_B
        ke_r_ab = first_derivative_ab * psi_bc * psi_abc * x / (self.scale_AB * self.mass_B * r_bc)
        ke_r_bc = first_derivative_bc * psi_ab * psi_abc * x / (self.scale_BC * self.mass_B * r_ab)
        ke_bend = (1 / (self.reduced_mass_AB * r_ab ** 2) + 1 / (self.reduced_mass_BC * r_bc ** 2) - 2 * x) / (
                self.mass_B * r_ab * r_bc) * x * first_derivative_abc * psi_ab * psi_bc / (-2 * y)
        ke_const = psi_ab * psi_bc * psi_abc * x / (self.mass_B * r_ab * r_bc)
        ke = ke_r_ab_2 + ke_r_bc_2 + ke_bend_2 + ke_bend_ab + ke_bend_bc - \
             ke_r_ab_r_bc + ke_r_ab + ke_r_bc - ke_bend + ke_const
        f = lambdify([(p, q, x, y)], ke, "numpy")
        KE = [f(i) for i in self.points]
        return KE

    def wavefunction(self, n):
        """Calculates |n> as a lambda function of a tuple (p,q,cos(theta),sin(theta))"""
        ab, bc, abc = n
        theta = symbols('theta')
        p, q, x, y = symbols('p q x y')
        psi_ab = self.wavefuncs_AB[ab]
        psi_ab = psi_ab.subs(q, p)
        psi_bc = self.wavefuncs_BC[bc]
        psi_abc = self.wavefuncs_bend[abc]
        psi_ab = psi_ab.subs(exp, 1)
        psi_bc = psi_bc.subs(exp, 1)
        psi_abc = psi_abc.subs(cos(theta), x)
        f = psi_ab * psi_bc * psi_abc
        f = lambdify([(p, q, x, y)], f, "numpy")
        PE = [f(i) for i in self.points]
        return PE

    def save_values(self):
        """Saves the values of |H|n> and |n> for all n into files to load in when calculating matrix elements"""
        h_values = []
        psi_values = []
        for i in self.basis_keys:
            h_values.append(self.apply_hamiltonian(i))
            psi_values.append(self.wavefunction(i))
        saving = savez_compressed(self.values_filename, h_values, psi_values)
        return h_values, psi_values

    def load_values(self):
        """Checks if |H|n> and |n> evaluated for all wavefunctions at all points have been put into a file and loads
        them in, else creates and saves them """
        # TODO this is still too slow to create matrix elements - need to use dot product
        if os.path.isfile(self.values_filename):
            opening = load(self.values_filename)
            hamiltonian_applied_values, wavefunction_values = opening['arr_0'], opening['arr_1']
            opening.close()
        else:
            hamiltonian_applied_values, wavefunction_values = self.save_values()
        return hamiltonian_applied_values, wavefunction_values

    def generate_matrices(self):
        """ Overriden method from abstract class to include load values """
        hvib = zeros((len(self.basis_keys), len(self.basis_keys)))
        svib = zeros((len(self.basis_keys), len(self.basis_keys)))
        hamiltonian_applied_values, wavefunction_values = self.load_values()
        for i in range(len(self.basis_keys)):
            for j in range(i + 1):
                hvib[i][j] = self.matrix_element(wavefunction_values[i], hamiltonian_applied_values[j])
                svib[i][j] = self.matrix_element(wavefunction_values[i], wavefunction_values[j])
                hvib[j][i] = hvib[i][j]
                svib[j][i] = svib[i][j]
        saving = savez_compressed(self.matrices_filename, hvib, svib)
        return hvib, svib

    def run_hvib_triatomic(self):
        """
        Method to run whole class.
        1)Loads in H, S matrices if already made otherwise creates and saves them
        2)Diagonalise H to give eigenvalues, eigenvectors and eigenfunctions
        3)Eigenvectors and eigenvalues converted to wavenumber and saved to an outfile
        :return: Energies and eigenfunctions
        """

        filename = self.molecule + "_Hvib_data.dat"
        outfile = open(filename, "w")
        wavenumber = 219475
        H, S = self.load_matrices()
        # E, c, psi = self.diagonalise_matrices(H, S)
        E, c = LA.eigh(H)
        E_wav = [i * wavenumber for i in E]
        outfile.write(str(E_wav) + '\n' + str(c) + '\n')
        # return E, psi
        return E, c


class HVibTriatomic2(Hamiltonian):
    """
    Class for calculating vibrational energies and wavefunctions of a triatomic in a 3 dimensional basis
    of bond stretch AB, bond stretch BC and bend.
    Attributes:
        user_args: max_stretch_level_AB, max_stretch_level_BC, max_bend_level, molecule
        GP_args: (GP_args_AB, GP_args_BC, GP_args_ABC, full_potential)
                GP_args_AB: scale_factor, reduced_mass, r_eq, potential
                GP_args_BC: scale_factor, reduced_mass, r_eq, potential
                GP_args_ABC: mass_B, theta_eq, potential
        oneD_args: (stretch_args_AB, stretch_args_BC, bend_args_ABC)
                stretch_args_AB: E, psi
                stretch_args_BC: E, psi
                bend_args_ABC: E, psi

    Methods to run:
        run_hvib_triatomic

    """

    def __init__(self, user_args, GP_args, oneD_args):
        self.user_args = user_args
        self.GP_args_AB, self.GP_args_BC, self.GP_args_ABC, self.potential = GP_args
        self.oneD_args_AB, self.oneD_args_BC, self.oneD_args_ABC = oneD_args

        super().__init__(self.user_args, self.potential)

        self.points, self.weights = self.generate_sampling_points_weights()
        self.basis_keys = [(i, j, k) for i in range(self.user_args.max_stretch_level_AB) for j in
                           range(self.user_args.max_stretch_level_BC)
                           for k in range(self.user_args.max_bend_level)]

        # TODO check if self.basis is needed, otherwise only labels are
        self.basis = [self.full_wavefunction(i) for i in self.basis_keys]
        self.matrices_filename = self.user_args.molecule + "_Hvib_matrices_(%i,%i,%i).npz" % (
            self.user_args.max_stretch_level_AB, self.user_args.max_stretch_level_BC, self.user_args.max_bend_level)
        self.values_filename = self.user_args.molecule + "_hamiltonian_applied&wavefunction_values_(%i,%i,%i).npz" % (
            self.user_args.max_stretch_level_AB, self.user_args.max_stretch_level_BC, self.user_args.max_bend_level)

    def full_wavefunction(self, basis_keys):
        """ Calculates the wavefunction as a product of 3 one-dimensional eigenfunctions"""
        p, q = symbols('p q')
        i, j, k = basis_keys
        ab = self.oneD_args_AB.psi[i]
        ab = ab.subs(q, p)
        bc = self.oneD_args_BC.psi[j]
        abc = self.oneD_args_ABC[k]
        return ab * bc * abc

    def generate_sampling_points_weights(self):
        """Calculates the weights and sampling points for triple integral quadrature"""
        points_AB, weights_AB = gauss_hermite(2 * self.user_args.max_stretch_level_AB, 9)
        points_AB = asarray(points_AB, dtype=float)
        points_BC, weights_BC = gauss_hermite(2 * self.user_args.max_stretch_level_BC, 9)
        points_BC = asarray(points_BC, dtype=float)
        cos_points, weights_ABC = gauss_legendre(2 * self.user_args.max_bend_level, 9)
        cos_points = asarray(cos_points, dtype=float)
        sin_points = asarray([sqrt(1 - i ** 2) for i in cos_points], dtype=float)
        full_points = [(points_AB[i], points_BC[j], cos_points[k], sin_points[k]) for i in
                       range(len(points_AB)) for j in range(len(points_BC)) for k in
                       range(len(cos_points))]
        full_weights = [weights_AB[i] * weights_BC[j] * weights_ABC[k] for i in
                        range(len(points_AB)) for j in range(len(points_BC)) for k in
                        range(len(cos_points))]
        return full_points, full_weights

    def kinetic_energy(self, n):
        """
        Calculates |T|n> as defined by Handy-Sutcliffe as a lambda function of a tuple (p,q,cos(theta),sin(theta))
        """
        ab, bc, abc = n
        p, q, x, y = symbols('p q x y')
        theta = symbols('theta')
        psi_ab = self.oneD_args_AB.psi[ab]
        psi_ab = psi_ab.subs(q, p)
        psi_bc = self.oneD_args_BC.psi[bc]
        psi_abc = self.oneD_args_ABC.psi[abc]
        r_ab = p * self.GP_args_AB.scale_factor + self.GP_args_AB.r_eq
        r_bc = q * self.GP_args_BC.scale_factor + self.GP_args_BC.r_eq
        first_derivative_ab = diff(psi_ab, p, 1)
        first_derivative_bc = diff(psi_bc, q, 1)
        first_derivative_abc = diff(psi_abc, theta, 1)
        second_derivative_ab = diff(psi_ab, p, 2)
        second_derivative_bc = diff(psi_bc, q, 2)
        second_derivative_abc = diff(psi_abc, theta, 2)
        psi_ab = psi_ab.subs(exp, 1)
        psi_bc = psi_bc.subs(exp, 1)
        psi_abc = psi_abc.subs(cos(theta), x)
        first_derivative_ab = first_derivative_ab.subs(exp, 1)
        first_derivative_bc = first_derivative_bc.subs(exp, 1)
        first_derivative_abc = first_derivative_abc.subs([(cos(theta), x), (sin(theta), y)])
        second_derivative_ab = second_derivative_ab.subs(exp, 1)
        second_derivative_bc = second_derivative_bc.subs(exp, 1)
        second_derivative_abc = second_derivative_abc.subs([(cos(theta), x), (sin(theta), y)])
        ke_r_ab_2 = second_derivative_ab / (
                -2 * self.GP_args_AB.reduced_mass * self.GP_args_AB.scale_factor ** 2) * psi_bc * psi_abc
        ke_r_bc_2 = second_derivative_bc / (
                -2 * self.GP_args_BC.reduced_mass * self.GP_args_BC.scale_factor ** 2) * psi_ab * psi_abc
        ke_bend_2 = second_derivative_abc * psi_ab * psi_bc * (
                1 / (self.GP_args_AB.reduced_mass * r_ab ** 2) + 1 / (
                self.GP_args_BC.reduced_mass * r_bc ** 2) - 2 * x) / (
                            self.GP_args_ABC.mass_B * r_ab * r_bc)
        ke_bend_ab = (first_derivative_ab / self.GP_args_AB.scale_factor) * first_derivative_abc * y * psi_bc / (
                r_ab * self.GP_args_ABC.mass_B)
        ke_bend_bc = (first_derivative_bc / self.GP_args_BC.scale_factor) * first_derivative_abc * y * psi_ab / (
                r_bc * self.GP_args_ABC.mass_B)
        ke_r_ab_r_bc = (first_derivative_ab / self.GP_args_AB.scale_factor) * (
                first_derivative_bc / self.GP_args_BC.scale_factor) * psi_abc * x / self.GP_args_ABC.mass_B
        ke_r_ab = first_derivative_ab * psi_bc * psi_abc * x / (
                self.GP_args_AB.scale_factor * self.GP_args_ABC.mass_B * r_bc)
        ke_r_bc = first_derivative_bc * psi_ab * psi_abc * x / (
                self.GP_args_BC.scale_factor * self.GP_args_ABC.mass_B * r_ab)
        ke_bend = (1 / (self.GP_args_AB.reduced_mass * r_ab ** 2) + 1 / (
                self.GP_args_BC.reduced_mass * r_bc ** 2) - 2 * x) / (
                          self.GP_args_ABC.mass_B * r_ab * r_bc) * x * first_derivative_abc * psi_ab * psi_bc / (-2 * y)
        ke_const = psi_ab * psi_bc * psi_abc * x / (self.GP_args_ABC.mass_B * r_ab * r_bc)
        ke = ke_r_ab_2 + ke_r_bc_2 + ke_bend_2 + ke_bend_ab + ke_bend_bc - \
             ke_r_ab_r_bc + ke_r_ab + ke_r_bc - ke_bend + ke_const
        f = lambdify([(p, q, x, y)], ke, "numpy")
        KE = [f(i) for i in self.points]
        return KE

    def wavefunction(self, n):
        """Calculates |n> as a lambda function of a tuple (p,q,cos(theta),sin(theta))"""
        ab, bc, abc = n
        theta = symbols('theta')
        p, q, x, y = symbols('p q x y')
        psi_ab = self.oneD_args_AB.psi[ab]
        psi_ab = psi_ab.subs(q, p)
        psi_bc = self.oneD_args_BC.psi[bc]
        psi_abc = self.oneD_args_ABC.psi[abc]
        psi_ab = psi_ab.subs(exp, 1)
        psi_bc = psi_bc.subs(exp, 1)
        psi_abc = psi_abc.subs(cos(theta), x)
        f = psi_ab * psi_bc * psi_abc
        f = lambdify([(p, q, x, y)], f, "numpy")
        PE = [f(i) for i in self.points]
        return PE

    def save_values(self):
        """Saves the values of |H|n> and |n> for all n into files to load in when calculating matrix elements"""
        h_values = []
        psi_values = []
        for i in self.basis_keys:
            h_values.append(self.apply_hamiltonian(i))
            psi_values.append(self.wavefunction(i))
        saving = savez_compressed(self.values_filename, h_values, psi_values)
        return h_values, psi_values

    def load_values(self):
        """Checks if |H|n> and |n> evaluated for all wavefunctions at all points have been put into a file and loads
        them in, else creates and saves them """
        # TODO this is still too slow to create matrix elements - need to use dot product
        if os.path.isfile(self.values_filename):
            opening = load(self.values_filename)
            hamiltonian_applied_values, wavefunction_values = opening['arr_0'], opening['arr_1']
            opening.close()
        else:
            hamiltonian_applied_values, wavefunction_values = self.save_values()
        return hamiltonian_applied_values, wavefunction_values

    def generate_matrices(self):
        """ Overriden method from abstract class to include load values """
        hvib = zeros((len(self.basis_keys), len(self.basis_keys)))
        svib = zeros((len(self.basis_keys), len(self.basis_keys)))
        hamiltonian_applied_values, wavefunction_values = self.load_values()
        for i in range(len(self.basis_keys)):
            for j in range(i + 1):
                hvib[i][j] = self.matrix_element(wavefunction_values[i], hamiltonian_applied_values[j])
                svib[i][j] = self.matrix_element(wavefunction_values[i], wavefunction_values[j])
                hvib[j][i] = hvib[i][j]
                svib[j][i] = svib[i][j]
        saving = savez_compressed(self.matrices_filename, hvib, svib)
        return hvib, svib

    def run_hvib_triatomic(self):
        """
        Method to run whole class.
        1)Loads in H, S matrices if already made otherwise creates and saves them
        2)Diagonalise H to give eigenvalues, eigenvectors and eigenfunctions
        3)Eigenvectors and eigenvalues converted to wavenumber and saved to an outfile
        :return: Energies and eigenfunctions
        """

        filename = self.user_args.molecule + "_Hvib_data.dat"
        outfile = open(filename, "w")
        wavenumber = 219475
        H, S = self.load_matrices()
        # self.E, self.c, self.psi = self.diagonalise_matrices(H, S)
        self.E, self.c = LA.eigh(H)
        E_wav = [i * wavenumber for i in self.E]
        outfile.write(str(E_wav) + '\n' + str(self.c) + '\n')
        # return E, psi
        return self.E, self.c
