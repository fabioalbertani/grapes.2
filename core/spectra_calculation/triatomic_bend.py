#!/bin/python
# File: core.spectra_calculation.triatomic_bend.py

from mpmath import radians
from numpy import zeros, asarray
from sympy import sqrt, symbols, cos, sin, lambdify
from sympy.core.function import diff
from sympy.functions.special.polynomials import legendre
from sympy.integrals.quadrature import gauss_legendre

from hamiltonian import Hamiltonian


class TriatomicBend(Hamiltonian):
    """
    Class for calculating the one dimensional bending energies and wavefunctions for a triatomic.

    Attributes:
        max bend level: int (user defined)
        potential: list (calculated from GP)
        reduced masses: floats (calculated from GP)
        equilibrium geometry: floats (calculated from GP)
        AB/BC: tuple (bond length, scaling, reduced mass, 1D eigenfunctions)
               analogous for full Hvib but 2nd and 4th arguments are unpacked as unnecessary
        molecule: string

    Methods to run:
        run_h3_triatomic
    """

    def __init__(self, max_bend_level, potential, equilibrium_angle, mass_B, AB, BC, molecule):
        super().__init__(max_bend_level, potential)
        self.theta_eq = equilibrium_angle
        self.mass_B = mass_B
        self.r_AB_eq, _, self.reduced_mass_AB, _ = AB
        self.r_BC_eq, _, self.reduced_mass_BC, _ = BC
        self.molecule = molecule

        self.cos_points, self.weights = gauss_legendre(len(self.potential), 10)
        self.cos_points = asarray(self.cos_points)
        self.sin_points = asarray([sqrt(1 - i ** 2) for i in self.cos_points], dtype=float)
        theta = symbols('theta')
        self.basis = [sqrt((2 * i + 1) / 2) * legendre(i, cos(theta)) for i in range(self.max_quantum_numbers)]
        self.matrices_filename = self.molecule + "_bend_matrices_%i.npz" % self.max_quantum_numbers

    def kinetic_energy(self, n):
        theta = symbols('theta')
        ket = self.basis[n]
        KE = (1 / (self.reduced_mass_AB * self.r_AB_eq ** 2) + 1 / (
                self.reduced_mass_BC * self.r_BC_eq ** 2) - 2 * cos(radians(self.theta_eq)) / (
                      self.mass_B * self.r_AB_eq * self.r_BC_eq)) * diff(ket, theta, 2) / (-2)
        KE = lambdify([cos(theta), sin(theta)], KE, "numpy")
        if n == 0:
            KE = zeros(len(self.potential))
        else:
            KE = list(KE(self.cos_points, self.sin_points))
        return KE

    def wavefunction(self, n):
        theta = symbols('theta')
        ket = self.basis[n]
        PE = lambdify(cos(theta), ket, "numpy")
        if n == 0:
            PE = [ket for i in range(len(self.potential))]
        else:
            PE = list(PE(self.cos_points))
        return PE

    def run_h3_triatomic(self):
        """
        Method to run whole class.
        1)Loads in H, S matrices if already made otherwise creates and saves them
        2)Diagonalise H to give eigenvalues, eigenvectors and eigenfunctions
        3)Eigenvectors and eigenvalues converted to wavenumber and saved to an outfile
        :return: Energies and eigenfunctions
        """

        filename = self.molecule + "_bend.dat"
        outfile = open(filename, "w")
        wavenumber = 219475
        h3, s3 = self.load_matrices()
        E, c, psi = self.diagonalise_matrices(h3, s3)
        E_wav = [i * wavenumber for i in E]
        outfile.write(str(E_wav) + '\n' + str(c) + '\n')
        return E, psi


class TriatomicBend2(Hamiltonian):
    """
    Class for calculating the one dimensional bending energies and wavefunctions for a triatomic.

    Attributes:
        user_args: max_stretch_level_AB, max_stretch_level_BC, max_bend_level, molecule
        GP_args: (GP_args_AB, GP_args_BC, GP_args_ABC, full_potential)
                GP_args_AB: scale_factor, reduced_mass, r_eq, potential
                GP_args_BC: scale_factor, reduced_mass, r_eq, potential
                GP_args_ABC: mass_B, theta_eq, potential

    Methods to run:
        run_h3_triatomic
    """

    def __init__(self, user_args, GP_args):
        self.user_args = user_args
        self.GP_args_AB, self.GP_args_BC, self.GP_args_ABC, _ = GP_args
        super().__init__(self.user_args.max_bend_level, self.GP_args_ABC.potential)

        self.cos_points, self.weights = gauss_legendre(len(self.GP_args_ABC.potential), 10)
        self.cos_points = asarray(self.cos_points)
        self.sin_points = asarray([sqrt(1 - i ** 2) for i in self.cos_points], dtype=float)
        theta = symbols('theta')
        self.basis = [sqrt((2 * i + 1) / 2) * legendre(i, cos(theta)) for i in range(self.max_quantum_numbers)]
        self.matrices_filename = self.user_args.molecule + "_bend_matrices_%i.npz" % self.max_quantum_numbers

    def kinetic_energy(self, n):
        theta = symbols('theta')
        ket = self.basis[n]
        KE = (1 / (self.GP_args_AB.reduced_mass * self.GP_args_AB.r_eq ** 2) + 1 / (
                self.GP_args_BC.reduced_mass * self.GP_args_BC.r_eq ** 2) - 2 * cos(
            radians(self.GP_args_ABC.theta_eq)) / (
                      self.GP_args_ABC.mass_B * self.GP_args_AB.r_eq * self.GP_args_BC.r_eq)) * diff(ket, theta, 2) / (
                 -2)
        KE = lambdify([cos(theta), sin(theta)], KE, "numpy")
        if n == 0:
            KE = zeros(len(self.potential))
        else:
            KE = list(KE(self.cos_points, self.sin_points))
        return KE

    def wavefunction(self, n):
        theta = symbols('theta')
        ket = self.basis[n]
        PE = lambdify(cos(theta), ket, "numpy")
        if n == 0:
            PE = [ket for i in range(len(self.potential))]
        else:
            PE = list(PE(self.cos_points))
        return PE

    def run_h3_triatomic(self):
        """
        Method to run whole class.
        1)Loads in H, S matrices if already made otherwise creates and saves them
        2)Diagonalise H to give eigenvalues, eigenvectors and eigenfunctions
        3)Eigenvectors and eigenvalues converted to wavenumber and saved to an outfile
        :return: Energies and eigenfunctions
        """

        filename = self.user_args.molecule + "_bend.dat"
        outfile = open(filename, "w")
        wavenumber = 219475
        h3, s3 = self.load_matrices()
        self.E, c, self.psi = self.diagonalise_matrices(h3, s3)
        E_wav = [i * wavenumber for i in self.E]
        outfile.write(str(E_wav) + '\n' + str(c) + '\n')
        return self.E, self.psi
