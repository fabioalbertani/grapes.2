#!/bin/python
# File: core.spectra_calculation.diatomic_h_vib.py

from numpy import asarray
from sympy import exp, sqrt, oo, symbols, lambdify
from sympy.core.function import diff
from sympy.integrals import integrate
from sympy.integrals.quadrature import gauss_hermite
from sympy.polys.orthopolys import hermite_poly

from hamiltonian import Hamiltonian


# TODO remove symbolic functions as soon as possible, list of values at the points is usually good enough


class HVibDiatomic(Hamiltonian):
    """Class for calculating the vibrational energies and wavefunctions of a diatomic as a linear combination of
    harmonic oscillator wavefunctions.
    Attributes (Atomic Units):
        scale factor: float (calculated from GP)
        reduced mass: float (calculated from GP)
        max vib level: int (user defined)
        potential: list (calculated from GP)
        bond: string

    Methods to run:
        run_hvib_diatomic
    """

    def __init__(self, potential, scale_factor, reduced_mass, max_vib_level, bond):
        super().__init__(max_vib_level, potential)
        self.scale_factor = scale_factor
        self.reduced_mass = reduced_mass
        self.bond = bond

        self.points, self.weights = gauss_hermite(len(self.potential), 10)
        self.points = asarray(self.points, dtype=float)
        self.basis = [self.HO_wavefunction(i) for i in range(self.max_quantum_numbers)]
        self.matrices_filename = self.bond + "_matrices_%i.npz" % self.max_quantum_numbers

    def HO_wavefunction(self, n):
        """
        Calculates normalised harmonic oscialltor wavefunctions
        :param n: int, quantum number
        :return: symbolic wavefunction
        """
        x, q = symbols('x q')
        polynomial = hermite_poly(n, q)
        exponential = exp(-(x / self.scale_factor) ** 2 / 2)
        rescale_exponential = exponential.subs(x, q * self.scale_factor)
        wavefunction = polynomial * rescale_exponential
        normalisation = integrate(wavefunction * wavefunction, (q, -oo, oo))
        return wavefunction / sqrt(normalisation)

    def kinetic_energy(self, n):
        q = symbols('q')
        ket = self.basis[n]
        KE = diff(ket, q, 2) / (-2 * self.reduced_mass * self.scale_factor ** 2)
        KE = KE.subs(exp, 1)
        KE = lambdify(q, KE, "numpy")
        KE = list(KE(self.points))
        return KE

    def wavefunction(self, n):
        q = symbols('q')
        ket = self.basis[n]
        ket = ket.subs(exp, 1)
        f = lambdify(q, ket, "numpy")
        if n == 0:
            f = [ket for i in range(len(self.potential))]
        else:
            f = list(f(self.points))
        return f

    def run_hvib_diatomic(self):
        """
        Method to run whole class.
        1)Loads in H, S matrices if already made otherwise creates and saves them
        2)Diagonalise H to give eigenvalues, eigenvectors and eigenfunctions
        3)Eigenvectors and eigenvalues converted to wavenumber and saved to an outfile
        :return: Energies and eigenfunctions
        """

        filename = self.bond + "_hvib_data.dat"
        outfile = open(filename, "w")
        wavenumber = 219475
        H, S = self.load_matrices()
        E, c, psi = self.diagonalise_matrices(H, S)
        E_wav = [i * wavenumber for i in E]
        outfile.write(str(E_wav) + '\n' + str(c) + '\n')
        outfile.close()
        return E, psi


class HVibDiatomic2(Hamiltonian):
    """
    Class for calculating the vibrational energies and wavefunctions of a diatomic as a linear combination of
    harmonic oscillator wavefunctions.
    Attributes (Atomic Units):
        GP_args: scale_factor, reduced_mass, equilbrium_bond_length, potential
        user_args: max_vib_level, max_rot_level, temperature, transitions, bond

    Methods to run:
        run_hvib_diatomic
    """

    def __init__(self, GP_args, user_args):
        self.GP_args = GP_args
        self.user_args = user_args
        super().__init__(self.user_args.max_vib_level, self.GP_args.potential)

        self.points, self.weights = gauss_hermite(len(self.potential), 10)
        self.points = asarray(self.points, dtype=float)
        self.basis = [self.HO_wavefunction(i) for i in range(self.max_quantum_numbers)]
        self.matrices_filename = self.user_args.bond + "_matrices_%i.npz" % self.max_quantum_numbers

    def HO_wavefunction(self, n):
        """
        Calculates normalised harmonic oscialltor wavefunctions
        :param n: int, quantum number
        :return: symbolic wavefunction
        """
        x, q = symbols('x q')
        polynomial = hermite_poly(n, q)
        exponential = exp(-(x / self.GP_args.scale_factor) ** 2 / 2)
        rescale_exponential = exponential.subs(x, q * self.GP_args.scale_factor)
        wavefunction = polynomial * rescale_exponential
        normalisation = integrate(wavefunction * wavefunction, (q, -oo, oo))
        return wavefunction / sqrt(normalisation)

    def kinetic_energy(self, n):
        q = symbols('q')
        ket = self.basis[n]
        KE = diff(ket, q, 2) / (-2 * self.GP_args.reduced_mass * self.GP_args.scale_factor ** 2)
        KE = KE.subs(exp, 1)
        KE = lambdify(q, KE, "numpy")
        KE = list(KE(self.points))
        return KE

    def wavefunction(self, n):
        q = symbols('q')
        ket = self.basis[n]
        ket = ket.subs(exp, 1)
        f = lambdify(q, ket, "numpy")
        if n == 0:
            f = [ket for i in range(len(self.potential))]
        else:
            f = list(f(self.points))
        return f

    def run_hvib_diatomic(self):
        """
        Method to run whole class.
        1)Loads in H, S matrices if already made otherwise creates and saves them
        2)Diagonalise H to give eigenvalues, eigenvectors and eigenfunctions
        3)Eigenvectors and eigenvalues converted to wavenumber and saved to an outfile
        :return: Energies and eigenfunctions
        """

        filename = self.user_args.bond + "_hvib_data.dat"
        outfile = open(filename, "w")
        wavenumber = 219475
        H, S = self.load_matrices()
        self.E, c, self.psi = self.diagonalise_matrices(H, S)
        E_wav = [i * wavenumber for i in self.E]
        outfile.write(str(E_wav) + '\n' + str(c) + '\n')
        outfile.close()
        return self.E, self.psi


class TriatomicStretch(HVibDiatomic2):

    def __init__(self, GP_args, max_strech_level, fragment):
        self.GP_args = GP_args
        self.max_quantum_numbers = max_strech_level
        self.fragment = fragment
        self.potential = GP_args.potential

        self.points, self.weights = gauss_hermite(len(self.potential), 10)
        self.points = asarray(self.points, dtype=float)
        self.basis = [self.HO_wavefunction(i) for i in range(self.max_quantum_numbers)]
        self.matrices_filename = self.fragment + "_matrices_%i.npz" % self.max_quantum_numbers
