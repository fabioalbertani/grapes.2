#!/bin/python
# File: core.spectra_calculation.triatomic_main.py

from diatomic_h_vib import TriatomicStretch as StretchH
from triatomic_bend import TriatomicBend2 as BendH
from triatomic_h_vib import HVibTriatomic2 as HVib


class RunTriatomic:
    """
    Class to run full method (only zeroth order vibrational currently) on triatomic
    Attributes:
        user_args: max_stretch_level_AB, max_stretch_level_BC, max_bend_level, molecule
        GP_args: (GP_args_AB, GP_args_BC, GP_args_ABC, full_potential)
                GP_args_AB: scale_factor, reduced_mass, r_eq, potential
                GP_args_BC: scale_factor, reduced_mass, r_eq, potential
                GP_args_ABC: mass_B, theta_eq, potential
        oneD_args: (stretch_args_AB, stretch_args_BC, bend_args_ABC)
                stretch_args_AB: E, psi
                stretch_args_BC: E, psi
                bend_args_ABC: E, psi
    """

    @staticmethod
    def RegisterOptions(args):
        m = args.RegisterModule('spectra')
        args.Register(m, '--max_stretch_level_AB', type=int, default=10,
                      help='Basis size for AB bond stretch. This is not a normal mode.')
        args.Register(m, '--max_stretch_level_BC', type=int, default=10,
                      help='Basis size for BC bond stretch. This is not a normal mode.')
        args.Register(m, '--max_bend_level', type=int, default=20,
                      help='Basis size for bend mode. This requires more basis functions the further away from linear '
                           'the equilibrium geometry of the molecule is. This is a normal mode.')
        args.Register(m, '--molecule', type=str, default="ABC", help='Molecule name')

    def __init__(self, user_args, GP_args):
        # TODO interface to get GP args
        self.Moduleargs = user_args.spectra
        self.GP_args = GP_args

    def run_triatomic(self):
        h1 = StretchH(max_stretch_level=self.Moduleargs.max_stretch_level_AB,
                      GP_args=self.GP_args[0], fragment=self.Moduleargs.molecule[0:2])
        h1.run_hvib_diatomic()
        h2 = StretchH(max_stretch_level=self.Moduleargs.max_stretch_level_BC,
                      GP_args=self.GP_args[1], fragment=self.Moduleargs.molecule[1:3])
        h2.run_hvib_diatomic()
        h3 = BendH(user_args=self.Moduleargs,
                   GP_args=self.GP_args)
        h3.run_h3_triatomic()
        H = HVib(user_args=self.Moduleargs,
                 GP_args=self.GP_args, oneD_args=(h1, h2, h3))
        H.run_hvib_triatomic()
