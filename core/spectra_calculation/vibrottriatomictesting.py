from sympy import exp, sqrt, oo, simplify, symbols, N, cos, pi, I, sin, lambdify, acos, factor
from sympy.integrals import integrate
from sympy.integrals.quadrature import gauss_hermite, gauss_legendre
from sympy.core.function import diff
from sympy.polys.orthopolys import hermite_poly
from sympy.functions.special.polynomials import legendre
from mpmath import radians
from numpy import zeros, dot, genfromtxt, c_, asarray, asscalar, linspace, savetxt, loadtxt
from scipy import linalg as LA, optimize
import time
from matplotlib import pyplot

outfile = open("vibrottriatomictesting.log", "w")

start = time.time()

# triatomic ABC
# TODO add constraint on min(p), min(q) to change weight to 0
atom_mass = {"C": 12.000, "O": 16.000, "H": 1.000}
R_ABe = 0.958  # A-B in angstrom (6-31g* 0.9685)
R_BCe = 0.958  # B-C in angstrom (6-31g* 0.9685)
theta_e = 104.476  # ABC bond angle in degrees (6-31g* 103.66)
max_vib_level_RAB = 10  # HO wavefunctions
max_vib_level_RBC = 10  # HO wavefunctions
max_rot_level = 20  # Spherical Harmonics with J=0 (Zonal Harmonics)
theta = symbols('theta')
x, y, p, q, v, w = symbols('x y p q v w')
points_AB, weights_AB = gauss_hermite(2 * max_vib_level_RAB, 9)
points_AB = asarray(points_AB, dtype=float)
points_BC, weights_BC = gauss_hermite(2 * max_vib_level_RBC, 9)
points_BC = asarray(points_BC, dtype=float)
points_ABC, weights_ABC = gauss_legendre(2 * max_rot_level, 9)
cos_points = asarray(points_ABC, dtype=float)
sin_points = asarray([sqrt(1 - i ** 2) for i in points_ABC], dtype=float)
theta_points = asarray([acos(i) for i in points_ABC], dtype=float)
points_ABC = asarray(points_ABC, dtype=float)
cos_half_points=asarray([cos(i/2) for i in theta_points],dtype=float)
sin_half_points=asarray([sin(i/2) for i in theta_points],dtype=float)
degrees = [pi * i / 180 for i in range(181)]
degrees = asarray(degrees, dtype=float)
bohr = 0.529177  # convert bohr to angstrom
wavenumber = 219475.0  # convert hartree to wavenumber
k_b = 0.6950356  # in wavenumber
amu = 1822.89  # convert amu to m_e

def reduced_mass(atom1, atom2):
    m1 = atom_mass[atom1]
    m2 = atom_mass[atom2]
    mu = (m1 * m2) / (m1 + m2)
    return mu


mu_AB = reduced_mass("H", "O")
De_AB = 0.291567
w_AB = 3600.0
#  kfit from Morse (3320)
scale_AB = 1 / (sqrt((w_AB / wavenumber) * (mu_AB * amu)))
morseexponent_AB = (w_AB / wavenumber) * scale_AB * sqrt(mu_AB * amu) / sqrt(2 * De_AB)  # fit was 0.1546172
R_AB = p * scale_AB + R_ABe  # Symbolic expression

mu_BC = reduced_mass("H", "O")
De_BC = 0.291567
w_BC = 3600.0
scale_BC = 1 / sqrt((w_BC / wavenumber) * (mu_BC * amu))
morseexponent_BC = (w_BC / wavenumber) * scale_BC * sqrt(mu_BC * amu) / sqrt(2 * De_BC)  # fit was 0.1546172
R_BC = q * scale_BC + R_BCe  # Symbolic expression

for i in range(len(points_AB)):
    if points_AB[i]<-R_ABe/(scale_AB*bohr):
        weights_AB[i]=0

for i in range(len(points_BC)):
    if points_BC[i]<-R_BCe/(scale_BC*bohr):
        weights_BC[i]=0



full_points=[]
full_weights=[]
for i in range(len(points_AB)):
    for j in range(len(points_BC)):
        for k in range(len(cos_points)):
            a=(points_AB[i],points_BC[j],cos_points[k],sin_points[k])
            full_points.append(a)
            b=weights_AB[i]*weights_BC[j]*weights_ABC[k]
            full_weights.append(b)

# returns normalised HO wavefunctions
def HO_wavefunction(n, scale):
    a = hermite_poly(n, x)
    c = exp(-(x / scale) ** 2 / 2)
    d = c.subs(x, x * scale)
    e = a * d
    f = integrate(e * e, (x, -oo, oo))
    g = e / sqrt(f)
    return g


HO_basis_RAB = []  # A-B HO basis
for i in range(max_vib_level_RAB):
    a = HO_wavefunction(i, scale_AB)
    a = a.subs(x, p)
    HO_basis_RAB.append(a)


stretch_AB = De_AB * (1 - exp(-morseexponent_AB * p)) ** 2
Potential_hAB = lambdify(p, stretch_AB, "numpy")
test=Potential_hAB(1.0)
Potential_hAB =(list(Potential_hAB(points_AB)))  # A-B stretching at R_2e, theta_e 6-31g* minR=0.96853
Potential_hAB = [[i] for i in Potential_hAB]

def matrix_element_hAB(m, n):
    a = HO_basis_RAB[m]
    b = HO_basis_RAB[n]
    c = exp(-p ** 2)
    KE = a * diff(b, p, 2) / (-2 * c * mu_AB * amu * scale_AB ** 2)
    PE = a * b / c
    KE = lambdify(p, KE, "numpy")
    PE = lambdify(p, PE, "numpy")
    KE = list(KE(points_AB))
    PE = list(PE(points_AB))
    integral = 0
    for i in range(len(points_AB)):
        if points_AB[i] < -R_ABe / scale_AB:
            integral += 0
        else:
            d = KE[i]
            e = PE[i]
            f = Potential_hAB[i][0]
            g = weights_AB[i]
            integral += d * g + e * f * g
    return integral


hAB = zeros((max_vib_level_RAB, max_vib_level_RAB))
for i in range(max_vib_level_RAB):
    for j in range(i + 1):
        hAB[i][j] = matrix_element_hAB(i, j)
        hAB[j][i] = hAB[i][j]

sAB = zeros((max_vib_level_RAB, max_vib_level_RAB))
for i in range(max_vib_level_RAB):
    for j in range(i + 1):
        a = HO_basis_RAB[i]
        b = HO_basis_RAB[j]
        c = exp(-p ** 2)
        d = a * b / c
        d = lambdify(p, d, "numpy")
        d = list(d(points_AB))
        integral = 0
        for k in range(len(points_AB)):
            f = d[k]
            g = weights_AB[k]
            integral += f * g
        sAB[i][j] = integral
        sAB[j][i] = sAB[i][j]

# diagonalise A-B stretch

hAB_energies = []
hAB_wavefunctions = []
hAB_evals, hAB_evecs = LA.eigh(hAB, sAB)
hAB_energies.append(hAB_evals)
hAB_energies = [i * wavenumber for i in hAB_energies]
for v in range(max_vib_level_RAB):
    a = hAB_evecs[:, v]
    b = HO_basis_RAB
    hAB_wavefunctions.append(factor(dot(a, b)))
outfile.write("A-B stretch energies:" + str(hAB_energies) + "\n")
outfile.write("A-B stretch eigenvectors:" + str(hAB_evecs) + "\n")

print hAB_energies

HO_basis_RBC = []  # B-C HO basis
for i in range(max_vib_level_RBC):
    a = HO_wavefunction(i, scale_BC)
    a = a.subs(x, q)
    HO_basis_RBC.append(a)

stretch_BC = De_BC * (1 - exp(-morseexponent_BC * q)) ** 2
Potential_hBC = lambdify(q, stretch_BC, "numpy")
Potential_hBC = list(Potential_hBC(points_BC))  # B-C stretching at R_2e, theta_e 6-31g* minR=0.96853
Potential_hBC = [[i] for i in Potential_hBC]


def matrix_element_hBC(m, n):
    a = HO_basis_RBC[m]
    b = HO_basis_RBC[n]
    c = exp(-q ** 2)
    KE = a * diff(b, q, 2) / (-2 * c * mu_BC * amu * scale_BC ** 2)
    PE = a * b / c
    KE = lambdify(q, KE, "numpy")
    PE = lambdify(q, PE, "numpy")
    KE = list(KE(points_BC))
    PE = list(PE(points_BC))
    integral = 0
    for i in range(len(points_BC)):
        if points_BC[i] < -R_BCe / scale_BC:
            integral += 0
        else:
            d = KE[i]
            e = PE[i]
            f = Potential_hBC[i][0]
            g = weights_BC[i]
            integral += d * g + e * f * g
    return integral


hBC = zeros((max_vib_level_RBC, max_vib_level_RBC))
for i in range(max_vib_level_RBC):
    for j in range(i + 1):
        hBC[i][j] = matrix_element_hBC(i, j)
        hBC[j][i] = hBC[i][j]

sBC = zeros((max_vib_level_RBC, max_vib_level_RBC))
for i in range(max_vib_level_RBC):
    for j in range(i + 1):
        a = HO_basis_RBC[i]
        b = HO_basis_RBC[j]
        c = exp(-q ** 2)
        d = a * b / c
        d = lambdify(q, d, "numpy")
        d = list(d(points_BC))
        integral = 0
        for k in range(len(points_BC)):
            f = d[k]
            g = weights_BC[k]
            integral += f * g
        sBC[i][j] = integral
        sBC[j][i] = sBC[i][j]

# diagonalise B-C stretch
hBC_energies = []
hBC_wavefunctions = []
hBC_evals, hBC_evecs = LA.eigh(hBC, sBC)
hBC_energies.append(hBC_evals)
hBC_energies = [i * wavenumber for i in hBC_energies]
for v in range(max_vib_level_RBC):
    a = hBC_evecs[:, v]
    b = HO_basis_RBC
    hBC_wavefunctions.append(factor(dot(a, b)))
outfile.write("B-C stretch energies:" + str(hBC_energies) + "\n")
outfile.write("B-C stretch eigenvectors:" + str(hBC_evecs) + "\n")

print hBC_energies

Bending_basis = []  # ABC bend basis
for n in range(max_rot_level):
    Bending_basis.append(simplify(sqrt(((2 * n + 1) / (2.0))) * legendre(n, cos(theta))))

# TODO change to internal angle, currently this measures the external angle from pi
# ABC bending at R_ABe, R_BCe, minTheta=104.27 (should be 103.66 in 6-31g*) in radians, 0=linear
bend = (4.5534 * 10 ** -18) * (theta * 180 / pi) ** 8 + (-1.7617 * 10 ** -13) * (theta * 180 / pi) ** 6 + \
       (3.29912 * 10 ** -9) * (theta * 180 / pi) ** 4 + (-2.39161 * 10 ** -5) * (theta * 180 / pi) ** 2 + 0.0569653827

Potential_hABC = lambdify(theta, bend, "numpy")
Potential_hABC = list(Potential_hABC(theta_points))
Potential_hABC = [[i] for i in Potential_hABC]

def matrix_element_hABC(n, m):
    integral = 0
    a = Bending_basis[n]
    b = Bending_basis[m]
    KE = a * (1 / (mu_AB * amu * (R_ABe / bohr) ** 2) + 1 / (mu_BC * amu * (R_BCe / bohr) ** 2) - 2 * cos(
        radians(theta_e)) / (
                      atom_mass["O"] * amu * (R_ABe / bohr) * (R_BCe / bohr))) * diff(b, theta, 2) / (-2)
    PE = b * a
    if m == 0:
        KE = zeros(len(cos_points))
    else:
        KE = lambdify([cos(theta), sin(theta)], KE, "numpy")
        KE = list(KE(cos_points, sin_points))
    if n == 0 and m == 0:
        PE = [PE] * len(cos_points)
    else:
        PE = lambdify(cos(theta), PE, "numpy")
        PE = list(PE(cos_points))
    for i in range(len(cos_points)):
        c = KE[i]
        d = PE[i]
        e = Potential_hABC[i][0]
        f = weights_ABC[i]
        integral += f * (c + d * e)
    return integral


hABC = zeros((max_rot_level, max_rot_level))
for i in range(len(Bending_basis)):
    for j in range(i + 1):
        hABC[i][j] = matrix_element_hABC(i, j)
        hABC[j][i] = hABC[i][j]

sABC = zeros((max_rot_level, max_rot_level))
for i in range(len(Bending_basis)):
    for j in range(i + 1):
        integral = 0
        a = Bending_basis[i]
        b = Bending_basis[j]
        if i == 0 and j == 0:
            integral += 1
        else:
            overlap = lambdify(cos(theta), a * b, "numpy")
            overlap = list(overlap(cos_points))
            for k in range(len(overlap)):
                integral += overlap[k] * weights_ABC[k]
        sABC[i][j] = integral
        sABC[j][i] = sABC[i][j]

# diagonalise ABC bend
hABC_energies = []
hABC_wavefunctions = []
hABC_evals, hABC_evecs = LA.eigh(hABC, sABC)
hABC_energies.append(hABC_evals)
hABC_energies = [i * wavenumber for i in hABC_energies]
for J in range(max_rot_level):
    a = hABC_evecs[:, J]
    b = Bending_basis
    hABC_wavefunctions.append(factor(dot(a, b)))
outfile.write("ABC bend energies:" + str(hABC_energies) + "\n")
outfile.write("ABC bend eigenvectors:" + str(hABC_evecs) + "\n")

print hABC_energies


def full_vib_wavefunction(i, j, k):
    a = hAB_wavefunctions[i]
    b = hBC_wavefunctions[j]
    c = hABC_wavefunctions[k]
    return a * b * c


# order (m00),(mn0),(mnp) in python 3, scrambled in python 2
full_vib_basis = {}
for k in range(max_rot_level):
    for j in range(max_vib_level_RBC):
        for i in range(max_vib_level_RAB):
            full_vib_basis[(i, j, k)] = full_vib_wavefunction(i, j, k)
full_vib_basis_keys = list(full_vib_basis.keys())
full_vib_basis_keys.sort()
full_vib_basis_values=[full_vib_basis[full_vib_basis_keys[i]] for i in range(len(full_vib_basis))]

# [p[0]q[0]theta[all],p[0]q[1]theta[all]....,p[1]q[0]theta[all]....]
PotentialV_vib = []
for i in range(len(Potential_hAB)):
    for j in range(len(Potential_hBC)):
        for k in range(len(Potential_hABC)):
            potential = Potential_hAB[i][0] + Potential_hBC[j][0] + Potential_hABC[k][0]
            PotentialV_vib.append([potential])

# calculates |T|m>
def apply_Tvib(m):
    b_RAB = (hAB_wavefunctions[m[0]])
    b_RBC = (hBC_wavefunctions[m[1]])
    b_bend = hABC_wavefunctions[m[2]]
    first_derivative_b_RAB = factor(diff(b_RAB, p, 1))
    first_derivative_b_RBC = factor(diff(b_RBC, q, 1))
    first_derivative_b_bend = factor(diff(b_bend, theta, 1))
    second_derivative_b_RAB = factor(diff(b_RAB, p, 2))
    second_derivative_b_RBC = factor(diff(b_RBC, q, 2))
    second_derivative_b_bend = factor(diff(b_bend, theta, 2))
    b_RAB = b_RAB.subs(exp, 1)
    b_RBC = b_RBC.subs(exp, 1)
    b_bend = b_bend.subs(cos(theta), x)
    first_derivative_b_RAB = first_derivative_b_RAB.subs(exp, 1)
    first_derivative_b_RBC = first_derivative_b_RBC.subs(exp, 1)
    first_derivative_b_bend = first_derivative_b_bend.subs([(cos(theta), x), (sin(theta), y)])
    second_derivative_b_RAB = second_derivative_b_RAB.subs(exp, 1)
    second_derivative_b_RBC = second_derivative_b_RBC.subs(exp, 1)
    second_derivative_b_bend = second_derivative_b_bend.subs([(cos(theta), x), (sin(theta), y)])
    PE_RAB = b_RAB
    PE_RBC = b_RBC
    PE_bend = b_bend
    PE = PE_RAB * PE_RBC * PE_bend
    KE_RAB_RAB = second_derivative_b_RAB / (-2 * mu_AB * amu * scale_AB ** 2)
    KE_RAB_RAB *= PE_RBC * PE_bend
    KE_RBC_RBC = second_derivative_b_RBC / (-2 * mu_BC * amu * scale_BC ** 2)
    KE_RBC_RBC *= PE_RAB * PE_bend
    KE_bendABC_bendABC =  second_derivative_b_bend
    KE_bendABC_bendABC *= PE_RAB * PE_RBC * (1 / (mu_AB * amu * (R_AB / bohr) ** 2) + 1 /
                                             (mu_BC * amu * (R_BC / bohr) ** 2) - 2 * x) / (
                                  atom_mass["O"] * amu * (R_AB / bohr) * (R_BC / bohr))
    KE_RAB = first_derivative_b_RAB / scale_AB
    KE_RBC = first_derivative_b_RBC / scale_BC
    KE_bendABC = first_derivative_b_bend
    KE_bendABC_RAB = KE_RAB * KE_bendABC * y * PE_RBC / ((R_AB / bohr) * atom_mass["O"] * amu)
    KE_bendABC_RBC = KE_RBC * KE_bendABC * y * PE_RAB / ((R_BC / bohr) * atom_mass["O"] * amu)
    KE_RAB_RBC = KE_RAB * KE_RBC * PE_bend * x / (atom_mass["O"] * amu)
    KE_RAB *= x * PE_bend * PE_RBC / (atom_mass["O"] * amu * R_BC)
    KE_RBC *= x * PE_bend * PE_RAB / (atom_mass["O"] * amu * R_AB)
    KE_bendABC *= (1 / (mu_AB * amu * (R_AB / bohr) ** 2) + 1 / (mu_BC * amu * (R_BC / bohr) ** 2) - 2 * x) / (
            atom_mass["O"] * amu * (R_AB / bohr) * (R_BC / bohr)) * x * PE_RBC * PE_RAB / (-2 * y)
    KE_const = PE * x / (atom_mass["O"] * (R_AB / bohr) * (R_BC / bohr))
    KE = KE_RAB_RAB + KE_RBC_RBC + KE_bendABC_bendABC - KE_RAB_RBC + KE_bendABC_RAB + \
         KE_bendABC_RBC - KE_bendABC + KE_RAB + KE_RBC + KE_const
    
    f_KE=lambdify([(p,q,x,y)],KE,"numpy")
    return f_KE



# calculates <n|
def apply_Vvib(n):
    a_RAB = (hAB_wavefunctions[n[0]])
    a_RBC = (hBC_wavefunctions[n[1]])
    a_bend = hABC_wavefunctions[n[2]]
    a_RAB = a_RAB.subs(exp, 1)
    a_RBC = a_RBC.subs(exp, 1)
    a_bend = a_bend.subs(cos(theta), x)
    PE=a_RAB*a_RBC*a_bend
    f_PE=lambdify([(p,q,x,y)],PE,"numpy")
    return f_PE



#kinetic_energy_list=[]
#potential_energy_list=[]
#for i in range(len(full_vib_basis)):
#    m=full_vib_basis_keys[i]
#    print m
#    timer_start=time.time()
#    kin=apply_Tvib(m)
#    pot=apply_Vvib(m)
#    kins=[kin(j) for j in full_points]
#    pots=[pot(j) for j in full_points]
#    kinetic_energy_list.append(kins)
#    potential_energy_list.append(pots)
#    timer_end=time.time()
#    print timer_end-timer_start
#
#outfilekin=open("kineticenergylist.dat","w")
#outfilepot=open("potentialenergylist.dat","w")
#
#outfilekin.write("kinetic energy values:")
#outfilekin.write(str(kinetic_energy_list)+'\n')
#outkin=savetxt("kineticenergyarray.dat", kinetic_energy_list)
#
#outfilepot.write("potential energy values")
#outfilepot.write(str(potential_energy_list)+'\n')
#outpot=savetxt("potentialenergyarray.dat",potential_energy_list)
#

#kinetic_energy_list=loadtxt("kineticenergyarray.dat")
#potential_energy_list=loadtxt("potentialenergyarray.dat")


#weighted_bra_list=[]
#full_ket_list=[]
#for i in range(len(kinetic_energy_list)):
#    a=potential_energy_list[i]
#    b=kinetic_energy_list[i]
#    c=[a[k]*full_weights[k] for k in range(len(full_weights))]
#    d=[b[k]+a[k]*PotentialV_vib[k][0] for k in range(len(full_weights))]
#    weighted_bra_list.append(c)
#    full_ket_list.append(d)
#    end=time.time()
#outbra=savetxt("fullbralist.dat",weighted_bra_list)
#outket=savetxt("fullketlist.dat",full_ket_list)
#
#

#full_bra_list=loadtxt("fullbralist.dat")
#full_ket_list=loadtxt("fullketlist.dat")



#
#def Hvib_matrix_element(i,j):
#    #start=time.time()
#    bra=potential_energy_list[i]
#    ket_T=kinetic_energy_list[j]
#    ket_V=potential_energy_list[j]
#    integral=0
#    l=[bra[k]*full_weights[k]*(ket_T[k]+ket_V[k]*PotentialV_vib[k][0]) for k in range(len(full_weights))]
#    end=time.time()
#    integral+=sum(l)
#    #print full_vib_basis_keys[i], full_vib_basis_keys[j]
#    #print end-start
#    return integral
#
#print Hvib_matrix_element(0,0)
#print dot(full_bra_list[0],full_ket_list[0])
#print Hvib_matrix_element(0,1)
#print dot(full_bra_list[0],full_ket_list[1])
#print Hvib_matrix_element(1,1)
#print dot(full_bra_list[1],full_ket_list[1])
#print Hvib_matrix_element(0,2)
#print dot(full_bra_list[0],full_ket_list[2])
#print Hvib_matrix_element(2,1)
#print dot(full_bra_list[2],full_ket_list[1])
#






#liststart=time.time()
#Tvib=[]
#Vvib=[]
#for i in range(len(full_vib_basis)):
#    m=full_vib_basis_keys[i]
#    Tvib.append(apply_Tvib(m))
#    Vvib.append(apply_Vvib(m))
## takes list index as argument
#listend=time.time()
#print "list generating time:", listend-liststart
#

#test0=apply_Tvib((0,0,0))
#test1=apply_Tvib((1,0,0))
#test2=apply_Tvib((2,0,0))
#test3=apply_Vvib((0,0,0))
#test4=apply_Vvib((1,0,0))
#test5=apply_Vvib((2,0,0))
#testkin=[test0,test1,test2]
#testpot=[test3,test4,test5]
#
## TODO multiple variable function wont take array as argument, need to iterate through list
## TODO precompute values of |T|n> and <m| into arrays
#def matrix_elem_Hvib(n,m):
#    timer_start=time.time()
#    #print timer-start
#    #print full_vib_basis_keys[n], full_vib_basis_keys[m]
#    #a=Vvib[n]
#    #b=Tvib[m]
#    #c=Vvib[m]
#    a=testpot[n]
#    b=testkin[m]
#    c=testpot[m]
#    integral=0
#    #KE_ans=list(b(full_points))
#    #PE_ans=list(c(full_points))
#    #bra=list(a(full_points))
#    KE_ans=list(map(b,full_points))
#    PE_ans=list(map(c,full_points))
#    bra=list(map(a,full_points))
#    timer_middle=time.time()
#    print timer_middle-timer_start
#    #for i in range(len(full_points)):
#    #    integral+=bra[i]*full_weights[i]*(KE_ans[i]+PE_ans[i]*PotentialV_vib[i][0])
#    l=[bra[i]*full_weights[i]*(KE_ans[i]+PE_ans[i]*PotentialV_vib[i][0]) for i in range(len(full_weights))]
#    integral+=sum(l)
#    timer_end=time.time()
#    print timer_end-timer_middle
#    
#    #for i in range(len(points_AB)):
#    #   for j in range(len(points_BC)):
#    #       for k in range(len(points_ABC)):
#    #            point=(points_AB[i],points_BC[j],cos_points[k],sin_points[k])
#    #            #KE_ans=b(points_AB[i],points_BC[j],cos_points[k],sin_points[k]) # |T|n>
#    #            KE_ans=b(point)
#    #            #PE_ans=c(points_AB[i],points_BC[j],cos_points[k],sin_points[k])*PotentialV_vib[i*len(points_BC)*len(points_ABC)+j*len(points_ABC)+k][0]# calculates |V|n>
#    #            PE_ans=c(point)
#    #            H=KE_ans+PE_ans# |H|n>
#    #            #bra=a(points_AB[i],points_BC[j],cos_points[k],sin_points[k])#<m|
#    #            bra=a(point)
#    #            ans=(bra*H)*weights_AB[i]*weights_BC[j]*weights_ABC[k]
#    #            integral+=ans
#    #timer_forloop=time.time()
#    #print timer_forloop-timer_start
#    return integral
#print "calculating a couple of matrix elements"
#print matrix_elem_Hvib(0,0)
#print matrix_elem_Hvib(1,0)
#print matrix_elem_Hvib(1,1)
#print matrix_elem_Hvib(2,0)
#print matrix_elem_Hvib(2,1)
#print matrix_elem_Hvib(2,2)
## takes tuples as argument
#def matrix_element_Hvib(n, m):
#    print n,m
#    integral = 0
#    a_RAB = (hAB_wavefunctions[n[0]])
#    a_RBC = (hBC_wavefunctions[n[1]])
#    a_bend = hABC_wavefunctions[n[2]]
#    b_RAB = (hAB_wavefunctions[m[0]])
#    b_RBC = (hBC_wavefunctions[m[1]])
#    b_bend = hABC_wavefunctions[m[2]]
#    first_derivative_b_RAB = factor(diff(b_RAB, p, 1))
#    first_derivative_b_RBC = factor(diff(b_RBC, q, 1))
#    first_derivative_b_bend = factor(diff(b_bend, theta, 1))
#    second_derivative_b_RAB = factor(diff(b_RAB, p, 2))
#    second_derivative_b_RBC = factor(diff(b_RBC, q, 2))
#    second_derivative_b_bend = factor(diff(b_bend, theta, 2))
#    a_RAB = a_RAB.subs(exp, 1)
#    a_RBC = a_RBC.subs(exp, 1)
#    a_bend = a_bend.subs(cos(theta), x)
#    b_RAB = b_RAB.subs(exp, 1)
#    b_RBC = b_RBC.subs(exp, 1)
#    b_bend = b_bend.subs(cos(theta), x)
#    first_derivative_b_RAB = first_derivative_b_RAB.subs(exp, 1)
#    first_derivative_b_RBC = first_derivative_b_RBC.subs(exp, 1)
#    first_derivative_b_bend = first_derivative_b_bend.subs([(cos(theta), x), (sin(theta), y)])
#    second_derivative_b_RAB = second_derivative_b_RAB.subs(exp, 1)
#    second_derivative_b_RBC = second_derivative_b_RBC.subs(exp, 1)
#    second_derivative_b_bend = second_derivative_b_bend.subs([(cos(theta), x), (sin(theta), y)])
#    PE_RAB = a_RAB * b_RAB
#    PE_RBC = a_RBC * b_RBC
#    PE_bend = a_bend * b_bend
#    PE = PE_RAB * PE_RBC * PE_bend
#    KE_RAB_RAB = a_RAB * second_derivative_b_RAB / (-2 * mu_AB * amu * scale_AB ** 2)
#    KE_RAB_RAB *= PE_RBC * PE_bend
#    KE_RBC_RBC = a_RBC * second_derivative_b_RBC / (-2 * mu_BC * amu * scale_BC ** 2)
#    KE_RBC_RBC *= PE_RAB * PE_bend
#    KE_bendABC_bendABC = a_bend * second_derivative_b_bend
#    KE_bendABC_bendABC *= PE_RAB * PE_RBC * (1 / (mu_AB * amu * (R_AB / bohr) ** 2) + 1 /
#                                             (mu_BC * amu * (R_BC / bohr) ** 2) - 2 * x) / (
#                                  atom_mass["O"] * amu * (R_AB / bohr) * (R_BC / bohr))
#    KE_RAB = a_RAB * first_derivative_b_RAB / scale_AB
#    KE_RBC = a_RBC * first_derivative_b_RBC / scale_BC
#    KE_bendABC = a_bend * first_derivative_b_bend
#    KE_bendABC_RAB = KE_RAB * KE_bendABC * y * PE_RBC / ((R_AB / bohr) * atom_mass["O"] * amu)
#    KE_bendABC_RBC = KE_RBC * KE_bendABC * y * PE_RAB / ((R_BC / bohr) * atom_mass["O"] * amu)
#    KE_RAB_RBC = KE_RAB * KE_RBC * PE_bend * x / (atom_mass["O"] * amu)
#    KE_RAB *= x * PE_bend * PE_RBC / (atom_mass["O"] * amu * R_BC)
#    KE_RBC *= x * PE_bend * PE_RAB / (atom_mass["O"] * amu * R_AB)
#    KE_bendABC *= (1 / (mu_AB * amu * (R_AB / bohr) ** 2) + 1 / (mu_BC * amu * (R_BC / bohr) ** 2) - 2 * x) / (
#            atom_mass["O"] * amu * (R_AB / bohr) * (R_BC / bohr)) * x * PE_RBC * PE_RAB / (-2 * y)
#    KE_const = PE * x / (atom_mass["O"] * (R_AB / bohr) * (R_BC / bohr))
#    KE = KE_RAB_RAB + KE_RBC_RBC + KE_bendABC_bendABC - KE_RAB_RBC + KE_bendABC_RAB + \
#         KE_bendABC_RBC - KE_bendABC + KE_RAB + KE_RBC + KE_const
#    f_KE=lambdify((p,q,x,y),KE,"numpy")
#    f_PE=lambdify((p,q,x,y),PE,"numpy")
#    for i in range(len(points_AB)):
#       for j in range(len(points_BC)):
#           for k in range(len(points_ABC)):
#                KE_ans=f_KE(points_AB[i],points_BC[j],cos_points[k],sin_points[k])
#                PE_ans=f_PE(points_AB[i],points_BC[j],cos_points[k],sin_points[k])
#                KE_ans*=weights_AB[i]*weights_BC[j]*weights_ABC[k]
#                PE_ans*=weights_AB[i]*weights_BC[j]*weights_ABC[k]*PotentialV_vib[i*len(points_BC)*len(points_ABC)+j*len(points_ABC)+k][0]
#                integral+=KE_ans+PE_ans
#    #KE = lambdify(p, KE, "numpy")
#    #PE = lambdify(p, PE, "numpy")
#    #KE = list(KE(points_AB))
#    #PE = list(PE(points_AB))
#    #for I in range(len(points_AB)):
#    #    a = KE[I] * weights_AB[I]
#    #    b = PE[I] * weights_AB[I]
#    #    KE[I] = a
#    #    PE[I] = b
#    ## p integrated out,only functions of q, theta now[p[0],p[1],.....p[end]]
#    #for i in range(len(points_AB)):
#    #    f = KE[i]
#    #    g = PE[i]
#    #    h1 = lambdify(q, f, "numpy")
#    #    h2 = lambdify(q, g, "numpy")
#    #    h1 = list(h1(points_BC))
#    #    h2 = list(h2(points_BC))
#    #    for I in range(len(points_BC)):
#    #        a = h1[I] * weights_BC[I]
#    #        b = h2[I] * weights_BC[I]
#    #        h1[I] = a
#    #        h2[I] = b
#    #    KE[i] = h1
#    #    PE[i] = h2
#    ## q integrated out, only theta remains[p[0]q[0],p[0]q[1]...,p[0]q[end],p[1]q[0]......]
#    #for i in range(len(points_AB)):
#    #    for j in range(len(points_BC)):
#    #        F = KE[i][j]
#    #        G = PE[i][j]
#    #        H1 = lambdify([x, y], F, "numpy")
#    #        H2 = lambdify([x, y], G, "numpy")
#    #        H1 = list(H1(cos_points, sin_points))
#    #        H2 = list(H2(cos_points, sin_points))
#    #        for J in range(len(points_ABC)):
#    #            a = H1[J] * weights_ABC[J]
#    #            b = H2[J] * weights_ABC[J]
#    #            H1[J] = a
#    #            H2[J] = b
#    #        KE[i][j] = list(H1)
#    #        PE[i][j] = list(H2)
#    ##theta integrated out, now just numbers
#    #KE = asarray(KE)
#    #KE = list(KE.flatten())
#    #PE = asarray(PE)
#    #PE = list(PE.flatten())
#    #for K in range(len(V_vib)):
#    #    c = PE[K] * V_vib[K][0]
#    #    PE[K] = c
#    #integral += sum(KE)
#    #integral += sum(PE)
#    #print "integral", integral
#    return integral
#
#
##for i in range(10):
##    for j in range(i+1):
##        n=full_vib_basis_keys[i]
##        m=full_vib_basis_keys[j]
##        print "precalc", matrix_elem_Hvib(i,j)
##        print "onflycalc", matrix_element_Hvib(n,m)
##
##
#
#
#
#
#def Hvib_mat_elem(i,j):
#    bra=full_bra_list[i]
#    ket=full_ket_list[j]
#    return dot(bra,ket)
#
#print "calculating matrix elements"
#start=time.time()
#Hvib = zeros((len(full_vib_basis), len(full_vib_basis)))
#for i in range(len(full_vib_basis)):
#    for j in range(i + 1):
#        #Hvib[i][j] = matrix_elem_Hvib(full_vib_basis_keys[i], full_vib_basis_keys[j])
#        #Hvib[j][i] = Hvib[i][j]
#        Hvib[i][j] = Hvib_mat_elem(j,i)
#        Hvib[j][i] = Hvib[i][j]
##outfile.write("Hvib matrix", + str(Hvib))
#end=time.time()
#print end-start
#Hvibout=savetxt("Hvib.dat", Hvib)
#print "matrix generated"
#

# TODO is overlap matrix needed?

Hvib=loadtxt("Hvib.dat")
print "loaded Hvib"
Hvib_energies = []
Hvib_wavefunctions = []
Hvib_evals, Hvib_evecs = LA.eigh(Hvib)
print "matrix diagonalised"
#Hvib_energies.append(Hvib_evals)
Hvib_energies = [i * wavenumber for i in Hvib_evals]
Hvib_energies=list(Hvib_energies)
print "eigenvalues done"
print Hvib_energies[0:10]
print Hvib_energies[10:20]
print Hvib_energies[20:30]
##for i in range(len(full_vib_basis_values)):
#for i in range(30):
#    a = Hvib_evecs[:, i]
#    b= dot(a,full_vib_basis_values)
#    b=b.subs([(exp,1),(cos(theta),x),(sin(theta),y)])
#    c=lambdify([(p,q,x,y)],b,"numpy")
#    Hvib_wavefunctions.append(c)
outfile.write("H vib energies:" + str(Hvib_energies)+'\n')
##outfile.write("H vib eigenvectors:" + str(Hvib_evecs)+'\n')
#print "eigenfunctions created"
#full_wavefunctions=[]
#for i in range(30):
#    a=Hvib_wavefunctions[i]
#    values=[a(j) for j in full_points]
#    full_wavefunctions.append(values)
#
#wavefunctionout=savetxt("fullwavefunctionvalues.dat", full_wavefunctions)
full_wavefunction=loadtxt("fullwavefunctionvalues.dat")
# transition dipole moment formulae, evaluated at correct points
v,w=symbols('v w')

#mu_x=v*(p+q+R_ABe+R_BCe)
#mu_z=w*(p-q+R_ABe+R_BCe)
#mu_x_points=[(points_AB[i],points_BC[j],cos_half_points[k]) for i in range(len(points_AB)) for j in range(len(points_BC)) for k in range(len(points_ABC))]
#mu_z_points=[(points_AB[i],points_BC[j],sin_half_points[k]) for i in range(len(points_AB)) for j in range(len(points_BC)) for k in range(len(points_ABC))]
#trans_dipole_x=lambdify([(p,q,v)],mu_x,"numpy")
#trans_dipole_z=lambdify([(p,q,w)],mu_z,"numpy")
#transition_dipole_x=[trans_dipole_x(i) for i in mu_x_points]
#transition_dipole_z=[trans_dipole_z(i) for i in mu_z_points]
#
#
#def apply_mux(x):
#    a=full_wavefunction[x]
#    ans=[a[k]*transition_dipole_x[k]*full_weights[k] for k in range(len(a))]
#    return ans
#
#def apply_muz(x):
#    a=full_wavefunction[x]
#    ans=[a[k]*transition_dipole_z[k]*full_weights[k] for k in range(len(a))]
#    return ans
#print "applying dipolemoment"
#muxvalues=[]
#muzvalues=[]
#for i in range(30):
#    a=apply_mux(i)
#    b=apply_muz(i)
#    muxvalues.append(a)
#    muzvalues.append(b)
#
#mux=savetxt("mux.dat",muxvalues)
#muz=savetxt("muz.dat",muzvalues)
#

#mux=loadtxt("mux.dat")
#muz=loadtxt("muz.dat")
#
#
#def dipole_moment(i,j):
#    a=full_wavefunction[i]
#    b=mux[j]
#    c=muz[j]
#    xcomp=dot(a,b)
#    zcomp=dot(a,c)
#    scalar=xcomp**2+zcomp**2
#    return scalar
#
#
#dipolemoments={}
#print 'calculating dipole moments'
#for j in range(30):
#    for i in range(j):
#        print (i,j),dipole_moment(i,j)
#        dipolemoments[(i,j)]=dipole_moment(i,j)
#
#
#outfile.write("transition dipoles:" + str(dipolemoments)+'\n')
#
##list of functions
#full_wavefunctions=[]
#for i in range(len(Hvib_wavefunctions)):
#    a=Hvib_wavefunctions[i]
#    a=a.subs([(exp,1),(cos(theta),x),(sin(theta),y)])
#    b=lambdify([(p,q,x,y)],a,"numpy")
#    full_wavefunctions.append(b)
#
#full_wavefunction_values=[[full_wavefunctions[i](j) for j in full_points] for i in range(len(full_wavefunctions)]]

#for i in range(len(points_AB)):
#    for j in range(len(points_BC)):
#        for k in range(len(points_ABC)):
#            a=(points_AB[i],points_BC[j],cos_half_points[k])
#            b=(points_AB[i],points_BC[j],sin_half_points[k])
#            mu_x_points.append(a)
#            mu_z_points.append(b)

#end = time.time()
#print end - start
#
#
#outfile.close()
