#!/bin/python
# File: core.spectra_calculation.main.py

from core.grapes_utils.main_utils import SuperPrint

from diatomic_h_vib import HVibDiatomic2 as HVib
from diatomic_perturbation_theory import DiatomicPerturbationTheory2 as PT
from diatomic_spectrum import DiatomicSpectrum2 as Spectrum

mol = "AB"

vib, rot, t, s, m, r = 1, 1, 1, 1, 1, 1
p, trans = [1], [(0, 1)]


class Run():
    """
    Class to run the full method on a diatomic
    Attributes (all user defined):
        GP_args: scale_factor, reduced_mass, equilbrium_bond_length, potential
        user_args: max_vib_level, max_rot_level, temperature, transitions, bond
        h_vib_args: E, psi
        PT_args: B, D
    """
    __metaclass__ = abc.ABCMeta

    @staticmethod
    def RegisterOptions(args):
        pass

    @staticmethod
    def ParseOptions(options):
        pass
    

    def __init__(self, args, GP_args):

        self.Moduleargs = args.spectra
        self.SP = SuperPrint('Spectra.log')

        self._reduced_mass = GP_args.mass ##whatever
        self.GP_args = GP_args


    @property
    def reduced_mass(self):
        return self._reduced_mass


    @abc.abstractmethod
    def Run(self):
        #print('hey whatsup')
        self.SP('\they whatsup') #file + terminal
        self.SP('\they whatsup', stdout=False) #file only
        pass


    @abc.abstractmethod
    def ProduceData():
        ##print files

    @abc.abstracmethod
    def PlotSpectrum(...):
        pass
