#!/bin/python
# File: core.spectra_calculation.diatomic_perturbation_theory.py

from sympy import exp, symbols, lambdify, pi
from sympy.integrals.quadrature import gauss_hermite
from numpy import asarray


class DiatomicPerturbationTheory:
    """
    Class that evaluates first and second order perturbations to zeroth order vibrational solution.

    Attributes (Atomic Units):
        equilibrium bond length: float (from GP)
        scale factor: float (calculated from GP)
        reduced mass: float (calculated from GP)
        max vib level: int (user defined)
        energies: list (calculated from Hvibdiatomic)
        eigenfunctions: list (calculated from Hvibdiatomic)

    Methods to run:
        run_perturbation_theory
    """

    def __init__(self, equilibrium_bond_length, scale_factor, reduced_mass, energies, efuncs, max_vib_level, molecule):
        # TODO change to tuples
        self.equilibrium_bond_length = equilibrium_bond_length
        self.scale_factor = scale_factor
        self.reduced_mass = reduced_mass
        self.max_vib_level = max_vib_level
        self.efuncs = efuncs
        self.points, self.weights = gauss_hermite(2 * self.max_vib_level, 10)
        self.points = asarray(self.points, dtype=float)
        self.energies = energies
        self.molecule = molecule

    def rotational_constant(self, v):
        """
        Calculates the rotational constant for vth level Bv=<v|H(1)|v> where H1=1/2mur2
        by expanding 1/r2 as a Taylor series around r_eq
        :param v: int, quantum number
        :return: B_v as a float
        """
        q = symbols('q')
        operator = 0
        for i in range(10):
            t = (((-q * self.scale_factor / self.equilibrium_bond_length) ** i) * (i + 1))
            operator += t
        operator = lambdify(q, operator, "numpy")
        operator = list(operator(self.points))
        wavefunction = self.efuncs[v] ** 2 / exp(-q ** 2)
        wavefunction = lambdify(q, wavefunction, "numpy")
        wavefunction = list(wavefunction(self.points))
        terms = [self.weights[i] * operator[i] * wavefunction[i] for i in range(len(self.weights))]
        integral = sum(terms)
        B_v = integral / (2 * self.reduced_mass * self.equilibrium_bond_length ** 2)
        return B_v

    def second_order_correction(self, w, v):
        """
        Calculates one term in the summation for the Rayleigh-Schrodinger PT E(2) |<w|H1|v>|^2/(E_w-E_v)
        :param w: int, quantum number
        :param v: int, quantum number
        :return: float
        """
        q = symbols('q')
        bra = self.efuncs[w]
        ket = self.efuncs[v]
        operator = 0
        for i in range(10):
            t = (((-q * self.scale_factor / self.equilibrium_bond_length) ** i) * (i + 1))
            operator += t
        operator = lambdify(q, operator, "numpy")
        operator = list(operator(self.points))
        wavefunction = bra * ket / exp(-q ** 2)
        wavefunction = lambdify(q, wavefunction, "numpy")
        wavefunction = list(wavefunction(self.points))
        terms = [self.weights[i] * operator[i] * wavefunction[i] for i in range(len(self.weights))]
        integral = (sum(terms) / (2 * self.reduced_mass * self.equilibrium_bond_length ** 2)) ** 2
        D_wv = integral / (self.energies[w] - self.energies[v])
        return D_wv

    def second_order_energy(self, v):
        """
        Sums the second order terms w!=v to calculate the second order correction for the vth level
        :param v: int, quantum number
        :return: D_v, float
        """
        terms = [self.second_order_correction(w, v) for w in range(self.max_vib_level) if w != v]
        D_v = sum(terms) * -1
        return D_v

    def perturbation_constants(self):
        """
        Calculates B_v and D_v for all v
        :return: Lists of first and second order rotational constants
        """
        B = [self.rotational_constant(v) for v in range(self.max_vib_level)]
        D = [self.second_order_energy(v) for v in range(self.max_vib_level)]
        return B, D

    def run_diatomic_perturbation_theory(self):
        """
        Method to run whole class
        1) Calculates B, D
        2) Saves B, D in wavenumber to outfile
        :return: B, D
        """
        filename = self.molecule + "_pt_data.dat"
        outfile = open(filename, "w")
        wavenumber = 219475
        B, D = self.perturbation_constants()
        B_wav = [i * wavenumber for i in B]
        D_wav = [i * wavenumber for i in D]
        outfile.write(str(B_wav) + '\n' + str(D_wav) + '\n')
        outfile.close()
        return B, D


class DiatomicPerturbationTheory2:
    """
    Class that evaluates first and second order perturbations to zeroth order vibrational solution.

    Attributes (Atomic Units):
        GP_args: scale_factor, reduced_mass, equilbrium_bond_length, potential
        user_args: max_vib_level, max_rot_level, temperature, transitions, bond
        h_vib_args: E, psi
    Methods to run:
        run_perturbation_theory
    """

    def __init__(self, GP_args, user_args, h_vib_args):
        self.GP_args = GP_args
        self.user_args = user_args
        self.h_vib_args = h_vib_args

        self.points, self.weights = gauss_hermite(2 * self.user_args.max_vib_level, 9)
        self.points = asarray(self.points, dtype=float)

    def rotational_constant(self, v):
        """
        Calculates the rotational constant for vth level Bv=<v|H(1)|v> where H1=1/2mur2
        by expanding 1/r2 as a Taylor series around r_eq
        :param v: int, quantum number
        :return: B_v as a float
        """
        q = symbols('q')
        operator = 0
        for i in range(10):
            t = (((-q * self.GP_args.scale_factor / self.GP_args.equilibrium_bond_length) ** i) * (i + 1))
            operator += t
        operator = lambdify(q, operator, "numpy")
        operator = list(operator(self.points))
        wavefunction = self.h_vib_args.psi[v] ** 2 / exp(-q ** 2)
        wavefunction = lambdify(q, wavefunction, "numpy")
        wavefunction = list(wavefunction(self.points))
        terms = [self.weights[i] * operator[i] * wavefunction[i] for i in range(len(self.weights))]
        integral = sum(terms)
        B_v = integral / (2 * self.GP_args.reduced_mass * self.GP_args.equilibrium_bond_length ** 2)
        return B_v

    def second_order_correction(self, w, v):
        """
        Calculates one term in the summation for the Rayleigh-Schrodinger PT E(2) |<w|H1|v>|^2/(E_w-E_v)
        :param w: int, quantum number
        :param v: int, quantum number
        :return: float
        """
        q = symbols('q')
        bra = self.h_vib_args.psi[w]
        ket = self.h_vib_args.psi[v]
        operator = 0
        for i in range(10):
            t = (((-q * self.GP_args.scale_factor / self.GP_args.equilibrium_bond_length) ** i) * (i + 1))
            operator += t
        operator = lambdify(q, operator, "numpy")
        operator = list(operator(self.points))
        wavefunction = bra * ket / exp(-q ** 2)
        wavefunction = lambdify(q, wavefunction, "numpy")
        wavefunction = list(wavefunction(self.points))
        terms = [self.weights[i] * operator[i] * wavefunction[i] for i in range(len(self.weights))]
        integral = (sum(terms) / (2 * self.GP_args.reduced_mass * self.GP_args.equilibrium_bond_length ** 2)) ** 2
        D_wv = integral / (self.h_vib_args.E[w] - self.h_vib_args.E[v])
        return D_wv

    def second_order_energy(self, v):
        """
        Sums the second order terms w!=v to calculate the second order correction for the vth level
        :param v: int, quantum number
        :return: D_v, float
        """
        terms = [self.second_order_correction(w, v) for w in range(self.user_args.max_vib_level) if w != v]
        D_v = sum(terms) * -1
        return D_v

    def perturbation_constants(self):
        """
        Calculates B_v and D_v for all v
        :return: Lists of first and second order rotational constants
        """
        B = [self.rotational_constant(v) for v in range(self.user_args.max_vib_level)]
        D = [self.second_order_energy(v) for v in range(self.user_args.max_vib_level)]
        return B, D

    def run_diatomic_perturbation_theory(self):
        """
        Method to run whole class
        1) Calculates B, D
        2) Saves B, D in wavenumber to outfile
        :return: B, D
        """
        filename = self.user_args.bond + "_pt_data.dat"
        outfile = open(filename, "w")
        wavenumber = 219475
        self.B, self.D = self.perturbation_constants()
        B_wav = [i * wavenumber for i in self.B]
        D_wav = [i * wavenumber for i in self.D]
        outfile.write(str(B_wav) + '\n' + str(D_wav) + '\n')
        outfile.close()
        return self.B, self.D
