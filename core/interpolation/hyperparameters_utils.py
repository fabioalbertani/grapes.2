#!/bin/python
#File: core.interpolation.hyperparameters_utils.py

from numpy import ones, log, exp, zeros, identity, matmul
from numpy.random import shuffle
from numpy.linalg import eig
from copy import copy

from core.geometry.Utils import OptimiseGeometry, GetHessian
from core.geometry.molecule import MoleculeManager
from core.grapes_utils.progress_bar import DisplayBar
from core.qchemistry_modules.qchem_run_interface import QCRunInterface as RunModule


def getHP(kernel, HP, log_transformed=False):
    '''
    Return a specfic hyperparameter
    '''
    if HP=='constant_value':
        theta = getattr(getattr(kernel, 'k1'), 'k1').theta
    elif HP=='length_scale':
        theta = getattr(getattr(kernel, 'k1'), 'k2').theta
    elif HP=='noise_level':
        theta = getattr(kernel, 'k2').theta
    else:
        return None

    if log_transformed:
        return theta
    else:
        return exp(theta)



def setHP(kernel, HP, val, idx=None):
    '''
    Sets a specific hyperparameter (or set of hyperparameters)
    '''
    theta = kernel.theta
    if HP=='constant_value':
        theta[0] = log(val)
    elif HP=='length_scale':
        if idx is None:
            theta[1:-1] = log(val)
        else:
            theta[1+idx] = log(val)
    elif HP=='noise_level':
        theta[-1] = log(val)

    kernel_ = kernel.clone_with_theta(theta)
    return kernel_





def SubHPOptimiser(Q, E, GPM, NMax):
    '''
    Performs the a Gaussian Regression on a subset of the given points and returns the 
    optimised hyperparamters

    All the parameters for the GP are set in the KernelOrganiser module and the GP module will be
    recreated properly (without optimisation) in the GPI module
    '''
    print('\n\tFinding sub-optimal hyperparameters using '+str(NMax)+' points out of '+\
          str(Q.shape[0])+'\n')

    idx = range(Q.shape[0])
    shuffle(idx)
    idx = idx[:NMax]

    SubQ = Q[idx,:]
    SubE = E[idx]

    SubGP = GPM.fit(SubQ, SubE)

    return {
        'k1': getHP(SubGP.kernel_, 'constant_value'),
        'k2': getHP(SubGP.kernel_, 'length_scale'),
        'k3': getHP(SubGP.kernel_, 'noise_level')
           }







def ApproximateHP_direct(args, CalcDetails):

    XYZ_opt = OptimiseGeometry(args, CalcDetails)
    if XYZ_opt is None:
        raise RuntimeError('Geometry optimisation failed: check /scratch/$USER/'+args.Label+\
                           '/outputOPT\n')

    Hess = GetHessian(args, CalcDetails, XYZ_in=XYZ_opt)
    if Hess is None:
        raise RuntimeError('Hessian calculation failed: check /scratch/$USER/'+args.Label+\
                           '/outputFREQ\n')

    #make hess(xyz) -> hess(q)
    Molecule = MoleculeManager(args)
    P = zeros((args.D, 3*len(args.AtomsList)))

    for q in range(args.D):
        P[q,q] = 1.0
        #print 'Q vector: ', P[q,:args.D].reshape(1,args.D)
        P[q,:] = Molecule.q2cart(P[q,:args.D].reshape(1,args.D)).reshape(3*len(args.AtomsList))
        #print 'xyz vector: ', P[q,:]


    Hess = matmul(P, matmul(Hess, P.T))
    #print 'Hess\n', Hess

    sigma_, sigma_v = eig(Hess)
    #print 'q HP: ', sigma_
    sigma_ = [0.1 for x in sigma_]

    return {
        'k1': 1.0, #should be optimised
        'k2': sigma_,
        'k3': 0.00016
           }



def ApproximateHP(args, CalcDetails, dq=1e-4):

    XYZ_opt = OptimiseGeometry(args.main, CalcDetails)
    if XYZ_opt is None:
        raise RuntimeError('Geometry optimisation failed: check /scratch/$USER/'+\
                           args.main.Label+'/outputOPT\n')

    #make hess(xyz) -> hess(q)
    Molecule = MoleculeManager(args.main)
    RunInterface = RunModule(args, CalcDetails)
    Q_opt = Molecule.cart2q(XYZ_opt.reshape(len(args.main.AtomsList),1,3))

    print('Calculating Hessian matrix element wise within Q-representation')
    Hess = zeros((args.main.D, args.main.D))
    vec = zeros(args.main.D)
    PB = DisplayBar(args.main.D*args.main.D)

    for d1 in range(args.main.D):
        for d2 in range(args.main.D):
            shift = copy(vec)
            shift[[d1,d2]] = [dq, dq]
            Hess[d1,d2] = RunInterface.SingleEnergy(Molecule.q2cart(Q_opt+shift).reshape(len(\
                          args.main.AtomsList),3)) + RunInterface.SingleEnergy(\
                          Molecule.q2cart(Q_opt-shift).reshape(len(args.main.AtomsList),3))
            shift = copy(vec)
            shift[[d1,d2]] = [dq,-dq]
            Hess[d1,d2] -= RunInterface.SingleEnergy(Molecule.q2cart(Q_opt+shift).reshape(len(\
                           args.main.AtomsList),3)) + RunInterface.SingleEnergy(\
                           Molecule.q2cart(Q_opt-shift).reshape(len(args.main.AtomsList),3))
            PB.Show(d1*args.main.D+d2)

    Hess *= dq**(-2) #normalise hessian matrix

    sigma_, sigma_v = eig(Hess)
    print '\nq HP: \n', sigma_
    sigma_ = [max(0.01,abs(1/x)) for x in sigma_]
    print '\ntreated sigma_\n', sigma_

    return {
        'k1': 1.0, #should be optimised
        'k2': sigma_,
        'k3': 0.00016
           }
