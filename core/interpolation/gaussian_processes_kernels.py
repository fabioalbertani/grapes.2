#!/bin/python
#File: core.interpolation.gaussian_processes_kernels.py


import numpy as np
from abc import ABCMeta, abstractmethod
from collections import namedtuple
import math

from scipy.special import kv, gamma
from scipy.spatial.distance import pdist, cdist, squareform
#this allows to copy the needed classes 
from sklearn.gaussian_process import kernels



def _check_length_scale(X, length_scale):
    length_scale = np.squeeze(length_scale).astype(float)
    if np.ndim(length_scale) > 1:
        raise ValueError("length_scale cannot be of dimension greater than 1")
    if np.ndim(length_scale) == 1 and X.shape[1] != length_scale.shape[0]:
        raise ValueError("Anisotropic kernel must have the same number of "
                         "dimensions as data (%d!=%d)"
                         % (length_scale.shape[0], X.shape[1]))
    return length_scale





class Kernel(kernels.Kernel):
    '''
    Redefines the kernel class to allow the gradient call 
    of compound kernels 
    '''
    def __add__(self, b):
        if not isinstance(b, Kernel):
            return Sum(self, ConstantKernel(b))
        return Sum(self, b)

    def __radd__(self, b):
        if not isinstance(b, Kernel):
            return Sum(ConstantKernel(b), self)
        return Sum(b, self)

    def __mul__(self, b):
        if not isinstance(b, Kernel):
            return Product(self, ConstantKernel(b))
        return Product(self, b)

    def __rmul__(self, b):
        if not isinstance(b, Kernel):
            return Product(ConstantKernel(b), self)
        return Product(b, self)





class KernelOperator(Kernel):
    """Base class for all kernel operators.

    .. versionadded:: 0.18
    """

    def __init__(self, k1, k2):
        self.k1 = k1
        self.k2 = k2
        self._analytical_gradient = bool(self.k1.analytical_gradient and\
                                         self.k2.analytical_gradient)


    def get_params(self, deep=True):
        """Get parameters of this kernel.

        Parameters
        ----------
        deep : boolean, optional
            If True, will return the parameters for this estimator and
            contained subobjects that are estimators.

        Returns
        -------
        params : mapping of string to any
            Parameter names mapped to their values.
        """
        params = dict(k1=self.k1, k2=self.k2)
        if deep:
            deep_items = self.k1.get_params().items()
            params.update(('k1__' + k, val) for k, val in deep_items)
            deep_items = self.k2.get_params().items()
            params.update(('k2__' + k, val) for k, val in deep_items)

        return params

    @property
    def analytical_gradient(self):
        return self._analytical_gradient

    @property
    def hyperparameters(self):
        """Returns a list of all hyperparameter."""
        r = []
        for hyperparameter in self.k1.hyperparameters:
            r.append(kernels.Hyperparameter("k1__" + hyperparameter.name,
                                    hyperparameter.value_type,
                                    hyperparameter.bounds,
                                    hyperparameter.n_elements))
        for hyperparameter in self.k2.hyperparameters:
            r.append(kernels.Hyperparameter("k2__" + hyperparameter.name,
                                    hyperparameter.value_type,
                                    hyperparameter.bounds,
                                    hyperparameter.n_elements))
        return r

    @property
    def theta(self):
        """Returns the (flattened, log-transformed) non-fixed hyperparameters.

        Note that theta are typically the log-transformed values of the
        kernel's hyperparameters as this representation of the search space
        is more amenable for hyperparameter search, as hyperparameters like
        length-scales naturally live on a log-scale.

        Returns
        -------
        theta : array, shape (n_dims,)
            The non-fixed, log-transformed hyperparameters of the kernel
        """
        return np.append(self.k1.theta, self.k2.theta)

    @theta.setter
    def theta(self, theta):
        """Sets the (flattened, log-transformed) non-fixed hyperparameters.

        Parameters
        ----------
        theta : array, shape (n_dims,)
            The non-fixed, log-transformed hyperparameters of the kernel
        """
        k1_dims = self.k1.n_dims
        self.k1.theta = theta[:k1_dims]
        self.k2.theta = theta[k1_dims:]


    @property
    def bounds(self):
        """Returns the log-transformed bounds on the theta.

        Returns
        -------
        bounds : array, shape (n_dims, 2)
            The log-transformed bounds on the kernel's hyperparame
        """
        if self.k1.bounds.size == 0:
            return self.k2.bounds
        if self.k2.bounds.size == 0:
            return self.k1.bounds
        return np.vstack((self.k1.bounds, self.k2.bounds))

    def __eq__(self, b):
        if type(self) != type(b):
            return False
        return (self.k1 == b.k1 and self.k2 == b.k2) \
            or (self.k1 == b.k2 and self.k2 == b.k1)

    def is_stationary(self):
        """Returns whether the kernel is stationary. """
        return self.k1.is_stationary() and self.k2.is_stationary()






class Sum(KernelOperator):
    '''
    The full documentation can be seen on sklearn.gaussian_process.kernels
    this class mirrors the functions adding the gradient call for compound
    kernels summed together
    '''

    def __call__(self, X, Y=None, eval_gradient=False):
        if eval_gradient:
            K1, K1_gradient = self.k1(X, Y, eval_gradient=True)
            K2, K2_gradient = self.k2(X, Y, eval_gradient=True)
            return K1 + K2, np.dstack((K1_gradient, K2_gradient))
        else:
            return self.k1(X, Y) + self.k2(X, Y)

    def diag(self, X):
        return self.k1.diag(X) + self.k2.diag(X)

    def __repr__(self):
        return "{0} + {1}".format(self.k1, self.k2)

    def gradient(self, X, Y=None, dim=None):
        '''
        Returns the gradient of a sum of Kernels along a specific dimension
        '''
        return self.k1.gradient(X,Y,dim) + self.k2.gradient(X,Y,dim)


class Product(KernelOperator):
    '''
    The full documentation can be seen on sklearn.gaussian_process.kernels
    this class mirrors the functions adding the gradient call for compound
    kernels multiplied togetehr
    '''

    def __call__(self, X, Y=None, eval_gradient=False):
        if eval_gradient:
            K1, K1_gradient = self.k1(X, Y, eval_gradient=True)
            K2, K2_gradient = self.k2(X, Y, eval_gradient=True)
            return K1 * K2, np.dstack((K1_gradient * K2[:, :, np.newaxis],
                                       K2_gradient * K1[:, :, np.newaxis]))
        else:
            return self.k1(X, Y) * self.k2(X, Y)

    def diag(self, X):
        return self.k1.diag(X) * self.k2.diag(X)

    def __repr__(self):
        return "{0} * {1}".format(self.k1, self.k2)

    def gradient(self, X, Y=None, dim=None):
        '''
        Returns the gradient of a product of Kernels along a specific dimension
        '''
        return self.k1.gradient(X,Y,dim) * self.k2.gradient(X,Y,dim)




class ConstantKernel(kernels.StationaryKernelMixin, Kernel):
    '''
    Constant kernel i.e. amplitude kernel
    full documentation available on sklearn.gaussian_process.kernels
    '''
    def __init__(self, constant_value=1.0, constant_value_bounds=(1e-5, 1e5)):
        self.constant_value = constant_value
        self.constant_value_bounds = constant_value_bounds
        self._analytical_gradient = True

    @property
    def analytical_gradient(self):
        return self._analytical_gradient

    @property
    def hyperparameter_constant_value(self):
        return kernels.Hyperparameter(
            "constant_value", "numeric", self.constant_value_bounds)

    def __call__(self, X, Y=None, eval_gradient=False):
        X = np.atleast_2d(X)
        if Y is None:
            Y = X
        elif eval_gradient:
            raise ValueError("Gradient can only be evaluated when Y is None.")

        K = self.constant_value * np.ones((X.shape[0], Y.shape[0]))
        if eval_gradient:
            if not self.hyperparameter_constant_value.fixed:
                return (K, self.constant_value
                        * np.ones((X.shape[0], X.shape[0], 1)))
            else:
                return K, np.empty((X.shape[0], X.shape[0], 0))
        else:
            return K

    def diag(self, X):
        return self.constant_value * np.ones(X.shape[0])

    def __repr__(self):
        return "%.3g**2" %np.sqrt(self.constant_value)

    def gradient(self, X, Y=None, dim=None):
        '''
        The gradient is not dependant on the amplitude i.e. the same
        as the non differentiated kernel is returned
        '''
        X = np.atleast_2d(X)
        return self(X, Y)




class RadialBasisFunction(kernels.StationaryKernelMixin, kernels.NormalizedKernelMixin, 
                          Kernel):
    '''
    Radial Basis Functions
    full documentation on sklearn.gaussian_process.kernels
    '''
    def __init__(self, length_scale=1.0, length_scale_bounds=(1e-5, 1e5)):
        self.length_scale = length_scale
        self.length_scale_bounds = length_scale_bounds
        self._analytical_gradient = True

    @property
    def analytical_gradient(self):
        return self._analytical_gradient

    @property
    def anisotropic(self):
        return np.iterable(self.length_scale) and len(self.length_scale) > 1

    @property
    def hyperparameter_length_scale(self):
        if self.anisotropic:
            return kernels.Hyperparameter("length_scale", "numeric",
                                  self.length_scale_bounds,
                                  len(self.length_scale))
        return kernels.Hyperparameter(
            "length_scale", "numeric", self.length_scale_bounds)

    def __call__(self, X, Y=None, eval_gradient=False):
        X = np.atleast_2d(X)
        length_scale = _check_length_scale(X, self.length_scale)
        if Y is None:
            dists = pdist(X / length_scale, metric='sqeuclidean')
            K = np.exp(-.5 * dists)
            # convert from upper-triangular matrix to square matrix
            K = squareform(K)
            np.fill_diagonal(K, 1)
        else:
            if eval_gradient:
                raise ValueError(
                    "Gradient can only be evaluated when Y is None.")
            dists = cdist(X / length_scale, Y / length_scale,
                          metric='sqeuclidean')
            K = np.exp(-.5 * dists)

        if eval_gradient:
            if self.hyperparameter_length_scale.fixed:
                # kernels.Hyperparameter l kept fixed
                return K, np.empty((X.shape[0], X.shape[0], 0))
            elif not self.anisotropic or length_scale.shape[0] == 1:
                K_gradient = \
                    (K * squareform(dists))[:, :, np.newaxis]
                return K, K_gradient
            elif self.anisotropic:
                # We need to recompute the pairwise dimension-wise distances
                K_gradient = (X[:, np.newaxis, :] - X[np.newaxis, :, :]) ** 2 \
                    / (length_scale ** 2)
                K_gradient *= K[..., np.newaxis]
                return K, K_gradient
        else:
            return K

    def __repr__(self):
        if self.anisotropic:
            return "%s (length_scale=[%s])" %(
                self.__class__.__name__, ", ".join(["%.3g" %l for l in  self.length_scale]))
        else:  # isotropic
            return "%s (length_scale={%.3g})" %(
                self.__class__.__name__, np.ravel(self.length_scale)[0])


    def gradient(self, X, Y=None, dim=None):
        
        X = np.atleast_2d(X)
        if dim>X.shape[1]:
            raise ValueError('Cannot calcualte gradient along the '+\
                  str(dim)+' dimension for a '+str(X.shape[1])+' dimensional'\
                  'training set')

        length_scale = _check_length_scale(X, self.length_scale)
        if Y is None:
            dists = pdist(X / length_scale, metric='sqeuclidean')
            K = np.exp(-.5 * dists)
            # convert from upper-triangular matrix to square matrix
            K = squareform(K)
            np.fill_diagonal(K, 1)
        else:
            dists = cdist(X / length_scale, Y / length_scale,
                          metric='sqeuclidean')
            K = np.exp(-.5 * dists)
            K_grad = np.zeros_like(K)
            for sample in range(Y.shape[0]): 
                K_grad[:,sample] = X[:,dim] - Y[sample,dim]
            K *= K_grad

        return K



class Matern(kernels.Matern):
    '''
    Place holder for potential Matern definition
    '''
    def __init__(self, **kwargs):
        self._analytical_gradient = False
        super(Matern, self).__init__(kwargs)



class WhiteKernel(kernels.StationaryKernelMixin, Kernel):
    '''
    White noise kernel
    full documentation on sklearn.gaussian_process.kernels
    '''
    def __init__(self, noise_level=1.0, noise_level_bounds=(1e-5, 1e5)):
        self.noise_level = noise_level
        self.noise_level_bounds = noise_level_bounds
        self._analytical_gradient = True

    @property
    def analytical_gradient(self):
        return self._analytical_gradient

    @property
    def hyperparameter_noise_level(self):
        return kernels.Hyperparameter(
            "noise_level", "numeric", self.noise_level_bounds)

    def __call__(self, X, Y=None, eval_gradient=False):
        X = np.atleast_2d(X)
        if Y is not None and eval_gradient:
            raise ValueError("Gradient can only be evaluated when Y is None.")

        if Y is None:
            K = self.noise_level * np.eye(X.shape[0])
            if eval_gradient:
                if not self.hyperparameter_noise_level.fixed:
                    return (K, self.noise_level
                            * np.eye(X.shape[0])[:, :, np.newaxis])
                else:
                    return K, np.empty((X.shape[0], X.shape[0], 0))
            else:
                return K
        else:
            return np.zeros((X.shape[0], Y.shape[0]))

    def diag(self, X):
        return self.noise_level * np.ones(X.shape[0])

    def __repr__(self):
        return "%s (noise_level={%.3g})" %(self.__class__.__name__,
                                                 self.noise_level)

    def gradient(self, X, Y=None, dim=None):
        '''
        The noise signal is not dependant on the cooridnates technically 
        the real differentiation should give d K_noise(q) / dq = 0.0 
        '''
        X = np.atleast_2d(X)
        return self(X, Y)



