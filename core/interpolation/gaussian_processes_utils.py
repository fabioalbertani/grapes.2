#!/bin/python
#File: core.interpolation.gaussian_processes_utils.py

from numpy import ones, log, exp, zeros, identity, matmul
from numpy.random import shuffle
from numpy.linalg import eig
from copy import copy

from core.interpolation import gaussian_processes_kernels as kernels
from core.interpolation.hyperparameters_utils import getHP, setHP
from core.interpolation.gaussian_processes_regression import GaussianProcessRegressor as GPR

from core.geometry.Utils import OptimiseGeometry, GetHessian
from core.geometry.molecule import MoleculeManager
from core.grapes_utils.progress_bar import DisplayBar
from core.qchemistry_modules.qchem_run_interface import QCRunInterface as RunModule


class KernelOrganiser:
    '''
    Interface to set up different Kernels that might be useful for kernel interpolation

    For now mostly works with RBFs and White noise but would eventually include more options
    '''

    def __init__(self, args, _direct=False):

        self.Moduleargs = args.interpolation


    def SetKernel(self, D, hyperDict, anisotropic=True, n_restart_scale=10, 
                  optimiseHP=2):
        '''
        Sets a specific kernel for initiating a GP as well as the optimisation parameters 
        (see sklearn modules for a complete list)

        default: anisotropic RBF
        default bounds: 0.1 - 10.0 (chemical intuition?)
        nb the amplitude bounds (i.e. enegry) are large to account for different units

        optimise: 0 (off), 1 (single search), 2 (full search scaled to system dimensionality)
        '''
        bounds = {'k1': (1e-4, 1e4), 'k2': (1e-2,1e2), 'k3': (1e-10, 1e-8)}

        if optimiseHP==2:
            if anisotropic:
                hyperDict = {'k1': self.Moduleargs.cFit,
                             'k2': ones(D)*self.Moduleargs.sigmaFit,
                             'k3': self.Moduleargs.lambdaFit}
            else:
                hyperDict = {'k1': self.Moduleargs.cFit,
                             'k2': self.Moduleargs.sigmaFit,
                             'k3': self.Moduleargs.lambdaFit}

        elif hyperDict is None:
            raise RuntimeError('No dictionaries of hyperparameters was supplied --'\
                               'no lml maximisation will not be fruitful\n')


        if self.Moduleargs.CovarianceFunction == 'RBF':
            K = kernels.ConstantKernel(hyperDict['k1'], bounds['k1'])*\
                kernels.RadialBasisFunction(hyperDict['k2'], length_scale_bounds=bounds['k2'])

        elif self.Moduleargs.CovarianceFunction == 'Matern':
            K = kernels.ConstantKernel(hyperDict['k1'], bounds['k1'])*\
                kernels.Matern(hyperDict['k2'], nu=self.Moduleargs.nuFit, 
                    length_scale_bounds=bounds['k2'])

        else:
            raise Exception('Covariance function not known')


        try:
            self.NHyperparameters = 1 + len(hyperDict['k2'])
        except TypeError:
            self.NHyperparameters = 2


        if self.Moduleargs.NoiseFunction == 'White':
            K += kernels.WhiteKernel(hyperDict['k3'], noise_level_bounds=bounds['k3'])
            self.NHyperparameters += 1


        if not optimiseHP:
            return GPR(K, alpha=0.0, normalize_y=True, optimise_hyperparameters=False, 
                       random_state=0)

        else:
            '''lml will be maximised'''
            return GPR(K, alpha=0.0, normalize_y=True, n_restarts_optimizer=\
                       min(n_restart_scale*D,100) if optimiseHP==2 else 1, random_state=None)





class KeyOrganiser:
    '''
    Organises different Gaussian processes with 'keys' in a master dictionary
    '''

    def __init__(self, args=None):

        self.Types = {}
        self.Info = {}

        self.GAPi = 1
        self.GAPn = 0


    def SetKey(self, CalcDetails, el, Bi=None, Error=False):
        '''
        Example key:
        for an hf calculation with a given basis set on a indexed element (el) s as a difference
        one would find: key basisset1: 'ela0' and key basisset2: 'ela1'

        the GP to correctly predict the hf at basisset2 is GP('ela0') + GP('ela1')

        More complex: ccsd for basisset2 as a difference to hf (for which basisset2 is also
        a difference)

        the GP to correctly predict ccsd(basisset2) is GP('ela0') + GP('ela1') + GP('elb2')

        each GP is given has a letter (energy, dipole, gradient ...) and a number (basis set, ...)
        '''
        if not CalcDetails['label'] in self.Types.keys():
            self.Types[CalcDetails['label']] = chr(ord('a') + self.GAPn)
            self.GAPn += 1

        if not CalcDetails['label'] in self.Info.keys():
            self.Info[CalcDetails['label']] = [el]
            self.GAPi += 1

        else:
            if not el in self.Info[CalcDetails['label']]:
                self.Info[CalcDetails['label']].append(el)
                self.GAPi += 1


        key = 'el:'+ el + '_' + self.Types[CalcDetails['label']] + str(self.GAPi)
        self.Info[CalcDetails['label']+el] = key


        if Bi is None:
            self.Info[key+'bagged'] = False
        else:
            self.Info[key+'bagged'] = True
            if not key+'bagged_count' in self.Info.keys():
                self.Info[key+'bagged_count'] = 1
            else:
                self.Info[key+'bagged_count'] += 1


        if CalcDetails['Difference']:
            self.Info[key+'deltaType'] = True
            self.Info[key+'deltaTypelabel'] = CalcDetails['Difference']
        else:
            self.Info[key+'deltaType'] = False

        return key if Bi is None else 'b'+str(Bi)+key



    def GetSingleKey(self, CalcDetails, el, bi=None):
        '''
        Returns relevant keys for a specific calculation

        is used internally to build dictionaries of keys needed to predict a single surface
        '''
        try:
            key = self.Info[CalcDetails['label']+el]
        except KeyError:
            print(CalcDetails['label']+el+' not found in '+' '.join(self.Info.keys())+'\n')

        if self.Info[key+'bagged']:
            if bi is None:
                return ['b'+str(bi)+key for bi in range(self.Info[key+'bagged_count'])]
            else:
                return 'b'+str(bi)+key
        else:
            return key


    def GetKey(self, CalcDetails, el, main=True):
        '''
        Function returning all the keys (as a list) needed for the prediction of a specific
        distribution (including the energy differences and so on)

        each element is a key or list of keys (for bagged GPs)
        '''
        keys = []
        key = self.Info[CalcDetails['label']+el]

        if self.Info[key+'bagged']:
            '''
            For now bagged ML should not be used with differences
            '''
            keys.append(['b'+str(Bi)+key for Bi in range(self.Info[key+'bagged_count'])])

        else:
            keys.append(key)

        if self.Info[key+'deltaType']:
            if isinstance(self.GetKey(self.Info[key+'deltaTypelabel'], el, main=False), list):
                for key in self.GetKey(self.Info[key+'deltaTypelabel'], el, main=False):
                    keys.append(key)
            else:
                keys.append(self.GetKey(self.Info[key+'deltaTypelabel'], el, main=False))

        if main or isinstance(keys, list):
            return keys
        else:
            return keys[0]


