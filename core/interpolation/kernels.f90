module kernels

implicit none

!all the possible hyperparameters should be included here and only 
!the useful ones will be initialised by specific subroutines
real:: amplitude, noise
real, dimension(:), allocatable:: length_scale

private:: amplitude, noise, length_scale


type kernel_type
    procedure(kernel_load), nopass, pointer:: hyperparameters_load => null()
    procedure(kernel_cov), nopass, pointer:: cov => null()

    contains
        procedure, nopass:: assign_kernel
end type kernel_type


interface
subroutine kernel_load(read_unit, d)
import kernel_type
integer:: read_unit, d
end subroutine
end interface

interface
function kernel_cov(q_training, q_test)
import kernel_type
real, intent(in), dimension(:,:):: q_training, q_test 
real, dimension(:,:), allocatable:: kernel_cov
end function
end interface



type(kernel_type):: kernel

!all the "true" functions are private and only the kernel pointer is used
private:: kernel_type, kernel_load, kernel_cov
private:: RBF_load, RBF_cov

contains

subroutine assign_kernel(kernel_name_)
    character(255), intent(in):: kernel_name_

    select case (kernel_name_)
        case('RBF')
            kernel%hyperparameters_load => RBF_load
            kernel%cov => RBF_cov
    
    end select
end subroutine assign_kernel


subroutine RBF_load(read_unit, d)
    integer, intent(in):: d, read_unit 
    character(255):: hyp_name, hyp_value
    integer:: ios, i
    integer:: done=0
    logical:: hyp_section=.TRUE.

    allocate(length_scale(d))

    do while (hyp_section)
        read(read_unit, iostat=ios) hyp_name, hyp_value
        if (hyp_name=='noise') then 
            read(hyp_value, *) noise
            done = done + 1
        else if (hyp_name=='amplitude') then
            read(hyp_value, *) amplitude
            done = done + 1
        else if (hyp_name=='length_scale') then
            do i=1,size(length_scale)
                read(hyp_value, *) length_scale(i)
                read(read_unit, iostat=ios) hyp_value
            enddo
            done = done + 1
        endif
        if (done>2) hyp_section=.FALSE.
    enddo

end subroutine RBF_load


function RBF_cov(q_training, q_test)
    real, intent(in), dimension(:,:):: q_training, q_test
    real, dimension(:,:), allocatable:: RBF_cov
    real:: expo
    integer:: i,j,d

    allocate(RBF_cov(size(q_test(:,0)),size(q_training(:,0))))

    do i=1,size(q_test(:,0))
        do j=1, size(q_training(:,0))
            do d=1, size(length_scale)
                expo = expo + (q_training(j,d) - q_test(i,d))**2/(2*length_scale(d))
            enddo
            RBF_cov(i,j) = amplitude*exp(-expo) + noise
        enddo
    enddo

end function RBF_cov


!Can potentially add Matern kernels and so on

end module kernels
