!include 'mkl_lapack.fi'

module gaussian_processes

!use mkl_lapack, only: DGETRF, DGETRI, DTPTRS
use kernels, only: kernel

implicit none 
include 'mkl_lapack.fi'

!Data arrays
real, dimension(:,:), allocatable:: X_train_
real, dimension(:,:), allocatable:: y_train_
real:: y_train_mean_
private:: X_train_, y_train_, y_train_mean_

!Gaussian process arrays
real, dimension(:), allocatable:: alpha_ 
real, dimension(:), allocatable:: L_
real, dimension(:,:), allocatable:: L_inv_
double precision, dimension(:,:), allocatable:: K_inv_
private:: alpha_, L_

contains

subroutine read_gaussian_process(filename_qdat, filename_gpdat, USE_LAPACK_DIRECT)
    !Reads in a qdat file and gpdat file (from supplied filename) to load 
    !a Gaussian process (called from subsequent subroutine)
    character(255), intent(in):: filename_qdat, filename_gpdat
    logical, intent(in), optional:: USE_LAPACK_DIRECT
    character(255):: line_value
    character(255):: kernel_name_ = 'RBF' !eventually get it from gpdat file 
    character(255), dimension(:), allocatable:: title_dump 
    integer:: ios1, ios2, i, info
    integer:: n_samples, d
    integer, dimension(:), allocatable:: ipiv
    double precision, dimension(:), allocatable:: work

    !check that both files exists and are not empty 
    open(UNIT=101, FILE=filename_qdat, action='read', iostat=ios1)
    if (ios1/=0) then
        stop 'Error: .qdat file not found'
    end if
    open(UNIT=102, FILE=filename_gpdat, action='read', iostat=ios2)
    if (ios2/=0) then
        stop 'Error: .gpdat file not found'
    end if

    do i=1,3
        !skips bags, hyperparameters and length scale info lines
        read(102, iostat=ios2) line_value
    enddo

    allocate(title_dump(4)) !getting 'Number of training points' string
    read(102, iostat=ios2) title_dump, line_value
    read(line_value, *) n_samples

    deallocate(title_dump)
    allocate(title_dump(3)) !getting 'Number of dimensions:' string
    read(102, iostat=ios2) title_dump, line_value
    read(line_value, *) d

    allocate(X_train_(n_samples,d), y_train_(n_samples,1))   
    allocate(alpha_(n_samples), K_inv_(n_samples, n_samples))
    allocate(L_(n_samples*(n_samples+1)/2))
    L_ = 0.0
    K_inv_ = 0.0

    call kernel%assign_kernel(kernel_name_)
    call kernel%hyperparameters_load(102, d)

    !needed before to allocate qdat properly 
    call read_gpdat_file(102, n_samples)
    call read_qdat_file(101, d)

    !loads K_inv_ which is needed to evaluate the variance of the prediction
    if (present(USE_LAPACK_DIRECT)) then
        !Get inverse covariance matrix from direct matrix inversion
        K_inv_ = kernel%cov(X_train_, X_train_) !this is a standard covariance matrix
        call DGETRF(n_samples, n_samples, K_inv_, &
                    n_samples, ipiv, info)
        call DGETRI(n_samples, K_inv_, n_samples, ipiv, work, &
                    -1, info)

    else
        !Get inverse covariance matrix from lower triangular Cheolesky decomposition
        allocate(L_inv_(n_samples,n_samples))
        L_inv_ = eyes(n_samples)
        call SPPSV('L', n_samples, n_samples, L_, L_inv_, &
                    n_samples, info) 
        K_inv_ = MATMUL(L_inv_, TRANSPOSE(L_inv_))

    endif

    close(101)
    close(102)
end subroutine read_gaussian_process





subroutine read_qdat_file(read_unit, d, key_use)
    !reading in of the training data (n_samples is taken from the gpdat file since it filtered out
    !potential data points where the energy was not converged and the data is absent 
    integer, intent(in):: read_unit, d
    character(255), optional:: key_use
    character(255):: title_line, data_use
    character(255), dimension(:), allocatable:: coordinates, data_skip_1,&
                                                data_skip_2
    integer:: ios, pre_values, post_values, sample_read

    if (.not. present(key_use)) key_use = 'E0'

    !Read training set data
    read(read_unit, iostat=ios) title_line
    do while (title_line/='--Nodes Information--')
        read(read_unit, iostat=ios) title_line
    enddo
    read(read_unit, iostat=ios) title_line
    post_values = index(title_line, 'p-gen') - index(title_line, key_use)
    pre_values = index(title_line, 'p-gen') - index(title_line, key_use)&
                 -2 -d !skips the integer and the wanted data (as well as coordinates)
    
    allocate(coordinates(d), data_skip_1(pre_values), data_skip_2(post_values))
    sample_read = 1

    read(read_unit, iostat=ios) title_line !skips the first blank line
    do while (ios==0) 
        !here title_line is a dump value for the index
        read(read_unit, iostat=ios) title_line, coordinates, data_skip_1, data_use, data_skip_2
        if (data_use/='nan') then
            read(coordinates,*) X_train_(sample_read,:)
            read(data_use,*)    y_train_(sample_read,1)
            sample_read = sample_read + 1
        endif
    enddo                                                      


end subroutine read_qdat_file



subroutine read_gpdat_file(read_unit, n_samples)
    !reading in of the gaussian process coefficient
    integer, intent(in):: read_unit, n_samples
    character(255):: coefficient, data_line
    character(255), dimension(:), allocatable:: coefficient_array
    integer:: ios, i

    read(read_unit, iostat=ios) data_line !skips dual coefficients title line

    do while (data_line/='Dual Coefficients:')
        read(read_unit, iostat=ios) coefficient
    enddo

    do i=1,n_samples
        read(read_unit, iostat=ios) coefficient
        read(coefficient, *) alpha_(i) 
    enddo

    read(read_unit, iostat=ios) coefficient !skips Cholesky title line
    do i=1,n_samples
        allocate(coefficient_array(i))
        read(read_unit, iostat=ios) coefficient_array
        read(coefficient_array,*) L_(i*(i-1)/0.5+1:i*(i+1)/0.5)
        deallocate(coefficient_array)
    enddo                                  

end subroutine read_gpdat_file




function predict_mean(q) result(y_predict_)
    real, intent(in), dimension(:,:):: q
    real, dimension(:,:), allocatable:: y_predict_
    real, dimension(:,:), allocatable:: K
    allocate(y_predict_(size(q(:,0)),1),&
             K(size(q(:,0)), size(X_train_(:,0))))

    K = kernel%cov(X_train_, q)
    y_predict_ = MATMUL(K, y_train_)
    y_predict_ = y_predict_ + y_train_mean_

end function predict_mean                       



function predict_variance(q) result(sigma_predict_)
    real, intent(in), dimension(:,:):: q
    real, dimension(:,:), allocatable:: sigma_predict_
    real, dimension(:,:), allocatable:: K, K_test_
    allocate(sigma_predict_(size(q(:,0)),1),&
             K(size(q(:,0)), size(X_train_(:,0))),&
             K_test_(size(q(:,0)), size(q(:,0))))

    !K = kernel%cov(X_train_, q)
    !K_test_ = kernel%cov(q, q)
    !sigma_predict_ = K_test_ + K*K_inv_*K

end function predict_variance                         



function eyes(n)
    integer, intent(in):: n
    real, dimension(:,:), allocatable:: eyes
    integer:: i
    allocate(eyes(n,n))
    eyes = 0.0

    do i=1,n
        eyes(i,i) = 1.0
    enddo
end function eyes

function eyes_vector(n)
    integer, intent(in):: n
    real, dimension(:), allocatable:: eyes_vector
    integer:: i
    allocate(eyes_vector(n))
    eyes_vector = 1.0
end function eyes_vector

end module gaussian_processes 
