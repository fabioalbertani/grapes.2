#!/bin/python
#File: Barycentric.py


#Interpolation:
#   -Barycentric interpolation is a form of NN extrapolation


#Details:
#   -arbitrary D is supported
#   -extrapolation is not supported
#   -the order (linear E = S: alpha(i)*f(i), quadratic E = S: alpha(i)*[f(i) + g(i)*(q-q(i))] ... )
#   -the amount of input is not limited (interpolation is local)



class Main:

    @classmethod
    def RegisterOptions(cls, args):
        m = args.RegisterModule('Interpolation')

        args.Register(m, '--type', type=str, default='Barycentric', help='allows to keep a trace of what has been done')
        args.Register(m, '--InterpolationOrder', type=str, default='Linear')


    def __init__(self, args=None):

        self.Moduleargs = args['Interpolation']
        self.Globalargs = args['Main']

        if self.Globalargs.UseSymmetry:

            if self.Globalargs.CoordinatesTransform=='lineartriatomic':
                from ..FORTRANModules.FORTRANGeometry import lineartriatomic
                self.qtransformsymmetry = lineartriatomic

            elif self.Globalargs.CoordinatesTransform=='h2ofixedangle':
                from ..FORTRANModules.FORTRANGeometry import h2ofixedangle
                self.qtransformsymmetry = h2ofixedangle

            elif self.Globalargs.CoordinatesTransform=='hcnplanar':
                from ..FORTRANModules.FORTRANGeometry import hcnplanar
                self.qtransformsymmetry = hcnplanar

            else:
                raise Exception('The system requested has no f90 module to treat symmetry associated') 





    def BuildInterpolation(self, Data, basis_set, Differences=False, basis_set_2=None):
        from ..DataManagement.Utils import WithoutNaN

        if Differences:
            self.InterpolationKeys[basis_set+'basis_setasdiff'] = True
            self.InterpolationKeys[basis_set+'diffto'] = basis_set_2
        else:
            self.InterpolationKeys[basis_set+'basis_setasdiff'] = False


        setattr(self, 'Data', Data)

        try:
            from numpy import any as npany
            if not npany(Data.gradient) and self.Moduleargs.InterpolationOrder=='Quadratic': 
                raise Exception('All quadratic terms are set to zeros: Interpolation cannot be quadratic')

        except NameError:
            print('\nInterpolation changed to linear since gradient is not defined\n')
            self.Moduleargs.InterpolationOrder = 'Linear'


    def BarycentricCoordinates(self, Q, Delaunay):
        m = Delaunay.find_simplex(Q)

        if m==-1:
            alpha = None

        else:
            from numpy import append

            alpha = Delaunay.transform[m,:self.Globalargs.D,:self.Globalargs.D].dot(Q-Delaunay.transform[m,self.Globalargs.D,:])
            alpha = append(alpha, 1-sum(alpha))
        
        return m, alpha
    



    def PointInterpolation(self, Q, basis_set, fill_value=0.0):
    #Since barycentric is an interpolation only scheme (no extrapolation) the fill value spans the energy outside the convex hull

        m, alpha = self.BarycentricCoordinates(Q, getattr(getattr(self, 'Data'), 'Delaunay'))

        if m==-1:
            return fill_value

        else:

            for state in xrange(self.Globalargs.NStates):

                if self.Moduleargs.InterpolationOrder=='Linear':
                    try:
                        E = sum(alpha*getattr(getattr(self, 'Data').energy, basis_set)[getattr(getattr(self, 'Data'), 'Delaunay').simplices[m], state])
                    except:
                        print(getattr(getattr(self, 'Data').energy, basis_set))
                        pass

                elif self.Moduleargs.InterpolationOrder=='Quadratic':

                    E = sum(alpha*getattr(getattr(self, 'Data').energy, basis_set)[getattr(getattr(self, basis_set), 'Delaunay').simplices[m], state])

                    for i in xrange(self.Globalargs.D+1):
                        E += sum(getattr(getattr(self, 'Data').gradient, basis_set)[self.Globalargs.D*getattr(getattr(self, 'Data'), 'Delaunay').simplices[m,i]:self.Globalargs.D*
                                 getattr(getattr(self, 'Data'), 'Delaunay').simplices[m,i]+1, state]*(Q-getattr(getattr(self, 'Data'), 'Delaunay').points[getattr(getattr(self, 'Data'), 'Delaunay').simplices[m,i]]))*alpha[i]
                else:
                    raise Exception('Interpolation order not taken into account')

            return E



    def Interpolate(self, QInput, basis_set):

        from numpy import zeros
        EnergyInterpolated = zeros((len(QInput[:,0]), self.Globalargs.NStates))        

        if self.Globalargs.UseSymmetry:
            from numpy import asfortranarray
            Q = asfortranarray(QInput)       
            self.qtransformsymmetry.constrain(Q, len(Q[:,0]))

        else:
            Q = QInput

        #Looping over states is done in the self.PointInterpolaton function
        if self.InterpolationKeys[basis_set+'basis_setasdiff']: 
            for i in xrange(len(Q[:,0])):
                EnergyInterpolated[i,:] = self.PointInterpolation(Q[i,:], self.InterpolationKeys[basis_set+'diffto'])
                EnergyInterpolateddiff[i,:] = self.PointInterpolation(Q[i,:], basis_set)

            EnergyInterpolated += EnergyInterpolateddiff

        else:
            for i in xrange(len(Q[:,0])):
                EnergyInterpolated[i,:] = self.PointInterpolation(Q[i,:], basis_set)


        from ..DataManagement.DataStructure import Process
    
        return Process(self.Globalargs.D, self.Globalargs.NStates, QInput, EnergyInterpolated, notrim=True)


