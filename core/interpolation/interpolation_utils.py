#!/bin/python
#File: core.interpolation.interpolation_utils.py

from numpy import ndarray

def CheckInterpolationData(Q, D):
    '''
    Check the consistency in data shapes
    if non-consistent data is found, the data is printed to help the
    user work out where it might have gone wrong
    '''
    if not isinstance(Q, ndarray):
        raise Exception('\n\tData not usable (not an instance of numpy.ndarray)'
                        '\n\tProblem with Q: Q type is '+type(Q)+'\n')
    if not isinstance(D, ndarray):
        raise Exception('\n\tData not usable (not an instance of numpy.ndarray)'
                        '\n\tProblem with D: D type is '+type(D)+'\n')

    if not Q.ndim==2 and D.ndim==1:
        raise Exception('\n\tData has not expected dimensions'
                        '\n\tQ dimensions: '+str(Q.ndim)+', D dimensions: '+str(D.ndim)+'\n')
    
    if not Q.shape[0]==len(D):
        raise Exception('\n\tData has different shapes'
                        '\n\tQ shape: '+str(Q.shape)+', D shape: '+str(D.shape)+'\n')
    return True

