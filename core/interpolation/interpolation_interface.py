#!/bin/python
#File: core.interpolation.interpolation_interface.py

import abc

import core.grapes_utils.main_utils as UTILS


class InterpolationInterface:

    __metaclass__ = abc.ABCMeta

    def __init__(self, args):
        '''
        The common attributes will be set by a super() call
        '''
        self.Globalargs = args.main
        self.Moduleargs = args.interpolation

        self.SP = UTILS.SuperPrint('Interpolation.log', stdout=False)


    @abc.abstractmethod
    def BuildInterpolation(self, Data, **kwargs):
        return None


    @abc.abstractmethod
    def PredictMean(self, Q, **kwargs):
        '''
        Interpolation function
        '''  
        return None


    @abc.abstractmethod
    def PredictVariance(self, Q, **kwargs):
        '''
        Machine-learning specific confidence function
        defined as a 95% confidence interval

        for method with variance prediction this equates to
        twice the variance => [-2v+mu, 2v+mu]
        '''
        return None

