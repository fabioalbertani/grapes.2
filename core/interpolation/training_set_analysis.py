#!/bin/python
#File: core.interpolation.training_set_analysis.py

import time

from numpy import argsort, zeros, delete, mean, insert
from scipy.stats import moment

import core.grapes_utils.main_utils as UTILS

from core.grapes_utils.progress_bar import DisplayBar
from core.data_management.data_utils import ReadInputCoordinates
from core.geometry.minimalistic_sampling_utils import GPReductor


class TrainingSetAnalysis:

    def __init__(self, args, Molecule):

        self.args = args
        self.Molecule = Molecule

        self.SP = UTILS.SuperPrint('TrainingSetAnalysis.log', stdout=False)


    def TransAnalysis(self, INTERPOLATION, CalcDetails, elements, metric='covariance'):
        '''
        Does full analysis of a potential training set using a given kernel (which could 
        be taken from an optimised gaussian process or given completely a priori)
        '''

        self.SP('\n\tAnalysing full set (producing error and variance at each point)\n')
        D = {}

        for el in elements if isinstance(elements, list) else [elements]:
            self.SP('\n\tAnalysing potential training set for '+el+' of '+\
                    UTILS.GetLabel(CalcDetails), stdout=True)
            keys = INTERPOLATION.KeyOrganiser.GetKey(CalcDetails, el)

            if len(keys)>1:
                raise Exception('A single GP should be passed')

            else:
                key = keys[0]

                filename = self.args.main.CDPath+'/SavedData/'+self.args.main.Label+\
                                    '/TrainingSamples_'+UTILS.GetDataLabel(CalcDetails)+'.qdat'
                Q, EnergyFlag = ReadInputCoordinates(self.args.main, self.Molecule,
                                                     CoordinatesOnly=True, filename=filename)
                kernel_ = INTERPOLATION.InterpolationKeys[key].kernel_

                D[el] = self.GetStatistics(Q, kernel_)

        return D



    def CisAnalysis(self, INTERPOLATION, CalcDetails, elements, state=None, metric='covariance'):
        '''
        Does a full analysis of the training set associated to the gaussian process
        '''

        self.SP('\n\tAnalysing training set (producing error and variance at each point)\n')
        D = {}

        for el in elements if isinstance(elements, list) else [elements]:
            self.SP('\n\tAnalysing training set for '+el+' of '+UTILS.GetLabel(CalcDetails),\
                    stdout=True)
            keys = INTERPOLATION.KeyOrganiser.GetKey(CalcDetails, el)

            if len(keys)>1:
                raise Exception('A single GP should be passed')

            else:
                key = keys[0]

                if metric=='Variance':
                    D[el] = self.GetVarianceSubset(INTERPOLATION.InterpolationKeys[key])

                elif metric=='covariance':
                    Q = INTERPOLATION.InterpolationKeys[key].X_train_
                    kernel_ = INTERPOLATION.InterpolationKeys[key].kernel_

                    D[el] = self.GetStatistics(Q, kernel_)
        
        return D


    def GetVarianceSubset(self, GP):
        '''Returns a dictionary containing all the points and their respective variance
           once removed from the training set'''
        GPR = GPReductor()

        Data = {'Points': GP.X_train_}
        Data['Error'] = zeros(Data['Points'].shape[0])
        Data['Variance'] = zeros(Data['Points'].shape[0])

        GPR.load(GP)
        PB = DisplayBar(GP.X_train_.shape[0])

        for i in range(Data['Points'].shape[0]):

            GPR.reduce(i)
            q = Data['Points'][i,:].reshape((1,Data['Points'].shape[1]))
            Data['Variance'][i] = GPR._Interpolation.predict(q, return_std=True)[1]
            Data['Error'][i] = GPR._Interpolation.predict(q, return_std=False)[0] -\
                                                         (GP.y_train_[i] + GP._y_train_mean)

            GPR.reload()
            PB.Show(i)


        return Data





    def GetStatistics(self, Q, kernel_, moments=3):
        '''
        The Covariance will depend on the hyperparameters (but they do not need to be supplied)
        '''
        k_statistics = zeros((Q.shape[0], moments+1))
        PB = DisplayBar(Q.shape[0])

        kk = zeros((Q.shape[0], Q.shape[0]))
        for sample_test in range(Q.shape[0]):
                
            idxneg = delete(range(0, Q.shape[0]), [sample_test])
            k_temp = kernel_(
                Q[sample_test,:].reshape((1,self.args.main.D)),
                Q[idxneg,:]
                            ).flatten()
            kk[sample_test,:] = insert(k_temp, sample_test, 0.0)

            k_statistics[sample_test,0] = mean(k_temp)
            for mom in range(1,moments+1):
                k_statistics[sample_test, mom] = moment(k_temp, moment=mom+1)

            PB.Show(sample_test)

        return {
            'Statistics': k_statistics,
            'Points': Q
               }



    def SelectFromTrainingSet(self, Q, k, moment=1):
        '''
        Selection of points according to kernel statistics
        default: moment=1 i.e. selection according to mean covariance
        '''
        #TODO TODO TODO MAKE THE SELECTION SMART HERE
        idx = argsort(k[:,moment-1])
        slc = [slice(0,self.args.interpolation.NOptimalTrainingSetMax), slice(None)]
        Q = Q[idx,:]

        return Q[slc], k[slc]




    def getTestingSet(self, CalcDetails, N=20, random=False):
        '''
        Select N random points to use as a testing set
        '''

        if random:
            from numpy.random import randint
            indices = randint(0, self.Info['Ni'], N)
        else:
            indices = zeros(N)
            indices[:int(N/2)] = argsort(getattr(self.Data.Energy, 
                CalcDetails['label'])[:,0])[:int(N/2)] #selects some minima points
            indices[int(N/2):] = argsort(getattr(self.Data.Energy, 
                CalcDetails['label'])[:,0])[-int(N/2):]

            indices = [int(i) for i in indices]

        return {
            'Indices': indices,
            'Points': getattr(self.Data, 'Delaunay').points[indices,:],
            'Energy': getattr(self.Data.Energy, CalcDetails['label'])[indices,:]
               }





    def PrintOptimalSamples(self, CalcDetails, Data, Format='Q'):

        for el in Data.keys():
            Q, k = self.SelectFromTrainingSet(Data[el]['Points'], Data[el]['Statistics'])

            extension = '.qdat' if Format=='Q' else '.xyzdat'
            f=open(self.args.main.CDPath+'/SavedData/'+self.args.main.Label+\
                   '/OptimalTrainingSamples_'+UTILS.GetDataLabel(CalcDetails)+\
                   '_el:'+el+extension, 'w')
            f.write('--Nodes Information--')

            template='\ni\t\t'
            for i in range(self.args.main.D if Format=='Q' else 3*len(self.args.main.AtomsList)):
                template+='q'+str(i)+'\t\t\t\t'

            for mom in range(k.shape[1]):
                if not mom: 
                    template += 'k_mu\t\t\t'
                else:
                    template += 'k_moment_'+str(mom)+'\t\t'
            f.write(template+'\n')

            self.SP('\n\tProducing dat file for '+str(Q.shape[0] if Format=='Q' else Q.shape[1])+\
                    ' points\n')

            for n in range(Q.shape[0] if Format=='Q' else Q.shape[1]):
                f.write('\n'+str(n)+'\t\t')

                if Format=='Q':
                    for d in range(self.args.main.D):
                        f.write("%f\t\t" %Q[n,d])
                else:
                    for at in range(len(self.args.main.AtomsList)):
                        for d in range(3):
                            f.write("%f\t\t" %Q[at,n,d])

                for mom in range(k.shape[1]):
                    f.write("%f\t\t" %k[n,mom])

            f.close()





