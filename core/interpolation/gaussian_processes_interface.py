#!/bin/python
#File: core.interpolation.gaussian_processes_interface.py

import os

from numpy import asarray, zeros, ones, r_
from numpy.linalg import norm
from copy import deepcopy, copy

from sklearn.base import clone

from core.auxiliary.woodbury import get_superinverse_matrix
import core.grapes_utils.main_utils as UTILS
import core.interpolation.gaussian_processes_utils as utils
from core.interpolation.hyperparameters_utils import ApproximateHP, SubHPOptimiser

from core.interpolation.interpolation_interface import InterpolationInterface
from core.interpolation.interpolation_utils import CheckInterpolationData
from core.data_management.data_filter import MD_unpack
from core.data_management.data_utils import Repack, Imap, UnpackIndices


class GaussianProcessesInterface(InterpolationInterface):

    @staticmethod
    def RegisterOptions(args):
        m = args.RegisterModule('interpolation')

        args.Register(m, '--type', type=str, default='Gaussian Approximation')
        args.Register(m, '--FitDifferences', action='store_true', default=False,
            help='Used in DataManagement modules to feed either energy differences or directly\
                  the energy from a givne method to the interpolation module')

        args.Register(m, '--cFit', type=float, default=2.0, help='Kernel amplitude parameter')
        args.Register(m, '--sigmaFit', default=2.0, help='Kernel length-scale parameter')
        args.Register(m, '--lambdaFit', type=float, default=0.0016,
            help='White noise Kernel parameter, default is the chemical accuracy i.e. 1 kcal/mol\
                 (given here in hartree)')
        args.Register(m, '--nuFit', type=float, default=2.5, help='Matern nu parameter')

        args.Register(m, '--CovarianceFunction', type=str, default='RBF')
        args.Register(m, '--NoiseFunction', type=str, default='White')

        args.Register(m, '--fixHP', action='store_true', default=False,
            help='Does not allow hyperparameters to be optimised')
        args.Register(m, '--approxHP', action='store_true', default=False,
            help='Uses the Hessian at the minimum to approximate the hyperparameters')

        args.Register(m, '--NTrainingSetMax', type=int, default=5000,
            help='Limits the amount of points given to training sets')
        args.Register(m, '--NOptimalTrainingSetMax', type=int, default=120000,
            help='Selects the N points with the lowest covariance from the rest of the training\
                  samples, i.e. the sparser set')
        args.Register(m, '--Bagging', action='store_true', default=False,
            help='Enables the use of bagging techniques on the GPs')



    @staticmethod
    def ParseOptions(options):

        if not options.interpolation.sigmaFit is None:
            if isinstance(options.interpolation.sigmaFit, float):
                options.interpolation.sigmaFit =\
                ones(options.main.D)*options.interpolation.sigmaFit

            else:
                options.interpolation.sigmaFit =\
                asarray([s for s in options.interpolation.sigmaFit.split(',')])

        
#        if options.run('savescratchfiles')!=None:
#            options.run.set('savescratchfiles', options.run.savescratchfiles.split(' '))



    def __init__(self, args):
        '''
        Class initiation
        '''
        super(GaussianProcessesInterface, self).__init__(args)
        if self.Moduleargs.approxHP:
            self.args = args #they need to be passed down to the RunInterface

        self.KeyOrganiser = utils.KeyOrganiser(args.main)
        self.KernelOrganiser = utils.KernelOrganiser(args)

        self.InterpolationKeys = {}
        self._bag_indices = {}
        self.printed_keys = []


#----------------------------------------------------------------- properties
    
    def number_of_samples(self, CalcDetails, el):
        key = self.KeyOrganiser.GetSingleKey(CalcDetails, el)
        return self.InterpolationKeys[key].X_train_.shape[0]

    @property
    def bag_indices(self):
        return self._bag_indices


#----------------------------------------------------------------- quick links

    def SetSingleGP(self, GP, el):
        '''
        A single Gaussian process can be loaded for a specific element
        (does not work with bagging currently)
        '''
        key = self.KeyOrganiser.SetKey(CalcDetails, el, None)
        self.InterpolationKeys[key] = GP


    def BuildInterpolation(self, Data, CalcDetails, Error=False, optimiseHP=2, Read=False):
        '''
        Interface between DataManagement modules and Gaussian regression modules

        Data:       [dict] "Treated" (see DataManagement/Utils.py functions)  data dictonary
        '''
        Set = False
        DataS = deepcopy(Data) #used to reload data in case GP read has failed

        if Read or not self.Globalargs.ReadData is None and\
               self.Globalargs.ReadData[CalcDetails['label']]:
            try:
                self.ReadInputGaussianProcess(Data, CalcDetails)
                Set = True
            except IOError:
                self.SP('\n\tGPInformation dat file was not found -> calculating GP from data')
                Data = deepcopy(DataS)
                Set = False
            except RuntimeError:
                self.SP('\tFailed to load pre-optimised/calculated GP items -> recalculating...')
                Data = deepcopy(DataS)
                Set = False

        if not Set:
            if self.Moduleargs.Bagging:
                for el in Data['Indices']:
                    iterator, indices = Bagging(Data['Points'+el], Data['Data'+el])
                    self._bag_indices[CalcDetails['label']+el] = indices
                    for Bindex, DBag in iterator:
                        self.SP('\n\tBuilding Gaussian Process for '+UTILS.GetLabel(CalcDetails)+\
                                ' on element '+el+' (Bagging index: '+str(Bindex)+')\n')
                        self.SingleGaussianProcessSetUp(DBag['Points'], DBag['Data'], CalcDetails,
                                   el, optimiseHP, Error=Error, Bi=Bindex)
                    self.PrintData(CalcDetails, el)

            else:
                for el in Data['Indices']:
                    self.SP('\n\tBuilding Gaussian Process for '+UTILS.GetLabel(CalcDetails)+\
                            ' on element '+el+'\n')
                    self.SingleGaussianProcessSetUp(Data['Points'+el], Data['Data'+el], CalcDetails,
                               el, optimiseHP, Error=Error)
                    self.PrintData(CalcDetails, el)


        self.PrintInfo()
        if int(os.getenv('verbose')): 
            for k,v in self.InterpolationKeys.items():
                self.SP('\nkey >> '+k)
                self.SP(v)



    def SingleGaussianProcessSetUp(self, Q, Data, CalcDetails, el, 
                                   optimiseHP, Bi=None, Error=False):
        '''
        Creates a key (identifier) and initiates a Gaussian process (which will be stored
        in a dictionary as an attribute `InterpolationKeys`)
    
        optimseHP controls how hyperparameters are handled:
            0: hyperparameters are taken from the current GP (at same key)
            1: a single optimisation is done
            2: a large lml-space optimisation is done
        '''
        if not optimiseHP:
            key = self.KeyOrganiser.GetSingleKey(CalcDetails, el, Bi)
            hyperDict = { 'k1': utils.getHP(self.InterpolationKeys[key], 'constant_value'),
                          'k2': utils.getHP(self.InterpolationKeys[key], 'length_scale'),
                          'k3': utils.getHP(self.InterpolationKeys[key], 'noise_level')}

        else:
            key = self.KeyOrganiser.SetKey(CalcDetails, el, Bi=Bi, Error=Error)
            if self.Moduleargs.approxHP:
                '''hyperparameters approximated (instead of optimised)'''
                optimiseHP = 1
                hyperDict = ApproximateHP(self.args, CalcDetails)


            elif self.Moduleargs.fixHP:
                ''' hyperparameters are given by user '''
                optimiseHP = 0
                hyperDict = { 'k1': self.Moduleargs.cFit,
                              'k2': self.Moduleargs.sigmaFit,
                              'k3': self.Moduleargs.lambdaFit}


            elif Q.shape[0]>self.Moduleargs.NTrainingSetMax:
                GPRM = self.KernelOrganiser.SetKernel(self.Globalargs.D, None, 
                                                      optimiseHP=optimiseHP)
                hyperDict = SubHPOptimiser(Q, Data, GPRM, self.Moduleargs.NTrainingSetMax)
                optimiseHP = 0

            else:
                hyperDict = None

        GPRM = self.KernelOrganiser.SetKernel(self.Globalargs.D, hyperDict, 
                                                  optimiseHP=optimiseHP)

        if CheckInterpolationData(Q, Data):
            self.InterpolationKeys[key] = GPRM.fit(Q, Data)



    def SinglePointUpdate(self, Data, CalcDetails, optimiseHP=0, Error=False):
        '''
        Updates a Gaussian process with a single point using the 
        woodbury identity
        
        nb: bagging methods cannot be used in this instance
        '''

        for el in Data['Indices']:
            self.SP('\n\tUpdating Gaussian Process for '+UTILS.GetLabel(CalcDetails)+\
                    ' on element '+el+'\n')
            key = self.KeyOrganiser.SetKey(CalcDetails, el, Bi=None, Error=Error)

            self.InterpolationKeys[key].define_inverse_matrix()

            _K_inv = get_superinverse_matrix(
                    self.InterpolationKeys[key]._K_inv,
                    self.InterpolationKeys[key].kernel_(self.InterpolationKeys[key].X_train_,
                                                        Data['Points'+el]),
                    self.PredictVariance(Data['Points'+el], CalcDetails, el)[0,0]
                                            )
       
            self.InterpolationKeys[key]._K_inv = copy(_K_inv)
            self.InterpolationKeys[key].increase_training_set(Data['Points'+el], Data['Data'+el])
            self.InterpolationKeys[key].update_prediction_values()


#--------------------------------------------------------------------------------- PRINTING 

    def PrintData(self, CalcDetails, el):
        '''
        Prints the hyperparameters, parameters and X,Y training data of a chosen interpolation
        in the self.SP (not the same as self.PrintInfo)
        '''
        for el in self.KeyOrganiser.Info[CalcDetails['label']]:
            keys = self.KeyOrganiser.GetSingleKey(CalcDetails, el)

            for key in keys if isinstance(keys, list) else [keys]:
                self.SP('\nGaussian process initiated: '+key+'\n', stdout=False)
                for k,v in self.InterpolationKeys[key].get_params(deep=True).items():
                    self.SP(str(k)+' >>> ', stdout=False)
                self.SP('Parameters:\n', stdout=False)
                for v in range(len(self.InterpolationKeys[key].alpha_)):
                    self.SP(str(v)+':\t'+str(self.InterpolationKeys[key].alpha_[v])+'\n',
                            stdout=False)

                self.SP('y value used:\n', stdout=False)
                for i in range(self.InterpolationKeys[key].y_train_.shape[0]):
                    self.SP('\t>'+str(self.InterpolationKeys[key].y_train_[i,:])+'\n', stdout=False)


        self.SP('\nIntermediate summary:\n')
        for k,v in self.KeyOrganiser.Info.items():
            self.SP(str(k)+' -- '+str(v)+'\n')


        for el in self.KeyOrganiser.Info[CalcDetails['label']]:
            keys = self.KeyOrganiser.GetSingleKey(CalcDetails, el)
            f=open(self.Globalargs.WDPath+'/GRAPESData/SavedData/'+self.Globalargs.Label+\
                   '/GPInformation_'+UTILS.GetDataLabel(CalcDetails)+'_el'+el+'.gpdat', 'w')
            #The return cd path saves the data in the general GRAPESData folder 

            if isinstance(keys, list):
                iterator = enumerate(keys)
                f.write('Bags: '+str(len(keys))+'\n')
            else:
                iterator = enumerate([keys])
                f.write('Bags: 0\n')

            for index, key in iterator:
                if CalcDetails['label']+el in self._bag_indices.keys():
                    f.write('bag indices: '+' '.join(self._bag_indices[CalcDetails['label']\
                            +el][index]))
                f.write('number of hyperparameters: '+\
                        str(len(self.InterpolationKeys[key].kernel_.theta))+'\n')
                f.write('length scale dimensions: ' + str(1 if not\
                        hasattr(utils.getHP(self.InterpolationKeys[key].kernel_,'length_scale'),\
                        '__len__') else len(utils.getHP(self.InterpolationKeys[key].kernel_,\
                        'length_scale')))+'\n')
                f.write('number of training points: '+\
                        str(self.InterpolationKeys[key].X_train_.shape[0])+\
                        '\nnumber of dimensions: '+\
                        str(self.InterpolationKeys[key].X_train_.shape[1])+'\n')
                for v in range(self.KernelOrganiser.NHyperparameters):
                    if v==self.KernelOrganiser.NHyperparameters-1:
                        f.write('noise_level:\t'+"%.12f" %MD_unpack(utils.getHP(
                                self.InterpolationKeys[key].kernel_, 'noise_level'))+'\n')
                    elif not v:
                        f.write('constant_value:\t\t'+str(MD_unpack(utils.getHP(
                                self.InterpolationKeys[key].kernel_, 'constant_value')))+'\n')
                    else:
                        if hasattr(utils.getHP(self.InterpolationKeys[key].kernel_,
                                               'length_scale'), '__len__'):
                            f.write('length_scale:\t') if v==1 else f.write(4*'\t')
                            f.write(str(MD_unpack(utils.getHP(self.InterpolationKeys[key].kernel_,
                                    'length_scale')[v-1]))+'\n')
                        else:
                            f.write('length_scale:\t'+str(MD_unpack(utils.getHP(
                                    self.InterpolationKeys[key].kernel_, 'length_scale')))+'\n')

                f.write('Dual coefficients:\n')
                for v in range(len(self.InterpolationKeys[key].alpha_)):
                    f.write(str(self.InterpolationKeys[key].alpha_[v][0])+'\n')

                f.write('Cholesky factor:\n')
                for v1 in range(self.InterpolationKeys[key].L_.shape[0]):
                    for v2 in range(v1+1):
                        f.write(str(self.InterpolationKeys[key].L_[v1,v2])+'\t')
                    f.write('\n')

                f.close()

    def ReadInputGaussianProcess(self, Data, CalcDetails, _direct=False, filenames=None):
        '''
        The relevant values needed for mean and variance predictions are read in from saved data:

        The hyperparameters are not optimised (they should be if printed in a saved file) and to
        avoid unnecessary calculations, the alpha vector and L_ matrix are also loaded
        '''

        for el in [s[4:] for s in Data.keys() if 'Data' in s]:
            key = self.KeyOrganiser.SetKey(CalcDetails, el, None)

            if filenames is None:
                filename = self.Globalargs.WDPath+'/GRAPESData/SavedData/'+self.Globalargs.Label\
                           +'/GPInformation_'+UTILS.GetDataLabel(CalcDetails)+'_el'+el+'.gpdat'
            else:
                filename = filenames[state]
            f=open(filename, 'r')
            self.SP('\tReading GP coefficients/hyperparameters from file: '+\
                    filename, stdout=True)

            l=f.readline()
            if not int(l.split()[1]):
                bags = False
                total_bags=1
            else: 
                bags = True
                total_bags = int(l.split()[1])
            l=f.readline()
            for bi in range(total_bags):
                key_final = key if not bags else 'b'+str(bi)+key
                if bags:
                    indices = [int(s) for s in l.split()[2:]]
                    self.SP('\n\tReading '+str(len(indices))+' coefficients for bag '+str(bi)+\
                            ' (key '+key_final+')\n', stdout=True)
                    l=f.readline()
                else:
                    self.SP('\n\tReading all coefficients (key '+key_final+\
                            ')\n', stdout=True)
                number_of_hyperparameters = int(l.split()[3])
                l=f.readline()
                number_of_lengthscales = int(l.split()[3])
                l=f.readline()
                training_set_size = int(l.split()[4])
                l=f.readline() #skips dimension line
                l=f.readline()
                val = float(l.split()[1].split('**')[0])**float(l.split()[1].split('**')[1]) if\
                      len(l.split())>2 else float(l.split()[1])
                hyperDict = {'k1': val}
                l=f.readline()
                for index in range(number_of_lengthscales):
                    if not index:
                        length_scale = [float(l.split()[1])]
                    else:
                        length_scale += [float(l.split()[0])]
                    l=f.readline()
                hyperDict['k2'] = length_scale
                hyperDict['k3'] = float(l.split()[1])
                l=f.readline()
                alpha_ = zeros((training_set_size,1))
                l=f.readline()
                for index in range(training_set_size):
                    alpha_[index,0] = float(l.split()[0])
                    l=f.readline()
                L_ = zeros((training_set_size, training_set_size))
                l=f.readline()
                for index in range(training_set_size):
                    L_[index,0:(index+1)] = [float(x) for x in l.split()]
                    l=f.readline()
            
                GPRM = self.KernelOrganiser.SetKernel(self.Globalargs.D, hyperDict, optimiseHP=0)

                self.InterpolationKeys[key_final] = GPRM
                self.InterpolationKeys[key_final].kernel_ = clone(
                                                  self.InterpolationKeys[key_final].kernel)
                if bags:
                    self.InterpolationKeys[key_final].load_training_set(
                                    Data['Points'+el][indices,:], Data['Data'+el][indices,:])
                else:
                    self.InterpolationKeys[key_final].load_training_set(Data['Points'+el],
                                                                        Data['Data'+el])
                self.InterpolationKeys[key_final].load_precalculated_values(alpha_, L_)

            f.close() 



#-------------------------------------------------------------------------------- PREDICTION 

    def getGPShape(self, Q, CalcDetails, el, pure, Type='scalar'):
        '''
        Creates an array with the correct shape to return the 
        result of a mean/variance prediction of a GP or a c-GP
        '''
        if Q==[]:
            return None

        else:
            if pure:
                #A single key is not returned as a list
                keys = [self.KeyOrganiser.GetSingleKey(CalcDetails, el)]
            else:
                keys = self.KeyOrganiser.GetKey(CalcDetails, el)

            if Type=='scalar':
                return zeros(tuple([Q.shape[0],1])), keys
            elif Type=='vector':
                return zeros(tuple([Q.shape[0],Q.shape[1],1])), keys
            elif Type=='matrix':
                return zeros(tuple([Q.shape[0],Q.shape[1],Q.shape[1],1])), keys


    def getGPVal(self, Q, key, std, derivative=0, dim=None, full=False):
        '''
        easier and general call to the interpolation functions (mean, gradient ...)
        if key is a list the Gaussian process has been baged and the mean of the bags is taken here
        '''
        if isinstance(key, list):
            for _key in key:
                if not derivative:
                    if std:
                        Pt = self.InterpolationKeys[_key].predict_variance(Q)
                    else:
                        Pt = self.InterpolationKeys[_key].predict_mean(Q)
                elif derivative==1:
                    #REDO TODO
                    Pt = self.InterpolationKeys[key].predict_mean_gradient(Q, dim, full)
                P = [P[i]+Pt[i] for i in range(len(Pt))] if 'P' in locals() else Pt
            P = [P[i]/len(key) for i in range(len(Pt))]
        else:
            if not derivative:
                if std:
                    P = self.InterpolationKeys[key].predict_variance(Q)
                else:
                    P = self.InterpolationKeys[key].predict_mean(Q)
            elif derivative==1:
                P = self.InterpolationKeys[key].predict_mean_gradient(Q, dim, full)
        return P



    def PredictMean(self, Q, CalcDetails, el, pure=False):
        '''
        Interpolate the energy for the specified calculation details on all states

        The key (or keys for c-GP) are collected and then used to return the predicted mean
        '''
        Data, keys = self.getGPShape(Q, CalcDetails, el, pure)

        for key in keys:
            Data += self.getGPVal(Q, key, False).reshape(Data.shape)

        if pure: print Data
        return Data


    def PredictVariance(self, Q, CalcDetails, el, pure=False):
        '''
        Finds the confidence interval for the specified calculation details on all states

        The key (or keys for c-GP) are collected and then used to return the predicted variance
        '''
        Data, keys = self.getGPShape(Q, CalcDetails, el, pure)

        for key in keys:
            Data += self.getGPVal(Q, key, True).reshape(Data.shape)

        return Data

    def PredictMeanForce(self, Q, CalcDetails, el, dim, full=False):
        '''
        Interpolate the energy for the specified calculation details on all states

        The key (or keys for c-GP) are collected and then used to return the predicted gradient
        '''
        Data, shp, keys = self.getGPShape(Q, CalcDetails, el, Type='vector' if full else 'scalar')

        for key in keys:
            Data += self.getGPVal(Q, key, False, 1, dim, full).reshape(Data.shape)

        return Data


    def DrawSamples(self, QInput, samples=None):
        '''
        The samples will only be taken for a single key (the first one)

        the return array is of shape (n_points, states, samples)
        '''
        from numpy import zeros
        Samples = zeros((len(QInput[:,0]), self.Globalargs.NStates, samples))

        for state in range(self.Globalargs.NStates):
            keys = self.KeyOrganiser.GetKey(CalcDetails, state)

            Samples[:,state,:] = self.InterpolationKeys[keys[0]].sample_y(QInput, samples)

        return Samples




    def PrintInfo(self):
        '''
        Prints a summary of all initiated GPs
        '''
        f = open('GPSummary.log', 'a')

        if True:
            for i, key in enumerate(self.InterpolationKeys):
                if not key in self.printed_keys:
                    self.printed_keys.append(key)

                    if i: f.write('\n\n')
                    f.write('-- GP '+str(i)+': '+key+'\n')

                    f.write('\n\tcovariance kernel: ')
                    f.write(str(getattr(self.InterpolationKeys[key].kernel_, 'k1')))
                    f.write('\n\tnoise kernel: ')
                    f.write(str(getattr(self.InterpolationKeys[key].kernel_, 'k2')))
                    f.write('\n\tnumber of points: '+str(len(self.InterpolationKeys[key].X_train_)))
                    f.write('\n\tmean(y): '+str(self.InterpolationKeys[key]._y_train_mean))
                    f.write('\n\n\tFull associated dictionary:\n')

                    for _key in self.KeyOrganiser.Info.keys():
                        if key in _key:
                            key_print = _key[len(key):]
                            if key_print=='deltaTypelabel':
                                f.write('\t\t'+key_print+'\t'+UTILS.GetLabel(self.KeyOrganiser.\
                                        Info[_key])+'\n')
                            else:
                                f.write('\t\t'+key_print+'\t'+str(self.KeyOrganiser.\
                                        Info[_key])+'\n')


                    f.write('\n\tData preview'+'' if self.InterpolationKeys[key].X_train_.shape[0]\
                            <100 else '(100 out of '+str(self.InterpolationKeys[key].X_train_.\
                            shape[0])+')')
                    f.write(':\n')
                    for sample in range(self.InterpolationKeys[key].X_train_.shape[0] if\
                        self.InterpolationKeys[key].X_train_.shape[0]<100 else 99):
                        f.write('\t\t'+'\t\t\t'.join([str(x).strip() for x in\
                        self.InterpolationKeys[key].X_train_[sample,:]])+'\t\t'+\
                        str(self.InterpolationKeys[key].y_train_[sample,0])+'\n')

                    if self.InterpolationKeys[key].X_train_.shape[0]>100: 
                        f.write('\t\t...\n')

        else:
            f.write('\n\tWriting failed ...\n')

        f.close()
