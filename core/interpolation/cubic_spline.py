#!/usr/bin/python
#File: CubicSpline.py


#Interpolation:
#   -Uses the implemented cubic spline package in scipy


#Details:
#   -only 1D and 2D are available
#   -extrapolation is supported
#   -the amount of input data is limited by the scipy module (does not create error but
#    does not take into account extra data)


class Main:

    def __init__(self, args=None, s=1, k=3):

        self.D = args.D

        self.s = s
        self.k = k

        if args.UseSymmetry:

            if transform=='linear_triatomic':
                from ..FORTRANModules.FORTRANGeometry import lineartriatomic
                self.qtransform_symmetry = lineartriatomic

            elif transform=='h2o_fixed_angle':
                from ..FORTRANModules.FORTRANGeometry import h2ofixedangle
                self.qtransform_symmetry = h2ofixedangle

            elif transform=='hcn_planar':
                from ..FORTRANModules.FORTRANGeometry import hcnplanar
                self.qtransform_symmetry = hcnplanar

            else:
                raise Exception('The system requested has no f90 module to treat symmetry associated')

        self.UseSymmetry = args.UseSymmetry


    #Set the cubic spline parameters for interpolation:
    #   -Is called within PointsStructure.py (is not implemented with the Delaunay mesh)

    def BuildInterpolation(self, Data, basis_set, NStates):

        import scipy.interpolate
    
        if self.D==1:
            for i in xrange(NStates):
                setattr(self, "%s_%s" %(basis_set, i), scipy.interpolate.splrep(getattr(Data, 'points'), getattr(Data.energy, basis_set), k=self.k, s=self.s))
        else:   
            for i in xrange(NStates):
                setattr(self, "%s_s%s" %(basis_set, i), scipy.interpolate.bisplrep(getattr(Data, 'points')[:,0], getattr(Data, 'points')[:,1], getattr(Data.energy, basis_set)))




    def Interpolate(self, QInput, basis_set, state):

        from numpy import zeros
        import scipy.interpolate

        EnergyInterpolated = zeros(len(QInput[:,0]))

        if self.UseSymmetry:
            from numpy import asfortranarray
            Q = asfortranarray(QInput)
            self.qtransform_symmetry.constrain(Q, len(Q[:,0]))
        else:
            Q = QInput


        if self.D==1:
            for i in xrange(len(Q[:,0])):
                EnergyInterpolated[i] = scipy.interpolate.splev(Q[i], getattr(self, "%s_s%s" %(basis_set, state)), der=0)
        else:
            for i in xrange(len(Q[0,:])):
                EnergyInterpolated[i] = scipy.interpolate.bisplev(Q[i,0], Q_symm[i,1], getattr(self, "%s_s%s" %(basis_set, state)))

        from ..DataManagement.DataStructure import Process

        return Process(self.D, 1, QInput, EnergyInterpolated, notrim=True)






