#!/bin/python
#File: core.interpolation.gaussian_processes_regression.py

"""Gaussian processes regression. """

# Authors: Jan Hendrik Metzen <jhm@informatik.uni-bremen.de>
#
# License: BSD 3 clause

import warnings
from operator import itemgetter

import numpy as np
from scipy.linalg import cholesky, cho_solve, solve_triangular, inv
from scipy.optimize import fmin_l_bfgs_b

from sklearn.base import BaseEstimator, RegressorMixin, clone
#import sklearn.gaussian_process.kernels as kernels
import core.interpolation.gaussian_processes_kernels as kernels
from sklearn.utils import check_random_state
from sklearn.utils.validation import check_X_y, check_array
from sklearn.utils.deprecation import deprecated
from sklearn.exceptions import ConvergenceWarning

class GaussianProcessRegressor(BaseEstimator, RegressorMixin):
    """
    Gaussian process regression (GPR).
    Compact version provided by F.E.A. Albertani
    """
    def __init__(self, kernel=None, alpha=1e-10, optimise_hyperparameters=True,
                 optimizer="fmin_l_bfgs_b", n_restarts_optimizer=0, normalize_y=False, 
                 random_state=None):

        self.kernel = kernel
        self.alpha = alpha
        self.optimise_hyperparameters = optimise_hyperparameters
        self.optimizer = optimizer
        self.n_restarts_optimizer = n_restarts_optimizer
        self.normalize_y = normalize_y
        self.random_state = random_state


    def load_training_set(self, X=None, y=None):
        '''
        The training set is updated without optimising hyperparameters or 
        calculating the dual coefficients. The prediction tools of the class
        will not work if the dual coefficients are not recalculated (covariance
        vectors will not be of the correct shape)
        '''
        if X is None or y is None:
            if self.X_train_ is None or self.y_train_ is None:
                raise RuntimeError('\n\tfit() called without preloaded X and y training set\n')
            else:
                pass
        else:
            X, y = check_X_y(X, y, multi_output=True, y_numeric=True)

            if self.normalize_y:
                self._y_train_mean = np.mean(y, axis=0)
                y = y - self._y_train_mean
            else:
                self._y_train_mean = np.zeros(1)

            if np.iterable(self.alpha) and self.alpha.shape[0] != y.shape[0]:
                if self.alpha.shape[0] == 1:
                    self.alpha = self.alpha[0]
                else:
                    raise ValueError("alpha must be a scalar or an array"
                                     " with same number of entries as y.(%d != %d)"
                                     % (self.alpha.shape[0], y.shape[0]))

            self.X_train_ = np.copy(X)
            self.y_train_ = np.copy(y)

    def increase_training_set(self, X, y):
        self.X_train_ = np.r_[self.X_train_, X]

        self.y_train_ += self._y_train_mean
        self.y_train_ = np.r_[self.y_train_, y]
        self._y_train_mean = np.mean(self.y_train_, axis=0)
        self.y_train_ -= self._y_train_mean


    def load_precalculated_values(self, alpha_, L_):
        '''
        If the dual coefficients and the lower triangular Cholesky matrix
        are known they can be loaded directly
        '''
        self.alpha_ = alpha_
        self.L_ = L_


    def define_inverse_matrix(self):
        '''
        This functions allow to make sure the inverse covariance matrix is defined
        to allow an external module to call it without getting a None object
        '''
        if not hasattr(self, '_K_inv') or self._K_inv is None:
            if not hasattr(self, 'L_'):
                K = self.kernel_(self.X_train_)
                self.L_ = cholesky(K, lower=True)
            L_inv = solve_triangular(self.L_.T, np.eye(self.L_.shape[0]))
            self._K_inv = L_inv.dot(L_inv.T)

    
    def update_prediction_values(self):
        '''
        Recalculates the dual coefficients from the inverse matrix (mostly
        used for seuqential updater of the gaussian process)
        '''
        self.alpha_ = np.matmul(self._K_inv, self.y_train_) 


    def fit(self, X=None, y=None):
        '''
        Calculates the alpha coefficients and the Cholesky decomposition
        for the gaussian process
        '''
        if self.kernel is None:
            self.kernel = kernels.ConstantKernel()*kernels.RBF() + kernels.WhiteKernel() 

        else:
            self.kernel_ = clone(self.kernel)

        self._rng = check_random_state(self.random_state)

        self.load_training_set(X, y)

        if self.optimise_hyperparameters:
            if self.optimizer is not None and self.kernel_.n_dims > 0:
                # Choose hyperparameters based on maximizing the log-marginal likelihood
                # (potentially starting from several initial values)

                def obj_func(theta, eval_gradient=True):
                    if eval_gradient:
                        lml, grad = self.log_marginal_likelihood(
                            theta, eval_gradient=True)
                        return -lml, -grad
                    else:
                        return -self.log_marginal_likelihood(theta)

                # First optimize starting from theta specified in kernel
                optima = [(self._constrained_optimization(obj_func,
                                                          self.kernel_.theta,
                                                          self.kernel_.bounds))]
                theta_list = self.kernel_.theta

                # Additional runs are performed from log-uniform chosen initial theta
                if self.n_restarts_optimizer > 0:
                    if not np.isfinite(self.kernel_.bounds).all():
                        raise ValueError('Multiple optimizer restarts (n_restarts_optimizer>0)'\
                                         ' requires that all bounds are finite.')

                    bounds = self.kernel_.bounds
                    for iteration in range(self.n_restarts_optimizer):
                        theta_list = np.c_[theta_list,self._rng.uniform(bounds[:, 0], bounds[:, 1])]
                        optima.append(self._constrained_optimization(obj_func,
                                      theta_list[:,-1],bounds))

                lml_values = list(map(itemgetter(1), optima))
                self.kernel_.theta = optima[np.argmin(lml_values)][0]
                self.log_marginal_likelihood_value_ = -np.min(lml_values)
            else:
                self.log_marginal_likelihood_value_ =\
                    self.log_marginal_likelihood(self.kernel_.theta)

        #Precompute quantities required for predictions which are independent of actual query points
        K = self.kernel_(self.X_train_)
        K[np.diag_indices_from(K)] += self.alpha
        self._K_inv = None

        try:
            self.L_ = cholesky(K, lower=True)  # Line 2
            # self.L_ changed, self._K_inv needs to be recomputed
            self.Get_K_inv()


        except np.linalg.LinAlgError as exc:
            exc.args = ('The kernel, %s, is not returning a positive definite matrix.'
                        'Try gradually increasing the alpha parameter of your'
                        'core.interpolation.gaussian_process_regressor estimator.'
                        % self.kernel_,) + exc.args
            raise
        self.alpha_ = cho_solve((self.L_, True), self.y_train_)  # Line 3
        return self



    def Get_K_inv(self):
        #TO BE REMOVED
        '''
        Saves _K_inv to allow to use the matrix form of the interpolation:
        i.e.      f = K(X*,X) _K_inv(X,X) f(X) vs  f = K(X*,X) alpha_(X)
        '''
        self._K_inv = inv(self.kernel_(self.X_train_))


    def Cholesky(self):
        #TO BE REMOVED
        '''
        Solves the Cholesky decomposition (used for detrending)
        '''
        K = self.kernel_(self.X_train_)
        try:
            self.L_ = cholesky(K, lower=True)

        except np.linalg.LinAlgError as exc:
            exc.args = ('The kernel, %s, is not returning a positive definite matrix.'
                        'Try gradually increasing the alpha parameter of your'
                        'core.interpolation.gaussian_process_regressor estimator.'
                        % self.kernel_,) + exc.args
            raise
        self.alpha_ = cho_solve((self.L_, True), self.y_train_)  # Line 3



    def predict_mean(self, X):

        X = check_array(X)

        if not hasattr(self, "X_train_"):
            if self.kernel is None:
                raise RuntimeError('\n\tKernel was not fitted (or supplied as a default)\n')
            else:
                kernel = self.kernel

            y_mean = np.zeros(X.shape[0])
            return y_mean

        else:
            K_trans = self.kernel_(X, self.X_train_)
            y_mean = K_trans.dot(self.alpha_)  # Line 4 (y_mean = f_star)
            y_mean = self._y_train_mean + y_mean  # undo normal.
            return y_mean


    def predict_variance(self, X):

        X = check_array(X)

        if not hasattr(self, "X_train_"):
            if self.kernel is None:
                raise RuntimeError('\n\tKernel was not fitted (or supplied as a default)\n')
            else:
                kernel = self.kernel

            y_mean = np.zeros(X.shape[0])
            y_var = kernel.diag(X)
            return np.sqrt(y_var)

        else:
            K_trans = self.kernel_(X, self.X_train_)
            if not hasattr(self, '_K_inv') or self._K_inv is None:
                # compute inverse K_inv of K based on its Cholesky decomposition
                L_inv = solve_triangular(self.L_.T, np.eye(self.L_.shape[0]))
                self._K_inv = L_inv.dot(L_inv.T)

            y_var = self.kernel_.diag(X)
            y_var -= np.einsum("ij,ij->i", np.dot(K_trans, self._K_inv), K_trans)

            y_var_negative = y_var < 0
            if np.any(y_var_negative): y_var[y_var_negative] = 0.0

            return np.sqrt(y_var)


    def predict_mean_gradient(self, X, dim=0, full=False):
        '''
        Can provide the full gradient vector (full=True) or the gradient value along a specific dim
        '''
        X = check_array(X)

        if full:
            y_mean = np.zeros(X.shape)
            for d in xrange(self.X_train_.shape[1]):
                K_trans = self.kernel_.gradient(X, self.X_train_, dim=d)
                y_mean[d] = K_trans.dot(self.alpha_)
        else:
            K_trans = self.kernel_.gradient(X, self.X_train_, dim=dim)
            y_mean = K_trans.dot(self.alpha_)

        return y_mean


    #remove !!
    def predict_matrix_form(self, X, return_std=False, return_cov=False):
        '''
        Uses the explicit formulation of K_inv_ rather than the alpha coefficients
        '''

        X = check_array(X)
        if not hasattr(self, '_K_inv') or self._K_inv is None:
            self.Get_K_inv()

        K_trans = self.kernel_(X, self.X_train_)
        y_mean = K_trans.dot(np.matmul(self._K_inv,self.y_train_))

        if return_std:
            y_var = self.kernel_.diag(X)
            y_var -= np.einsum("ij,ij->i", np.dot(K_trans, self._K_inv), K_trans)
            return y_mean, np.sqrt(y_var)

        else:
            return y_mean,


    def sample_y(self, X, n_samples=1, random_state=0):
        rng = check_random_state(random_state)

        y_mean, y_cov = self.predict(X, return_cov=True)
        if y_mean.ndim == 1:
            y_samples = rng.multivariate_normal(y_mean, y_cov, n_samples).T
        else:
            y_samples = [rng.multivariate_normal(y_mean[:, i], y_cov, n_samples).T[:, np.newaxis]
                         for i in range(y_mean.shape[1])]
            y_samples = np.hstack(y_samples)

        return y_samples



    def log_marginal_likelihood(self, theta=None, eval_gradient=False, fit_bias=1):

        if theta is None:
            if eval_gradient:
                raise ValueError(
                    "Gradient can only be evaluated for theta!=None")
            return self.log_marginal_likelihood_value_

        kernel = self.kernel_.clone_with_theta(theta)

        if eval_gradient:
            K, K_gradient = kernel(self.X_train_, eval_gradient=True)
        else:
            K = kernel(self.X_train_)

        K[np.diag_indices_from(K)] += self.alpha
        try:
            L = cholesky(K, lower=True)  # Line 2
        except np.linalg.LinAlgError:
            return (-np.inf, np.zeros_like(theta)) if eval_gradient else -np.inf

        # Support multi-dimensional output of self.y_train_
        y_train = self.y_train_
        if y_train.ndim == 1:
            y_train = y_train[:, np.newaxis]

        alpha = cho_solve((L, True), y_train)  # Line 3

        # Compute log-likelihood (compare line 7)
        log_likelihood_dims = -0.5 * fit_bias * np.einsum("ik,ik->k", y_train, alpha)
        log_likelihood_dims -= np.log(np.diag(L)).sum()
        log_likelihood_dims -= K.shape[0] / 2 * np.log(2 * np.pi)
        log_likelihood = log_likelihood_dims.sum(-1)  # sum over dimensions

        if eval_gradient:  # compare Equation 5.9 from GPML
            tmp = np.einsum("ik,jk->ijk", alpha, alpha)  # k: output-dimension
            tmp -= cho_solve((L, True), np.eye(K.shape[0]))[:, :, np.newaxis]
            # Compute "0.5 * trace(tmp.dot(K_gradient))" without
            # constructing the full matrix tmp.dot(K_gradient) since only
            # its diagonal is required
            log_likelihood_gradient_dims = 0.5 * np.einsum("ijl,ijk->kl", tmp, K_gradient)
            log_likelihood_gradient = log_likelihood_gradient_dims.sum(-1)

        if eval_gradient:
            return log_likelihood, log_likelihood_gradient
        else:
            return log_likelihood



    def _constrained_optimization(self, obj_func, initial_theta, bounds):
        '''Single optimization'''
        if self.optimizer == "fmin_l_bfgs_b":
            theta_opt, func_min, convergence_dict = fmin_l_bfgs_b(obj_func, 
                                                    initial_theta, bounds=bounds)

            if convergence_dict["warnflag"] != 0 and False: #remove printing
                warnings.warn("fmin_l_bfgs_b terminated abnormally with the "
                              " state: %s" % convergence_dict,
                              ConvergenceWarning)
        elif callable(self.optimizer):
            theta_opt, func_min = self.optimizer(obj_func, initial_theta, bounds=bounds)
        else:
            raise ValueError("Unknown optimizer %s." % self.optimizer)

        return theta_opt, func_min
