#!/bin/python
#File: core.geometry.coordinates_transformer.py

import os, sys

from core.geometry.molecule import MoleculeManager  
from core.data_management.data_utils import ReadInputCoordinates

class arguments:

    def __init__(self, D, AtomsList, CoordinatesTransform):
        self.D = D
        self.AtomsList = AtomsList
        self.CoordinatesTransform = CoordinatesTransform
        self.InputFileName = 'InputFiles/H2.input'



def main():

    os.environ['verbose'] = '0'
    args = arguments(
        12,
        ['O', 'O', 'H', 'H', 'H', 'H'],
        'waterdimer'
                    )

    print_mol = True
    Transform = MoleculeManager(args)

    filename = raw_input('Provide path to file and filename (omitting the .qdat/xyzdat): ')
    Format = raw_input('Which coordinate format would you like to read in? (XYZ/Q):  ')
    args.CoordinatesFormat = Format
    Coordinates, dump = ReadInputCoordinates(args, Transform, CoordinatesOnly=True, 
                                 filename=filename+'.qdat' if Format=='Q' else filename+'.xyzdat')

    print(str(Coordinates.shape[0 if Format=='Q' else 1])+' samples have been read in\n')

    #ReadInputCoordinates will return point in q-format
    if Format=='Q':
        Coordinates = Transform.q2cart(Coordinates)

    destination_copy = raw_input('Would you like to (potentially) overwrite the coordinate '\
                                 'file in the folder? (y/n): ')
    
    #The q/xyz are inverted
    if destination_copy=='y':
        filename_out = filename
        filename_out += '.qdat' if Format=='XYZ' else '.xyzdat'
    else:
        filename_out = 'MeshInformation' 
        filename_out += '.qdat' if Format=='XYZ' else '.xyzdat'

    print('Writing new file to '+filename_out)
    f = open(filename_out, 'w')
    if print_mol: f2 = open('VMD_'+filename_out, 'w')

    template='\ni\t\t'
    for i in range(args.D if Format=='Q' else 3*len(args.AtomsList)):
        template+="q%i\t\t\t\t" %i
    f.write(template+'\n')


    for sample in range(Coordinates.shape[1 if Format=='Q' else 0]):

        f.write('\n'+str(sample)+'\t\t')

        if Format=='XYZ':
            for d in range(args.D):
                f.write("%f\t\t" %Coordinates[sample,d])

        else:
            for at in range(len(args.AtomsList)):
                if print_mol: f2.write('\n'+args.AtomsList[at]+'\t')
                for d in range(3):
                    f2.write("%f\t\t" %Coordinates[at,sample,d])
                    f.write("%f\t\t" %Coordinates[at,sample,d])

    f.close()

if __name__=='__main__':
    main()
