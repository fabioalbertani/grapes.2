#!/bin/python
#File: core.geometry.molecule.py

import os, abc
from core.geometry import geometry_utils as utils

from numpy import asarray, linalg, insert, zeros, dot, arctan, arccos
from numpy import cos as npcos
from numpy import sin as npsin
from copy import copy
from math import cos, sin, sqrt, pi


class Transform:
    ''' 
    Defines the structure of a transformation class 
    Extra arguments are allowed for given classes (to calculated geometries at 
    fixed angles for example)
    '''
    __metaclass__ = abc.ABCMeta

    def __init__(self):
        pass

    def _make(self, *args):
        pass

    def cart2q(self, XYZ, Dq):
        return Q

    def q2cart(self, Q, Atot):
        return XYZ

    def XYZ(self):
        return XYZ



class MoleculeManager:
    '''
    Interface to the different modules to convert xyz to q coordinates, normal modes and so on
    '''

    def __init__(self, args):

        TList = {
        'normalmodes': normalmodes(),
        'xyzmodes': xyzmodes(),
        'diatomic': diatomic(),
        'fulltriatomic': fulltriatomic(),
        'fixedtriatomic': fixedtriatomic(), 
        'ethane3d': ethane3d(),
        'waterdimer': waterdimer(),
        'waterdimer9D': waterdimer9D(),
        'Nitrobenzene5D': Nitrobenzene5D(),
        'CH5': CH5()
                }
    
        self.masses = utils.GetMasses(args.AtomsList)
        try:
            self.reduced_mass = utils.GetReducedMass(args, self.masses)
        except:
            self.reduced_mass = None

        self.Atot = len(args.AtomsList)
        self.Dq = args.D

        self.GeometryTransformations = utils.GeometryTransformations(self.masses) 


        if args.CoordinatesTransform is None:
            if not 'anal' in args.Types:
                print('\n\tNo coordinates transformation provided... working in normal modes\n')
    
            self.TF = normalmodes()
            self.TF._make(args, self.GeometryTransformations)

        else:
            self.TF = TList[args.CoordinatesTransform]
            self.TF._make(args, self.GeometryTransformations)

            self.TransformationType = args.CoordinatesTransform
            print('\n\tSelected transformation type: '+self.TransformationType+'\n')

        self.XYZParsed = utils.ReadXYZ(args, self.GeometryTransformations) 
        if int(os.getenv('verbose')): print('Parsed equilibrium geometry:\n', self.XYZParsed)


    def q2cart(self, Q, fix=False):
        '''
        q2cart interface (see relevant class to see actual function)
        '''
        if fix:
            return self.TF.q2cart(Q, self.Atot)
        else:
            XYZ_cen = self.GeometryTransformations.center(self.TF.q2cart(Q, self.Atot))
            if self.Atot<=2:
                return self.GeometryTransformations.align2D(XYZ_cen)
            else:
                return self.GeometryTransformations.align(XYZ_cen)


    def cart2q(self, XYZ):
        '''
        cart2q interface (see relevant class to see actual function)
        '''
        return self.TF.cart2q(XYZ, self.Dq)


    def XYZ(self):
        '''Returns the xyz << equilibrium >> geometry (not always relevant) '''
        if self.XYZParsed is None:
            return self.TList[self.TransformationType].XYZ()
        else:
            return self.XYZParsed



#----------------------------------------------------------------------------------- General Transforms

class xyzmodes(Transform):
    '''
    this module does not change the coordinates systems but affect the shapes between the representations
    '''

    def q2cart(self, Q, Atot):
        XYZ = zeros((Atot, Q.shape[0], 3))
        for sam in range(Q.shape[0]):
            for at in range(self.Atot):
                XYZ[at,sam,:] = Q[sam,3*at:3*at+3]
        return XYZ


    def cart2q(self, XYZ, Dq):
        Q = zeros((XYZ.shape[1], Dq))
        for at in range(Dq):
            Q[:,3*at:3*at+3] = XYZ[at,:,:]
        return Q

    def XYZ(self):
        return None



class normalmodes(Transform):

    def _make(self, args, GT):
        ''' Calls the normal mode finder from Utils '''
        self.NormalModes = utils.NormalModes(args, GT)


    def q2cart(self, Q, Atot):
        XYZ = zeros((Atot, Q.shape[0], 3))

        for sam in range(Q.shape[0]):
            XYZ_ = dot(self.NormalModes.eigvectors_inv, insert(self.NormalModes.transrot, 0, Q[sam,:]+self.NormalModes.NM0))
            XYZ[:,sam,:] = self.NormalModes.XYZ 
        
            for at in range(Atot):
                XYZ[at,sam,:] = XYZ_[3*at:3*at+3]


        #print 'preproj ', insert(self.NormalModes.transrot, 0, Q[0,:]+self.NormalModes.NM0)
        #print 'example nm:\n', dot(self.NormalModes.eigvectors_inv, insert(self.NormalModes.transrot, 0, Q[0,:]+self.NormalModes.NM0))
        #print 'preproj ', insert(self.NormalModes.transrot, 0, zeros(3)+self.NormalModes.NM0)
        #print 'eq ', dot(self.NormalModes.eigvectors_inv, insert(self.NormalModes.transrot, 0, zeros(3)+self.NormalModes.NM0))
        #f=open('nmdat','w')
        #from numpy import ones
        #for i in range(100):
        #    xyz_ = (-0.1+0.002*i)*ones(3) + self.NormalModes.NM0
        #    print xyz_
        #    xyz = dot(self.NormalModes.eigvectors_inv, insert(self.NormalModes.transrot, 0, xyz_))
        #    f.write(str(i)+'\t'+'\t'.join([str(x) for x in xyz])+'\n')
        #f.close()
        return XYZ


    def cart2q(self, XYZ, Dq):
        Q = zeros((XYZ.shape[1], Dq))

        for sam in range(XYZ.shape[1]):
            XYZ[:,sam,:] -= self.NormalModes.XYZ

            Q_ = dot(self.NormalModes.eigvectors_, XYZ[:,sam,:].flatten()) 
            Q[sam,:] = Q_[:3*Atot-6]

        return Q

    
    def XYZ(self):
        return self.NormalModes.XYZ
    


#----------------------------------------------------------------------------------------- Specific Transforms


class diatomic(Transform):

    def q2cart(self, Q, Dtot):
        XYZ = zeros((Dtot, Q.shape[0], 3))
        XYZ[1,:,0] = Q[:,0]
        return XYZ


    def cart2q(self, Q, Dq):
        Q = zeros((XYZ.shape[1], Dq))
        Q[:,0] = XYZ[1,:,0] - XYZ[0,:,0]
        return Q



class fixedtriatomic(Transform):
    '''
    Default angle 104.4 (water molecule)
    '''
    def _make(self, args, GT): 
        self.angle = args.angle                                                                                                                                                                                                                                                                                  

    def q2cart(self, Q, Dtot):
        XYZ = zeros((Dtot, Q.shape[0], 3))

        for i in range(Q.shape[0]):
            XYZ[1,i,0] = Q[i,0]
            XYZ[2,i,0] = Q[i,1]*cos(self.angle)
            XYZ[2,i,1] = Q[i,1]*sin(self.angle)

        return XYZ


    def cart2q(self, XYZ, Dq):
        Q = zeros((XYZ.shape[1], Dq))

        for i in range(len(XYZ[:,0,0])):

            Q[i,0] = sqrt((XYZ[i,0,0]-XYZ[i,1,0])**2 + (XYZ[i,0,1]-XYZ[i,1,1])**2)
            Q[i,1] = sqrt((XYZ[i,0,0]-XYZ[i,2,0])**2 + (XYZ[i,0,1]-XYZ[i,2,1])**2)
            Q[i,2] = self.angle

        return Q




class fulltriatomic(Transform):

    def angle(self,v1,v2):
        return arccos(dot(v1,v2)/(linalg.norm(v1)*linalg.norm(v2)))

    def q2cart(self, Q, Atot):
        XYZ = zeros((Atot, Q.shape[0], 3))       

        for i in range(Q.shape[0]):
            XYZ[1,i,0] = Q[i,0]
            XYZ[2,i,0] = Q[i,1]*cos(Q[i,2])
            XYZ[2,i,1] = Q[i,1]*sin(Q[i,2])
            
        return XYZ


    def cart2q(self, XYZ, Dq):
        Q = zeros((XYZ.shape[1], Dq))

        for i in range(XYZ.shape[1]):
            Q[i,0] = sqrt((XYZ[0,i,0]-XYZ[1,i,0])**2 + (XYZ[0,i,1]-XYZ[1,i,1])**2)
            Q[i,1] = sqrt((XYZ[0,i,0]-XYZ[2,i,0])**2 + (XYZ[0,i,1]-XYZ[2,i,1])**2)

            Q[i,2] = self.angle(XYZ[0,i,0]-XYZ[1,i,0], XYZ[0,i,0]-XYZ[2,i,0]) 
    
        return Q








#--------------------------------------------------------------------- Specific Molecules


class ethane3d(Transform):

    def angle(self,v1,v2):
        return arccos(dot(v1,v2)/(linalg.norm(v1)*linalg.norm(v2)))

    def distance(self, d1, d2):
        return sqrt((d1-d2)**2)


    def _make(self, args, GT):
        self.r = 1.10


    def q2cart(self, Q, Atot):
        XYZ = zeros((Atot, Q.shape[0], 3))

        XYZ[1,:,0] = Q[:,0]

        for n in range(XYZ.shape[1]):
            XYZ[2:5,n,0] = -self.r*cos(Q[n,2])
            XYZ[2:5,n,1] = self.r*sin(Q[n,2])*npcos(asarray([0, 2*pi/3.0, 4*pi/3.0]))
            XYZ[2:5,n,2] = self.r*sin(Q[n,2])*npsin(asarray([0, 2*pi/3.0, 4*pi/3.0]))

            XYZ[5:8,n,0] = XYZ[1,n,0] + self.r*cos(Q[n,2])
            XYZ[5:8,n,1] = self.r*sin(Q[n,2])*npcos(asarray([0, 2*pi/3.0, 4*pi/3.0])+Q[n,1])
            XYZ[5:8,n,2] = self.r*sin(Q[n,2])*npsin(asarray([0, 2*pi/3.0, 4*pi/3.0])+Q[n,1])

        return XYZ


    def cart2q(self, XYZ, Dq):
        Q = zeros((XYZ.shape[1], Dq))

        for n in range(XYZ.shape[1]):
            Q[:,0] = self.distance(XYZ[0,n,:], XYZ[1,n,:])
            Q[:,2] = self.angle(1,2) #define 
            Q[:,1] = self.angle(1,2) #define
       
        return Q 



class waterdimer(Transform):
    ''' 
    To work with align and center everything is shifted to have the first oxygen
    on the origin (and the oxygens are atoms 0/1 so that the bond is always aligned
    along the O-O direction
    #0.87091 angle of bisector

    #Equilibrium geometry:
    -2.81497115  0.95700961  0.94856896  1.94704951  2.14965257  2.55904139
     0.94959234  0.9494303   1.95835046  2.75476024  0.9003411   1.6522065
    '''
    def angle(self,v1,v2):
        return arccos(dot(v1,v2)/(linalg.norm(v1)*linalg.norm(v2)))
    def angle_az(self,v1,v2):
        ''' the main axis here is defined as x '''
        return self.angle(v1+v2, asarray([1.0,0.0,0.0]))
    def angle_lat(self,v1,v2):
        v = v1+v2
        v[0] = 0 # proj on yz                                  
        return self.angle(v, asarray([0.0,1.0,0.0]))
    def angle_twist(self,v1,v2):
        '''rotate v1+v2 on the main axis and calculate the twist of one of the bonds'''
        R = utils.rotation_matrix(v1+v2, asarray([1.0,0.0,0.0]))
        v = dot(R,v1)                                        
        v[0] = 0 #proj out x
        return self.angle(v,asarray([0.0,1.0,0.0]))
    def triatomic_angle_project(self,r,az,lat,ang,tw):
        ''' gives the xyz for a centered (on heaviest atom) triatomic '''
        xyz = asarray([cos(az), sin(az)*cos(lat), sin(az)*sin(lat)])
        R = utils.rotation_matrix(asarray([1.0,0.0,0.0]), xyz)      
        #print 'r: ',r,' az: ',az,' lat: ',lat,' ang: ',ang,' tw: ',tw,' -> ', R.dot(asarray([r*cos(ang), r*sin(ang)*cos(tw), -r*sin(ang)*sin(tw)]))
        return R.dot(asarray([r*cos(ang), r*sin(ang)*cos(tw), r*sin(ang)*sin(tw)])) 

    def Fix(self,XYZ):
       '''Fixes O1 at the origin, O2 along x and the H11/H12 bisector on the xy plane'''
       for d in range(3): XYZ[:,:,d] -= XYZ[0,:,d]
       for i in range(XYZ.shape[1]):
           R = utils.rotation_matrix(XYZ[1,i,:], asarray([1.0,0.0,0.0]))
           for j in range(1,6): XYZ[j,i,:] = dot(R, XYZ[j,i,:])
           Rp = utils.rotation_matrix_xrot(XYZ[2,i,:]+XYZ[3,i,:])
           for j in range(2,6): 
              XYZ[j,i,:] = dot(Rp, XYZ[j,i,:])

       return XYZ


#    def _make(self, args, GT):
#        XYZ = [[ 1.4178343765,    0.0321166209,    0.2167141090],  
#               [-1.3971367722,   -0.1140678878,   -0.1363261746],
#               [ 0.4694328546,   -0.0411144679,    0.1116467648],
#               [ 1.8812389664,   -0.4174705106,   -0.4782036365],
#               [-1.8290896003,    0.7048987481,   -0.3471247106],
#               [-1.9460246014,   -0.6847756505,    0.3875370695]]
#        from numpy import asarray
#        XYZ = asarray(XYZ).reshape(6,1,3)
#        print 'no proj\n', XYZ
#        print 'XYZ -> NM\n', self.cart2q(XYZ,12)
#        print 'XYZ -> NM -> XYZ\n', self.q2cart(self.cart2q(XYZ,12), 6)
#        print self.Fix(XYZ)


    def q2cart(self, Q, Atot):
        XYZ = zeros((Atot, Q.shape[0], 3))

        XYZ[1,:,0] = Q[:,0]

        for i in range(Q.shape[0]):
            XYZ[2,i,:] = self.triatomic_angle_project(Q[i,1], Q[i,3], 0, Q[i,4]/2, Q[i,5])
            XYZ[3,i,:] = self.triatomic_angle_project(Q[i,2], Q[i,3], 0, Q[i,4]/2, Q[i,5]+pi)     

            #the second hydrogens are shifted back on the oxygen
            XYZ[4,i,:] = self.triatomic_angle_project(Q[i,6], Q[i,8], Q[i,9], Q[i,10]/2, Q[i,11]+pi) + XYZ[1,i,:]
            XYZ[5,i,:] = self.triatomic_angle_project(Q[i,7], Q[i,8], Q[i,9], Q[i,10]/2, Q[i,11]) + XYZ[1,i,:]

        return XYZ


    def cart2q(self, XYZ, Dq):
        XYZc = copy(XYZ)
        Q = zeros((XYZc.shape[1], Dq))
        XYZc = self.Fix(XYZc)
 
        Q[:,0] = XYZc[1,:,0]

        #makes the hydrogen on O2 "centered"
        for atom in range(2): XYZc[4+atom,:,:] -= XYZc[1,:,:]
 
        for i in range(XYZc.shape[1]):
        
            #spherical harmonics H11 & H12
            Q[i,1] = sqrt(sum([x**2 for x in XYZc[2,i,:]]))
            Q[i,2] = sqrt(sum([x**2 for x in XYZc[3,i,:]]))

            #angles
            Q[i,3] = self.angle_az(XYZc[2,i,:], XYZc[3,i,:])
            Q[i,4] = self.angle(XYZc[2,i,:], XYZc[3,i,:])
            Q[i,5] = self.angle_twist(XYZc[2,i,:], XYZc[3,i,:])

            #radiuses H21 & H22
            Q[i,6] = sqrt(sum([x**2 for x in XYZc[4,i,:]]))
            Q[i,7] = sqrt(sum([x**2 for x in XYZc[5,i,:]]))

            #angles
            Q[i,8] = self.angle_az(XYZc[4,i,:], XYZc[5,i,:])
            Q[i,9] = self.angle_lat(XYZc[4,i,:], XYZc[5,i,:])
            Q[i,10] = self.angle(XYZc[4,i,:], XYZc[5,i,:])
            Q[i,11] = self.angle_twist(XYZc[4,i,:], XYZc[5,i,:])

        return Q



class Nitrobenzene5D(Transform):

    def q2cart(self, Q):
        XYZ = zeros((self.Atot, len(Q[:,0]), 3*self.Atot)) 

        for i in range(len(Q[:,0])):

            XYZ[0,i,0] =  Q[i,0] + (Q[i,1]+Q[i,2])*cos(Q[i,3]/2.0)*cos(Q[i,4])
            XYZ[0,i,1] =  (Q[i,1]+Q[i,2])*sin(Q[i,3]/2.0)*cos(Q[i,4])
            XYZ[0,i,2] =  (Q[i,1]+Q[i,2])*sin(Q[i,4])
            XYZ[1,i,0] =  Q[i,0] + (Q[i,1]-Q[i,2])*cos(Q[i,3]/2.0)*cos(Q[i,4])
            XYZ[1,i,1] =  (Q[i,1]-Q[i,2])*sin(Q[i,3]/2.0)*cos(Q[i,4])
            XYZ[1,i,2] =  (Q[i,1]-Q[i,2])*sin(Q[i,4])
            XYZ[2,i,0] =  Q[i,1]
            XYZ[3,i,0] =  0.0
            XYZ[3,i,1] =  0.0
            XYZ[4,i,0] = -0.669265
            XYZ[4,i,1] = -1.212564
            XYZ[5,i,0] = -0.669265
            XYZ[5,i,1] =  1.212564
            XYZ[6,i,0] = -2.053874
            XYZ[6,i,1] = -1.205454
            XYZ[7,i,0] = -2.053874
            XYZ[7,i,1] =  1.205454
            XYZ[8,i,0] = -2.743700
            XYZ[8,i,1] =  0.0
            XYZ[9,i,0] = -0.115639
            XYZ[9,i,1] = -2.127328
            XYZ[10,i,0] = -0.115639
            XYZ[10,i,1] =  2.127328
            XYZ[11,i,0] = -2.588908
            XYZ[11,i,1] = -2.133865
            XYZ[12,i,0] = -2.588908
            XYZ[12,i,1] =  2.133865
            XYZ[13,i,0] = -3.816209

            XYZ[:,i,:] = self.Center(XYZ[:,i,:])

        return XYZ


    def cart2q(self, XYZ):
        pass




class waterdimer9D(Transform):
    '''
    One water is fixed (on the xz plane) with an hydrogen at its origin while the other water is free to move
    the q coordinates represent a sphere centered at the origin for the oxygen with another one
    centered at the oxygen for an hydrogen and the last qs give the difference between the hydrogens
    '''

    def q2cart(self, Q):
        XYZ = zeros((self.Atot, Q.shape[1], 3))

        for i in range(len(Q[:,0])):
            XYZ[0,i,0] = -0.7493682  
            #1 (i.e. an hydrogen) is kept at the origin
            XYZ[2,i,0] = -0.9356396
            XYZ[2,i,2] = -0.7258482

            XYZ[3,i,0] = Q[i,0]*cos(Q[i,1])*sin(Q[i,2]) 
            XYZ[3,i,1] = Q[i,0]*sin(Q[i,1])*sin(Q[i,2])
            XYZ[3,i,2] = Q[i,0]*cos(Q[i,2])
            
            XYZ[4,i,0] = XYZ[3,i,0] + Q[i,3]*cos(Q[i,4])*sin(Q[i,5])
            XYZ[4,i,1] = XYZ[3,i,1] + Q[i,3]*sin(Q[i,4])*sin(Q[i,5])
            XYZ[4,i,2] = XYZ[3,i,2] + Q[i,3]*cos(Q[i,5])

            XYZ[5,i,0] = XYZ[3,i,0] + (Q[i,3]+Q[i,6])*cos(Q[i,4]+Q[i,7])*sin(Q[i,5]+Q[i,8])     
            XYZ[5,i,1] = XYZ[3,i,1] + (Q[i,3]+Q[i,6])*sin(Q[i,4]+Q[i,7])*sin(Q[i,5]+Q[i,8])  
            XYZ[5,i,2] = XYZ[3,i,2] + (Q[i,3]+Q[i,6])*cos(Q[i,5]+Q[i,8])             


            XYZ[:,i,:] = self.Center(XYZ[:,i,:])

        return XYZ




    def cart2q(self, XYZ):
        Q = zeros((len(XYZ[0,:,0]), self.Dq))

        for i in range(len(XYZ[:,0,0])):
            Q[i,0] = sqrt(XYZ[3,i,0]**2 + XYZ[3,i,1]**2 + XYZ[3,i,2]**2)
            Q[i,1] = arctan(XYZ[3,i,1]/XYZ[3,i,0])
            Q[i,2] = arccos(XYZ[3,i,2]/Q[i,0])

            Q[i,3] = sqrt((XYZ[4,i,0]-XYZ[3,i,0])**2 + (XYZ[4,i,1]-XYZ[3,i,1])**2 + (XYZ[4,i,2]-XYZ[3,i,2])**2)
            Q[i,4] = arctan((XYZ[4,i,1]-XYZ[3,i,1])/(XYZ[4,i,0]-XYZ[3,i,0]))
            Q[i,5] = arccos((XYZ[4,i,2]-XYZ[3,i,2])/Q[i,3])

            Q[i,6] = -Q[i,3] + sqrt((XYZ[5,i,0]-XYZ[3,i,0])**2 + (XYZ[5,i,1]-XYZ[3,i,1])**2 + (XYZ[5,i,2]-XYZ[3,i,2])**2)
            Q[i,7] = -Q[i,4] + arctan((XYZ[5,i,1]-XYZ[3,i,1])/(XYZ[5,i,0]-XYZ[3,i,0]))
            Q[i,7] = -Q[i,5] + arccos((XYZ[5,i,2]-XYZ[3,i,2])/Q[i,6])
        return Q






class CH5(Transform):
    '''
    If reaction coordinates are used then Q scales the selected normal modes
    i.e. if N modes are chosen, the N lowest-eigenvalue-modes are used
    '''

    def q2cart(self, Q):
        XYZ = zeros((self.Atot, len(Q[:,0]), 3))

        if self.RC:
            for i in range(len(Q[:,0])):

                XYZ[:,i,:] = self.NormalModes.XYZ #Sets XYZ to initial geometry
            
                for j in range(self.Dq):
                    XYZ[:,i,:] += self.NormalModes.NM[:,j,:]*Q[i,j]
                
                #XYZ[:,i,:] = self.Center(XYZ[:,i,:])

        else:
            for i in range(len(Q[:,0])):
                #TODO MAKE AN APPROXIMATE RC

                XYZ[1,i,0] = 0.5541    
                XYZ[1,i,1] = 0.7996    
                XYZ[1,i,2] = 0.4965 
                XYZ[2,i,0] = 0.6833   
                XYZ[2,i,1] = -0.8134   
                XYZ[2,i,2] = -0.2536 
                XYZ[3,i,0] = -0.7782   
                XYZ[3,i,1] = -0.3735    
                XYZ[3,i,2] = 0.6692 
                XYZ[4,i,0] = -0.5593    
                XYZ[4,i,1] = 0.3874   
                XYZ[4,i,2] = -0.9121 
                XYZ[5,i,0] = -0.3593
                XYZ[5,i,1] = 0.3874
                XYZ[5,i,2] = -0.9121

                XYZ[:,i,:] = self.Center(XYZ[:,i,:])
       
 
        return XYZ







