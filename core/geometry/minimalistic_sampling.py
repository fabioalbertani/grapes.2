#!/bin/python
#File: core.geometry.minimalistic_sampling.py

import time
from copy import copy, deepcopy
from numpy import argsort, delete, insert, zeros, sum 
from numpy.random import shuffle

from sklearn.gaussian_process.kernels import ConstantKernel, RBF, WhiteKernel
from core.interpolation.gaussian_processes_regression import GaussianProcessRegressor

import core.grapes_utils.main_utils as UTILS
from core.grapes_utils.module_selection import SelectDataType
from core.grapes_utils.progress_bar import DisplayBar
from core.plotting.plot_interface import PlotInterface

from core.data_management import data_utils
from core.geometry.minimalistic_sampling_utils import GPReductor
from core.geometry.sampling import FramedGrid, Random


class MinimalisticSampling:

    def __init__(self, args):

        self.args = args

        self.kernel_ = RBF(args.interpolation.sigmaFit)
        self.kernel_ += WhiteKernel(args.interpolation.lambdaFit, noise_level_bounds=(1e-10, 
                                    args.interpolation.lambdaFit)) 

        self.SP = UTILS.SuperPrint('MinimalisticSampling.log', stdout=False)
        self.Info = {}
        self.timeout = time.time() + 24*60*60 #24h 


    def OptimiseTrainingSet(self, INTERPOLATION, Data, Molecule, RunOrganiser, CalcDetails):

        self.SP('\n\tSearching for minimalistic sampling for '+UTILS.GetLabel(CalcDetails)+'\n', 
                stdout=True)

        for el in Data.Components.GetElements(CalcDetails['Type']):
            self.SP('\n\tFinding minimalistic sampling for '+str(el)+' of '+\
                    UTILS.GetLabel(CalcDetails)+'\n', stdout=True)
            OptimisedData, key, OptimisedGP =\
                    self.CoreSelection(INTERPOLATION, CalcDetails, el)

            Data.Components.Clear(CalcDetails['Type'])
            Data.Components.AddData(OptimisedData)
            INTERPOLATION.InterpolationKeys[key] = deepcopy(OptimisedGP) 
            #do they need to be returned ? probably not

        #Can't really use the last J if the things have been switched around
        #RunOrganiser.RunInterface.LastJ = None



    def UpdateData(self, Data, Type):
        '''Returns all the updated Data'''
        self.SP('\n\tData structure updated with new optimal training set\n', stdout=True)
        Data.ClearComponent(Type)
        Data.CreateStructure(self.OptimisedData)




    def GetIdxtry(self, GP=None, Stype='variance'):
        '''
        Returns indices sorted given the Sorting type 
        '''
        if Stype=='random':
            idxtry = range(0,self.Info['Ni'])
            shuffle(idxtry)
            return idxtry

        elif Stype=='gradient':
            grads = self.GPGradient(GP)
            return argsort(grads)
        
        elif Stype=='variance':
            '''No Sense if point not removed!!! ''' #TODO TODO 
            var = self.GPVariance(GP)
            return argsort(var)

        


    def CoreSelection(self, INTERPOLATION, CalcDetails, el, method='GPReduction'): 
        '''
        Called by core.runners:
        Selects the GP and orders point using a selected method. When finished all previous 
        .dat/.pdf files are copied for comparison purposes
        '''

        keys = INTERPOLATION.KeyOrganiser.GetKey(CalcDetails, el)

        if len(keys)>1:
            raise Exception('Detrending a GP should be performed on a GP and not a composite'\
                            'GP or a bagged one')

        else:
            key = keys[0] #"unlists" the single key considered
      
            INTERPOLATION.InterpolationKeys[key].Get_K_inv()

            self.Info['Ni'] = len(INTERPOLATION.InterpolationKeys[key].X_train_[:,0])

            idxtry = self.GetIdxtry(GP=INTERPOLATION.InterpolationKeys[key], 
                                    Stype='random')
            self.SP('\nSorted points for detrending\n' + str(idxtry))
            indices, newGP = self.Loop(INTERPOLATION, idxtry, key, method)

            DataR = data_utils.Process(self.args.main)
            DataR.FeedFinalData(newGP.X_train_, {'Energy': newGP.y_train_ + newGP._y_train_mean})

            self.Info['Nf'] = len(newGP.X_train_)
       
            print('\n')
            return DataR.Return(), key, newGP





    def Loop(self, INTERPOLATION, idxtry, key, method, MinPoints=100, MaxTry=50000):
        '''
        Loops through the points (in the order given by idxtry) and tries to reduce the sampling

        Stops if: 
            - only MinPoints remain (this would however be a result from a too flexible 
              etching process)
            - MaxTry GP reduction have been performed
            - self.timeout has been reached (2h)
        '''

        PB = DisplayBar(self.Info['Ni'], extrastr='of points removed')
        GPR = GPReductor()

        #The borders are less sampled and strongly affect the RSMD
        if method=='GPReduction':
            QTest = Random(self.args.main, N=200)
        elif method=='TestSetDifference':
            QTest = Random(self.args.main, N=200) 
      
        Try=0
        GP = INTERPOLATION.InterpolationKeys[key]
        GPR.load(GP)
        self.SP('\n\tStarting detrending process ...\n')

        indicesRM = []

        while len(idxtry)>MinPoints:

            for po in idxtry:
                PB.Show(len(indicesRM))
                Try += 1
                if not idxtry[po] in indicesRM:
           
                    if method=='GPReduction':
                        success, GPR = self.GPReduction(GPR, QTest, Try%100==0, po)                
                        
                    elif method=='TestSetDifference':
                        success, GPR = self.TestSetDifference(GPR, QTest, Try%100==0, po)

                    else:
                        raise RuntimeError('Minimalistic sampling method unknown '+method+\
                                           ': stopping run\n')

                    if success: 
                        indicesRM.append(idxtry[po])
                        idxtry = [i-1 if i>idxtry[po] else i for i in idxtry]
                        idxtry = delete(idxtry, po)
                        break

                    else:
                        if po==len(idxtry)-1:
                            return indicesRM, GPR.Interpolation
                        elif Try>MaxTry:
                            return indicesRM, GPR.Interpolation

                        else:
                            GPR.reload()

        return indicesRM, GPR.Interpolation




    def GPReduction(self, GPR, QTest, reoptimiseHP, po, tolVariance=1e-6):

        GPR.reduce(po, method='Woodbury', reoptimiseHP=reoptimiseHP)

        Variance = GPR.Variance(QTest)
        self.SP('\nindex -> '+str(po)+'\n\tVariance: '+str(Variance)+'\n')


        if Variance < tolVariance:
            self.SP('\n\t\t!Point  '+str(po)+' removed (RMSD between GP+ and GP-: '+\
                          str(Variance)+')\n')
            newGP = GPR.Return()
            GPR.load(newGP)
            return True, GPR
        
        else:
            return False, GPR




    def TestSetDifference(self, GPR, QTest, reoptimiseHP, po, tolRMSD=1e-4):

        GPR.reduce(po, method='Woodbury', reoptimiseHP=reoptimiseHP)

        RMSD = GPR.RMSD(QTest)
        self.SP('\nindex -> '+str(po)+'\n\tRMSD: '+str(RMSD)+'\n')


        if RMSD < tolRMSD:
            self.SP('\n\t\t!Point  '+str(po)+' removed (RMSD between GP+ and GP-: '+str(RMSD)+')\n')
            UpdatedGPR = GPR.Return()
            GPR.load(UpdatedGPR)
            return True, GPR

        else:
            return False, GPR





    def GPGradient(self, GP):
        '''
        Used to sort points by the gradient
        '''
        grad = GP.gradient_predict(GP.X_train_)
    
        for i in xrange(len(grad[:,0])):
            grad[i,:] = grad[i,:]*grad[i,:]        

        return sum(grad, axis=1)


    def GPVariance(self, GP):
        '''
        Used to sort points by the model uncertainty
        !! Does it make sense on the training set ? !!
        '''
        return GP.predict(GP.X_train_, return_std=True)[1]
    




    def getTestingSet(self, CalcDetails, N=20, random=False):
        '''
        Select N random points to use as a testing set
        '''

        if random:
            from numpy.random import randint
            indices = randint(0, self.Info['Ni'], N)
        else:
            indices = zeros(N)
            indices[:int(N/2)] = argsort(getattr(self.Data.Energy, CalcDetails['label'])[:,0])\
                                 [:int(N/2)] #selects some minima points
            indices[int(N/2):] = argsort(getattr(self.Data.Energy, CalcDetails['label'])[:,0])\
                                 [-int(N/2):]
            
            indices = [int(i) for i in indices]

        return {'Indices': indices,
                'Points': getattr(self.Data, 'Delaunay').points[indices,:],
                'Energy': getattr(self.Data.Energy, CalcDetails['label'])[indices,:]}


        
    def ReturnGen(self, indices):

        try:
            gentot = 0

            for i in xrange(len(self.generations)):
                keep = []
                for j in xrange(len(indices)):

                    try:
                        #Only finds the index if it appears in the list
                        keep.append(self.generations[i].index(indices[j]))
                    except ValueError:
                        pass

                self.generations[i] = [gentot+idx for idx,n in enumerate(keep)] 
                gentot += len(self.generations[i])

            return self.generations

        except:
            return None




    def PrintInfo(self):

        Template = '''

            Initial  Points:            {Ni}
            Final Points:               {Nf}
            GP Score:                   {Rf} <- {Ri}

                   '''
        print(Template.format(**self.Info))




#----------------------------------------------------------------------------------- OTHER FUNCTIONS

    def SavePreviousData(self, CalcDetails): 
        from shutil import copyfile
        try:
            copyfile(self.args.main.CDPath+'/SavedData/'+self.args.main.Label+\
                     '/MeshInformation_'+UTILS.GetDataLabel(CalcDetails)+'.qdat',
                     self.args.main.CDPath+'/SavedData/'+self.args.main.Label+\
                     '/MeshInformation_'+UTILS.GetDataLabel(CalcDetails)+'_deprecated.qdat')
        except IOError:
            pass
        try:
            copyfile(self.args.main.CDPath+'/SavedData/'+self.args.main.Label+\
                     '/GPInformation_'+UTILS.GetDataLabel(CalcDetails)+'.qdat',
                     self.args.main.CDPath+'/SavedData/'+self.args.main.Label+\
                     '/GPInformation_'+UTILS.GetDataLabel(CalcDetails)+'_deprecated.qdat')
        except IOError:
            pass
        try:
            copyfile(self.args.main.CDPath+'/SavedData/'+self.args.main.Label+\
                     '/Contours_'+UTILS.GetDataLabel(CalcDetails)+'.pdf',
                     self.args.main.CDPath+'/SavedData/'+self.args.main.Label+\
                     '/Contours_'+UTILS.GetDataLabel(CalcDetails)+'deprecated.pdf')
        except IOError:
            pass
        try:
            copyfile(self.args.main.CDPath+'/SavedData/'+self.args.main.Label+\
                     '/Confidence_'+UTILS.GetDataLabel(CalcDetails)+'.pdf',
                     self.args.main.CDPath+'/SavedData/'+self.args.main.Label+\
                     '/Confidence_'+UTILS.GetDataLabel(CalcDetails)+'deprecated.pdf')
        except IOError:
            pass
        try:
            copyfile(self.args.main.CDPath+'/SavedData/'+self.args.main.Label+\
                     '/Surface_'+UTILS.GetDataLabel(CalcDetails)+'.pdf',
                     self.args.main.CDPath+'/SavedData/'+self.args.main.Label+\
                     '/Surface_'+UTILS.GetDataLabel(CalcDetails)+'deprecated.pdf')
        except IOError:
            pass

