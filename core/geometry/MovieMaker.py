


class Main:


    def __init__(self, args, Data, Molecule):
        from numpy import sort

        f=open("%s/SavedData/%s/MoleculeMovie.xyz" %(args['Main'].CDPath, args['Main'].Label), 'w')


        if args['Main'].MeshType=='Delaunay':
            Q = getattr(Data, 'Delaunay').points

        else:
            Q = getattr(Data, 'Points')
        
        XYZ = Molecule.q2cart(sort(Q, axis=0))


        for step in xrange(len(Q[:,0])):
            f.write(str(len(args['Main'].AtomsList))+'\n1D\n')

            for atom in xrange(len(args['Main'].AtomsList)):
                f.write('\t'+args['Main'].AtomsList[atom])

                for i in xrange(3):
                    f.write('\t'+str(XYZ[atom,step,i]))

                f.write('\n')


        f.close()


