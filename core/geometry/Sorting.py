#!/bin/python



#Input points are sorted to allow calculations to be performed with starting orbitals
#   -function WF: points are sorted by a weight function 
#   -function NN: points are sorted by their distance to the vertices (the returned array
#    represent the indices of the nearest neighbours)


def InputFormatWF(Q, alpha=100.0):

    from numpy import zeros, argsort

    weights = zeros(len(Q[0,:]))
    weights[0] = 1

    for i in xrange(len(weights)-1): weights[i+1] = weights[i]*alpha

    sorting_function = lambda X: sum(weights*X)

    indices = zeros(len(Q[:,0]))
    for i in xrange(len(indices)):
        indices[i] = sorting_function(Q[i,:])


    Q = Q[argsort(indices),:]

    return Q




class InputFormatNN:

    def __init__(self, D, Label="H3", dir1='/Work/PotentialEnergySurfaces/MeshInterpolation/QChemistryModule/ControlSurface/', dir2='RunSCRATCHCopy/', 
                 EnergyFile='MeshNodesEnergy.log', scratchName='P.scratch'):

        self.D = D

        from numpy import genfromtxt, asfortranarray
        import getpass

        try:
            DATA = genfromtxt("%s" %('/home/'+getpass.getuser()+dir1+Label+'Data/'+dir2+EnergyFile), unpack=True)
        
            self.Vertices = asfortranarray(DATA[:self.D,:].T)
            self.NVertices = len(self.Vertices[:,0])

            self.scratch = ["%s" %(Label+'SCRATCH/'+str(i+1)+scratchName) for i in xrange(self.NVertices) ]

        except:
            raise RuntimeError('A control mesh has not been calculated InputFormatNN cannot be used')
    

    def Format(self, N, Q):

        from ..FORTRANModules.FORTRANFunctions import distances
        from numpy import zeros, asfortranarray
        indices = zeros(N) 

        for i in xrange(N):
            Dv = distances.tomeshnodes(self.Vertices, asfortranarray(Q[i,:].flatten()), self.D, self.NVertices)
            
            which = min(xrange(len(Dv)), key=Dv.__getitem__)
            indices[i] = which 

        return [self.scratch[i] for i in indices.astype(int)]

