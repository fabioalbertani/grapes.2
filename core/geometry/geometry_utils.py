#!/bin/python
#File: core.geometry.geometry_utils.py

from numpy import array, asarray, identity, zeros
from numpy import arccos, sort, cross, dot, matmul
from math import sin, cos, sqrt, pi

from copy import copy
from scipy import linalg



def rotation_matrix(v1, v2):
    '''defines the matrix to rotate v1 onto v2'''
    R1 = zeros((3,3))
    v = cross(v1/linalg.norm(v1), v2/linalg.norm(v2))
    #s = linalg.norm(v)
    c = dot(v1/linalg.norm(v1),v2/linalg.norm(v2))
    R1[1,0] = v[2]
    R1[2,0] = -v[1]
    R1[2,1] = v[0]
    R1[0,1] = -R1[1,0]
    R1[0,2] = -R1[2,0]
    R1[1,2] = -R1[2,1]

    if c==-1:
        return identity(3)

    else:
        return identity(3) + R1 + matmul(R1,R1)/(1+c)


def rotation_matrix_projonxy(v1):
    v2 = copy(v1)
    v2[2] = 0.0
    return rotation_matrix(v1,v2)


def rotation_matrix_xrot(v1):
    R1 = identity(3)
    v = copy(v1)
    v[0] = 0
    R1[1,1] = cos(2*pi-arccos(dot(v, asarray([0.0,1.0,0.0]))/linalg.norm(v)))
    R1[2,2] = R1[1,1]
    R1[2,1] = sin(2*pi-arccos(dot(v, asarray([0.0,1.0,0.0]))/linalg.norm(v)))
    R1[1,2] = -R1[2,1]
    return R1

def rotate(XYZ, R):
    ''' The rotation matrix is suplied '''
    if XYZ.ndim==3:
        for sample in range(XYZ.shape[1]):
            for atom in range(XYZ.shape[0]):
                XYZ[atom,sample,:] = dot(R,XYZ[atom,sample,:])

    else:
        for atom in range(XYZ.shape[0]):
            XYZ[atom,:] = dot(R,XYZ[atom,:])

    return XYZ 




def GetMasses(AtomsList):

    Masses = {
        'H': 1.00784,
        'D': 2.01355,
        'He': 4.002602,
        'C': 12.0000,
        'N': 14.0067,
        'O': 15.999,
        'F': 18.998403,
        'Al': 26.981539,
        'P': 30.973762,
        'Cl': 35.453
             }

    return [Masses[s] for s in AtomsList]


def GetReducedMass(args, masses):

    if args.CoordinatesTransform=='diatomic':
        Mass = sum(masses)/2
    elif args.CoordinatesTransform=='triatomic':
        Mass = sum(masses)/3  
        #completely wrong .... check it 
    return Mass





class GeometryTransformations:

    def __init__(self, masses):
        self.masses = masses

        self._orientation0 = asarray([1.0,0.0,0.0])
        self._orientation1 = asarray([0.0,1.0,0.0])



    def center(self, XYZ, C=None):
        '''
        Finds the COM of the molecule and shifts the whole geometry to set it at the origin or 
        shifts the whole molecule so that the given atom is at the given position
        
        C is a tuple (atom, x, y, z) 
        '''
        if C is None:     
            if XYZ.ndim==3:
                for sample in range(XYZ.shape[1]):
                    for d in range(3):
                        shift = XYZ[:,sample,d].dot(self.masses)
                        XYZ[:,sample,d] -= shift/sum(self.masses)
 
            else:
                for d in range(3):
                    shift = XYZ[:,d].dot(self.masses)
                    XYZ[:,d] -= shift/sum(self.masses)
        
        else:
            if XYZ.ndim==3:
                for sample in range(XYZ.shape[1]):
                    for d in range(3):
                        shift = XYZ[C[0],sample,d] - C[d+1]
                        XYZ[:,sample,d] -= shift
 
            else:
                for d in range(3):
                    shift = XYZ[C[0],d] - C[d+1]
                    XYZ[:,d] -= shift

        return XYZ



    def align(self, XYZ):
        '''
        Aligns the first vector (atom0-atom1) and the vertical projections 
        '''
        if XYZ.ndim==3:
            #print 'example sample thingy\n', XYZ[:,0,:]
            #print 'vec diff', XYZ[0,0,:], XYZ[1,0,:], XYZ[0,0,:]-XYZ[1,0,:]
            for sample in range(XYZ.shape[1]):
                R = rotation_matrix(XYZ[0,sample,:]-XYZ[1,sample,:], self._orientation0)
                for atom in range(len(self.masses)):
                    XYZ[atom,sample,:] = dot(R,XYZ[atom,sample,:])

                p = XYZ[1,sample,:] - XYZ[2,sample,:]
                p[0] = 0.0 #Projects the vector on the yz plane
                R = rotation_matrix(p, self._orientation1)
                for atom in range(len(self.masses)):
                    XYZ[atom,sample,:] = dot(R,XYZ[atom,sample,:])

        else:
            R = rotation_matrix(XYZ[0,:]-XYZ[1,:], self._orientation0)
            for atom in range(len(self.masses)):
                XYZ[atom,:] = dot(R,XYZ[atom,:])

            p = XYZ[1,:] - XYZ[2,:]
            p[0] = 0.0 #Projects the vector on the yz plane
            R = rotation_matrix(p, self._orientation1)
            for atom in range(len(self.masses)):
                XYZ[atom,:] = dot(R,XYZ[atom,:])

        return XYZ


    def align2D(self, XYZ, B=None):
        '''Reduced version that skips the projected vector alignment '''
        if B is None:
            if XYZ.ndim==3:
                for sample in range(XYZ.shape[1]):
                    R = rotation_matrix(XYZ[0,sample,:]-XYZ[1,sample,:], self._orientation0)
                    for atom in range(len(self.masses)):
                        XYZ[atom,sample,:] = dot(R,XYZ[atom,sample,:])

            else:
                R = rotation_matrix(XYZ[0,:]-XYZ[1,:], self._orientation0)
                for atom in range(len(self.masses)):
                    XYZ[atom,:] = dot(R,XYZ[atom,:])
        else:
            if XYZ.ndim==3:
                for sample in range(XYZ.shape[1]):
                    R = rotation_matrix(XYZ[B[0],sample,:]-XYZ[B[1],sample,:], self._orientation0)
                    for atom in range(len(self.masses)):
                        XYZ[atom,sample,:] = dot(R,XYZ[atom,sample,:])

            else:
                R = rotation_matrix(XYZ[B[0],:]-XYZ[B[1],:], self._orientation0)
                for atom in range(len(self.masses)):
                    XYZ[atom,:] = dot(R,XYZ[atom,:])
        return XYZ







def ReadXYZ(args, GT):
    '''Reads the Geometry given in the input files and returns it as an centered XYZ'''

    f=open(args.InputFileName, 'r')
    l=f.readline()
    found = False

    while l!='':
        if l.split()==[]:
            l=f.readline()

        elif l.split()[0]=='geometry':
            found = True
            for i in range(len(args.AtomsList)):
                l = f.readline()
                if i==0:
                    XYZ = '\t'.join(l.split()[0:]) + '\n'
                    XYZtemp = '\t'.join(l.split()[1:]) + '\n'
                else:
                    XYZ += '\t'.join(l.split()[0:]) + '\n'
                    XYZtemp += '\t'.join(l.split()[1:]) + '\n'


            XYZ = array([[float(j) for j in i.split('\t')] for i in XYZtemp.splitlines()])
            XYZ = GT.align(GT.center(XYZ))
            
        else:
            l=f.readline()


    if found:
        return XYZ
    else:
        return None




class NormalModes:
    '''
    Calculates the normal modes of the molecule
    '''
    def __init__(self, args, GT, direct_invert=True):

        print('\n\tFinding Normal modes:\n')
        self.direct_invert=direct_invert

        self.XYZ = ReadXYZ(args, GT)
        if self.XYZ is None:
            raise IOError('\t-geometry- keyword not found in file '+args.InputFileName+'\n')    

        from  numpy import asarray
        
        self.RunFreq(args.AtomsList, len(args.AtomsList), args.Charge, args.Spin)

        print('\n\tLowest eigenvalue is ', self.eigvalues_[0], 'cm-1\n')
        self.PrintNM(len(args.AtomsList), args.AtomsList)
        self.PrintNMSingle(len(args.AtomsList), args.AtomsList, 0)
        self.PrintNMSingle(len(args.AtomsList), args.AtomsList, 1)



    def PrintNM(self, Atot, AtomsList):
        '''
        Prints all normal modes on XYZ
        '''
        fout = open('NormalModes.log', 'w')

        fout.write('TS Geometry:\n')
        for atom in range(Atot):
            fout.write(AtomsList[atom]+'\t'+'\t'.join([str(j) for j in self.XYZ[atom,:]])+'\n')

        fout.write('\nNormal Modes:\n')
        for ii in range(3*Atot):
            fout.write('NM '+str(ii+1)+':\tfreq '+str(self.eigvalues_[ii])+' cm-1'+'\n')
            for atom in range(Atot):
                fout.write(AtomsList[atom]+'\t'+'\t'.join([str(j) for j in self.eigvectors_[ii,:]])+'\n')

        fout.close()


    def PrintNMSingle(self, Atot, AtomsList, NMi, steps=100):
        '''
        Prints a few steps of a given normal mode in a VMD-friendly way
        '''
        fout = open("NormalModes_NM%i.log" %NMi, 'w')

        for step in range(-steps/2,steps/2+1):

            XYZ = self.XYZ.flatten() + (2.0*step/steps)*self.eigvectors_[NMi,:]

            fout.write(str(Atot)+'\nrandomtitle\n')
            for atom in range(Atot):
                fout.write('\t'+AtomsList[atom]+'\t'+'\t'.join([str(j) for j in XYZ[3*atom:3*atom+3]])+'\n')

        fout.close()




    def RunFreq(self, Atoms, Atot, charge=0, spin=1, scfError=9, mass_weighted=True):
        '''
        Calls QChem to run a freg job from a given TS geometry (in the input file)
        and reads in the resulting normal modes
        '''
        from subprocess import call

        self.eigvectors_ = zeros((3*Atot, 3*Atot))
        self.eigvalues_ = zeros(3*Atot)
        template = "$molecule\n%i %i\n" %(charge,spin) 
        for at in range(Atot): template += '\t'+Atoms[at]+'\t'+'\t'.join([str(x) for x in self.XYZ[at,:]])+'\n'
        template += """
$end
$rem
    jobtype         freq
    vibman_print    6
    exchange        hf
    basis           6-31g              
    scf_convergence %i
$end
                    """ %(scfError)
        with open('inputFREQ', 'w') as f:
            f.write(template)

        with open('subprocess.log', 'a') as out:
            call('qchem -nt 12 inputFREQ outputFREQ', shell=True, stdout=out)

        if self.direct_invert:
            self.H = zeros((3*Atot, 3*Atot))

            with open('outputFREQ', 'r') as f:
                l = f.readline()

                while l!='':

                    if l.split()==[]:
                        l = f.readline()

                    elif 'Hessian of the SCF Energy' in l and not mass_weighted:
                        counter = 0
                        l=f.readline()
                        while counter<3*Atot:
                            l=f.readline()
                            for at in range(3*Atot):
                                for mode in range(len(l.split())-1): self.H[at, counter+mode] = float(l.split()[mode+1])
                                l=f.readline()
                            counter += 6
                    
                    elif 'Mass-Weighted Hessian Matrix:' in l and mass_weighted:
                        counter = 0
                        l=f.readline()
                        while counter<3*Atot:
                            for i in range(2): l=f.readline()
                            for at in range(3*Atot):
                                for mode in range(len(l.split())-1): self.H[at, counter+mode] = float(l.split()[mode+1])
                                l=f.readline()
                            counter += 6                


                    else:
                        l = f.readline()

            print 'inverting H of shape ', self.H.shape
            self.eigvalues_, self.eigvectors_ = linalg.eig(self.H)
            self.eigvectors_inv = linalg.inv(self.eigvectors_)
            for i in range(9):
                print self.eigvectors_[:,i], '\nwith associated eignevalues:\n', self.eigvalues_[i]



        else:
            with open('outputFREQ', 'r') as f:
                l = f.readline()

                while l!='':

                    if l.split()==[]:
                        l = f.readline()

                    elif 'Vectors for Translations and Rotations' in l:
                        for i in range(3): l=f.readline()
                        for at in range(3*Atot):
                            for m in range(6):
                                self.eigvectors_[3*Atot-6+m, at] = float(l.split()[m])
                            l=f.readline()
                        self.eigvalues_[3*Atot-6:] = 0.0

                    elif 'Mode' in l:
                        counter = 0
                        while counter<3*Atot-6:
                            l=f.readline()
                            for mode in range(len(l.split())-1): self.eigvalues_[counter+mode] = float(l.split()[mode+1])
                            for i in range(7): l=f.readline()
                            for at in range(Atot):
                                for mode in range(len(l.split())-1):
                                    self.eigvectors_[at, counter+mode] = float(l.split()[mode+1])
                                l=f.readline()
                            counter += 3

                    else:
                        l = f.readline()
            

            from numpy.linalg import inv
            from numpy import dot
            self.eigvectors_inv = inv(self.eigvectors_)
            self.NM0 = dot(self.eigvectors_, self.XYZ.flatten())[:3*Atot-6]
            self.transrot = dot(self.eigvectors_, self.XYZ.flatten())[3*Atot-6:]


            for i in range(3):
                print 'mode '+str(i), self.eigvectors_[:,i]


            from numpy import asarray
            from copy import copy
            GT = GeometryTransformations(asarray([15.99, 1.0, 1.0]))

            from numpy import dot
            a = dot(self.eigvectors_, self.XYZ.flatten())
            tr = copy(a[3:])
            print 'XYZ', self.XYZ.flatten()
            print 'XYZ->NM  proj ', a
            print 'NM->XYZ proj ', dot(self.eigvectors_inv, a)
            a[3:] = 0.0
            print 'NM(red)->XYZ proj ', GT.align(GT.center(dot(self.eigvectors_inv, a).reshape(3,3)))
            eqgeom = asarray([0.0,0.0,0.0]+list(tr))
            print 'eqgeom in NM ', eqgeom, tr, list(tr)
            print 'NMeq->XYZ proj ', dot(self.eigvectors_inv, eqgeom)
            print 'NMeq->XYZ proj+center+relaign ', GT.align(GT.center(dot(self.eigvectors_inv, eqgeom).reshape(3,3)))
            print 'proj matrix\n', self.eigvectors_, '\ninv_one\n', self.eigvectors_inv

            b = self.XYZ.flatten() + 0.2*self.XYZ.flatten()
            b = GT.align(GT.center(b.reshape(3,3)))
            print 'mod vector ', b
            bnm = dot(self.eigvectors_, b.flatten())
            print 'XYZ -> NM ', bnm
            print 'NM -> XYZ ', dot(self.eigvectors_inv, bnm)
            print 'transrot diff: ', self.transrot, bnm[3:]        







def GetHessian(args, CalcDetails, XYZ_in=None):
    '''Returns the Hessian'''
    from subprocess import call

    GT = GeometryTransformations(GetMasses(args.AtomsList))

    if XYZ_in is None:
        XYZ_in = ReadXYZ(args, GT)                                                                                                                             
                                                                                                                                                           
    options=dict(exchange=args.exchange, basis_set=CalcDetails['basis_set'])                                                                    
                                                                                                                                                           
    template = '$molecule\n\t'+str(args.Charge)+'\t'+str(args.Spin)+'\n'                                                                                   
                                                                                                                                          
    for atom in range(len(args.AtomsList)):                                                                                                                     
        template += '\t'+args.AtomsList[atom]+'\t'+'\t'.join([str(x) for x in XYZ_in[atom,:]])+'\n'                                                        
    template += '$end'                                                                                                                                     
                                                                                                                                                           
    template += '''                                                                                                                                        
$rem                                                                                                                                                       
    jobtype         freq
    vibman_print    6                                                                                                             
    exchange        {exchange}                                                                                                                           
    basis           {basis_set}
    scf_convergence 10                                                                                                                  
$end                                                                                                                                                       
                '''                                                                                                                              
                                                                                                                                                           
    f = open('inputFREQ', 'w')                                                                                                                              
    f.write(template.format(**options))                                                                                                                    
    f.close()                                                                                                                                              
                                                                                                                                                           
    with open('subprocess.log', 'a') as out:                                                                                                               
        call('qchem -nt 12 inputFREQ outputFREQ', shell=True, stdout=out)                                                                                     
                                                                                                                                                           
    H = zeros((3*len(args.AtomsList),3*len(args.AtomsList)))                                                                                                  
                                                                                                                                                           
    with open('outputFREQ', 'r') as f:                                                                                                                      
        l=f.readline()                                                                                                                                     
                   
        while l!='':                                                                                                                                        
            if l.split()==[]:
                l = f.readline()

            elif 'Hessian of the SCF Energy' in l:
                counter = 0
                l=f.readline()
                while counter<3*len(args.AtomsList):
                    l=f.readline()
                    for atom in range(3*len(args.AtomsList)):
                        for mode in range(len(l.split())-1): H[atom, counter+mode] = float(l.split()[mode+1])
                        l=f.readline()
                    counter += 6 

                return H

            else:
                l=f.readline()

        return None






def OptimiseGeometry(args, CalcDetails):
    '''Returns an optimised geometry at the desired level of accuracy '''
    from subprocess import call

    GT = GeometryTransformations(GetMasses(args.AtomsList)) 
    
    XYZ_in = ReadXYZ(args, GT)

    options=dict(exchange=args.exchange, basis_set=CalcDetails['basis_set'])

    template = '$molecule\n\t'+str(args.Charge)+'\t'+str(args.Spin)+'\n'

    for atom in range(len(args.AtomsList)):
        template += '\t'+args.AtomsList[atom]+'\t'+'\t'.join([str(x) for x in XYZ_in[atom,:]])+'\n'
    template += '$end'

    template += '''
$rem
    jobtype     opt
    geom_opt_max_cycles 200

    exchange    {exchange}
    basis       {basis_set}

    scf_convergence 10
$end
                '''

    f = open('inputOPT', 'w')
    f.write(template.format(**options)) 
    f.close()

    with open('subprocess.log', 'a') as out:
        call('qchem -nt 12 inputOPT outputOPT', shell=True, stdout=out)            

    XYZ_out = zeros((len(args.AtomsList),3))

    with open('outputOPT', 'r') as f:
        l=f.readline()

        while l!='':
            if l.split()==[]:
                l=f.readline()

            elif l.split()[1:3] == ['OPTIMIZATION','CONVERGED']:
                for i in range(5): l=f.readline()
                for atom in range(len(args.AtomsList)):
                    for d in range(3): XYZ_out[atom,d] = float(l.split()[2+d])
                    l=f.readline()

                return GT.align(GT.center(XYZ_out))

            else:
                l=f.readline()
    

        return None



