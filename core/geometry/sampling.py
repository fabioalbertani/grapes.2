#!/bin/python
#File: core.geometry.sampling.py

from numpy import zeros, linspace, ones, random, asfortranarray, append


def Random(args=None, N=None):
    '''
    Creates uniformly and random samples within the limits set
    by the passed arguments
    '''
    if N is None: N = args.NSampling

    Q = zeros((N, args.D))

    for i in range(args.D):
        Q[:,i] = (-args.GridLimits[2*i]+args.GridLimits[2*i+1])*random.rand(N) +\
                   args.GridLimits[2*i]*ones(N)

    return Q



def FramedGrid(args=None, N=None, limits_shift=0.0):
    '''
    Frames space and produces a regular grid
    '''
    if N is None:
        N = args.NSampling

    Q = zeros((int(N**args.D), args.D))
    dims = zeros(args.D)
    Qsteps = []

    for dim in range(args.D):
        Qsteps += [linspace(args.GridLimits[2*dim]-limits_shift,\
                   args.GridLimits[2*dim+1]+limits_shift, N)]


    def GridPoint(i, j, Qt):
        '''
        recursive function to produce the grid
        '''   

        if j==args.D:
            return Qt

        else:
            if (i+1)%(N**j)==0 and i!=0:
                if dims[j]==N-1:
                    dims[j] = 0
                else:
                    dims[j] += 1
        
            Qt[j] = Qsteps[j][int(dims[j])] 
            j += 1
            return GridPoint(i, j, Qt)



    for i in range(N**args.D):
        Q[i,:] = GridPoint(i, 0, Q[i,:])


    return Q





def FramedRandom(args=None, N=None, NBorder=10):
    '''
    Frames space and creates homogenously distributed samples within it
    '''
    if N is None: N = args.NSampling

    Q = zeros((N, args.D))

    NBorder = 2**args.D

    Q[:NBorder,:] = BorderSampling(args.GridLimits, NBorder, args.D, False)
    for i in range(args.D):
        Q[NBorder:,i] = (-args.GridLimits[2*i]+args.GridLimits[2*i+1])*\
        random.rand(args.NSampling-NBorder) + args.GridLimits[2*i]*ones(args.NSampling-NBorder)

    return Q
    




def chunkIt(seq, num):
    '''
    Chunks a list in num roughly similar sized lists
    '''
    avg = len(seq) / float(num)
    out = []
    last = 0.0

    while last < len(seq):
        out.append(seq[int(last):int(last + avg)])
        last += avg

    return out



def FramedGaussian(args=None, N=None, NBorder=10, mu=1.0, mushift=0.1, sigma=0.1):
    '''
    Frames space and samples with a gaussian distribution with specified mu and sigma

    not very useful for general cases
    '''
    if N is None: N = args.NSampling

    Q = zeros((N, args.D))

    NBorder = 2**args.D

    Q[:NBorder,:] = BorderSampling(args.GridLimits, NBorder, args.D, False)

    bits = chunkIt(range(NBorder, N), 3*args.D) #Three "lines" per degree of freedom are created
    for d in range(args.D):
        for i in range(-1,2):
            for j in range(args.D):
                if j!=d:
                    Q[bits[3*d+i],j] = random.uniform(args.GridLimits[2*j], args.GridLimits[2*j+1], 
                                                      len(bits[3*d+i]))    
                else:
                    Q[bits[3*d+i],j] = (mu+mushift*i)*ones(len(bits[3*d+i])) + random.normal(0, 
                                        sigma, len(bits[3*d+i]))
    return Q



def BorderSampling(GridLimits, N, D, UseSymmetry, shift=0.0):
    '''
    Samples along the limits
    '''
    Q=zeros((2**D,D))

    arr = []
    for i in range(D):
        arr.append([GridLimits[2*i], GridLimits[2*i+1]])

    indices = [0 for i in range(D)]

    j=0
    while (1):

        for i in range(D):
            Q[j,i] = arr[i][indices[i]]
            if i==(D-1): j += 1

        nexti = D-1
        while (nexti>=0) and (indices[nexti]+1>=len(arr[nexti])):
            nexti -= 1

        if nexti < 0: break

        indices[nexti] += 1
        for i in range(nexti+1, D):
            indices[i] = 0

    return Q
            
