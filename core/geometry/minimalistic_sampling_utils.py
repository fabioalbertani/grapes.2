#!/bin/python
#File: core.geometry.minimalistic_sampling_utils.py

from copy import copy, deepcopy
from functools import reduce

from numpy import eye, zeros, delete, matmul
from numpy import sqrt, mean

from numpy.linalg import inv

from core.auxiliary.woodbury import get_sub_training_set, get_subinverse_matrix
from core.interpolation.hyperparameters_utils import setHP, getHP



class GPReductor:

    def __init__(self):

        self.Interpolation = None
        self._Interpolation = None


    def load(self, Interpolation):
        '''
        Loads up two versions of the passed GP to allow the comparison between the reduced
        one and the original one
        '''
        if not self.Interpolation is None:
            del self.Interpolation
        if not self._Interpolation is None:
            del self._Interpolation

        self.Interpolation = deepcopy(Interpolation)
        self._Interpolation = deepcopy(Interpolation)


    def reload(self):
        '''Restores the reduced GP to the original one'''
        self._Interpolation = deepcopy(self.Interpolation)


    def Return(self):
        '''
        Returns the reduced interpolation scheme (nb: most of the time it has been 
        reloaded and self._Interpolation is a copy of self.Interpolation)
        '''
        return self._Interpolation



    def reduce(self, swap_index, method='Woodbury', reoptimiseHP=False):
        '''
        Swaps the given index to the position 0 and then removes the point from the 
        X_train_ and y_train_ 

        Methods:
        Woodbury: uses the woodbury identity to calculate the new inverse matrix (the dual 
                  coefficients are then updated by matrix multiplication)
        Cholesky: recalculates the covariance matrix, the Cholesky factor and the dual coefficients
        '''
        #Updates the training set:
        #   swap row in X_train_ and y_train_ (nb: new K will already be correctly swapped)
       
        K = self._Interpolation.kernel_(self._Interpolation.X_train_)
        self._Interpolation.define_inverse_matrix()
        K_inv = self._Interpolation._K_inv  
        X_train_, y_train_ = get_sub_training_set(self._Interpolation.X_train_, 
                             self._Interpolation.y_train_ + self._Interpolation._y_train_mean, 
                             swap_index)

        self._Interpolation.load_training_set(X_train_, y_train_)


        if method=='Woodbury':
            '''
            KWood = K+ - UV  -- Woodbury formula -- (see get_UV for KWood definition)
            => KWood(i) = K+(i) + K+(i)U (I-VK+(i)U)(i) VK+(i) and K-(i) is the n-1 x n-1 submatrix
            '''
            _K, _K_inv = get_subinverse_matrix(K, K_inv, swap_index)
            self._Interpolation._K_inv = copy(_K_inv)
            self._Interpolation.alpha_ = matmul(self._Interpolation._K_inv, 
                                                self._Interpolation.y_train_)

        elif method=='Cholesky':
            '''
            Refit the procedure but do not optimise the hyperparameters
            Recalculating the inverse matrix is not extremely expensive and can be done
            '''
            self._Interpolation.optimise_hyperparameters = False
            self._Interpolation.fit()

        else:
            '''A direct inversion is performed on the reduced new covariance matrix '''
            self._Interpolation._K_inv = inv(K[1:,1:])
            self._Interpolation.alpha_ = matmul(self._Interpolation._K_inv, 
                                                self._Interpolation.y_train_)

   
        if reoptimiseHP:
            '''
            Reoptimises the hyperparameters from the self.Interpolation one
            a single loop is performed to keep the cost down
            '''
            self._Interpolation.optimise_hyperparameters = True
            self._Interpolation.n_restarts_optimizer = 1
            
            self._Interpolation.fit(None, None)
            
            self._Interpolation.optimise_hyperparameters = False


    def RMSD(self, Q):
        '''
        Calculates the Root Square Mean Difference

        if the dual coefficients are not recalculated in reduce then the solve=Inverse
        parameter should be used 
        '''
        fGP1 = self.Interpolation.predict_mean(Q)
        fGP2 = self._Interpolation.predict_mean(Q)

        for j in range(self._Interpolation.X_train_.shape[0]):
            pass

        RMSD = 0
        for i in range(Q.shape[0]):
            RMSD += sqrt((fGP1[i]-fGP2[i])**2)/Q.shape[0]

        if RMSD==0.0: #Issue that seem to appear
            return 1.0
        else:
            return RMSD


    def Variance(self, Q):

        sGP1 = self.Interpolation.predict_variance(Q)
        sGP2 = self._Interpolation.predict_variance(Q)

        RMSVD = 0
        for i in range(Q.shape[0]):
            RMSVD += sqrt((sGP1[i]-sGP2[i])**2)/Q.shape[0]

        if RMSVD<1e-14: #Issue that seem to appear
            return 1.0
        else:
            return RMSVD 





    def GetDistribution(self, GP):
        '''Returns a dictionary containing all the points and their respective variance
           once removed from the training set'''

        Data = {'Q': GP.X_train_}
        Data['Error'] = zeros(Data['Q'].shape[0])
        Data['Variance'] = zeros(Data['Q'].shape[0])

        self.load(GP)

        for i in range(Data['Q'].shape[0]):

            self.reduce(i)
            q = Data['Q'][i,:].reshape((1,Data['Q'].shape[1]))
            Data['Variance'][i] = self._Interpolation.predict(q, return_std=True)[1]
            Data['Error'][i] = self._Interpolation.predict(q, return_std=False)[0] -\
                                                           (GP.y_train_[i] + GP._y_train_mean)

            self.reload()

        return Data



