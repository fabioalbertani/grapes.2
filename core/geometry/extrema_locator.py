#!/bin/python

import sys

from core.grapes_utils.main_utils import HiddenPrints


class Main:
    
    def __init__(self, args, restarts=5):
        '''
        Locating minima to make sure the GP minima are physical

        Locating maxima allow to also prevent unphysical bumps from happening 

        Locating TS is also important and has to be done through a Hessian-based approach
        '''   

 
        self.args = args
        self.restarts = restarts

        if self.args.D==1:
            self.Locate = self.Locator1D

        elif self.args.D==2:
            self.Locate = self.Locator2D

        elif self.args.D==3:
            self.Locate = self.Locator3D

        else:
            pass #BassinHoppin


        self.ExtremaFile = open('ExtremaLocation.log', 'a')


    def Locator1D(self, Data, Type, basis_set):
        '''
        Data: DataManagement/*Structure type 
        '''
        from numpy.random import random
        from numpy import asarray, ndarray
        from scipy import optimize 

        ReturnDict = {'Minima': [], 'Maxima': []}

        def energy(q, sign):
            q = q.reshape((1,1))
            return sign*Data.Predict(Type, basis_set, q), sign*Data.GradientPredict(Type, basis_set, q)


        
        for tries in xrange(self.restarts):

            q0 = (-self.args.GridLimits[0]+self.args.GridLimits[1])*random()-self.args.GridLimits[1]
            self.ExtremaFile.write('\nStarting at:\t'+str(q0)+'\n')
            
            with HiddenPrints('ExtremaFinding.log'):
                resultmin = optimize.minimize(energy, [q0], args=(1.0), jac=True, tol=1e-4)
                resultmax = optimize.minimize(energy, [q0], args=(-1.0), jac=True, tol=1e-4)
            self.ExtremaFile.write(resultmin.message+'\n'+resultmax.message)       

            if resultmin.success:
                self.ExtremaFile.write('\nAdding minimum at '+str(resultmin.x))
                if len(ReturnDict['Minima'])==0:
                    ReturnDict['Minima'] = [resultmin.x]
                else:
                    for prev in xrange(len(ReturnDict['Minima'])):
                        if not ReturnDict['Minima'][prev]-resultmin.fun<1e-2:
                            ReturnDict['Minima'] += [resultmin.x]

            if resultmax.success:
                self.ExtremaFile.write('\nAdding minimum at '+str(resultmin.x))
                if len(ReturnDict['Maxima'])==0:
                    ReturnDict['Maxima'] = [resultmax.x]
                else:
                    for prev in xrange(len(ReturnDict['Maxima'])):
                        if not ReturnDict['Maxima'][prev]-resultmax.fun<1e-2:
                            ReturnDict['Maxima'] += [resultmax.x]

        
        return ReturnDict




    def Locator2D(self, Data, Type, basis_set):
        '''
        Data: DataManagement/*Structure type 
        '''
        from numpy.random import random
        from numpy import asarray, ndarray, zeros
        from scipy.optimize import minimize

        ReturnDict = {'Minima': [], 'Maxima': []}
        q0 = zeros(2)

        def energy(q, sign, dq=1e-3):
            jac = zeros(2)
            q = q.reshape((1,2))
            prediction = Data.Interpolate(Type, basis_set, q)

            for i in xrange(2):
                q[0,i] += dq
                predictionp = Data.Interpolate(Type, basis_set, q)
                q[0,i] += -2*dq
                predictionm = Data.Interpolate(Type, basis_set, q)
                q[0,i] += dq
                jac[i] = (getattr(predictionp, 'energy')-getattr(predictionm, 'energy'))/dq**2
  
            return sign*getattr(prediction, 'energy'), ndarray.flatten(sign*jac)


        for tries in xrange(self.restarts):

            q0[0] = (-self.args.GridLimits[0]+self.args.GridLimits[1])*random()-self.args.GridLimits[1]
            q0[1] = (-self.args.GridLimits[2]+self.args.GridLimits[3])*random()-self.args.GridLimits[3]

            with HiddenPrints('ExtremaFinding.log'):
                resultmin = minimize(energy, q0, args=(1.0), jac=True, tol=1e-4)
                resultmax = minimize(energy, q0, args=(-1.0), jac=True, tol=1e-4)
            self.ExtremaFile.write(resultmin.message+'\n'+resultmax.message)

            if resultmin.success:
                self.ExtremaFile.write('\nAdding minimum at '+str(resultmin.x))
                if len(ReturnDict['Minima'])==0:
                    ReturnDict['Minima'] = [resultmin.x]
                else:
                    for prev in xrange(len(ReturnDict['Minima'])):
                        if not norm(ReturnDict['Minima'][prev]-resultmin.fun)<1e-2:
                            ReturnDict['Minima'] += [resultmin.x]

            if resultmax.success:
                self.ExtremaFile.write('\nAdding minimum at '+str(resultmin.x))
                if len(ReturnDict['Maxima'])==0:
                    ReturnDict['Maxima'] = [resultmax.x]
                else:
                    for prev in xrange(len(ReturnDict['Maxima'])):
                        if not norm(ReturnDict['Maxima'][prev]-resultmax.fun)<1e-2:
                            ReturnDict['Maxima'] += [resultmax.x]

        return ReturnDict





    def Locator3D(self, Data, Type, basis_set):
        '''
        Data: DataManagement/*Structure type 
        '''
        from numpy.random import random
        from numpy.linalg import norm
        from numpy import asarray, ndarray, zeros
        from scipy.optimize import minimize

        ReturnDict = {'Minima': [], 'Maxima': []}
        q0 = zeros(3)

        def energy(q, sign):
            q = q.reshape((1,3))
            return sign*Data.Predict(Type, basis_set, q), sign*Data.GradientPredict(Type, basis_set, q).reshape((3,))

        for tries in xrange(self.restarts):

            q0[0] = (-self.args.GridLimits[0]+self.args.GridLimits[1])*random()-self.args.GridLimits[1]
            q0[1] = (-self.args.GridLimits[2]+self.args.GridLimits[3])*random()-self.args.GridLimits[3]
            q0[2] = (-self.args.GridLimits[4]+self.args.GridLimits[4])*random()-self.args.GridLimits[5]
            self.ExtremaFile.write('\n\nStarting from:\t'+str(q0)+'\n')
        
            with HiddenPrints('ExtremaFinding.log'):
                resultmin = minimize(energy, q0, args=(1.0), jac=True, tol=1e-4)
                resultmax = minimize(energy, q0, args=(-1.0), jac=True, tol=1e-4)
            self.ExtremaFile.write('min: '+resultmin.message+'\nmax: '+resultmax.message)


            if resultmin.success:
                self.ExtremaFile.write('\nAdding minimum at '+str(resultmin.x))
                if len(ReturnDict['Minima'])==0:
                    ReturnDict['Minima'] = [resultmin.x]
                else:
                    for prev in xrange(len(ReturnDict['Minima'])):
                        if not norm(ReturnDict['Minima'][prev]-resultmin.fun)<1e-2:
                            ReturnDict['Minima'] += [resultmin.x]

            if resultmax.success:
                self.ExtremaFile.write('\nAdding minimum at '+str(resultmin.x))
                if len(ReturnDict['Maxima'])==0:
                    ReturnDict['Maxima'] = [resultmax.x]
                else:
                    for prev in xrange(len(ReturnDict['Maxima'])):
                        if not norm(ReturnDict['Maxima'][prev]-resultmax.fun)<1e-2:
                            ReturnDict['Maxima'] += [resultmax.x]


        self.ExtremaFile.write('\n\nSummary of extrema found:\n\n')
        for k,v in ReturnDict.items():
            self.ExtremaFile.write(k+str(v)+'\n')

        return ReturnDict




