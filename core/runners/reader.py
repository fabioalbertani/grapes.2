#!/usr/bin/python
#File: core.runners.reader.py

import core.grapes_utils.main_utils as UTILS
import core.grapes_utils.module_selection as MODULE

from core.runners.runner_interface import RunnerInterface
from core.data_management.data_filter import FilterInterface
from core.data_management.data_utils import ReadInputCoordinates


class ReaderStructure(RunnerInterface):
    '''
    Main.py will assign automatically some self.Modules
    '''

    def __init__(self, args, MOLECULE, CalcDetails, Data=None):

        super(ReaderStructure, self).__init__(args, MOLECULE, CalcDetails)

        self.Data = MODULE.SelectDataType(args)
        self.INTERPOLATION = MODULE.SelectInterpolationType(args)

        self.RunOrganiser = MODULE.SelectRunInterface(args,
                            CalcDetails, Molecule=self.Molecule)


    def Initiate(self):

        self.SP('\n\tReading data for '+UTILS.GetLabel(self.details)+'\n')

        Samples = ReadInputCoordinates(self.args.main, self.Molecule, self.details)

        self.Data.CreateStructure(Samples)

        self.INTERPOLATION.BuildInterpolation(
            FilterInterface(Runners=self, CalcDetails=self.details),
            self.details,
            Error = self.details['Difference'],
            Read=True
                                             )


    def Evolve(self, INTERPOLATION):
        pass


    def ProduceDataFiles(self):
        '''
        Calls relevant modules to print data files
        (both intermediate and final files)
        '''
        self.SP('\n\tSaving Data...\n')
        self.Data.PrintFinal(self.details)
        self.Data.PrintFinal(self.details, Format='XYZ', Molecule=self.Molecule)
        self.RunOrganiser.PrintFinal(self.Data, self.details)

