#!/usr/bin/python
#File: core.runners.__init__.py

'''
Runners includes twpo types of modules:

main program runners            -- allow to feed an input file and run the GRAPES program
                                   for both simple plotting purposes or more complex    
                                   successions of calculations

calculation runners             -- allow to call and interact with all the modules in
                                   specific ways 
'''

__all__ = ['generational', 'main', 'main_interface', 'method_main', 'method_main_interface'
           'plot_interface', 'reader', 'runner_interface', 'sequential']
