#!/usr/bin/python
#File: core.runners.generational.py

from datetime import datetime
import core.grapes_utils.main_utils as UTILS
import core.grapes_utils.module_selection as MODULE

from core.geometry.sampling import Random as RandomSamples
from core.runners.runner_interface import RunnerInterface
from core.data_management.data_utils import ReadInputCoordinates, UnpackIndices



class GenerationalStructure(RunnerInterface):
    '''
    Main.py will assign automatically some self.Modules
    '''

    def __init__(self, args, MOLECULE, CalcDetails, Data=None):

        super(GenerationalStructure, self).__init__(args, MOLECULE, CalcDetails)

        self.Data = MODULE.SelectDataType(args)

        self.TestModules = MODULE.SelectTestType(args, MOLECULE)
        self.SampleModule = MODULE.SelectSamplingType(args)

        self.RunOrganiser = MODULE.SelectRunInterface(args,
                            self.details, Molecule=MOLECULE)

        self.PreviousData = Data
        self._generation = 0
        self._teststatus = False
        self._accuracy = False


    @property
    def generation(self):
        '''Generation of the set of points'''
        return self._generation

    @property
    def teststatus(self):
        '''
        test status: if the current training set has been tested at
        least once
        '''
        return self._teststatus

    @property
    def accuracy(self):
        '''
        accuracy status: if the ctraining set has passed succesfully
        the tests
        '''
        return self._accuracy


    def Initiate(self):

        self.SP('\n\tRunning for '+UTILS.GetLabel(self.details)+\
                      ' (dipole included: '+str(self.args.main.dipole)+')\n')

#        if 'self.RunOrganiser' in locals() and not self.RunOrganiser.RunInterface.J is None:
#            #TODO Specify reading pickled J in !!
#            self.RunOrganiser = MODULE.SelectRunInterface(self.args,
#                                self.details, Molecule=self.Molecule,
#                                J=self.RunOrganiser.RunInterface.J)
#        else:
#            self.RunOrganiser = MODULE.SelectRunInterface(self.args,
#                                self.details, Molecule=self.Molecule)

        Samples = self.GetSamples()
        self.Data.CreateStructure(Samples)

        if not self.args.main.ReadData[self.details['label']]:
            self.ProduceDataFiles()
            self.PrintGenerationInfo()

        self._generation = self.generation + 1
        

#------------------------------------------------------------------- Main Evolve

    def Evolve(self, INTERPOLATION):

        self.SP('\n\tAdding data for '+UTILS.GetLabel(self.details)+\
                      ' (dipole included: '+str(self.args.main.dipole)+')\n')

        while not self.accuracy and\
              self.generation <= self.args.main.MaximumMeshGenerations:

            for Test in self.TestModules:
                Q = self.GetSamplesTest()

                TestResult = Test.Test(
                    INTERPOLATION, self.details, 
                    UnpackIndices(self.Data.Components(self.details['Type']).shape)[0] if \
                    self.args.testing.ElementTest is None else self.args.testing.ElementTest,
                    RunOrganiser = self.RunOrganiser, 
                    Q = Q
                                      )

                if not TestResult['Success']:
                    self.Data.UpdateStructure(TestResult['Data'])


                Test.PrintTestResult()

                if not self.args.main.ReadData[self.details['label']]:
                    self.ProduceDataFiles()
                    self.PrintGenerationInfo()

                if not self._teststatus:
                    self._teststatus = True
                if TestResult['Success']:
                    self._accuracy = True
                self._generation = self.generation + 1



#------------------------------------------------------------ Sample functions

    def GetSamples(self):
        '''
        Selects samples for a generation
        '''
        if self.args.main.ReadData[self.details['label']]:
            self.SP('\n\tReading data for '+UTILS.GetLabel(self.details)+'\n')
            return ReadInputCoordinates(self.args.main, self.Molecule,
                                        self.details)

        elif not self.PreviousData is None:
            Q = self.PreviousData.GetGeneration(self.generation)
            return self.RunOrganiser.RunSamples(Q)


        elif self.args.main.ReadOldPoints:
            if self.PreviousData is None:
                Q, dump = ReadInputCoordinates(self.args.main, self.Molecule,
                          self.details, CoordinatesOnly=True)
                return self.RunOrganiser.RunSamples(Q)

            else:
                Q = self.PreviousData.GetGeneration(self.generation)
                return self.RunOrganiser.RunSamples(Q)

        else:
            self.SP('\n\tSampling new points for '+\
                          UTILS.GetLabel(self.details)+'\n')
            Q = self.SampleModule(self.args.main)
            return self.RunOrganiser.RunSamples(Q)


    def GetSamplesTest(self):
        '''
        Select samples for a test
        '''
        if not self.PreviousData is None:
            return self.PreviousData.GetGeneration(self.generation)

        else:
            #TODO Have a sampling method for tests only
            #AND if it is the data specific call the function
            return RandomSamples(self.args.main)


#------------------------------------------------------------ Produce/Print Data

    def ProduceDataFiles(self):
        '''
        Calls relevant modules to print data files
        (both intermediate and final files)
        '''
        self.SP('\n\tSaving Data...\n')
        self.Data.PrintFinal(self.details, self.PreviousData)
        self.Data.PrintFinal(self.details, self.PreviousData, Format='XYZ', Molecule=self.Molecule)
        self.RunOrganiser.PrintFinal(self.Data)



    def PrintGenerationInfo(self):

        Template = """
        Mesh Number %i

            Number of points:           %s
            Tested:                     %r
            Reached Accuracy:           %r
                   """ %(
    self.generation,
    self.Data.MeshDetails['MeshNodes'],
    self.teststatus,
    self.accuracy
                        )
        self.SP(Template)

