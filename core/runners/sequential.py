#!/bin/python
#File: core.runners.sequential.py

import core.grapes_utils.main_utils as UTILS
import core.grapes_utils.module_selection as MODULE

from core.data_management.data_utils import ReadInputCoordinates, UnpackIndices
from core.data_management.data_filter import FilterInterface
from core.runners.runner_interface import RunnerInterface


class SequentialStructure(RunnerInterface):
    '''
    Class to manage a sequential run of points through an interpolation module. The 
    sequential nature of the run forces some tricks to be used to perform things like 
    gaussian processes with faster methods

    the `SampleModule` attribute should be a sampler inherited from molecular dynamics 
    (not implemented yet) that allows single points to be fed to the evolving module
    '''
    def __init__(self, args, MOLECULE, CalcDetails):

        super(SequentialStructure, self).__init__(args, MOLECULE, CalcDetails)

        self.Data = MODULE.SelectDataType(args)

        self.TestModules = MODULE.SelectTestType(args, MOLECULE)
        self.SampleModule = MODULE.SelectSamplingType(args) #for testing purposes

        self.RunOrganiser = {}
        for CalcIteration, CalcType in enumerate(self.args.main.CalcTypes):
            CalcDetails = UTILS.GetTypeDetails(CalcType) 
            CalcDetails['Difference'] = UTILS.GetTypeDetails(
                                        args.main.CalcTypeDifference[CalcType])
            self.RunOrganiser[CalcType] = MODULE.SelectRunInterface(args,
                                          CalcDetails, Molecule=self.Molecule)

        self._teststatus = False
        self._accuracy = False


    @property
    def teststatus(self):
        '''
        test status: if the current training set has been tested at
        least once
        '''
        return self._teststatus

    @property
    def accuracy(self):
        '''
        accuracy status: if the training set has passed succesfully
        the tests
        '''
        return self._accuracy 



    def Initiate(self, INTERPOLATION):

        self.SP('\n\tCreating initial samples for sequential run\n')

        Q = self.GetInitialSamples()

        for CalcIteration, CalcType in enumerate(self.args.main.CalcTypes):
            CalcDetails = UTILS.GetTypeDetails(CalcType)
            CalcDetails['Difference'] = UTILS.GetTypeDetails(
                                        self.args.main.CalcTypeDifference[CalcType])
        
            Data = self.RunOrganiser[CalcType].RunSamples(Q)
            self.Data.CreateStructure(Data, components_only=(CalcIteration!=0))
            INTERPOLATION.BuildInterpolation(
                FilterInterface(Q=Q, Data=Data, CalcDetails=CalcDetails),
                CalcDetails,
                Error = CalcDetails['Difference']
                                            )

        self.ProduceDataFiles()

        return INTERPOLATION


    def Evolve(self, INTERPOLATION):

        self.SP('\n\tRunning for sequential run\n')

        for sample in range(self.args.main.NSampling):
            #Should also include the interpolation module once the code is interfaced with
            #dynamic sampling
            Q = self.SampleModule(self.args.main, N=1)

            for CalcIteration, CalcType in enumerate(self.args.main.CalcTypes):
           
                CalcDetails = UTILS.GetTypeDetails(CalcType)
                CalcDetails['Difference'] = UTILS.GetTypeDetails(
                                            self.args.main.CalcTypeDifference[CalcType])
                Data = self.RunOrganiser[CalcType].RunSamples(Q) 
                self.Data.UpdateStructure(Data, components_only=(CalcIteration!=0))
                #EVERY NOW AND THEN THE optimseHP should be set to 1
                INTERPOLATION.SinglePointUpdate(
                    FilterInterface(Q=Q, Data=Data, CalcDetails=CalcDetails),
                    CalcDetails
                                               ) 

                self.TestStructure(INTERPOLATION, self.RunOrganiser[CalcType], CalcDetails)

            self.ProduceDataFiles()

        return INTERPOLATION



    def TestStructure(self, INTERPOLATION, RunOrganiser, CalcDetails):

        self.SP('\n\tRunning tests on current data structure:\n')

        for Test in self.TestModules:
            #Q has to be provided ?
            TestResult = Test.Test(
                INTERPOLATION, CalcDetails,
                UnpackIndices(self.Data.Components(CalcDetails['Type']).shape)[0],
                RunOrganiser=RunOrganiser,
                Q = None
                                  )

            Test.PrintTestResult()




    def GetInitialSamples(self):
        '''
        Selects samples for the initial training set
        '''
        self.SP('\n\tSampling new points for sequential run\n')
        return self.SampleModule(self.args.main)


#------------------------------------------------------------ Produce/Print Data

    def ProduceDataFiles(self):
        '''
        Calls relevant modules to print data files
        (both intermediate and final files)
        '''
        self.SP('\n\tSaving Data...\n')
        for CalcIteration, CalcType in enumerate(self.args.main.CalcTypes):
            CalcDetails = UTILS.GetTypeDetails(CalcType)
            CalcDetails['Difference'] = UTILS.GetTypeDetails(
                                        self.args.main.CalcTypeDifference[CalcType])
            self.Data.PrintFinal(CalcDetails)
            self.Data.PrintFinal(CalcDetails, Format='XYZ', Molecule=self.Molecule)
            self.RunOrganiser[CalcType].PrintFinal(self.Data)
