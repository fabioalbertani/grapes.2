#!/bin/python
#File: core.runners.spectra.py

import sys, os
from datetime import datetime
from subprocess import call

import core.grapes_utils.main_utils as UTILS
import core.grapes_utils.module_selection as MODULE
from core.runners.runner_interface import header as HEADER

from core.options.options_interface import GlobalOptions, ParseOptions
from core.geometry.molecule import MoleculeManager

from core.runners.reader import ReaderStructure
from core.data_management.data_filter import FilterInterface
from core.plotting.plot_interface import PlotInterface


def main():
    '''
    Main program: Will print the summary, select "groups" of points to be run, loop over basis sets,
    improve the mesh iteratively and produce all requested plots

    called by: core.runners.method_main_interface.py in terminal as
    #python PATH/core/runners/method_main.py
    '''
    SP = UTILS.SuperPrint()
    SP(HEADER)
    CDPath = os.getcwd()
    os.environ['verbose'] = '1' if '--verbose' in sys.argv else '0'
    sys.path.append(CDPath)

    InputFileName = UTILS.GetInputFileName(CDPath, sys.argv)

    args = GlobalOptions
    args.GetInput(InputFileName, sys.argv)
    ParseOptions(args, sys.argv)
    args.main.set('CDPath', CDPath)
    args.main.set('InputFileName', CDPath +'/'+ args.main.InputFileName)

    WDPath = UTILS.MoveToWorkDirectory(subfolder=args.main.Label)
    call('mkdir -p '+CDPath+'/SavedData/'+args.main.Label, shell=True)
    call('mkdir -p '+WDPath+'/GRAPESData/SavedData/'+args.main.Label, shell=True)
    args.main.set('WDPath', WDPath)



    InputTemplate = """
        Key Parameters

            System:                     {Label} -> """
    InputTemplate += ' '.join(args.main.AtomsList) + """
            Dimensionality:             {D} (of %i DOF)
            Charge:                     {Charge}
            Spin Multiplicity:          {Spin}

            States considered:          {NStates}

            Calculating Types:
                   """       %(max(3*len(args.main.AtomsList)-6,1))

    for v in args.main.CalcTypes:
        if UTILS.GetTypeDetails(v)['interface']=='QChem':
            InputTemplate += """
                %s as difference to ->    %s (reading in %s)
                             """ %(v, args.main.CalcTypeDifference[v],
                                   str(args.main.ReadData[v]))


    if args.main.ReadOldPoints:
        InputTemplate = InputTemplate + '''
            Reading Old Points
                                        '''
    else:

        InputTemplate = InputTemplate + '''
            Numerical Gradient:         {NumericalGradient}
            Numerical Hessian:          {NumericalHessian}

            Grid:                       '''
        InputTemplate += ' '.join([str(x) for x in args.main.GridLimits]) + '''
            Number of Initial Samples:  {NSampling}
            Mesh Generation(s):         {MaximumMeshGenerations}

            Interpolation Method:       {InterpolationMethod}
            Testing Method:             '''
        InputTemplate += ' '.join(args.main.TestingMethods)

        if args.main.ProvidedStates:
            InputTemplate = InputTemplate + '''
            Reading states from file {ProvidedStates}
                                            '''

    SP(InputTemplate.format(**args.main.__dict__))


    MOLECULE = MoleculeManager(args.main)
    PLOT = MODULE.SelectPlotTypes(args)

    SP('\n\tPreparing for '+str(len(args.main.MethodTypes))+\
        ' methods ...\n')

    for MethodIteration, MethodType in enumerate(args.main.MethodTypes):

        SP('\n\tCalculation '+str(MethodIteration+1)+' of '+\
                 str(len(args.main.MethodTypes))+'\n')

        MethodDetails = UTILS.GetTypeDetails(MethodType)
        if MethodDetails['interface']!='GRAPES':
            raise RuntimeError('Only method calculations can be performed calling'\
                               'core.runners.method_main.py')

        CalcDetails = UTILS.MethodCalculationDetails(MethodDetails, args.main.CalcTypes)
        CalcDetails['Difference'] = UTILS.GetTypeDetails(
                                        args.main.CalcTypeDifference[CalcDetails['label']])

        READER = ReaderStructure(args, MOLECULE, CalcDetails)
        READER.Initiate()

        MAIN = MODULE.SelectSpectraType(args)     

        MAIN.Run()

        MAIN.ProduceData()



if __name__=='__main__':
    main()


    

