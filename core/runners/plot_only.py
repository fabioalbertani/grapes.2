#!/usr/bin/python
#File: Main.py

'''
Interface to simply plot the samples given in a file
'''


def main():
    '''
    Main program: Will print the summary, select "groups" of points to be run, loop over basis sets,
    improve the mesh iteratively and produce all requested plots

    called by: MainInterface.py as "python PATH/Main.py" in terminal
    '''
   
 
    import re, os, sys
    from datetime import datetime
    sys.path.append(os.getenv('MODULEPATH'))


    import MainUtils.Utils as UTILS
    from Options.OptionsInterface import GlobalOptions, ParseOptions
    from Geometry.Molecule import MoleculeManager
    from DataManagement.Utils import ReadInputCoordinates    

    from Plotting.PlotInterface import Main as ProducePlotsMain


    args = GlobalOptions
    argsdump = ParseOptions(args, sys.argv)


# --------------------------------------------          Calculation Details            ----------------------------------------------

    InputTemplate = """
        Key Parameters
        
            System:                     {Label} -> {AtomsList}
            Dimensionality:             {D} (of %i DOF)
            Charge:                     {Charge}
            Spin Multiplicity:          {Spin}

                    """ %(3*len(args['Main'].AtomsList)-6)

    print(InputTemplate.format(**args['Main']))

    PLOT = ProducePlotsMain()

    #Even though a single item shoudl be provided (cando a for loop) the first element is taken
    CalcDetails = UTILS.GetTypeDetails(args['Main'].CalcTypes[0])
    filename = 'SavedData/'+args['Main'].Label+'/MeshInformation_'+UTILS.GetDataLabel(CalcDetails)+'.qdat/xyzdat'
    decision = raw_input('Would you like to use '+filename+' as the input? (y/n)\n')
    if decision=='n':
        filename = raw_input('Which input file would you like to plot?\n')
    
    else:
        filename = filename[:-7] #checking the qdat file
        if not os.path.isfile(filename):
            filename = filename[:-4] + 'xyzdat' 
            args['Main'].CoordinatesFormat = 'XYZ'
        
        else:
            args['Main'].CoordinatesFormat = 'Q'


    Molecule = MoleculeManager(args['Main'])
    Q = ReadInputCoordinates(args['Main'], Molecule, filename=filename, CoordinatesOnly=True)

    print('\n\tFound '+str(Q[0].shape[0])+' samples to plot\n')

    PLOT.MakeSamplePlot(args, Molecule.q2cart(Q[0], fix=True))



#-------------------------------------------------------------------- call function
if __name__=='__main__':
    main()




