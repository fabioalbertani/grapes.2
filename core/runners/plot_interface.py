#!/bin/python
#File: PlotFileInterface.py

from subprocess import call
from getpass import getuser

import sys
import os
from datetime import datetime


from .MainUtils.FormatInput import PartialFormatter, ReadParameters

def main(ShowPlot=False, DeleteFiles=False):
    '''
    Interface to the main program which allow to select input files and creat/move calculation outputs   

    Args: 
        ShowPlot:       [bool] Automatically opens all plots that were produced
        DeleteFiles:    [bool] Automatically deletes all inputs and output created by calls to QChem (primitive run only)
    '''

    Format = PartialFormatter()
    CDPath = os.getcwd()
    sys.path.append(CDPath)
    if not CDPath in os.environ['MODULEPATH']:
        print '\n\tSetting $MODULEPATH to: '+CDPath+'\n'
        os.environ['MODULEPATH'] = CDPath

    ProgramName = CDPath + '/../bin/runPlotFile.py' #Main executable

    if len(sys.argv)>1:
        if sys.argv[1]=='-i' or sys.argv[1]=='--InputFile':
            InputFileName = CDPath+'/InputFiles/'+sys.argv[2]+'.input' 
            if not os.path.isfile(InputFileName): 
                print('\Provided input file: '+InputFileName)
                raise RuntimeError('The input file specfied was not found')
        else:
            InputFileName = CDPath+'/InputFiles/'+sys.argv[1]+'.input'
            decision = raw_input("Did you want to use %s as an input file? (Yes/No)\n" %InputFileName)
            if decision=='No':
                InputFileName=None
                print('Default values will be used')
                
    else:
        System = raw_input('No input files provided: What is the system?\n')
        InputFileName = CDPath+'/InputFiles/'+System+'.input'
        if os.path.isfile(InputFileName):
            decision = raw_input("Input file found: %s Would you like to use it? (Yes/No)\n" %InputFileName)
            if decision=='No':
                InputFileName=None
                print('Default values will be used')
        else:
            InputFileName=None
            print('Default values will be used')


    header = '''
    Running C-GRAPES program:

                           _____       _____ _____            _____  ______  _____ 
                          / ____|     / ____|  __ \     /\   |  __ \|  ____|/ ____|
                         | |   ______| |  __| |__) |   /  \  | |__) | |__  | (___  
                         | |  |______| | |_ |  _  /   / /\ \ |  ___/|  __|  \___ \ 
                         | |____     | |__| | | \ \  / ____ \| |    | |____ ____) |
                          \_____|     \_____|_|  \_\/_/    \_\_|    |______|_____/ 
                                                           


             '''
    try:
        import colorama
        colorama.init()
        cprint(header, 'red')
    except:
        print(header)


    Parameters = ReadParameters(InputFileName)
    Parameters['Program'] = ProgramName

    call("mkdir -p SavedData/%s" %Parameters['Label'].split()[-1], shell=True)


    if len(sys.argv)==2:
        ExtraArguments = ' '
    else:
        #Add any arguments given by the user on the command line
        ExtraArguments = ' '.join(sys.argv[3:])

    Parameters['ExtraArguments'] = ExtraArguments

    template = '{Program}'
    for key in Parameters.keys():
        template += ' {'+key+'}'

    os.environ['verbose'] = '0'
    
    print('\n\tStarting calculation '+'@'+str(datetime.now()))
    call(Format.format(template, **Parameters), shell=True)

    call("mv *.pdf SavedData/%s/." %Parameters['Label'].split()[-1], shell=True)
 
    print('\tProgram terminated '+'@'+str(datetime.now()))
    print('\n\tOutput files have been saved to /SavedData/'+Parameters['Label'].split()[-1]+'\n')



if __name__ == '__main__':
    '''
    Runs local main (which essentially moves to a work directory and runs Main.py)
    '''
    main() 

