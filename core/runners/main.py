#!/bin/python
#File: core.runners.main.py

import sys, os
from datetime import datetime
from subprocess import call

import core.grapes_utils.main_utils as UTILS
import core.grapes_utils.module_selection as MODULE
from core.runners.runner_interface import header as HEADER
                                                       
from core.options.options_interface import GlobalOptions, ParseOptions
from core.geometry.molecule import MoleculeManager

from core.data_management.data_filter import FilterInterface
from core.plotting.plot_interface import PlotInterface


def main():
    '''
    Main program: Will print the summary, select "groups" of points to be run, loop over basis sets,
    improve the mesh iteratively and produce all requested plots
    '''
    SP = UTILS.SuperPrint()
    SP(HEADER)
    CDPath = os.getcwd()
    os.environ['verbose'] = '1' if '--verbose' in sys.argv else '0'
    sys.path.append(CDPath)

    InputFileName = UTILS.GetInputFileName(CDPath, sys.argv) 

    args = GlobalOptions
    args.GetInput(InputFileName, sys.argv)
    ParseOptions(args, sys.argv)
    args.main.set('CDPath', CDPath)
    args.main.set('InputFileName', CDPath +'/'+ args.main.InputFileName)


    WDPath = UTILS.MoveToWorkDirectory(subfolder=args.main.Label)
    call('mkdir -p '+CDPath+'/SavedData/'+args.main.Label, shell=True)
    call('mkdir -p '+WDPath+'/GRAPESData/SavedData/'+args.main.Label, shell=True)
    args.main.set('WDPath', WDPath)


    
    SP('\n\tStarting calculation @ '+str(datetime.now()))
    InputTemplate = """
        Key Parameters
        
            System:                     {Label} -> """
    InputTemplate += ' '.join(args.main.AtomsList) + """
            Dimensionality:             {D} (of %i DOF)
            Charge:                     {Charge}
            Spin Multiplicity:          {Spin}

            States considered:          {NStates}

            Calculating Types:
                   """       %(max(3*len(args.main('AtomsList'))-6,1))

    for v in args.main('CalcTypes'):
        if UTILS.GetTypeDetails(v)['interface']=='QChem':
            InputTemplate += """
                %s as difference to ->    %s (reading in %s)
                             """ %(v, args.main.CalcTypeDifference[v],
                                   str(args.main.ReadData[v]))


    if args.main.ReadOldPoints:
        InputTemplate = InputTemplate + '''
            Reading Old Points
                                        '''
    else:        

        InputTemplate = InputTemplate + '''
            Numerical Gradient:         {NumericalGradient}
            Numerical Hessian:          {NumericalHessian}

            Grid:                       '''
        InputTemplate += ' '.join([str(x) for x in args.main.GridLimits]) + '''
    
            Number of Initial Samples:  {NSampling}
            Mesh Generation(s):         {MaximumMeshGenerations}
   
            Interpolation Method:       {InterpolationMethod}
            Testing Method:             '''
        InputTemplate += ' '.join(args.main.TestingMethods)

        if args.main.ProvidedStates:
            InputTemplate = InputTemplate + '''
            Reading states from file {ProvidedStates}
                                            '''

    SP(InputTemplate.format(**args.main.__dict__))

    #Shared module 
    MOLECULE = MoleculeManager(args.main) 

    if args.main.RunningMode.strip()=='Sequential':
        from core.runners.sequential import SequentialStructure as RUNNER
        MAIN = RUNNER(args, MOLECULE, None)
        INTERPOLATION = MODULE.SelectInterpolationType(args)

        INTERPOLATION = MAIN.Initiate(INTERPOLATION)
        INTERPOLATION = MAIN.Evolve(INTERPOLATION)


    elif args.main.RunningMode.strip()=='Generational':
        from core.runners.generational import GenerationalStructure as RUNNER
        Runners = {}
        INTERPOLATION = MODULE.SelectInterpolationType(args)
        
        SP('\n\tPreparing for '+str(len(args.main.CalcTypes))+\
            ' calculations ...\n')
           
        for CalcIteration, CalcType in enumerate(args.main.CalcTypes):

            SP('\n\tCalculation '+str(CalcIteration+1)+' of '+\
                     str(len(args.main.CalcTypes))+'\n')

            CalcDetails = UTILS.GetTypeDetails(CalcType)
            CalcDetails['Difference'] = UTILS.GetTypeDetails(
                                        args.main.CalcTypeDifference[CalcType])

            label = CalcDetails['label']

            if CalcDetails['Difference']:
                Runners[label] = RUNNER(args, MOLECULE, CalcDetails,
                                        Data = Runners[CalcDetails['Difference']['label']].Data)

            else:
                Runners[label] = RUNNER(args, MOLECULE, CalcDetails)

            Runners[label].Initiate()
            INTERPOLATION.BuildInterpolation(
                FilterInterface(Runners=Runners, CalcDetails=CalcDetails),
                CalcDetails,
                Error = CalcDetails['Difference']
                                            )
            

            if args.main.ReadData[label]:
                if args.main.ImproveReadData:
                    Runners[label].Evolve(INTERPOLATION)
                    INTERPOLATION.BuildInterpolation(
                        FilterInterface(Runners=Runners, CalcDetails=CalcDetails), 
                        CalcDetails,
                        Error = CalcDetails['Difference']
                                                    )

            else:
                Runners[label].Evolve(INTERPOLATION)
                INTERPOLATION.BuildInterpolation(
                    FilterInterface(Runners=Runners, CalcDetails=CalcDetails),
                    CalcDetails,
                    Error = CalcDetails['Difference']
                                                )



    else:
        raise Exception('\n\tRunning mode '+args.main.RunningMode+\
                        ' not known... interrupting\n')
    


    #plotting modules might be loaded and used during the validation phase   
    PLOT = MODULE.SelectPlotTypes(args)

    VALIDATION = MODULE.SelectValidationType(args)
    for validators in VALIDATION:
        validators(INTERPOLATION, MOLECULE)


    PLOT(INTERPOLATION, MOLECULE)


    UTILS.MoveDataFiles(WDPath, CDPath, args.main.Label)
    SP('\tProgram terminated @ ' + str(datetime.now()) + '\n\tOutput files have been saved to ' +\
       CDPath + '/SavedData/' + args.main.Label + '\n', stdout=True)



if __name__=='__main__':
    main()

