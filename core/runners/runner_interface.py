#!/usr/bin/python
#File: core.runners.runner_interface.py

from datetime import datetime
import core.grapes_utils.main_utils as UTILS
import abc

class RunnerInterface:

    __metaclass__ = abc.ABCMeta

    def __init__(self, args, MOLECULE, CalcDetails):
        '''
        Loads arguments and summary file (for output
        production)
        '''
        self.args = args
        self.Molecule = MOLECULE      

        self.SP = UTILS.SuperPrint()
        self._details = CalcDetails


    @property
    def stop(self):
        return self._stop

    @property
    def details(self):
        return self._details

    @details.setter
    def details(self, details):
        self._details = details


    @abc.abstractmethod
    def Initiate(self, **kwargs):
        '''
        First addition of data and setting up of all relevant modules
        '''

    @abc.abstractmethod
    def Evolve(self, **kwargs):
        '''
        Main initiation will have been called and structures to support
        data addition have been set

        This method simply adds data (in a very general manner)
        '''

    @abc.abstractmethod
    def ProduceDataFiles(self, **kwargs):
        '''
        This will simply condense the call to all sub-modules
        that produce data files (not consistently the same ones)
        '''
        return None



header = """

                           _____ _____            _____  ______  _____
                          / ____|  __ \     /\   |  __ \|  ____|/ ____|
                         | |  __| |__) |   /  \  | |__) | |__  | (___
                         | | |_ |  _  /   / /\ \ |  ___/|  __|  \___ \ 
                         | |__| | | \ \  / ____ \| |    | |____ ____) |
                          \_____|_|  \_\/_/    \_\_|    |______|_____/



    Running C-GRAPES program:
         """
