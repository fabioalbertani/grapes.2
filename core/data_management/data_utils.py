#!/bin/python
#File: core.data_maangement.data_utils.py

import os

import core.grapes_utils.main_utils as UTILS

from numpy import concatenate, zeros, isnan, nan, delete, empty
from numpy import r_, c_, genfromtxt
from numpy import float64 as npfloat

from math import exp, log
from copy import copy


#---------------------------------------------------------- Data Transfer Modules

class Process:
    '''
    This class allows to control each datum on-the-fly to then feed a dictionary which is 
    in the correct format for DataClass modules (see abstract class above)
    '''

    def __init__(self, args):

        self.Globalargs = args

        PotentialData = [
            'Energy', 'Distances', 'Dipole', 'Gradient', 'Hessian',
            'NormalModes', 'StateDistances', 'AlphaCoef', 'BetaCoef'
                        ]

        RunData = [
            'Points', 'Generations'
                  ]

        self.DataDict = {}

        for key in PotentialData:
            self.DataDict[key] = None
            self.DataDict['INT-'+key] = None
            self.DataDict['D-'+key] = None

        for key in RunData:
            self.DataDict[key] = None

        self.DefineShapes(1) #for initiation purposes


    def DefineShapes(self, n, keys=None, shapes=None):
        '''
        None shapes will yield lists while defined ones will yield numpy darray
        '''

        shapes = {
           'Points': (n, self.Globalargs.D),                             
           'Energy': (n, self.Globalargs.NStates),        
           'Distances': (n, self.Globalargs.NStates),
           'Dipole': (n, 4, self.Globalargs.NStates),                         
           'Gradient': (n, self.Globalargs.D, self.Globalargs.NStates),
           'NormalModes': (n, self.Globalargs.Dxyz, self.Globalargs.Dxyz-6,
                           self.Globalargs.NStates),
           'Hessian': (n, self.Globalargs.Dxyz, self.Globalargs.Dxyz,
                       self.Globalargs.NStates),
           'StateDistances': (n, self.Globalargs.NStates),
           'AlphaCoef': None,
           'BetaCoef': None,
           'Generations': None 
                 }


        self.shapes = {}
        for key in self.DataDict.keys():
            for shape_type in shapes.keys():
                if shape_type in key:
                    self.shapes[key] = shapes[shape_type]
                    break



    def FeedData(self, q, Data):
        '''
        Adds a datum to the data dictionary
        '''
        Data['Points'] = q

        for key in self.DataDict.keys():
            if key in Data.keys():
                if self.DataDict[key] is None:
                    if self.shapes[key] is None:
                        self.DataDict[key] = [Data[key]]
                    else:
                        try:
                            self.DataDict[key] = Data[key].reshape(self.shapes[key])
                        except ValueError:
                            shape = list(self.shapes[key])
                            shape[-1] = 1
                            self.shapes[key] = tuple(shape)
                            self.DataDict[key] = Data[key].reshape(self.shapes[key])

                else:
                    if self.shapes[key] is None:
                        self.DataDict[key].append(Data[key])
                    else:
                        self.DataDict[key] = concatenate((self.DataDict[key],
                                             Data[key].reshape(self.shapes[key])), axis=0) 



    def VoidEntry(self, Q):

        Data = copy(self.shapes)
        for key in Data.keys(): 
            if not self.shapes[key] is None: 
                Data[key] = empty(Data[key])
                Data[key][:] = nan
        Data['Points'] = Q

        for key in self.DataDict.keys():
            if key in Data.keys():
                if self.DataDict[key] is None:
                    if self.shapes[key] is None:
                        self.DataDict[key] = [Data[key]]
                    else:
                        self.DataDict[key] = Data[key].reshape(self.shapes[key])

                else:
                    if self.shapes[key] is None:
                        self.DataDict[key].append(Data[key])
                    else:
                        self.DataDict[key] = r_[self.DataDict[key], 
                                                Data[key].reshape(self.shapes[key])]



    def FeedFinalData(self, Q, Data):
        '''
        Reshaping is forced to ensure the (N,1) arrays are not flattened
        '''
        Data['Points'] = Q 

        if self.Globalargs.D:
            self.DefineShapes(len(Q))
        else:
            self.DefineShapes(Q.shape[0])        

        for key in self.DataDict.keys():
            
            if key in Data.keys():
                if self.shapes[key] is None:
                    self.DataDict[key] = Data[key]
                else:
                    self.DataDict[key] = Data[key].reshape(self.shapes[key])



    def SelectPoints(self, Indices, noMesh=False):
        '''
        Removes indexed data
        '''
        from numpy import delete, nan

        for key in self.DataDict.keys():
            if noMesh:
                if key!='Points':
                    self.DataDict[key][Indices,:] = nan
            else:
                self.DataDict[key] = delete(self.DataDict[key], Indices, axis=0)


    def AddKeys(self, keys, shapes=None):
        '''
        Allows to add a key to the self.DataDict (should be called before giving Data)
        nb: does not work if followed by a data dump (FeedFinalData)
        '''
        if shapes is None: _shapes = {}

        for key in keys:
            self.DataDict[key] = None 

            if shapes is None:
                _shapes[key] = None     
 
        self.DefineShapes(1, keys, shapes)



    def Return(self):
        '''
        Returns the data dictionary (the whole class is not useful and can be "discarded")
        '''
        self.DataDict['shapes'] = self.shapes
        return self.DataDict




#----------------------------------------------------------------------------------- Unpack Utils

def _Defined(Class, label, idx):
    ''' Only returns true if the index is defined and the value at the said index is a float '''
    try:
        slc = [slice(idx[i],idx[i]+1) for i in range(getattr(Class, label).ndim)]
        if type(Class(label)[slc].flatten()[0])==npfloat:
            if Class(label)[slc].flatten()[0]==npfloat('inf'):
                return False
            elif isnan(Class(label)[slc].flatten()[0]):
                return False
            else:
                return True
        else:
            return False
    except IndexError:
        return False

    except:
        '''any other error'''
        print('_Defined not responsive -> setting element to Nan')
        return False 


def _Difference(Class, label1, label2, idx):
    '''Difference between two objects (allows more compact functions)'''
    slc = [slice(idx[i],idx[i]+1) for i in range(Class(label1).ndim)]
    return Class(label1)[slc] - Class(label2)[slc]


def _Recursive(D, index): 
    '''Returns indices needed for the nth index'''
    if isinstance(D, tuple):
        D = zeros(D) #hack
    indices = list(D.shape)
    for d in range(D.ndim):
        indices[d] = index if not d else index//(int(exp(sum([log(D.shape[n])\
                     for n in range(0,d)]))))
    return [indices[i]%D.shape[i] for i in range(D.ndim)]


def UnpackIndices(t):
    '''
    Returns all relevant indices for the specific tuple
    '''
    elements = exp(sum([log(t[n]) for n in range(1,len(t))]))
    indices = []
    slc = [slice(None)]*len(t)
    slc[0] = slice(0,1)
    for el in range(int(elements)):
        Il = _Recursive(t[1:], index=el)
        indices.append(''.join([str(i) for i in Il]))

    return indices




def UnpackBasic(Data, CalcDetails1, CalcDetails2=None, full_data=False):
    '''
    Interface to Unpack Internal return a nanData array with the correct shape and with 
    only float or nan values 

    full_data switches between types (only full hessian to normal modes now)
    '''

    if True: #try:
        if CalcDetails1['Type']:
            nanData = zeros(Data.Components.GetShape(CalcDetails1['Type']))

            for element in range(int(exp(sum([log(nanData.shape[n]) for\
                                 n in range(len(nanData.shape))])))):

                slc = _Recursive(nanData, element)

                if CalcDetails2 is None:
                    nanData[i,j] = Data.Components(CalcDetails1['label'])[i,j] if\
                                   _Defined(Data.Energy, CalcDetails1['label'], [i,j]) else nan

                else:
                    if _Defined(Data.Energy, CalcDetails1['label'], [i,j]) and\
                       _Defined(Data.Energy, CalcDetails2['label'], [i,j]):
                        nanData[i,j] = _Difference(Data.Energy, CalcDetails1['label'],
                                                   CalcDetails2['label'], [i,j])
                    else:
                        nanData[i,j] = nan

        return UnpackInternal(Data.GetPoints(), nanData)

    else: #except AttributeError:
        print('\n\tData dictionary passed down\n')
        print(Data.__dict__)




def UnpackInternal(Q, D):
    '''
    Returns a dict with entries for points and associated data without the values where the 
    data is Nan for each element

    It is only used internally 
    '''
    Data = {'Indices': []}

    elements = exp(sum([log(D.shape[n]) for n in range(1,D.ndim)]))
    for el in range(int(elements)):
        slc = [slice(None)]*len(D.shape)
        slc[0] = slice(0,1)
        Il = _Recursive(D[slc].reshape(D.shape[1:]), index=el)

        slc = [slice(None)]*len(D.shape)
        for i in range(0,D.ndim-1): slc[i+1] = slice(Il[i], Il[i]+1)

        Data['Indices'].append(''.join([str(i) for i in Il]))
        Data['Data'+''.join([str(i) for i in Il])] = D[slc].flatten().reshape(D.shape[0],1)
        Data['Points'+''.join([str(i) for i in Il])] = copy(Q)

        idxneg = []
        for sample in range(D.shape[0]):
            if isnan(Data['Data'+''.join([str(i) for i in Il])][sample]):
                idxneg.append(sample)

        Data['Data'+''.join([str(i) for i in Il])] =\
            delete(Data['Data'+''.join([str(i) for i in Il])], idxneg, axis=0)
        Data['Points'+''.join([str(i) for i in Il])] =\
            delete(Data['Points'+''.join([str(i) for i in Il])], idxneg, axis=0)

    return Data




def Repack(keys, NS):
    '''Returns an array of zeros with the correct shape for the Gaussian process bundle provided'''
    shp = [max([int(keys[i][j]) for i in range(len(keys))])+1 for j in range(len(keys[0]))]
    shp.insert(0, NS)
    return zeros(tuple(shp))


def Imap(ix):
    '''Returns a slice along all the input Q for a specific matrix index'''
    slc = [slice(None)]*(len(ix)+1)
    for i in range(len(ix)): 
        slc[i+1] = slice(int(ix[i]), int(ix[i])+1, None)
    return slc








def ReadInputCoordinates(args, Molecule, CalcDetails=None, CoordinatesOnly=False, filename=None):
    '''
    Allows to read in points from previously run calculations
    -coordinates only can be read (in order to recalculate points at a different level of theory)
    -two files nodes output files (saved and overwritten) are created and passing no calculation
     details will allow to select which file is read
    -coordinates can be read in both xyz and q (relevant coordinates) format
    '''
    if not filename is None:
        pass #keep the filename given as input
    else:
        if CalcDetails:
            filename=args.CDPath+'/SavedData/'+args.Label+'/MeshInformation_'+\
                     UTILS.GetDataLabel(CalcDetails)
        else:
            filename=args.CDPath+'/SavedData/MeshTemporary'

        if args.CoordinatesFormat=='XYZ':
            filename += '.xyzdat'
        else:
            filename += '.qdat'

    for tries in range(2):
        try:
            print('\n\tReading data file: '+filename+'\n')
            with open(filename, 'r') as f:
                trim=0
                for line in f:
                    if line.strip()=="--Nodes Information--":
                        trim_stop = trim + 1 #accounts for the title, information line and blank
                        break
                    else:
                        trim += 1

            if int(os.getenv('verbose')): print('Reading from file: ' + filename)
            DataRead = genfromtxt(filename, skip_header=trim_stop, unpack=True, 
                                  names=True, deletechars='') 
            DataDict = {}

            for cols in DataRead.dtype.names:
                if cols[:2]=='D-':
                    read_col = cols[2:]
                    tag = 'D-'
                elif cols[:4]=='INT-':
                    read_col = cols[4:]
                    tag = 'INT-'
                else:
                    read_col = cols
                    tag = ''

                if read_col[0]=='E':
                    if not tag+'Energy' in DataDict.keys():
                        keys = [s[len(tag+'En'):] for s in DataRead.dtype.names\
                                        if (tag+'En' in s[:len(tag+'En')])]
                        DataDict['Indices'] = keys
                        DataDict[tag+'Energy'] = Repack(keys, len(DataRead[cols]))
               
                    DataDict[tag+'Energy'][:,int(read_col[2])] = DataRead[cols]

                if read_col[0]=='D':
                    if read_col[:2]=='Di':
                        if not tag+'Dipole' in DataDict.keys():
                            keys = [s[len(tag+'Di'):] for s in DataRead.dtype.names\
                                        if (tag+'Di' in s[:len(tag+'Di')])]
                            DataDict['Indices'] = keys
                            DataDict[tag+'Dipole'] = Repack(keys, len(DataRead[cols]))
               
                        DataDict[tag+'Dipole'][:,int(read_col[2])] = DataRead[cols]
                    else:
                        if not tag+'Distances' in DataDict.keys():
                            DataDict[tag+'Distances'] = DataRead[cols]
                        else:
                            DataDict[tag+'Distances'] = c_[DataDict['Distances'], DataRead[cols]]

                if read_col[:2]=='He':
                    if not tag+'Hessian' in DataDict.keys():
                        keys = [s[len(tag+'He'):] for s in DataRead.dtype.names\
                                        if (tag+'He' in s[:len(tag+'He')])]
                        DataDict['Indices'] = keys
                        DataDict[tag+'Hessian'] = Repack(keys, len(DataRead[cols]))
            
                    DataDict[tag+'Hessian'][:,int(read_col[2]), 
                            int(read_col[3]),int(read_col[4])] = DataRead[cols]

                if read_col[:2]=='No':
                    if not tag+'NormalModes' in DataDict.keys():
                        keys = [s[2:] for s in DataRead.dtype.names if (tag+'No' in s)]
                        DataDict['Indices'] = keys
                        DataDict[tag+'NormalModes'] = Repack(keys, len(DataRead[cols]))
            
                    DataDict[tag+'NormalModes'][:,int(read_col[2]),
                                int(read_col[3]),int(read_col[4])] = DataRead[cols] 

                if read_col[0]=='q':
                    if 'Q' in dir():
                        Q = c_[Q,DataRead[cols]]
                    else:
                        Q = DataRead[cols]

                if read_col=='generation':
                    GEN = DataRead[cols]
                    DataDict['Generations'] = []
                    for i in range(int(GEN[-1])):
                        DataDict['Generations'].append([j for j,x in enumerate(GEN) if x==i+1])


            #Converts coordinates to q-format if the file was .xyzdat
            if args.CoordinatesFormat=='XYZ' or tries:
                XYZ = zeros((len(args.AtomsList), len(Q[:,0]), 3))
                for at in range(len(args.AtomsList)): XYZ[at,:,:] = Q[:,3*at:3*at+3]
                Q =  Molecule.cart2q(XYZ)

            break

        except IOError:
            if filename[-5:]=='.qdat':
                filename = filename[:-5] + '.xyzdat'
                print('\n\tCould not locate file ... trying to locate '+filename+'\n') 
                continue
            else:
                print(filename)
                raise IOError('No files to read from')

    if CoordinatesOnly:
        EnergyFlag = 'Energy' in DataDict.keys()
        if Q.ndim==1:
            return Q.reshape((len(Q),1)), EnergyFlag
        else:
            return Q, EnergyFlag
    else:
        if not bool(DataDict):
            raise RuntimeError('\n\tGiven file contained no properties data (energy at '\
                               'least required)')

        Data = Process(args)
        Data.FeedFinalData(Q, DataDict)
        return Data.Return() 


