#!/bin/python
#File: core.data_management.points_structure.py

import os
from numpy import r_

import core.data_management.data_utils as utils
from core.data_management.data_structure import DataStructure


class PointsStructure(DataStructure):
    '''
    Class to store Data in a very primitive way
    '''

    def __init__(self, args=None, Modules=None):

        super(PointsStructure, self).__init__(args, Modules)



    def CreateStructure(self, Data, components_only=False):
        '''
        Initiate all the attributes

        Data is a dictionary of points and associated data passed
        by a running interface but which has been formated by the
        `core.data_management.data_utils.Process` class
        '''
        if int(os.getenv('verbose')):
            self.SP('Initialise routine in datamanagement.points_structure:'\
                    '\n\nInput Data\n', Data)

        if not components_only:
            self.SP('\n\tMesh has been initiated with '+str(Data['Points'].shape[0])+' points\n')
            self._points = Data['Points']
            self.SaveMeshDetails(Data)

        self.Components.AddData(Data)


    def UpdateStructure(self, Data, components_only=False):
        '''
        Updates the class with extra data


            Data:  [DataStructre] object containing extra points and energy data
            basis_set:   [str] basis set being investigated
            noMesh:     [bool] if set to True, the points are not set from the points given in Data
                        but are taken from the last initation (used for multiple runs)
        '''

        if int(os.getenv('verbose')):
            self.SP('Update routine in PointStructure for '+UTILS.GetLabel(CalcDetails)+'\n', Data)

        if not components_only:
            self.SP('\n\tCurrent mesh has been updated with '+str(Data['Points'].shape[0])+\
                    ' points, new total points -> '+str(self.number_of_points)+'\n')
            self._points = r_[self.points, Data['Points']]
            self.SaveMeshDetails(Data)

        self.Components.AddData(Data)



    def SaveMeshDetails(self, Data):
        '''
        Stores details about the generational mesh building which allows to follow the
        generational building process of a previous run

        This function will be called on the first run and then all the MeshDetails will
        saved each time the structure is updated
        '''
        if not Data is None:
            if not 'Initialised' in self.MeshDetails.keys():
                self.MeshDetails['Initialised'] = True

            if not Data['Generations'] is None: #Fed in one block
                self.MeshDetails['ImprovementIndices'] = Data['Generations']

            else:
                if not 'ImprovementIndices' in self.MeshDetails.keys():
                    self.MeshDetails['ImprovementIndices'] = [range(0, Data['Points'].shape[0])]
                else:
                    n = self.MeshDetails['MeshNodes']
                    self.MeshDetails['ImprovementIndices'] +=\
                        [range(0+n, Data['Points'].shape[0]+n)]

                if not 'MeshNodes' in self.MeshDetails.keys():
                    self.MeshDetails['MeshNodes'] = Data['Points'].shape[0]

                else:
                    self.MeshDetails['MeshNodes'] += Data['Points'].shape[0]

        if int(os.getenv('verbose')):
            self.SP('Mesh details in datamanagment.points_structure:\n', self.MeshDetails)



    def TestPoints(self, function_type='random'):
        """
        Create points from different functions (should eventually include kernel density
        functions)
        """
        if function_type=='random':
            return Random(self.Globalargs)

        else:
            raise RuntimeError('function_type not defined\n')


    def PrintFinalAddition(self, CalcDetails, f):
        '''
        No additional information is useful for the pointsstructure
        '''
        f.write('--Mesh Details--\n')
        try:
            f.write('Number of nodes:\t'+str(self.MeshDetails['MeshNodes'])+'\n')
        except:
            pass
