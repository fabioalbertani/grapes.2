#!/bin/python
#File: core.data_structure.delaunay_structure.py

from scipy.spatial import Delaunay

from core.data_management.data_structure import DataStructure


class DelaunayStructure(DataStructure):
    '''
    Class to store data (in coordinate space as well as properties) as a delaunay mesh
    Allows to work with Delaunay-specific modules (for example adding new data on
    barycenters)
    '''
    def __init__(self, args):

        super(DelaunayStructure, self).__init__(args)


    def CountSamples(self, Type, basis_set, state):
        return self.MeshDetails['MeshNodes']



    def CreateStructure(self, Data, components_only=False):
        '''
        Initiate the delaunay mesh and the relevant components

        The data is "filtered" (removing Nans)

        Args:
            Data:       [DataStructre] object containing points and energy data
        '''

        if int(os.getenv('verbose')):
            print('Initialise routine in datamanagement.delaunay_structure:\n\nInput Data\n', Data)

        if not components_only:
            self.Delaunay = Delaunay(Data['Points'], incremental=True, qhull_options='QJ')
            self._points = self.Delaunay.points
            self.SP('\n\tMesh has been initiated with '+str(Data['Points'].shape[0])+' points\n')

        self.Components.AddData(Data)



    def UpdateStructure(self, Data, components_only=False):
        '''
        Updates the class with extra data
        The Delaunay mesh will be updated (if noMesh is False) and the energy structures appended

        Args:
            Data:  [DataStructre] object containing extra points and energy data
        '''

        if not components_only:
            self.Delaunay.add_points(Data['Points'], restart=False)
            self._points = self.Delaunay.points #needed ? should be pointing correctly
            self.SP('\n\tDelaunay structure has been augmented with '+\
                              str(Data['Points'].shape[0])+' points\n')

        self.Components.AddData(Data)





    def SaveMeshDetails(self, Data):
        '''
        Stores details about the generational mesh building which allows to follow the
        generational building process of a previous run (defined as a basisset)

        '''
        #TODO Remove the basis_set on some keywords ??? -> Improvement
        from numpy import zeros

        if not Data is None:
            if not 'Initialised' in self.MeshDetails:
                self.MeshDetails['Initialised'] = True

            if not Data['Generations'] is None: #Fed in one block
                self.MeshDetails['ImprovementIndices'] = Data['Generations']
                self.MeshDetails['MeshNodes'] = sum([len(l) for l in Data['Generations']])
                self.MeshDetails['GenerationStop'] = len(self.MeshDetails['ImprovementIndices'])
                self.MeshDetails['AccurateSimplices'] = zeros(len(self.Delaunay.simplices[:,0]),
                                                              dtype=bool)


            else:
                if not 'MeshNodes' in self.MeshDetails.keys():
                    self.MeshDetails['MeshNodes'] = len(Data['Points'][:,0])
                elif not len(Data['Points'][:,0]) == 0:
                    self.MeshDetails['MeshNodes'] += len(Data['Points'][:,0])


                if not 'AccurateSimplices' in self.MeshDetails.keys():
                    self.MeshDetails['AccurateSimplices'] =\
                                zeros(len(self.Delaunay.simplices[:,0]), dtype=bool)
                else:
                    self.MeshDetails['AccurateSimplices'] =\
                                zeros(len(self.Delaunay.simplices[:,0]), dtype=bool)
                    #TODO Actually give those
                    if 'Indices' is Data.keys() and not Data['Indices'] is None:
                        for idx in Data['Indices']:
                            self.MeshDetails['AccurateSimplices'][idx] = True

                if not 'ImprovementIndices' in self.MeshDetails.keys():
                    self.MeshDetails['ImprovementIndices'] = [range(0,len(Data['Points'][:,0]))]
                elif not len(Data['Points'][:,0]) == 0:
                    self.MeshDetails['ImprovementIndices'] += [range(self.MeshDetails['MeshNodes'],\
                                            self.MeshDetails['MeshNodes']+len(Data['Points'][:,0]))]

                if len(self.Delaunay.simplices[:,0]) == len([si for si in\
                                        self.MeshDetails['AccurateSimplices'] if si]):
                    self.MeshDetails['Accurate'] = True


        if int(os.getenv('verbose')):
            sefl.SP('SaveMeshDetails routine in core.data_management.delaunay_structure finsihed'\
                    '\nCurrent MeshDetails:\n', self.MeshDetails)


    @property
    def accurate_simplices(self):
        self._accurate_simplices

    @accurate_simplices.setter
    def accurate_simplices(self):
        return len([si for si in self.MeshDetails['AccurateSimplices'] if si])


    def TestPoints(self):
        '''
        Structure specific test points

        Returns all the barycenters of simplices of the Delaunay mesh
        that have not succesfully passed an accuracy test
        '''

        Barycenters = zeros((self.points.shape[0] - self.accurate_simplices,
                             self.points.shape[1]))

        for simplex in range(len(self.Delaunay.simplices)-self.accurate_simplices):
            if not self.accurate_simplex[simplex]:
                Barycenters[simplex,:] =\
                mean(self.points[self.Delaunay.simplices[simplex]], axis=0)

        return Barycenters.reshape((self.points.shape[0] - self.accurate_simplices,
                                    self.points.shape[1]))




    def PrintFinalAddition(self, CalcDetails, f):

        f.write('--Mesh Details--\n')
        try:
            f.write('Number of nodes:\t'+str(self.MeshDetails['MeshNodes'])+'\n')
            f.write('Simplices:\n')
            for im_index,im in enumerate(self.Delaunay.simplices):
                f.write('\t#'+str(sim_index)+'\t'+'\t'.join([str(x) for x in sim])+'\n')
        except:
            pass
