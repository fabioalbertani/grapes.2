#!/bin/python
#File: core.data_management.data_filter.py

from numpy import zeros, isnan, nan, delete, ndarray
from numpy import float64 as npfloat
from math import exp, log

from copy import copy

import core.grapes_utils.main_utils as UTILS


def FilterInterface(Q=None, Data=None, DataStructure=None, DataStructure2=None,
                    Runners=None, CalcDetails=None, with_nan=False):
    '''
    Callable function that selects from a list of runners (for different 
    calculation types) the relevant one
    '''
    if (not Q is None) and (not Data is None):
        nanData = zeros(Data[CalcDetails['Type']].shape)
        slc = [slice(None)]*nanData.ndim

        for element in range(int(exp(sum([log(nanData.shape[n]) for\
                             n in range(len(nanData.shape))])))):

            for i, slice_index in enumerate(_Recursive(nanData, element)):
                slc[i] = slice(slice_index, slice_index+1)

                #include difference TODO
                nanData[slc] = Data[CalcDetails['Type']][slc] if\
                               _Defined(MD_unpack(Data[CalcDetails['Type']][slc])) else nan

        if with_nan:
            return nanData
        else:
            return UnpackInternal(Q, nanData)

    elif not DataStructure is None:
        if CalcDetails['Difference']:
            return Filter(
                DataStructure.points, CalcDetails['Type'],
                DataStructure.Components, DataStructure2.Components, with_nan=with_nan
                         )

        else:
            return Filter(
                DataStructure.points, CalcDetails['Type'],
                DataStructure.Components, with_nan=with_nan
                         )

    elif not Runners is None:
        if CalcDetails['Difference']:
            return Filter(
                Runners[CalcDetails['label']].Data.points, CalcDetails['Type'],
                Runners[CalcDetails['label']].Data.Components,
                Runners[CalcDetails['Difference']['label']].Data.Components,
                with_nan=with_nan
                         )
    
        else:
            Runner = Runners if not isinstance(Runners, dict) else\
                     Runners[CalcDetails['label']]
            return Filter(
                Runner.Data.points, CalcDetails['Type'],
                Runner.Data.Components, with_nan=with_nan
                         )

    else:
        raise Exception('\nThe data that need to be filtered is not a DataStructure, a '\
                        'Runner or raw data\n')



def Filter(Q, Type, Components1, Components2=None, with_nan=True):
    '''
    Allows to filter multiple `core.data_management.data_structure` instances to produce
    simple array containing only defined values or defined differences that can be 
    interpreted by `core.interpolation.interpolation_interface` instances
    '''
    nanData = zeros(Components1(Type).shape)
    slc = [slice(None)]*nanData.ndim

    for element in range(int(exp(sum([log(nanData.shape[n]) for\
                         n in range(len(nanData.shape))])))):

        for i, slice_index in enumerate(_Recursive(nanData, element)):
            slc[i] = slice(slice_index, slice_index+1)

        if Components2 is None:
            nanData[slc] = Components1(Type)[slc] if\
                           _Defined(MD_unpack(Components1(Type)[slc])) else nan

        else:
            if _Defined(MD_unpack(Components1(Type)[slc])) and\
               _Defined(MD_unpack(Components2(Type)[slc])):
                nanData[slc] = Components1(Type)[slc] -\
                               Components2(Type)[slc]
            else:
                nanData[slc] = nan

    if with_nan:
        return nanData 
    else:
        return UnpackInternal(Q, nanData)





def UnpackInternal(Q, D):
    '''
    Returns a dict with entries for points and associated data without the values 
    where the data is Nan for each element

    It is only used internally 
    '''
    Data = {'Indices': []}

    elements = exp(sum([log(D.shape[n]) for n in range(1,D.ndim)]))
    for el in range(int(elements)):
        slc = [slice(None)]*len(D.shape)
        slc[0] = slice(0,1)
        Il = _Recursive(D[slc].reshape(D.shape[1:]), index=el)

        slc = [slice(None)]*len(D.shape)
        for i in range(0,D.ndim-1): slc[i+1] = slice(Il[i], Il[i]+1)

        Data['Indices'].append(''.join([str(i) for i in Il]))
        Data['Data'+''.join([str(i) for i in Il])] = D[slc].flatten().reshape(D.shape[0],1)
        Data['Points'+''.join([str(i) for i in Il])] = copy(Q)

        idxneg = []
        for sample in range(D.shape[0]):
            if isnan(Data['Data'+''.join([str(i) for i in Il])][sample]):
                idxneg.append(sample)

        Data['Data'+''.join([str(i) for i in Il])] =\
                    delete(Data['Data'+''.join([str(i) for i in Il])], idxneg, axis=0)
        Data['Points'+''.join([str(i) for i in Il])] =\
                    delete(Data['Points'+''.join([str(i) for i in Il])], idxneg, axis=0)

    return Data




def MD_unpack(D):
    """ Returns the only element contained in a MD numpy array"""
    while isinstance(D, ndarray):
        D = D[0]
    return D




def _Recursive(D, index):
    '''Returns indices needed for the nth index'''
    if isinstance(D, tuple):
        D = zeros(D) #hack
    indices = list(D.shape)
    for d in range(D.ndim):
        indices[d] = index if not d else index//(int(exp(sum([log(D.shape[n])\
                     for n in range(0,d)]))))
    return [indices[i]%D.shape[i] for i in range(D.ndim)]



def _Defined(val):
    ''' Only returns true if the index is defined and the value at the said index is a float '''
    try:
        if type(val) == npfloat:
            if val == npfloat('inf'):
                return False
            elif isnan(val):
                return False
            else:
                return True
        else:
            return False
    except IndexError:
        return False

    except:
        '''any other error'''
        print('_Defined not responsive -> setting element to Nan')
        return False




def Repack(keys, NS):
    '''Returns an array of zeros with the correct shape for the Gaussian process bundle provided'''
    shp = [max([int(keys[i][j]) for i in range(len(keys))])+1 for j in range(len(keys[0]))]
    shp.insert(0, NS)
    return zeros(tuple(shp))


def Imap(ix):
    '''Returns a slice along all the input Q for a specific matrix index'''
    slc = [slice(None)]*(len(ix)+1)
    for i in range(len(ix)): 
        slc[i+1] = slice(int(ix[i]), int(ix[i])+1, None)
    return slc

