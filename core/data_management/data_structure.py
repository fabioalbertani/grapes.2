#!/bin/python
#File: core.data_management.data_structure.py

import abc
import core.grapes_utils.main_utils as UTILS
from core.data_management import data_utils as utils

from core.data_management.components_structure import ComponentsStructure
from core.data_management.data_filter import FilterInterface, MD_unpack


class DataStructure:
    '''
    Defines a class to store data 
    nb: the functionality of each method is given in the specific classes
    '''
    __metaclass__ = abc.ABCMeta

    def __init__(self, args, Modules=None):

        if not Modules is None:
            self.LoadModules(Modules)

        self.Globalargs = args.main
        self.SP = UTILS.SuperPrint()

        self._elements = {}
        self._MeshDetails = {}
        self.Components = ComponentsStructure()


    @property
    def points(self):
        '''
        The coordinates of the data points of the given structure
        '''
        return self._points


    @points.setter
    def points(self, Q):
        self._points = Q


    @property
    def number_of_points(self):
        return self.points.shape[0]


    @property
    def elements(self):
        return self._elements


    @property
    def MeshDetails(self):
        '''
        Generations and indices of relevant substructures (for example Delaunay)
        '''
        return self._MeshDetails

    @MeshDetails.setter
    def MeshDetails(self, MeshDetail):
        self._MeshDetails = MeshDetails


    def GetGeneration(self, gen):
        return self.points[self.MeshDetails['ImprovementIndices'][gen]]

    def GetSamplesCount(self):
        return self.MeshDetails['MeshNodes']


    @classmethod
    def GetInterpolationData(self, CalcDetails):
        '''
        Gets the data treated to be easily treated by an interpolation module
        '''
        if CalcDetails['Difference']:
            return utils.UnpackBasic(self, CalcDetails, CalcDetails['Difference'])
        else:
            return utils.UnpackBasic(self, CalcDetails)



#--------------------------------------------------- Components short-cuts

    def ClearComponent(self, Type):
        '''
        Interface to components module to clear all data within a Type
        the interface is needed because some properties are private
        '''
        self.Components.Clear(Type)


#--------------------------------------------------- Core functions
    @abc.abstractmethod
    def CreateStructure(self, Data, components_only=False):
        '''Initiate the class with points and associated data'''
        return None


    @abc.abstractmethod
    def UpdateStructure(self, Data, components_only=False):
        '''Updates the class with new points and associated data'''
        return None



#-------------------------------------------------- Mesh Details

    @abc.abstractmethod
    def SaveMeshDetails(self, Data, Accurate=False, noMesh=False):
        '''Stores everything related to the mesh'''
        return None


    def SaveTrainingSetShape(self, CalcDetails):
        
        for key in self.Components:
            getattr(self, key).SetTrainingSetShape(CalcDetails['label'])




    def PrintFinal(self, CalcDetails, PreviousData, PrintSimplices=False, PrintMeshDetails=True, 
                   Format='Q', Molecule=None):

        format = '.qdat' if Format=='Q' else '.xyzdat' 
        f=open(self.Globalargs.CDPath+'/SavedData/'+self.Globalargs.Label+'/MeshInformation_'+\
               UTILS.GetDataLabel(CalcDetails)+format, 'w')

        self.PrintFinalAddition(CalcDetails, f)
        Data = FilterInterface(DataStructure=self, DataStructure2=PreviousData, 
                               CalcDetails=CalcDetails, with_nan=True)
 
        template='i\t\t'
        t_line='index: 1\t'
        for i in range(self.Globalargs.D if Format=='Q' else 3*len(self.Globalargs.AtomsList)):
            template+="q%i\t\t\t\t" %i
        t_line+='coordinates: ' + str(self.Globalargs.D) + '\t'
        for key in self.Components.stored_data:
            if not key in ['AlphaCoef', 'BetaCoef']:
                for index in utils.UnpackIndices(self.Components.GetShape(key)):
                    template+=key[:2] + index + 4*'\t'
                    if key==CalcDetails['Type']:
                        template+='INT-'+key[:2] + index + 4*'\t'
                    if getattr(self.Components, key).has_error: 
                        template+='D-'+key[:2] + index + 4*'\t'
                t_line+= key + ': ' + str(len(utils.UnpackIndices(self.Components.GetShape(key))))\
                         + '\t'
               

        f.write(t_line+'generation: 1\n')
        f.write('--Nodes Information--\n'+template+'generation\n')
        gen = 0 #0 is used for python selection but gen+1 is printed

        Q = self.points if Format=='Q' else Molecule.q2cart(self.points)
        self.SP('\tProducing dat file for '+str(Q.shape[0] if Format=='Q' else Q.shape[1])+\
                ' points -> format '+Format+'\n')

        for n in range(Q.shape[0] if Format=='Q' else Q.shape[1]):
            f.write('\n'+str(n)+'\t\t')

            if Format=='Q':
                for d in range(self.Globalargs.D):
                    f.write("%f\t\t" %Q[n,d])
            else:
                for at in range(len(self.Globalargs.AtomsList)):
                    for d in range(3):
                        f.write("%f\t\t" %Q[at,n,d])

            for key in self.Components.stored_data:
                if not key in ['AlphaCoef', 'BetaCoef']:
                    for index in utils.UnpackIndices(self.Components.GetShape(key)):
                        slc = utils.Imap([int(s) for s in index])
                        try:
                            f.write(str(MD_unpack(self.Components(key)[slc][n]))+2*'\t')
                        except:
                            f.write('nan'+2*'\t')
                        if key==CalcDetails['Type']:
                            f.write(str(MD_unpack(Data[n]))+2*'\t')
                        if getattr(self.Components, key).has_error:
                            try:
                                f.write(str(MD_unpack(self.Components(key,True)[slc][n]))+2*'\t')
                            except:
                                f.write('nan'+2*'\t')

            if not 'Initialised' in self.MeshDetails:
                f.write("1\t\t")
            else:
                if n>self.MeshDetails['ImprovementIndices'][gen][-1]: 
                    gen += 1
                f.write(str(gen+1)+2*'\t')

        f.close()

        from shutil import copyfile
        format = '.qdat' if Format=='Q' else '.xyzdat'
        copyfile(self.Globalargs.CDPath+'/SavedData/'+self.Globalargs.Label+'/MeshInformation_'+\
                 UTILS.GetDataLabel(CalcDetails)+format,
                 self.Globalargs.CDPath+'/SavedData/MeshInformation'+format)



    @abc.abstractmethod
    def PrintFinalAddition(self, CalcDetails, f):
        '''
        Printing specific to the data structure
        '''
        return None
