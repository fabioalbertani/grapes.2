#!/usr/bin/python
#File: core.data_management.components_structure.py

from numpy import ndarray, r_
from math import exp, log

import core.grapes_utils.main_utils as UTILS
import core.data_management.data_utils as utils
from core.data_management.data_filter import _Recursive


class ComponentsStructure:
    '''
    Defines a class to store point data such as energies, orbital coefficients ...
    '''

    def __init__(self):
        self._stored_data = []     
        self._associated_elements = {}
        self.SP = UTILS.SuperPrint()

    @property
    def stored_data(self):
        return self._stored_data

    @property
    def associated_elements(self):
        return self._associated_elements

    @property
    def dimensions(self):
        return self._dimensions


    
    def SaveDimensions(self, shapes):
        self._dimensions = {}
        for k,v in shapes.items():
            if not k in self.dimensions.keys():
                if not v is None:
                    self._dimensions[k] = v
                else:
                    self._dimensions[k] = tuple([1]) #list

    
    def AddData(self, Data):
        '''
        Adds a package of data to the associated label
        '''
        self.SaveDimensions(Data['shapes'])
        self.SP('\n\tAdding following list data:\n'+' '.join(['\t\t'+s+' elements -> '+\
        str(len(Data[s]))+'\n' for s in Data.keys() if not Data[s] is None and\
                                               isinstance(Data[s], list)]))
        self.SP('\n\tAdding following array data:\n'+' '.join(['\t\t'+s+'.shape -> '+\
        str(Data[s].shape)+'\n' for s in Data.keys() if not Data[s] is None and\
                                                not isinstance(Data[s], list) and\
                                                not isinstance(Data[s], dict)]))        
        
        for key, D in Data.items():
            if not D is None and not (key in ['Points', 'shapes'] or 'Error' in key):
                if key in self.stored_data:
                    getattr(self, key).Augment(
                        D, Data[key+'Error'] if key+'Error' in Data.keys() else None)

                else:
                    self.SP('\tInitiated sub-structure for '+key+'\n')
                    setattr(self, key, ValueStructure(self.dimensions[key]))
                    getattr(self, key).Assign(
                        D, Data[key+'Error'] if key+'Error' in Data.keys() else None)

                    self._stored_data.append(key)
                    if isinstance(D, ndarray):
                        self._associated_elements[key] = []
                        for element in range(int(exp(sum([log(D.shape[n+1]) for\
                                             n in range(len(D.shape)-1)])))):
                            slc = [slice(None)]*len(D.shape)
                            slc[0] = slice(0,1)
                            index = _Recursive(D[slc].reshape(D.shape[1:]), element)
                            self._associated_elements[key].append(str(index[0]))


    def __call__(self, key, error=False):
        '''
        Interface to obtain the data value associated to the 
        key 
        '''
        if error:
            return getattr(self, key).value_error
        else:
            return getattr(self, key).value


    def GetElements(self, key):
        return self.associated_elements[key]
    
    def GetDimension(self, key):
        return getattr(self, key).dimension

    def GetShape(self, key):
        return self.dimensions[key]


    def Clear(self, key):
        self._stored_data = [s for s in self.stored_data if s!=key]
        self._associated_elements[key] = []
        delattr(self, key) 


class ValueStructure:
    '''
    Defines a class to store point data such as energies, orbital coefficients ...
    '''
    def __init__(self, dimension):
        self._dimension = len(dimension)
        self._has_error = False

    @property
    def value(self):
        return self._value

    @property
    def value_error(self):
        return self._value_error

    @property
    def has_error(self):
        return self._has_error

    @property
    def value_dimension(self):
        '''
        The internal dimension of the component (includes states, modes ...)
        '''
        return self._dimension


    def Assign(self, V, Ve=None):
        '''
        Initiates an attribute 

        if a single data point is provided the array is reshaped
        to facilitate data addition
        '''
        if isinstance(V, ndarray):
            if V.ndim==(self._dimension-1):
                shape = list(V.shape)
                V = V.reshape(tuple(shape+[1]))         
                if not Ve is None: 
                    Ve = Ve.reshape(V.shape)
            
            self._value = V
            if not Ve is None:
                self._value_error = Ve

        else:
            self._value = V
            if not Ve is None:
                sefl._value_error = Ve

        if not Ve is None: self._has_error = True


    def Augment(self, V, Ve=None):
        '''
        Adds data to an attribute

        if a single data point is provided the array is reshaped
        to facilitate data addition
        '''
        if isinstance(V, ndarray):
            if V.ndim==(self._dimension-1):
                shape = list(V.shape)
                V = V.reshape(tuple(shape+[1]))
                if not Ve is None:
                    Ve = Ve.reshape(V.shape)

            self._value = r_[self._value, V]
            if not Ve is None:
                self._value_error = r_[self._value_error, Ve]

        else:
            self.value.extend(V)
            if not Ve is None:
                self.value_error.extend(Ve)

    def SetTrainingSetShape(self, label):
        self.MainSet = label

    def TrainingSetShape(self):
        '''used for treating data'''
        return getattr(self, self.MainSet).shape

    def Slice(self, ix):
        '''
        Defines the slicing to get the data for a given index
        '''
        slc = [slice(None)]*self.dimension
        for i,v in enumerate(ix) if isinstance(ix, list) else enumerate([ix]):
            slc[-len(ix)+i] = slice(v, v+1)
        return slc
