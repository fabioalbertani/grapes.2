FC=ifort

#FFLAGS = -i8 -cpp -Dcmd

MKLROOT = /usr/local/shared/intel/compilers_and_libraries_2018/linux/mkl
#LDFLAGS = -llapack -lblas
#         -L$(LDPATH)linux/mkl/lib/intel64 -lmkl_intel_ilp64 -lmkl_intel_thread\
          -lmkl_core -liomp5 -lpthread -I$(LDPATH)linux/mkl/include


FFLAGS= -i8 -I${F95ROOT}/include/intel64/ilp64 -I${MKLROOT}/include
LDFLAGS=${F95ROOT}/lib/intel64/libmkl_blas95_ilp64.a ${F95ROOT}/lib/intel64/libmkl_lapack95_ilp64.a\
        ${MKLROOT}/lib/intel64/libmkl_scalapack_ilp64.a -Wl,--start-group\
        ${MKLROOT}/lib/intel64/libmkl_intel_ilp64.a ${MKLROOT}/lib/intel64/libmkl_intel_thread.a\
        ${MKLROOT}/lib/intel64/libmkl_core.a ${MKLROOT}/lib/intel64/libmkl_blacs_intelmpi_ilp64.a\
        -Wl,--end-group -liomp5 -lpthread -lm -ldl

VPATH = ${MKLINCLUDE}

INTERPOLATIONPATH = core/interpolation/

SOURCES = core/auxiliary/random.f90 $(INTERPOLATIONPATH)kernels.f90\
          $(INTERPOLATIONPATH)gaussian_processes.f90 tests/test_gaussian_processes_fortran.f90

OBJECTS = $(SOURCES:.f90=.o)

TARGETS = bin/test_gaussian_processes_fortran.f90c

all: $(TARGETS)

$(TARGETS): $(OBJECTS)
	$(LINK.f) $^ $(LDFLAGS) -o $@

$(OBJECTS): %.o : %.f90
	$(COMPILE.f) $< -o $@

clean: 
	rm -f $(OBJECTS)
	rm -f *.mod
	rm -f $(TARGETS)
