#!/bin/python
#File: routine_test.py

import os
from subprocess import call
from datetime import datetime


print('starting routine test '+str(datetime.now())+'\n')

import tests
tests.__test_all__()

for datfile in [os.path.join(path, file) for (path, dirs, files) in\
        os.walk('tests/') for file in files]:

    if datfile[-5:]=='.bash':
        call('./'+datfile)

print('finished routine test '+str(datetime.now())+'\n')
