
:set tabstop=4 shiftwidth=4 expandtab

" Data instances (both the entire structure class and all the coordinates
" components passed around
:hi c11 guifg=105 ctermfg=105 guibg=Black
:call matchadd('c11', '\(^\(\s*#\)\@!.*\)\@<=\<Data\>')
:call matchadd('c11', '\(^\(\s*#\)\@!.*\)\@<=\<PreviousData\>')
:call matchadd('c11', '\(^\(\s*#\)\@!.*\)\@<=\<DataRead\>')
:call matchadd('c11', '\(^\(\s*#\)\@!.*\)\@<=\<DataDict\>')
:call matchadd('c11', '\(^\(\s*#\)\@!.*\)\@<=\<DataBag\>')
:call matchadd('c11', '\(^\(\s*#\)\@!.*\)\@<=\<DataTreated\>')
:call matchadd('c11', '\(^\(\s*#\)\@!.*\)\@<=\<ValidationData\>')

" Main modules hloding function that are called from the 
" core.grapes_utils package
:hi c12 guifg=162 ctermfg=162 guibg=Black
:call matchadd('c12', '\(^\(\s*#\)\@!.*\)\@<=\<MAIN\>')
:call matchadd('c12', '\(^\(\s*#\)\@!.*\)\@<=\<RUNNER\>')
:call matchadd('c12', '\(^\(\s*#\)\@!.*\)\@<=\<MODULE\>')
:call matchadd('c12', '\(^\(\s*#\)\@!.*\)\@<=\<MOLECULE\>')
:call matchadd('c12', '\(^\(\s*#\)\@!.*\)\@<=\<MOLECULE_PLOT\>')
:call matchadd('c12', '\(^\(\s*#\)\@!.*\)\@<=\<INTERPOLATION\>')
:call matchadd('c12', '\(^\(\s*#\)\@!.*\)\@<=\<PLOT\>')
:call matchadd('c12', '\(^\(\s*#\)\@!.*\)\@<=\<VALIDATION\>')
:call matchadd('c12', '\(^\(\s*#\)\@!.*\)\@<=\<UTILS\>')

" Interface modules to external programs
:hi c13 guifg=131 ctermfg=131 guibg=Black
:call matchadd('c13', '\(^\(\s*#\)\@!.*\)\@<=\<RunOrganiser\>')
:call matchadd('c13', '\(^\(\s*#\)\@!.*\)\@<=\<RunInterface\>')

" Highlights where the current calculation details are being used
:hi c21 guifg=76 ctermfg=76 guibg=Black
:call matchadd('c21', '\(^\(\s*#\)\@!.*\)\@<=\<CalcDetails\>')
:call matchadd('c21', '\(^\(\s*#\)\@!.*\)\@<=\<CalcType\>')
:call matchadd('c21', '\(^\(\s*#\)\@!.*\)\@<=\<PlotDetails\>')
:call matchadd('c21', '\(^\(\s*#\)\@!.*\)\@<=\<PlotType\>')
:call matchadd('c21', '\(^\(\s*#\)\@!.*\)\@<=\<MethodDetails\>')
:call matchadd('c21', '\(^\(\s*#\)\@!.*\)\@<=\<MethodType\>')
:call matchadd('c21', '\(^\(\s*#\)\@!.*\)\@<=\<ValidationDetails\>')
:call matchadd('c21', '\(^\(\s*#\)\@!.*\)\@<=\<ValidationType\>')

" all the arguments that are passed from the external input files
:hi c31 guifg=202 ctermfg=202 guibg=Black
:call matchadd('c31', '\(^\(\s*#\)\@!.*\)\@<=\<args\>')
:call matchadd('c31', '\(^\(\s*#\)\@!.*\)\@<=\<Globalargs\>')
:call matchadd('c31', '\(^\(\s*#\)\@!.*\)\@<=\<Moduleargs\>')


" Helps to identify labeling and key loops
:hi c41 guifg=133 ctermfg=133 guibg=Black
:call matchadd('c41', '\(^\(\s*#\)\@!.*\)\@<=\<key\>')
:call matchadd('c41', '\(^\(\s*#\)\@!.*\)\@<=\<keys\>')
:call matchadd('c41', '\(^\(\s*#\)\@!.*\)\@<=\<label\>')


" Sets the UTILS that are used throughout the code
:hi c51 guifg=81 ctermfg=81 guibg=Black
:call matchadd('c51', '\(^\(\s*#\)\@!.*\)\@<=\<PB\>')
:call matchadd('c51', '\(^\(\s*#\)\@!.*\)\@<=\<SP\>')
:call matchadd('c51', '\(^\(\s*#\)\@!.*\)\@<=\<SPError\>')

set title



" Multiple windows 
autocmd WinEnter * if !exists('w:matchId') | :source ~/.vimrc | endif



" Highlights the searched keyword throughout the document
:set hlsearch


" Highlights the current line (set to bold)
set cursorline
hi CursorLine term=bold cterm=bold


"Enable numbers
"set number
"set scrolloff=5

"Sets splitting parameters
set splitbelow splitright
noremap <leader>s :vsplit<cr>
noremap <leader>i :split<cr>

match Error /\%101v.\+/

"Sources changes into split window
:nnoremap <leader>sv :source $MYVIMRC<cr> 
