#!/bin/python
#File: tests.test_woodbury.py


def main():

    import numpy as np
    from copy import copy
    import sklearn.gaussian_process.kernels as kernels

    from core.auxiliary.woodbury import get_subinverse_matrix, get_superinverse_matrix
    
    print('\tSTARTING TEST AUXILIARY WOODBURY\n')
    
    Q_super = np.random.random(11).reshape((11,1))
    Q = Q_super[:-1].reshape((10,1))
    Q_sub = copy(Q)
    Q_sub = Q_sub[1:] 

    #the shorter length_scales allows the inverse matrix elements to remain within 
    #sensible bounds (equivalent of spreading the sampling) 
    kernel_ = kernels.RBF(length_scale=0.05)

    A = kernel_(Q, Q)
    A_sub = kernel_(Q_sub, Q_sub)
    A_super = kernel_(Q_super, Q_super)

    A_inv = np.linalg.inv(A)
    A_sub_inv = np.linalg.inv(A_sub)
    A_super_inv = np.linalg.inv(A_super)

    dump, woodbury_sub = get_subinverse_matrix(A, A_inv)

    for row in range(9):
        #some issues due to multiple matrix multiplications (i.e error is 
        #not extremely small)
        test_diff = (A_sub_inv[row,:] - woodbury_sub[row,:]).flatten()
        assert all([x < 1e-6 for x in test_diff])

    Q_add = Q_super[-1,0].reshape((1,1))
    woodbury_super = get_superinverse_matrix(
        A_inv,
        kernel_(Q, Q_add),
        kernel_(Q_add,Q_add) -\
        reduce(np.matmul, [kernel_(Q, Q_add).T, A_inv, kernel_(Q, Q_add)])
                                            )

    for row in range(11):
        test_diff = (A_super_inv[row,:] - woodbury_super[row,:]).flatten()
        assert all([x < 1e-10 for x in test_diff])

    print('\tTEST PASSED SUCCESFULLY\n')


if __name__=='__main__':
    main()
