#!/usr/bin/python
#File: tests.__init__.py

'''
Test modules
'''

__all__ = ['test_input', 'test_module_load',
           'test_gaussian_processes_fortran_read', 'test_gaussian_processes_read',
           'test_woodbury']

import imp,os

def __test_all__(dir='tests'):
    test_id = 1
    list_modules=os.listdir(dir)
    list_modules.remove('__init__.py')
    for module_name in list_modules:
        if module_name.split('.')[-1]=='py':
            print 'Loading test '+module_name+' test id '+str(test_id)+':\n'
            test = imp.load_source('module', dir+os.sep+module_name)
            test.main()
            test_id += 1
