#!/bin/python
#File: tests.test_gaussian_processes_read.py


def main():

    import numpy as np    
    from tests.test_input import main as read_input

    import core.grapes_utils.module_selection as MODULE
    from core.interpolation.gaussian_processes_interface import GaussianProcessesInterface

    print('\tSTARTING TEST GAUSSIAN PROCESSES READ (PYTHON)\n')

    args = read_input(create=True)
    Q = np.random.random(50).reshape((50,1))

    GP = MODULE.SelectInterpolationType(args)
    #TO BE CONTINUED    


    print('\tTEST PASSED SUCCESFULLY\n')


if __name__=='__main__':
    main()
