program test_gaussian_processes_fortran

use random
use gaussian_processes, only: read_gaussian_process, predict_mean, predict_variance

implicit none

real, dimension(100,1):: q,&
                         y_mean_predict,&
                         y_variance_predict
integer:: i

call read_gaussian_processes('testfile.qdat', 'testfile.gpdat')

!create test array q
do i=1,100
    q(i,1) = gasdev()
enddo

y_mean_predict = predict_mean(q)
y_variance_predict = predict_variance(q)


end program test_gaussian_processes_fortran
