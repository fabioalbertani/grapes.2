#!/bin/python
#File: tests.test_module_load.py



def main():
    
    from tests.test_input import main as read_input

    from core.grapes_utils import module_selection as MODULE

    from core.data_management.delaunay_structure import DelaunayStructure
    from core.data_management.points_structure import PointsStructure
    from core.interpolation.gaussian_processes_interface import GaussianProcessesInterface 

    print('\tSTARTING TEST MODULE LOAD\n')

    args = read_input(create=True)

    Data = MODULE.SelectDataType(args)
    assert isinstance(Data, PointsStructure)
    args.main.set('MeshType', 'Delaunay')
    Data = MODULE.SelectDataType(args)
    assert isinstance(Data, DelaunayStructure)

    INTERPOLATION = MODULE.SelectInterpolationType(args)
    assert isinstance(INTERPOLATION, GaussianProcessesInterface)

    #Should check that options passed down are also correct

    print('\tTEST PASSED SUCCESFULLY\n')


if __name__=='__main__':
    main()
