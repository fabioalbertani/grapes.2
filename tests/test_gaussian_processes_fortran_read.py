#!/bin/python
#File: tests.test_gaussian_processes_fortran_read.py


def main():

    import numpy as np    
    from subprocess import call
    from tests.test_files import create_test_files

    print('\tSTARTING TEST GAUSSIAN PROCESSES READ (FORTRAN)\n')    
    create_test_files()

    call('./binruntest_gaussian_process_fortran.f90c >> test.log')
    
    print('\tTEST PASSED SUCCESFULLY\n')


if __name__=='__main__':
    main()
