#!/bin/python
#File: tests.tests_input.py


def main(create=False):
    '''
    Tests if an input file is read correctly by core.options modules
    this can also be used by another test to creat an example input arguments
    using the `create=True` keyword
    '''
    import sys
    from core.options.options_utils import OptionsStructure
    from core.options.options_interface import ParseOptions

    if not create:
        print('\tSTARTING TEST INPUT READ\n')

    f = open('test_input_file', 'w')
    f.write("""
save
local
parallel                    6


Label                       H2
AtomsList                   H H
Charge                      0
Spin                        1

D                           1
MeshType                    Points
RunningMode                 Generational


unrestricted
CalcTypes                   QChem:Energy--hf@sto-3g
CalcTypeDifference          none


NStates                     1

GridLimits                  0.1 4.0
SamplingMethod              Random
NSampling                   10
MaximumMeshGenerations      0


CoordinatesTransform        diatomic
InterpolationMethod         GaussianProcesses


TestingMethod               PointsVariance 

#ReadData                    QChem:Energy--hf@sto-3g 1

CalcTypePlot                QChem:Energy--hf@sto-3g
ElementsPlot                0
PotentialcutPlot            -2.0 0.0
              """)
    f.close()

    #Even though the args are initiated differently in `core.runners` but the direct
    #initiation is needed to avoid issues with re-parsing
    args = OptionsStructure() 
    args.GetInput('test_input_file', sys.argv+['-i','test_input_file']) 
    ParseOptions(args, sys.argv+['-i','test_input_file'])
    
    if create:
        return args

    else:
        assert args.main.Label=='H2'
        assert args.main.NSampling==10
        assert args.main.CalcTypes==['QChem:Energy--hf@sto-3g']
        
        print('\tTEST PASSED SUCCESFULLY\n')

if __name__=='__main__':
    main()
