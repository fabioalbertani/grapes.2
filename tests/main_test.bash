#!/bin/bash

#   !!!!!!!     MAKE SURE THAT THE FOLLOWING THINGS HAVE BEEN DONE      !!!!!!!
#    - Setup.bash was executed in the main directory (creates executables)
#    - Loaded modules reflect the modules given in README

printf "\nSTARTING MAIN TEST (BASH)"

#this directory will contain all input files created
mkdir -p InputFiles


#Writes a typical input file for the H2 system

cat << EOF > InputFiles/H2.input
save
local
parallel                        6

Label                           H2
AtomsList                       H H
Charge                          0
Spin                            1

D                               1
MeshType                        Points

NStates                         1
unrestricted
CalcTypes                       QChem:Energy--hf@sto-3g QChem:Energy--hf@6-31g
CalcTypeDifference              none QChem:Energy--hf@sto-3g

GridLimits                      0.1 4.0
NSampling                       20
MaximumMeshGenerations          1

CoordinatesTransform            diatomic
InterpolationMethod             GaussianProcesses

TestingMethod                   PointsVariance

CalcTypePlot                    QChem:Energy--hf@6-31g
PotentialcutPlot                -2 0
EOF



#Call the executable to run the program
bin/runmain.py -i H2

printf "\n\t** main_test.bash has finished **\n"

printf "\n\tExample of other run with extra arguments on the command line:\n"
printf "\n\tbin/runmain.py -i H2 --CalcTypes hf:ccsdt@cc-pVDZ --CalcTypePlot hf:ccsdt@cc-pVDZ\n\n\n"

